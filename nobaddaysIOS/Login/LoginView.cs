﻿using System;
using CoreGraphics;
using UIKit;
using System.Timers;
using Foundation;
using System.Collections.Generic;
using NoBadDaysPCL;
using System.IO;
using SQLite.Net.Platform.XamarinIOS;
using Valore.IOSDropDown;
using Security;
using Utils;
using NoBadDaysPCL.Items;
using NoBadDaysPCL.ViewModel;

namespace nobaddaysIOS
{
	public class CountryType
	{
		public string CountryCode  
		{
			get;
			set;
		}

		public string Country  
		{
			get;
			set;
		}
	}
		
	public class MyUITextField : UITextField
	{
		public event EventHandler CCChanged; 


		public  MyUITextField(CGRect frame) : base(frame)
		{
		}

		public override string Text
		{
			get
			{
				return base.Text;
			}
			set
			{
				if (value != base.Text)
				{
					base.Text = value;

					if (CCChanged != null)
					{
						CCChanged(this, null);
					}
				}
			}
		}
	}

	public class LoginView : UIView
	{
		LoginViewModel _viewModel = null;
		LoginViewModel ViewModel 
		{
			get 
			{ 
				if (_viewModel == null) 
				{
					string libraryPath = System.Environment.GetFolderPath (System.Environment.SpecialFolder.Personal);
					var path = Path.Combine (libraryPath, Container.SQLitePath);

					_viewModel = new LoginViewModel (new SQLitePlatformIOS (), path);
					_viewModel.PhoneDataSuccessfulWithIndex += _viewModel_PhoneDataSuccessfulWithIndex;
					_viewModel.PhoneDataErrorOccurred += _viewModel_PhoneDataErrorOccurred;
					_viewModel.RequestSuccessful += _viewModel_RequestSuccessful;
					_viewModel.RequestNotSuccessful += _viewModel_RequestNotSuccessful;
					_viewModel.ConfirmSuccessful += _viewModel_ConfirmSuccessful;
					_viewModel.ConfirmNotSuccessful += _viewModel_ConfirmNotSuccessful;
				} 
				return _viewModel;
			}
		}


		UserViewModel _userViewModel = null;
		UserViewModel UserViewModel 
		{
			get 
			{ 
				if (_userViewModel == null) 
				{
					string libraryPath = System.Environment.GetFolderPath (System.Environment.SpecialFolder.Personal);
					var path = Path.Combine (libraryPath, Container.SQLitePath);

					_userViewModel = new UserViewModel (new SQLitePlatformIOS (), path);

					_userViewModel.PhoneDataSuccessful += _userViewModel_PhoneDataSuccessful;
					_userViewModel.PhoneDataErrorOccurred += _userViewModel_PhoneDataErrorOccurred;
				} 
				return _userViewModel;
			}
		}

		void _userViewModel_PhoneDataErrorOccurred (object sender, ServerOutputItem e)
		{
			BlurView (false);

			//#newstring
			var alert = new UIAlertView (e.ResultString, "", null, NSBundle.MainBundle.LocalizedString("OK", "OK"), null);
			alert.Clicked += (sender1, buttonArgs) => 
			{
				if (buttonArgs.ButtonIndex == 0) //OK Clicked
				{ 
					SetInputPinViews();
				}
			};
			alert.Show ();
		}

		void _userViewModel_PhoneDataSuccessful (object sender, ServerOutputItem e)
		{
			BlurView (false);

			GlobalData.IsLoggedIn = true;

			GlobalData.UserItem = UserViewModel.GetUser ();

			if (GlobalData.UserItem != null) {

				Container.UserItem.Phone = GlobalData.UserItem.Phone;
				Container.UserItem.Prefix = GlobalData.UserItem.Prefix;
				Container.UserItem.Region = GlobalData.UserItem.Region;
				Container.UserItem.UserId = GlobalData.UserItem.UserId;
				Container.UserItem.Name = GlobalData.UserItem.Name;
			}


			if (LoginViewDone != null) {
				LoginViewDone (this, EventArgs.Empty);
			}
		}


		public event EventHandler LoginViewDone; 
	
		UIBarButtonItem myDoneButton;

		UIImageView blurOverlay = null;
		UIImageView LoginImageView;

		MyNBDView myNbdView = null;

		UILabel EnterCountryCodeLabel;
		UILabel EnterPhoneLabel;

		UIImageView FlagImage;
		UITextView CountryInfo;

		public UITextField EnterNumber;
		MyUITextField EnterCountryCode;

		UILabel EnterCodeTextLabel;
		UIToolbar acc_toolbar;
		UIImageView NBDImageView;
		//UIActivityIndicatorView Spinner;
		UIView EnterDigitsContainerView;
		Digit Digit1;
		Digit Digit2;
		Digit Digit3;
		Digit Digit4;
		UIImageView bckImage;
		UIViewController mycontroller;
		Timer blink;

		List<DropDownListItem> _countryList = new List<DropDownListItem>();
		List<UIImage> flaglist = new List<UIImage>();

		DropDownList ddl;

		int _max = -1;
		int _min = -1;
		int _index = -1;

		NSObject _notificationObserver;

		bool moveViewUp = false;
		nfloat scrollamount = 0.0f;

		int _completeOffset;

		bool _login = true;

		UIImageView _bckButton;
		UITapGestureRecognizer bckTap;
		public event EventHandler<bool> LogInEvent;
		UILabel header;





		public LoginView(object controller, bool login)
		{
			mycontroller = controller as UIViewController;

			_login = login;

			var frame = mycontroller.View.Bounds;

			Frame = new CGRect(0, 0, 320, 480);


			if (login) {	
				SetHeaderText (frame, new NSAttributedString(NSBundle.MainBundle.LocalizedString("LoginText", "LoginText"), GlobalData.HeaderAttrib));
				SetView (frame);
			} else
			{	
				SetHeaderText (frame, new NSAttributedString(NSBundle.MainBundle.LocalizedString("VerifyCodeText", "VerifyCodeText"), GlobalData.HeaderAttrib));
				SetCodeView (frame);
			}
			
			SetupDigits();

			blink = new Timer(500);
			blink.AutoReset = true;
			blink.Elapsed += Blink_Elapsed;

			_notificationObserver = NSNotificationCenter.DefaultCenter.AddObserver ((NSString)"UIKeyboardDidShowNotification", KeyboardUpNotificationAdd);

			if (login) {	

			    SetInputPhoneNumberViews ();
			
			    Frame = frame;

			    BlurView (true);

			   subscribeData ();
			}
			else SetInputPinViews ();
	
			NSNotificationCenter.DefaultCenter.AddObserver
			(UITextField.TextFieldTextDidChangeNotification, (notification) =>
				{
					Console.WriteLine ("Character received! {0}", notification.Object ==
						EnterCountryCode);
				});

			NSNotificationCenter.DefaultCenter.AddObserver
			(UITextField.TextDidEndEditingNotification, (notification) =>
				{
					Console.WriteLine ("Character received! {0}", notification.Object ==
						EnterCountryCode);
				});

		}

		private void SetHeaderText(CGRect frame, NSAttributedString title)
		{
			var tmpFrame = new CGRect(0, 0, frame.Width, frame.Height / 10);
			header = new UILabel(tmpFrame);
			header.TextAlignment = UITextAlignment.Center;

			header.AttributedText = title;
	
			Add (header);
		}


		private void AddBckButton() 
		{
			// Disable main wheel
			SavingsViewController view = mycontroller as SavingsViewController;
			view.WheelUserInteractionEnabled (false);

			if (_bckButton == null) {
				_bckButton = new UIImageView (new CGRect (5, 5, Frame.Height / 11, Frame.Height / 11));
				_bckButton.Image = UIImage.FromBundle ("Images/back.png");
				_bckButton.UserInteractionEnabled = true;

				bckTap = new UITapGestureRecognizer((g) => 
					{

						BlurView (false);

						if (_login)
						{
							if (LogInEvent != null) {
								LogInEvent (this, true);
							}

						}
						else
						{

							if (LoginViewDone != null) {
								LoginViewDone (this, EventArgs.Empty);
							}

						}
					});
				_bckButton.AddGestureRecognizer (bckTap);
			}
			Add (_bckButton);
		}



#region fetch supported phones

		void subscribeData()
		{
			ViewModel.GetSupportedPhoneData (System.Globalization.CultureInfo.CurrentUICulture.Name);

			//Spinner.StartAnimating();
			UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false; 
		}


		private void KeyboardUpNotificationAdd (NSNotification notification)
		{
			if (EnterDigitsContainerView.Hidden == false)
				return;
			var kbSize = ((NSValue)notification.UserInfo.ObjectForKey (new NSString ("UIKeyboardFrameBeginUserInfoKey"))).RectangleFValue.Size;


			var heightAccessoryView = 44f;
			var heightTextview = 30f;

			if (((mycontroller.View.Frame.Height-_completeOffset + heightTextview) < kbSize.Height + heightAccessoryView) ) {	
				scrollamount = (kbSize.Height + heightAccessoryView)-(mycontroller.View.Frame.Height-_completeOffset + heightTextview)+20f;
			} else
				scrollamount = 0;

		
			if (scrollamount > 0)
			{
				ScrollTheView(true);
			}
			else
			{  
				moveViewUp = false;
			}

		}

		void _viewModel_PhoneDataSuccessfulWithIndex (object sender, int e)
		{
			BlurView (false);
			// set up dropdown index. Server select index from culture sent up

			//Spinner.StopAnimating();
	//		UIApplication.SharedApplication.NetworkActivityIndicatorVisible = true; 

			EnterNumber.Enabled = true;
			_max = ViewModel.Items [e].MaxLength;
			_min = ViewModel.Items [e].MinLength;
			_index = e;

			var heightAccessoryView = 44f;
			var heigthKeyboard = 216f;
			var heightHeaderAndStatus = 64f;

			var freeSpace = Frame.Height - heigthKeyboard - heightHeaderAndStatus;

		
			EnterCountryCode.Text = ViewModel.Items [e].PhonePrefix;
			EnterCountryCode.TextColor = UIColor.DarkGray;
			EnterCountryCode.Font = UIFont.FromName ("Roboto-Regular", 18f);
			EnterNumber.Text = "";
			EnterNumber.BecomeFirstResponder();

			CountryInfo.AddGestureRecognizer (new UITapGestureRecognizer(HandleTextGesture));

			CountryInfo.UserInteractionEnabled = true;

							
			for (int i=0;i<ViewModel.Items.Count;i++)
			{	
				var image =  Convert.FromBase64String (ViewModel.Items[i].Base64Image);
				flaglist.Add (Extensions.ToImage (image));
			}

			CountryInfo.Text = ViewModel.Items [e].Native;
			CountryInfo.TextColor = UIColor.DarkGray;
			CountryInfo.Font = UIFont.FromName ("Roboto-Regular", 18f);


			FlagImage.Image = flaglist[e];

			for (int i=0;i<ViewModel.Items.Count;i++)
			{	
				_countryList.Add(new DropDownListItem()
				{
					Id = (i+1).ToString(),
						DisplayText = ViewModel.Items[i].Native+ " ("+ViewModel.Items[i].Region+")",
					Image = flaglist[i]
				});
			}
		}

		private void ScrollTheView(bool moveUp)
		{
			//To invoke a views built-in animation behaviour,
			//you create an animation block and
			//set the duration of te move...
			//Set the display scroll animation and duration...

			// Do not scroll if screen is from iPhone4/S
			if ((UIDevice.CurrentDevice.UserInterfaceIdiom  == UIUserInterfaceIdiom.Phone)
				&& (UIScreen.MainScreen.Bounds.Height > 568))
				return;
			
			if (((moveViewUp == true) && (moveUp == true)) || ((moveViewUp == false) && (moveUp == false)))
				return;

			if (scrollamount > 0) 
			{
				UIView.BeginAnimations (string.Empty, System.IntPtr.Zero);
				UIView.SetAnimationDuration (0.1);

				//Get Display size...
				CGRect frame = mycontroller.View.Frame;

				// CGPoint point = new CGPoint (EnterNumber.Frame.X, EnterNumber.Frame.Y);

				if (moveUp) {
					//If the view should be moved up,
					//subtract the keyboard height from the display...
					frame.Y -= scrollamount;
					frame.Y  += 0;

					moveViewUp = true;

				} else {
					//If the view shouldn't be moved up, restore it
					//by adding the keyboard height back to the original...
					frame.Y += scrollamount;
					frame.Y  -= 0;

					moveViewUp = false;

				}
				//Assign the new frame to the view...
				mycontroller.View.Frame = frame;

				//Tell the view that your all done with setting
				//the animation parameters, and it should
				//start the animation...
				UIView.CommitAnimations ();
			}
		}
			
		void HandleTextGesture (UITapGestureRecognizer sender)
		{
			if (EnterNumber.CanResignFirstResponder)
			{
				EnterNumber.ResignFirstResponder();
			}

			if (EnterCountryCode.CanResignFirstResponder)
			{
				EnterCountryCode.ResignFirstResponder();
			}

			ScrollTheView(false);

			UIView view = new UIView(new CGRect(0,0, mycontroller.View.Bounds.Width-30f, mycontroller.View.Bounds.Height));
			view.BackgroundColor = UIColor.FromRGBA(0x00, 0x00, 0x00, 0xAA);
		
			nfloat padding = 10.0f;

			UIScrollView ImageBackgroundView = new UIScrollView(new CGRect(padding,padding ,  mycontroller.View.Bounds.Width-(2*padding), (mycontroller.View.Bounds.Height-(2*padding))));
			ImageBackgroundView.BackgroundColor = UIColor.Clear;
			ImageBackgroundView.Layer.CornerRadius = 5f;

			ImageBackgroundView.ContentSize = new CGSize (mycontroller.View.Bounds.Width - 30f, ViewModel.Items.Count * 40.0f); 

			ddl = new DropDownList(ImageBackgroundView ,_countryList.ToArray())
			{
				BackgroundColor = UIColor.FromCGColor(new CGColor(255f/255f, 63f/255f, 63f/255f, 255f/255f)), //FromRGB(255, 255, 255),
				Opacity = 0.85f,
				TintColor = UIColor.Black,
				NavigationBarAssumedHeight = 1
			};
		
			ddl.DropDownListChanged += (e, a) =>
			{
				_index = e; // e is the index selected
				var strValue = a.DisplayText; //a is the dropdown list item object
			
				int id;
				if (!int.TryParse(a.Id, out id))
				{	
					if (id < 1)
					{	
						id = 1;
					}
				}
				id = id-1;
			
				CountryInfo.Text = ViewModel.Items [id].Native + "(" + ViewModel.Items [id].Region + ")";
				CountryInfo.TextColor = UIColor.DarkGray;
				CountryInfo.Font = UIFont.FromName ("Roboto-Regular", 20f);

				FlagImage.Image = flaglist[id];
				EnterCountryCode.Text = ViewModel.Items [id].PhonePrefix;

				ddl.Toggle();

				ImageBackgroundView.RemoveFromSuperview();
			};

			ddl.ClipsToBounds = false;
			Console.WriteLine ("size = " + ddl.Bounds);

			ImageBackgroundView.Add(ddl);
			ImageBackgroundView.BringSubviewToFront(ddl);
			view.Add(ImageBackgroundView);

			Add(ImageBackgroundView);
		
			ddl.Toggle ();
		}


		void _viewModel_PhoneDataSuccessful (object sender, EventArgs e)
		{
			// set up drowdown without index. Should not occur
		}

		#endregion



		private void TextChangedEvent(NSNotification notification)
		{
			UITextField field = (UITextField)notification.Object;
	//		Console.WriteLine("notification.Object - " + notification.Object);
		}

			
		private void SetAccessoryView()
		{
			acc_toolbar = new UIToolbar (new CGRect(0.0f, 0.0f, Frame.Size.Width, 44.0f));
			acc_toolbar.TintColor = UIColor.White;
			acc_toolbar.BarStyle = UIBarStyle.Black;

			acc_toolbar.Translucent = true;

			myDoneButton = new UIBarButtonItem(UIBarButtonSystemItem.Done, delegate
				{
					if (EnterNumber.CanResignFirstResponder)
					{
						EnterNumber.ResignFirstResponder();
					}

					if (EnterCountryCode.CanResignFirstResponder)
					{
						EnterCountryCode.ResignFirstResponder();
					}

					ScrollTheView(false);

					if (EnterNumber.Text == "")
					{
						EnterNumber.Text = "";//"Indtast telefonnummer";//#newstring
						//EnterNumber.TextAlignment = UITextAlignment.Justified;
						EnterNumber.TextColor = UIColor.Gray;
						//EnterNumber.AdjustsFontSizeToFitWidth = true;
						return;
					}

					if (ViewModel.Items.Count> 0)
					{
						if ((ViewModel.Items.Count> 0)&&(EnterNumber.Text.Length <= _max)&&(EnterNumber.Text.Length >= _min))
						{
							GlobalData.UserItem.Prefix =  ViewModel.Items [_index].PhonePrefix;
							GlobalData.UserItem.Region = ViewModel.Items [_index].Region;
							GlobalData.UserItem.Culture = System.Globalization.CultureInfo.CurrentUICulture.Name;
							GlobalData.UserItem.DeviceId = UniqueID();
							#if SIMULATOR
							user.Culture = System.Globalization.CultureInfo.CreateSpecificCulture("da-DK").ToString(); 
							#else
							GlobalData.UserItem.Culture = System.Globalization.CultureInfo.CurrentUICulture.Name;
							#endif

							#if SIMULATOR
							user.PushChannel = "IOS:SimulatorOnMac";
							#else
							string RawToken = DeviceToken.RetrieveToken();
							if (RawToken != null)
							{
								string DevToken = String.Copy(RawToken);
								DevToken.Replace ("<", "&lt;").Replace (">", "&gt;");
								GlobalData.UserItem.PushChannel = ("IOS:" + DevToken);
							}
							else
							{
								GlobalData.UserItem.PushChannel = "FailedToRegisterOnIOS";
							}
							#endif

							GlobalData.UserItem.Phone = EnterNumber.Text;
					
							ViewModel.Request(GlobalData.UserItem);
	
							BlurView(true);
							UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false; 

						}	
						else
						{   //#newstring
							UIAlertView alert = new UIAlertView (NSBundle.MainBundle.LocalizedString("WrongNumberText", "WrongNumberText"), "", null, NSBundle.MainBundle.LocalizedString ("OK", "OK"), null);
							alert.Clicked += (sender1, buttonArgs) => 
							{
								if (EnterNumber.CanBecomeFirstResponder)
									EnterNumber.BecomeFirstResponder();
							};
							alert.Show ();

						}
					}
					else
					{  
						UIAlertView alert = new UIAlertView (NSBundle.MainBundle.LocalizedString("DataNotReadyText", "DataNotReadyText"), "", null, NSBundle.MainBundle.LocalizedString ("OK", "OK"), null);
						alert.Show ();
					}
				});

			myDoneButton.TintColor = UIColor.White;
			acc_toolbar.Items = new UIBarButtonItem[]{
				new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace),
				myDoneButton
			};

			this.EnterNumber.InputAccessoryView = acc_toolbar;
		}

		void _viewModel_RequestNotSuccessful (object sender, ServerOutputItem e)
		{

			Container.UserItem.Phone = EnterNumber.Text;
			Container.UserItem.Prefix = ViewModel.Items [_index].PhonePrefix;

			if (e.Result == MainClass.eServerResult.UserNotFound)
			{  

				UIAlertView alert = new UIAlertView (Container.UserItem.Prefix+Container.UserItem.Phone + " " +
					NSBundle.MainBundle.LocalizedString("ProfileRequestText", "ProfileRequestText"), "", null, NSBundle.MainBundle.LocalizedString ("OK", "OK"), null);
				alert.Clicked += (sender1, buttonArgs) => 
				{
		    		if (LoginViewDone != null) {
						LoginViewDone (this, EventArgs.Empty);
					}
				};
				alert.Show ();
			}
			else 
			{ 
		    	UIAlertView alert = new UIAlertView (NSBundle.MainBundle.LocalizedString("ServerErrorText", "ServerErrorText"), "", null, NSBundle.MainBundle.LocalizedString ("OK", "OK"), null);
			   alert.Show ();
			}

			BlurView (false);
		}


		void SetInputPinViews()
		{

			AddBckButton ();



			if (_login)
			{ 	
			   EnterNumber.Hidden = true;
			   EnterCountryCode.Hidden = true;
			   FlagImage.Hidden = true;
			   CountryInfo.Hidden = true;
			   EnterCountryCodeLabel.Hidden = true;
			   EnterPhoneLabel.Hidden = true;
			}


			EnterDigitsContainerView.Hidden = false;
			EnterDigitsContainerView.Alpha = 1.0f;;
			EnterDigitsContainerView.UserInteractionEnabled = true;

			if (_notificationObserver != null) 
			{  
				NSNotificationCenter.DefaultCenter.RemoveObserver(_notificationObserver); 
				_notificationObserver = null; 
			}  
				
			LoginImageView.Image = UIImage.FromFile("Images/Login_PIN_256x256.png");
			ClearDigitFields();
		}


		void SetInputPhoneNumberViews()
		{
			EnterNumber.Hidden = false;
			EnterCountryCode.Hidden = false;
			FlagImage.Hidden = false;
			CountryInfo.Hidden = false;
			EnterCountryCodeLabel.Hidden = false;
			EnterPhoneLabel.Hidden = false;
			EnterDigitsContainerView.Hidden = true;
			LoginImageView.Image = UIImage.FromFile("Images/Phone_256x256.png");

			EnterNumber.BecomeFirstResponder();
		}

		void _viewModel_RequestSuccessful (object sender, EventArgs e)
		{
			BlurView (false);
			SetInputPinViews ();

			var frame = mycontroller.View.Bounds;

			header.RemoveFromSuperview ();

			SetHeaderText(frame, new NSAttributedString(NSBundle.MainBundle.LocalizedString("VerifyCodeText", "VerifyCodeText"), GlobalData.HeaderAttrib));

			Container.UserItem.Phone = EnterNumber.Text;
			Container.UserItem.Prefix = ViewModel.Items [_index].PhonePrefix;
			Container.UserItem.Region = ViewModel.Items [_index].Region;

			if (mycontroller.NavigationController != null) 
			{
				mycontroller.NavigationController.NavigationBar.TintColor = UIColor.White;
			}

			UIImageView imageView = new UIImageView(new CGRect(0,0,20,20));
			imageView.Image = UIImage.FromBundle("Images/Phone_256x256.png");

			string str = "("+ Container.UserItem.Prefix +")"+ Container.UserItem.Phone;
			UIFont font = UIFont.FromName("Roboto-Regular", 20f);
			UILabel centertext= new UILabel();
			centertext.Text = str;
			centertext.Font = font;
			centertext.TextColor = UIColor.White;

			NSString nsString = new NSString(str);
			UIStringAttributes attribs = new UIStringAttributes { Font = font };
			CGSize titleSize = nsString.GetSizeUsingAttributes(attribs);

			UIView centerView;

			// space px in each side
			nfloat space = 50;

			if ((titleSize.Width + 30) > (mycontroller.View.Bounds.Width - (space * 2)))
			{
				centertext.AdjustsFontSizeToFitWidth = true;
				centertext.Frame = new CGRect(30,0,mycontroller.View.Bounds.Width - (space * 2) - 30, 24);
				centerView = new UIImageView(new CGRect(0,0,mycontroller.View.Bounds.Width - (space * 2), 24));
			}
			else
			{
				centertext.AdjustsFontSizeToFitWidth = false;
				centertext.Frame = new CGRect(30,0,titleSize.Width,24);
				centerView = new UIImageView(new CGRect(0,0,titleSize.Width + 30,24));
			}

			centerView.Add(imageView);
			centerView.Add(centertext);

			mycontroller.NavigationItem.TitleView = centerView;
		}

		void _viewModel_PhoneDataErrorOccurred (object sender, string e)
		{
			BlurView (false);

			//#newstring
			UIAlertView alert = new UIAlertView (e, NSBundle.MainBundle.LocalizedString("OfflineText", "OfflineText"), null, NSBundle.MainBundle.LocalizedString ("OK", "OK"), "Cancel");
			alert.Clicked += (sender1, buttonArgs) => 
			{
				if (buttonArgs.ButtonIndex == 0) //OK Clicked
				{ 
					BlurView (true);
					subscribeData ();
				}
				else
				{
					if (LoginViewDone != null) {
						LoginViewDone (this, EventArgs.Empty);
					}
				}
			};
			//Console.WriteLine (e);
			alert.Show ();
		}

		public void AddBarButtonText(object sender, EventArgs e)
		{
			var barButtonItem = sender as UIBarButtonItem;
			if (barButtonItem != null)
				this.EnterNumber.Text += barButtonItem.Title;
		}
			
		void Blink_Elapsed (object sender, ElapsedEventArgs e)
		{
			InvokeOnMainThread (() => {
				if (Digit1.IsFirstResponder)
				{
					Digit1.Toggle();
				}
				else if (Digit2.IsFirstResponder)
				{
					Digit2.Toggle();
				}
				else if (Digit3.IsFirstResponder)
				{
					Digit3.Toggle();
				}
				else if (Digit4.IsFirstResponder)
				{
					Digit4.Toggle();
				}
				else
				{
				}
			});
		}


		public void SetView(CGRect frame)
		{
			AutoresizingMask = UIViewAutoresizing.All;

			SetFields(frame);

			bckImage = new UIImageView(this.Bounds);
			bckImage.AutoresizingMask = UIViewAutoresizing.All;
			bckImage.AddGestureRecognizer(new UITapGestureRecognizer(HandleTapGesture));
			bckImage.UserInteractionEnabled = true;
			bckImage.Add(LoginImageView);
			bckImage.Add(EnterCountryCodeLabel);
			bckImage.Add (EnterPhoneLabel);
			bckImage.Add(FlagImage);
			bckImage.Add(CountryInfo);
			bckImage.Add(EnterCountryCode);
			bckImage.Add(EnterNumber);
			bckImage.Add(NBDImageView);
			bckImage.Add(EnterDigitsContainerView);
			Add(bckImage);
		}

		#region Codefields


		public void SetCodeView(CGRect frame)
		{
			AutoresizingMask = UIViewAutoresizing.All;

			SetCodeFields(frame);

			float TopImageHeight = (float)frame.Height / 8f;
			LoginImageView = new UIImageView(new CGRect ((frame.Width - TopImageHeight) / 2, TopImageHeight, TopImageHeight, TopImageHeight));

			var labelOffset = TopImageHeight * 2 + 10f;

			var flagOffset = labelOffset + 35;


			var enterNumberViewOffset = flagOffset + 70;


			var NBDImgOffset = enterNumberViewOffset + 70f;
			var NBDImgHeight = frame.Height / 4;

	
			NBDImageView = new UIImageView(new CGRect((frame.Width-NBDImgHeight) / 2, NBDImgOffset , NBDImgHeight, NBDImgHeight));


			bckImage = new UIImageView(this.Bounds);
			bckImage.Add(NBDImageView);
			bckImage.Add(EnterDigitsContainerView);
			Add(bckImage);
		}



		public void SetCodeFields(CGRect frame)
		{
			var heigthKeyboard = 216f;
			var heightHeaderAndStatus = 44f; //64

			var freeSpace = frame.Height - heigthKeyboard - heightHeaderAndStatus;



			Digit1 = new Digit(new CGRect(this.Frame.Width / 5.2f, 34f, 40f, 40f));
			Digit2 = new Digit(new CGRect(this.Frame.Width / 2.9f, 34f, 40f, 40f));
			Digit3 = new Digit(new CGRect(this.Frame.Width / 2.0f, 34f, 40f, 40f));
			Digit4 = new Digit(new CGRect(this.Frame.Width / 1.52f, 34f, 40f, 40f));

			Digit1.Layer.CornerRadius = 20; 
			Digit2.Layer.CornerRadius = 20;
			Digit3.Layer.CornerRadius = 20;
			Digit4.Layer.CornerRadius = 20;

			Digit1.Font = UIFont.FromName("Roboto-Regular", 30f);
			Digit2.Font = UIFont.FromName("Roboto-Regular", 30f);
			Digit3.Font = UIFont.FromName("Roboto-Regular", 30f);
			Digit4.Font = UIFont.FromName("Roboto-Regular", 30f);

			EnterCodeTextLabel = new UILabel(new CGRect(0, 5, frame.Width, 25f));
			EnterCodeTextLabel.Font = UIFont.FromName("Roboto-Regular", 14f);
			EnterCodeTextLabel.TextColor = UIColor.White;
			EnterCodeTextLabel.Text = "Enter received PIN code";
			EnterCodeTextLabel.TextAlignment = UITextAlignment.Center;

			EnterDigitsContainerView = new UIView(new CGRect(5f, frame.Height/3-20f, 310f, 80f));
			EnterDigitsContainerView.AutoresizingMask = UIViewAutoresizing.FlexibleLeftMargin | UIViewAutoresizing.FlexibleRightMargin;
			EnterDigitsContainerView.Add(EnterCodeTextLabel);

			EnterDigitsContainerView.Add(Digit1);
			EnterDigitsContainerView.Add(Digit2);
			EnterDigitsContainerView.Add(Digit3);
			EnterDigitsContainerView.Add(Digit4);
		}


		#endregion



			
		public class LoginTextFieldDelegate : UITextFieldDelegate
		{
			public EventHandler<UITextField> OnShouldBeginEditing;

			public override void EditingEnded(UITextField textField)
			{
				throw new System.NotImplementedException ();
			}

			public override void AddObserver(NSObject observer, NSString keyPath, NSKeyValueObservingOptions options, IntPtr context)
			{
				base.AddObserver(observer, keyPath, options, context);
			}

			public override bool ShouldBeginEditing(UITextField textField)
			{
				if (OnShouldBeginEditing != null)
				{
					OnShouldBeginEditing(this, textField);
				}
				return false;
			}
		}

		public void SetFields(CGRect frame)
		{
			var heigthKeyboard = 216f;
			var heightHeaderAndStatus = 44f; //64
		
			var freeSpace = frame.Height - heigthKeyboard - heightHeaderAndStatus;

			float TopImageHeight = (float)frame.Height / 8f;
			LoginImageView = new UIImageView(new CGRect ((frame.Width - TopImageHeight) / 2, TopImageHeight, TopImageHeight, TopImageHeight));

			var labelOffset = TopImageHeight * 2 + 10f;

			EnterCountryCodeLabel = new UILabel(new CGRect(44, labelOffset, frame.Width-80f , 40f));
			EnterCountryCodeLabel.Font = UIFont.FromName("Roboto-Regular", 14f);
			EnterCountryCodeLabel.TextColor = UIColor.White;
			EnterCountryCodeLabel.TextAlignment = UITextAlignment.Center;
			EnterCountryCodeLabel.Text = "Choose country";
			EnterCountryCodeLabel.Lines = 0;

			var flagOffset = labelOffset + 35;

			FlagImage = new UIImageView();
			FlagImage.Frame = new CoreGraphics.CGRect (44f, flagOffset, 40f, 30f);

			CountryInfo = new UITextView(new CGRect(90, flagOffset, 192f, 30f));
			CountryInfo.BackgroundColor = UIColor.White;
			CountryInfo.TextAlignment = UITextAlignment.Center;
			CountryInfo.AutocorrectionType = UITextAutocorrectionType.No;
			CountryInfo.AutoresizingMask = UIViewAutoresizing.FlexibleLeftMargin | UIViewAutoresizing.FlexibleWidth;
			CountryInfo.Font = UIFont.FromName ("OpenSans", 18f);
			CountryInfo.TextColor = UIColor.DarkGray;
			CountryInfo.Layer.CornerRadius = 15;   
			CountryInfo.ContentInset = new UIEdgeInsets (-4, 0, 4, 0);
				
			var enterphonerViewOffset = labelOffset + 70;

			EnterPhoneLabel = new UILabel(new CGRect(44, enterphonerViewOffset, frame.Width-80f , 40f));
			EnterPhoneLabel.Font = UIFont.FromName("Roboto-Regular", 14f);
			EnterPhoneLabel.TextColor = UIColor.White;
			EnterPhoneLabel.TextAlignment = UITextAlignment.Center;
			EnterPhoneLabel.Text = "Enter phone number for verification";
			EnterPhoneLabel.Lines = 0;

			var enterNumberViewOffset = flagOffset + 70;
			_completeOffset = (int)enterNumberViewOffset;

			UIView leftview = new UIView(new CGRect(0, 0, 10, 30));
			leftview.BackgroundColor = UIColor.Clear;
		
			EnterNumber = new UITextField(new CGRect(90, enterNumberViewOffset, 192f, 30f));
			EnterNumber.Enabled = false;
			EnterNumber.BackgroundColor = UIColor.White;
			EnterNumber.Font = UIFont.FromName("Roboto-Regular", 16f);
			EnterNumber.TextAlignment = UITextAlignment.Center;
			EnterNumber.KeyboardType = UIKeyboardType.NumberPad;
			EnterNumber.ReturnKeyType = UIReturnKeyType.Go;
			EnterNumber.AutocorrectionType = UITextAutocorrectionType.No;
			EnterNumber.KeyboardAppearance = UIKeyboardAppearance.Dark;
			EnterNumber.AutoresizingMask = UIViewAutoresizing.FlexibleLeftMargin | UIViewAutoresizing.FlexibleWidth;
			EnterNumber.TextColor = UIColor.DarkGray;
			EnterNumber.Layer.CornerRadius = 15; 

			EnterNumber.LeftViewMode = UITextFieldViewMode.Always;
			EnterNumber.LeftView = leftview;

			SetAccessoryView();

			EnterCountryCode = new MyUITextField(new CGRect(44f, enterNumberViewOffset, 40f, 30f));
			EnterCountryCode.TextColor = UIColor.DarkGray;
			EnterCountryCode.Enabled = true;
			EnterCountryCode.Font = UIFont.FromName ("OpenSans", 18f);
			EnterCountryCode.AutoresizingMask = UIViewAutoresizing.FlexibleRightMargin;
			EnterCountryCode.UserInteractionEnabled = false;
			EnterCountryCode.TextAlignment = UITextAlignment.Center;
			EnterCountryCode.BackgroundColor = UIColor.White;
			EnterCountryCode.Layer.CornerRadius = 15; 

			// Set default 
			FlagImage.Image = GlobalData.LightGrayBackGround;
			FlagImage.Layer.MasksToBounds = true;
			FlagImage.Layer.CornerRadius = 15;
				
			var NBDImgOffset = enterNumberViewOffset + 70f;
			var NBDImgHeight = frame.Height / 4;

			NBDImageView = new UIImageView(new CGRect((frame.Width-NBDImgHeight) / 2, NBDImgOffset , NBDImgHeight, NBDImgHeight));

			Digit1 = new Digit(new CGRect(this.Frame.Width / 5.2f, 34f, 40f, 40f));
			Digit2 = new Digit(new CGRect(this.Frame.Width / 2.9f, 34f, 40f, 40f));
			Digit3 = new Digit(new CGRect(this.Frame.Width / 2.0f, 34f, 40f, 40f));
			Digit4 = new Digit(new CGRect(this.Frame.Width / 1.52f, 34f, 40f, 40f));

			Digit1.Layer.CornerRadius = 20; 
			Digit2.Layer.CornerRadius = 20;
			Digit3.Layer.CornerRadius = 20;
			Digit4.Layer.CornerRadius = 20;

			Digit1.Font = UIFont.FromName("Roboto-Regular", 30f);
			Digit2.Font = UIFont.FromName("Roboto-Regular", 30f);
			Digit3.Font = UIFont.FromName("Roboto-Regular", 30f);
			Digit4.Font = UIFont.FromName("Roboto-Regular", 30f);

			EnterCodeTextLabel = new UILabel(new CGRect(0, 5, frame.Width, 25f));
			EnterCodeTextLabel.Font = UIFont.FromName("Roboto-Regular", 14f);
			EnterCodeTextLabel.TextColor = UIColor.White;
			EnterCodeTextLabel.Text = "Enter received PIN code";
			EnterCodeTextLabel.TextAlignment = UITextAlignment.Center;
	
			EnterDigitsContainerView = new UIView(new CGRect(5f, frame.Height/3-20f, 310f, 80f));
			EnterDigitsContainerView.AutoresizingMask = UIViewAutoresizing.FlexibleLeftMargin | UIViewAutoresizing.FlexibleRightMargin;
			EnterDigitsContainerView.Add(EnterCodeTextLabel);

			EnterDigitsContainerView.Add(Digit1);
			EnterDigitsContainerView.Add(Digit2);
			EnterDigitsContainerView.Add(Digit3);
			EnterDigitsContainerView.Add(Digit4);
		}
			
		public void SetupDigits()
		{

			NBDImageView.Image = UIImage.FromFile("Images/Logo_Small_250x250.png");

			if (_login)
			{
			     EnterNumber.EditingChanged += delegate(object sender, EventArgs e)
			    {
				   if (EnterNumber.Text == NSBundle.MainBundle.LocalizedString("EnterNumberText", "EnterNumberText"))  
				   {
					   EnterNumber.Text = "";
				   }
			};
				

			EnterNumber.EditingDidBegin += delegate(object sender, EventArgs e)
			{
				if (EnterNumber.Text == NSBundle.MainBundle.LocalizedString("EnterNumberText", "EnterNumberText"))
				{
					EnterNumber.Text = "";
				}
					
				//EnterNumber.TextAlignment = UITextAlignment.Center;
				//EnterNumber.ClearButtonMode = UITextFieldViewMode.WhileEditing;
			};

			EnterNumber.KeyboardType = UIKeyboardType.NumberPad;
			EnterNumber.KeyboardAppearance = UIKeyboardAppearance.Dark;
			EnterNumber.SpellCheckingType = UITextSpellCheckingType.No;
			EnterNumber.Font = UIFont.FromName ("OpenSans", 17f);
			EnterNumber.TextColor = UIColor.Gray;
			}

			Digit1.Changed += delegate(object sender, EventArgs e) {
				if(Digit1.Text.Length != 0)
				{
			//		Digit1.DigitEntered = true;
					Digit1.Turnoff();
					Digit2.BecomeFirstResponder();
				}
			};
			Digit1.ShouldChangeText += (fld, rng, str) => CheckText (fld, rng, str, 1, null);


			Digit2.Changed += delegate(object sender, EventArgs e) {
				if (Digit2.Text.Length != 0)
				{

				//	Digit2.DigitEntered = true;
					Digit2.Turnoff();
					Digit3.BecomeFirstResponder ();
				}
			};
			Digit2.ShouldChangeText += (fld, rng, str) => CheckText (fld, rng, str, 1, Digit1);


			Digit3.Changed += delegate(object sender, EventArgs e) {
				if(Digit3.Text.Length != 0)
				{	
				//	Digit3.DigitEntered = true;
					Digit3.Turnoff();
					Digit4.BecomeFirstResponder();
				}
			};
			Digit3.ShouldChangeText += (fld, rng, str) => CheckText (fld, rng, str, 1, Digit2);




			Digit4.Changed += delegate(object sender, EventArgs e) {
				if(Digit4.Text.Length != 0)
				{
			//		Digit4.DigitEntered = true;
					Digit4.Turnoff();
					Digit4.ResignFirstResponder();

					blink.AutoReset = false;

					blink.Stop();

					AssembleCode();
				}
			};
			Digit4.ShouldChangeText += (fld, rng, str) => CheckText (fld, rng, str, 1, Digit3);
	

		}	



		void HandleTapGesture (UITapGestureRecognizer sender)
		{
			if (EnterNumber.CanResignFirstResponder)
			{
				EnterNumber.ResignFirstResponder();
			}


			ScrollTheView(false);

			if (EnterNumber.Text == "")
			{
				EnterNumber.Text = NSBundle.MainBundle.LocalizedString("EnterNumberText", "EnterNumberText"); 
				//EnterNumber.TextAlignment = UITextAlignment.Justified;
				EnterNumber.TextColor = UIColor.Gray;
				//EnterNumber.AdjustsFontSizeToFitWidth = true;
			}
		}

	
		bool CheckText(UITextView fld, NSRange rng, string newChar, int maxLength, Digit nextView)
		{
			var ff = fld as Digit;

			const string numbers = "0123456789";
			if (newChar == ("") &&  (nextView != null))
			{
				ff.Turnoff();
				nextView.BecomeFirstResponder ();

			//	nextView.DigitEntered = false;
				nextView.Text = "";
				return true;
			}
			else if (ff.Text.Length >= maxLength && rng.Length == 0) {
				return false;
			} else {
				if (numbers.IndexOf(newChar) >= 0)
				{
					return true;
				}
				return false;
			}
		}

		private void AssembleCode()
		{
			string assembledCode = Digit1.Text + Digit2.Text + Digit3.Text + Digit4.Text;

			EnterDigitsContainerView.Alpha = 0.1f;
			EnterDigitsContainerView.UserInteractionEnabled = false;

			BlurView (true);

			if (_login)
			{	
		    	ViewModel.Confirm(GlobalData.UserItem, assembledCode);
			}
			else
			{	
				GlobalData.UserItem.Code = assembledCode;
				UserViewModel.UpdateUser (GlobalData.UserItem); 
			}	
		}

		void _viewModel_ConfirmNotSuccessful (object sender, ServerOutputItem e)
		{
			BlurView (false);

			//#newstring
			var alert = new UIAlertView (NSBundle.MainBundle.LocalizedString("WrongCodeText", "WrongCodeText"), "", null, NSBundle.MainBundle.LocalizedString("OK", "OK"), null);
			alert.Clicked += (sender1, buttonArgs) => 
			{
				if (buttonArgs.ButtonIndex == 0) //OK Clicked
				{ 
					SetInputPinViews();
				}
			};
			alert.Show ();
		}

		void _viewModel_ConfirmSuccessful (object sender, EventArgs e)
		{
			BlurView (false);
		
			GlobalData.IsLoggedIn = true;

			string libraryPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
			var path = Path.Combine(libraryPath, Container.SQLitePath);

			var viewModel = new UserViewModel (new SQLitePlatformIOS(), path);

			GlobalData.UserItem = viewModel.GetUser ();

			if (GlobalData.UserItem != null) {

				Container.UserItem.Phone = GlobalData.UserItem.Phone;
		//		Container.UserItem.PhoneBarCodeBase64 = user.PhoneBarCodeBase64;
		//		Container.UserItem.PhoneBarCodeHeight = user.PhoneBarCodeHeight;
		//		Container.UserItem.PhoneBarCodeWidth = user.PhoneBarCodeWidth;
				Container.UserItem.Prefix = GlobalData.UserItem.Prefix;
				Container.UserItem.Region = GlobalData.UserItem.Region;
				Container.UserItem.UserId = GlobalData.UserItem.UserId;
				Container.UserItem.Name = GlobalData.UserItem.Name;
			}


			if (LoginViewDone != null) {
				LoginViewDone (this, EventArgs.Empty);
			}
		}


		private void ClearDigitFields()
		{
//			Digit1.DigitEntered = false;
//			Digit2.DigitEntered = false;
//			Digit3.DigitEntered = false;
//			Digit4.DigitEntered = false;

			Digit1.Text = "";
			Digit2.Text = "";
			Digit3.Text = "";
			Digit4.Text = "";

			Digit1.UserInteractionEnabled = false;
			if (Digit1.CanBecomeFirstResponder) {
				Digit1.BecomeFirstResponder ();
				blink.Start();
				blink.AutoReset = true;
			} 
			else 
			{
				int i = 1;
			}
		}
			
		private string UniqueID() 
		{
			var query = new SecRecord(SecKind.GenericPassword);
			query.Service = NSBundle.MainBundle.BundleIdentifier;
			query.Account = "UniqueID";

			NSData uniqueId = SecKeyChain.QueryAsData(query);
			if(uniqueId == null)
			{
				query.ValueData = NSData.FromString(System.Guid.NewGuid().ToString());
				var err = SecKeyChain.Add (query);
				if (err != SecStatusCode.Success && err != SecStatusCode.DuplicateItem)
						throw new Exception("Cannot store Unique ID");

				return query.ValueData.ToString();
			}
			else
    		{
				return uniqueId.ToString();
			}
		
		}

		private async void BlurView(bool isVisible)
		{
			if (isVisible) {
				if (blurOverlay == null) 
				{
					blurOverlay = new UIImageView (new CGRect (0, Frame.Height / 11, Frame.Width, Frame.Height - Frame.Height / 11));
					blurOverlay.Image = GlobalData.GrayBackGround;

					UILabel label = new UILabel(new CGRect (0, Frame.Height / 3, Frame.Width, Frame.Height / 11));
					label.Text = NSBundle.MainBundle.LocalizedString ("PleaseWaitText", "PleaseWaitText");
					label.TextColor = UIColor.White;
					label.TextAlignment = UITextAlignment.Center;
					label.Font = UIFont.FromName("Roboto-Regular", 18f);
					blurOverlay.AddSubview (label);

					UIActivityIndicatorView Spinner = new UIActivityIndicatorView (new CGRect (0, Frame.Height / 3 + 30, Frame.Width, Frame.Height / 11));
					Spinner.StartAnimating ();
					blurOverlay.Add (Spinner);

					Add (blurOverlay);
				}
				blurOverlay.Layer.Opacity = 0;

				await UIView.AnimateAsync (0.2f, () => {
					blurOverlay.Layer.Opacity = 0.7f;
				});
			} 
			else 
			{
				if (blurOverlay != null) 
				{
					blurOverlay.Layer.Opacity = 0.7f;
					await UIView.AnimateAsync (0.2f, () => {
						blurOverlay.Layer.Opacity = 0f;
					});

					blurOverlay.RemoveFromSuperview ();
					blurOverlay.Dispose ();
					blurOverlay = null;
				}
			}
		}

	}
}

