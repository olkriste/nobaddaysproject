﻿using System;
using CoreGraphics;
using UIKit;
using System.Timers;
using Foundation;
using System.Collections.Generic;
using NoBadDaysPCL;
using System.IO;
using SQLite.Net.Platform.XamarinIOS;
using Valore.IOSDropDown;
using Security;
using Utils;
using NoBadDaysPCL.Items;
using NoBadDaysPCL.ViewModel;
using System.Threading.Tasks;
using System.Net;
using System.Drawing;
using System.Text.RegularExpressions;
using CoreAnimation;

namespace nobaddaysIOS
{
	public class MyNBDView: UIView
	{
		UIViewController viewcontroller;

		UIImageView SubScriptionView;
		UIImageView blurOverlay = null;
		UIImageView _bckButton;

		UIWebView webView;

		// Tap bar
		UIButton btnRight;
		UIButton btnLeft;

		// Buttom bar
		UIImageView ButtomBar;
		UIImageView editPhone;
		UIImageView editEmail;
		UIImageView editSlider;
		UIImageView editProfilePicture;
		UIImageView editName;

		// Sub views
		UIImageView editPhoneView;
		UIImageView editEmailView;
		UIImageView editSliderView;
		UIImageView editProfilePictureView;
		UIImageView editNameView;
		UIImageView profileImage;
		UIImageView bckImageScan;

		// Profile Image 
		UIImagePickerController imagePicker;
		UIImageView profileImageGhost;

		// Edit phone
		DropDownList ddl;
		UITextView CountryInfo;
		MyUITextField EnterCountryCode;
		UIImageView FlagImage;
		List<UIImage> flaglist;
		List<DropDownListItem> _countryList = new List<DropDownListItem>();
		int _index = -1;

		// Subviews labels
		UILabel sliderValueLabel;
		UILabel logInOutLabel;
		UILabel subscriptionLabel;
		UILabel editLabel;

		// Slider
		UILabel SliderName;


		// Uservalues
		UILabel nameLabel;
		UILabel countryLabel;
		UILabel phoneLabel;
		UILabel emailLabel;

		// Uitext
		UITextView phoneText;
		UITextView pinText;

		bool userChanged = false;
		bool isSaving = false;
		bool isScanSave = true;

		private UserItem EditedUserItem;

		// Events
		public event EventHandler LogOutEvent; 
		public event EventHandler<bool> LogInEvent;
		public event EventHandler ImageSelectionEvent; 

		// Gestures
		UITapGestureRecognizer logInOutTap;
		UIGestureRecognizer subScriptionTap;
		UIGestureRecognizer editTap;
		UITapGestureRecognizer refreshTap;

		UITapGestureRecognizer ButtomBarTap;
		UITapGestureRecognizer editPhoneTap;
		UITapGestureRecognizer editEmailTap;
		UITapGestureRecognizer editSliderTap;
		UITapGestureRecognizer editProfilePictureTap;
		UITapGestureRecognizer editNameTap;

	
		public MyNBDView (object controller)
		{
			viewcontroller = controller as UIViewController;
			//this.window.RootViewController = new TabBarController ();

			var frame = viewcontroller.View.Bounds;
			Frame = new CGRect(0, 0, frame.Width, frame.Height);

			EditedUserItem = new UserItem ();
			SetDefaultValues ();
			isScanSave = true;

			SetView(frame);
			SetScanSave (frame);
			SetTabBar (frame);
			SetButtomBar (frame);

			if (GPSHelper.mylocation != null) {
				ViewModel.GetUserFromServer ((float)GPSHelper.mylocation.Coordinate.Latitude, 
					(float)GPSHelper.mylocation.Coordinate.Longitude);
			} else {
				ViewModel.GetUserFromServer (0,0);
			}
		}

		UserViewModel _viewModel = null;
		UserViewModel ViewModel 
		{
			get 
			{ 
				if (_viewModel == null) 
				{
					string libraryPath = System.Environment.GetFolderPath (System.Environment.SpecialFolder.Personal);
					var path = Path.Combine (libraryPath, Container.SQLitePath);

					_viewModel = new UserViewModel (new SQLitePlatformIOS (), path);
					_viewModel.PhoneDataSuccessful += (object sender, ServerOutputItem e) => 
					{
						HandleDataSuccessful(e);
					};
					_viewModel.PhoneDataErrorOccurred += (object sender, ServerOutputItem e) => 
					{
						HandleDataUnSuccessful(e);
					};
					_viewModel.SMSIsSent += (object sender, ServerOutputItem e) => 
					{
						HandleDataSMS(e);
					};
					_viewModel.SupportedPhoneDataSuccessful += (object sender, int e) =>  
					{
						HandlePhoneData();
					};
				} 
				return _viewModel;
			}
		}

		private bool _visible; 
		public bool Visible 
		{
			get{ return _visible;}
			set 
			{
				_visible = value;
			}
		}

		void SetDefaultValues()
		{
			if (GlobalData.IsLoggedIn) {
				EditedUserItem.NotificationDistance = GlobalData.UserItem.NotificationDistance;
				EditedUserItem.Email = GlobalData.UserItem.Email;
				EditedUserItem.BarCode = GlobalData.UserItem.BarCode;
				EditedUserItem.Code = GlobalData.UserItem.Code;
				EditedUserItem.Bio = GlobalData.UserItem.Bio;
				EditedUserItem.Culture = GlobalData.UserItem.Culture;
				EditedUserItem.DeviceId = GlobalData.UserItem.DeviceId;
				EditedUserItem.Guid = GlobalData.UserItem.Guid;
				EditedUserItem.Name = GlobalData.UserItem.Name;
				EditedUserItem.Phone = GlobalData.UserItem.Phone;
				EditedUserItem.Picture = GlobalData.UserItem.Picture;
				EditedUserItem.Prefix = GlobalData.UserItem.Prefix;
				EditedUserItem.PushChannel = GlobalData.UserItem.PushChannel;
				EditedUserItem.Region = GlobalData.UserItem.Region;
				EditedUserItem.Token = GlobalData.UserItem.Token;
				EditedUserItem.UserId = GlobalData.UserItem.UserId;
			}
		}

		void HandlePhoneData()
		{
			if (ViewModel.PhoneData != null && ViewModel.PhoneData.Count > 0) {
				GlobalData.UserItem.Region = ViewModel.PhoneData [ViewModel.Index].Region;
			}
		}

		void HandleDataSuccessful(ServerOutputItem e)
		{
			GlobalData.UserItem = ViewModel.GetUser ();
			if (GlobalData.UserItem != null && GlobalData.UserItem.UserId > 0) {
				GlobalData.IsLoggedIn = true;
				#if DEBUG
				logInOutLabel.Text = NSBundle.MainBundle.LocalizedString ("LogOutText", "LogOutText");
				#endif
				if (!GlobalData.IsPushRegistered) {
					PushHelper push = new PushHelper ();
					push.RegisterPush ();
				}
			}
			SetProfileImage ();
			AnimateSubView ();
			userChanged = false;
			isSaving = false;
			UpdateFields (true);
			SetDefaultValues ();
			BlurView(false);
			SubScriptionView.UserInteractionEnabled = false;
			if (GlobalData.IsLoggedIn) {

				editLabel.Text = NSBundle.MainBundle.LocalizedString ("EditAccountText", "EditAccountText");

			} else {
				editLabel.Text = NSBundle.MainBundle.LocalizedString ("CreateAccountText", "CreateAccountText");
			}
			editLabel.BackgroundColor = UIColor.Clear;
			AddBarCode ();
		}

		void DropSubView()
		{
			if (editPhoneView != null) {
				editPhoneView.RemoveFromSuperview ();
				editPhoneView.Dispose ();
				editPhoneView = null;
			}
			else if (editEmailView != null) {
				editEmailView.RemoveFromSuperview ();
				editEmailView.Dispose ();
				editEmailView = null;
			}
			else if (editSliderView != null) {
				editSliderView.RemoveFromSuperview ();
				editSliderView.Dispose ();
				editSliderView = null;
			}
			else if (editProfilePictureView != null) {
				editProfilePictureView.RemoveFromSuperview ();
				editProfilePictureView.Dispose ();
				editProfilePictureView = null;
			}
			else if (editNameView != null) {
				editNameView.RemoveFromSuperview ();
				editNameView.Dispose ();
				editNameView = null;
			}
		}

		void HandleDataUnSuccessful(ServerOutputItem e)
		{
			UIAlertView alert = new UIAlertView (e.ResultString, "", null, NSBundle.MainBundle.LocalizedString ("OK", "OK"), null);
			alert.Clicked += (sender1, buttonArgs) => 
			{

			};
			alert.Show ();
			BlurView(false);
			if (isSaving && editPhoneView == null) {
				AddEditIcons (true);
			}
		}

		void HandleDataSMS(ServerOutputItem e)
		{
			UIAlertView alert = new UIAlertView (e.ResultString, "", null, NSBundle.MainBundle.LocalizedString ("OK", "OK"), null);
			alert.Clicked += (sender1, buttonArgs) => 
			{
		/*		BlurView(false);
				phoneText.UserInteractionEnabled = false;
				phoneText.TextColor = UIColor.Black;

				if (editPhoneView == null)
				{
					HandleEditPhoneTap();
					AddSMSView();
				}
				*/

			
				if (LogInEvent != null) {
					LogInEvent (this, false);
				}



			};
			alert.Show ();
		}

		private void SetView(CGRect frame)
		{
			//AutoresizingMask = UIViewAutoresizing.All;
			SetHeaderText (frame);
			SetProfileImage ();
			SetInfoText (frame);
		}

		private async void SetProfileImage()
		{
			float TopImageHeight = (float)Frame.Height / 4.6f;

			if (profileImage == null) {
				profileImage = new UIImageView (new CGRect ((Frame.Width - TopImageHeight) / 2, TopImageHeight / 1.8f, TopImageHeight, TopImageHeight));
				Add (profileImage);
			}
				
			if (profileImageGhost == null) {
				profileImageGhost = new UIImageView (new CGRect ((Frame.Width - TopImageHeight) / 2, TopImageHeight / 1.8f, TopImageHeight, TopImageHeight));
				profileImageGhost.Image = UIImage.FromBundle ("Images/GhostProfile_128x128.png");
				Add(profileImageGhost);
			}
				
			if (GlobalData.IsLoggedIn && !string.IsNullOrEmpty (GlobalData.UserItem.Picture.Uri)) {
				profileImage.Image = await LoadImage (GlobalData.UserItem.Picture.Uri);

				profileImage.Layer.CornerRadius = TopImageHeight / 2;
				profileImage.Layer.MasksToBounds = true;

				await UIView.AnimateAsync (0.1f, () => {
					profileImageGhost.Layer.Opacity = 0.0f;
				});
			} else {
				profileImageGhost.Layer.Opacity = 1;
				profileImage.Image = null;
			}
		}
			
		private async Task<UIImage> LoadImage (string imageUrl)
		{
			Task<byte[]> contentsTask = ViewModel.LoadImage (imageUrl);
			var contents = await contentsTask;
			return UIImage.LoadFromData (NSData.FromArray (contents));
		}

		private async void SetScanSave(CGRect frame)
		{
			bckImageScan = new UIImageView(new CGRect (0, frame.Height / 2, frame.Width, frame.Height / 2));
			bckImageScan.Image = GlobalData.WhiteBackGround;
			Add (bckImageScan);
			AddBarCode ();
		}
			
		private async void AddBarCode()
		{
			if (GlobalData.UserItem.BarCode != null && GlobalData.UserItem.BarCode.Uri != null &&
				GlobalData.UserItem.BarCode.Uri != "" && GlobalData.IsLoggedIn) {
				// Add Barcode
				UIImageView barCodeImage = new UIImageView (new CGRect (0, Frame.Height / 7, Frame.Width, Frame.Height / 5));
				bckImageScan.AddSubview (barCodeImage);

				barCodeImage.Layer.Opacity = 0.0f;
				barCodeImage.Image = await LoadImage (GlobalData.UserItem.BarCode.Uri);

				UIView.AnimateAsync (0.1f, () => {
					barCodeImage.Layer.Opacity = 1.0f;
				});
			} else if (bckImageScan != null) {
				UILabel notLoggedInLabel = new UILabel (new CGRect (0, Frame.Height / 7, Frame.Width, Frame.Height / 5));
				notLoggedInLabel.Text = NSBundle.MainBundle.LocalizedString ("NotLoggedInText", "NotLoggedInText");
				notLoggedInLabel.TextAlignment = UITextAlignment.Center;
				notLoggedInLabel.TextColor = UIColor.Gray;
				notLoggedInLabel.Font = UIFont.FromName("Roboto-Regular", 18f);
				bckImageScan.AddSubview (notLoggedInLabel);
			}
		}

		private void SetHeaderText(CGRect frame)
		{
			var tmpFrame = new CGRect(0, 0, frame.Width, frame.Height / 10);
			UILabel header = new UILabel(tmpFrame);
			header.TextAlignment = UITextAlignment.Center;
			header.AttributedText = new NSAttributedString(NSBundle.MainBundle.LocalizedString ("MyNoBadDaysText", "MyNoBadDaysText"), GlobalData.HeaderAttrib);
			Add (header);
		}

		private void SetInfoText(CGRect frame)
		{
			var tmpFrame = new CGRect(0, frame.Height / 2.9, frame.Width, frame.Height / 15);
			nameLabel = new UILabel(tmpFrame);
			nameLabel.TextAlignment = UITextAlignment.Center;
			if (GlobalData.IsLoggedIn) {
				nameLabel.AttributedText = new NSAttributedString (GlobalData.UserItem.Name, GlobalData.HeaderAttrib);
			} else {
				nameLabel.AttributedText = new NSAttributedString (NSBundle.MainBundle.LocalizedString ("NameText", "NameText"), GlobalData.HeaderAttrib);
			}
			Add (nameLabel);

			tmpFrame = new CGRect(0, frame.Height / 2.6, frame.Width, frame.Height / 15);
			countryLabel = new UILabel(tmpFrame);
			countryLabel.TextAlignment = UITextAlignment.Center;
			if (GlobalData.IsLoggedIn) {
				countryLabel.AttributedText = new NSAttributedString(GlobalData.UserItem.Bio, GlobalData.CountryAttrib);
			} else {
				countryLabel.AttributedText = new NSAttributedString (NSBundle.MainBundle.LocalizedString ("BioText", "BioText"), GlobalData.HeaderAttrib);
			}
			Add (countryLabel);
		}

		private void UpdateFields(bool updated)
		{
			if (updated) {
				nameLabel.AttributedText = new NSAttributedString (GlobalData.UserItem.Name, GlobalData.HeaderAttrib);
				countryLabel.AttributedText = new NSAttributedString (GlobalData.UserItem.Bio, GlobalData.CountryAttrib);
				phoneLabel.Text = GlobalData.UserItem.Prefix + GlobalData.UserItem.Phone;
				emailLabel.Text = GlobalData.UserItem.Email;
	//			mainSlider.Text = GlobalData.UserItem.NotificationDistance.ToString();
			} else {
				if (GlobalData.UserItem.Name != null) {
					nameLabel.AttributedText = new NSAttributedString (GlobalData.UserItem.Name, GlobalData.HeaderAttrib);
				}
				if (GlobalData.UserItem.Bio != null) {
					countryLabel.AttributedText = new NSAttributedString (GlobalData.UserItem.Bio, GlobalData.CountryAttrib);
				}
				if (GlobalData.UserItem.Phone != null) {
					phoneLabel.Text = GlobalData.UserItem.Prefix + GlobalData.UserItem.Phone;
				}
				if (GlobalData.UserItem.Email != null) {
					emailLabel.Text = GlobalData.UserItem.Email;
				}
	//			mainSlider.Text = GlobalData.UserItem.NotificationDistance.ToString();
			}
		}

		private void SetTabBar(CGRect frame)
		{
			btnLeft = new UIButton (new CGRect (0, frame.Height / 2, frame.Width / 2, frame.Height / 25));
			btnLeft.Hidden = false;
			btnLeft.TintColor = UIColor.Black;
			btnLeft.SetAttributedTitle(new NSAttributedString(NSBundle.MainBundle.LocalizedString ("AccountText", "AccountText"), GlobalData.TapBarBlackAttrib), UIControlState.Normal);
			btnLeft.BackgroundColor = UIColor.Black;
			btnLeft.SetTitleColor (UIColor.White, UIControlState.Normal);
			btnLeft.Layer.BorderColor = UIColor.Clear.CGColor;
			btnLeft.TouchUpInside += LeftBtn_TouchUpInside;
			Add (btnLeft);

			btnRight = new UIButton (new CGRect(frame.Width / 2, frame.Height / 2, frame.Width / 2, frame.Height / 25));
			btnRight.Hidden = false;
			btnRight.TintColor = UIColor.White;
			btnRight.SetAttributedTitle(new NSAttributedString(NSBundle.MainBundle.LocalizedString ("ScanNSaveText", "ScanNSaveText"), GlobalData.TapBarWhiteAttrib), UIControlState.Normal);
			btnRight.BackgroundColor = UIColor.White;
			btnRight.SetTitleColor (UIColor.Black, UIControlState.Normal);
			btnRight.Layer.BorderColor = UIColor.Clear.CGColor;
			btnRight.TouchUpInside += RightBtn_TouchUpInside;
			Add (btnRight);
		}

		void LeftBtn_TouchUpInside (object sender, EventArgs e)
		{
			btnRight.BackgroundColor = UIColor.Black;
			btnRight.SetAttributedTitle(new NSAttributedString(NSBundle.MainBundle.LocalizedString ("ScanNSaveText", "ScanNSaveText"), GlobalData.TapBarBlackAttrib), UIControlState.Normal);

			btnLeft.BackgroundColor = UIColor.White;
			btnLeft.SetAttributedTitle(new NSAttributedString(NSBundle.MainBundle.LocalizedString ("AccountText", "AccountText"), GlobalData.TapBarWhiteAttrib), UIControlState.Normal);

			if (isScanSave) {
				AnimateButtomBar ();
			}

			isScanSave = false;
			SetMySubscriptionView (true);
		}

		void RightBtn_TouchUpInside (object sender, EventArgs e)
		{
			btnRight.BackgroundColor = UIColor.White;
			btnRight.SetAttributedTitle(new NSAttributedString(NSBundle.MainBundle.LocalizedString("ScanNSaveText", "ScanNSaveText"), GlobalData.TapBarWhiteAttrib), UIControlState.Normal);

			btnLeft.BackgroundColor = UIColor.Black;
			btnLeft.SetAttributedTitle(new NSAttributedString(NSBundle.MainBundle.LocalizedString("AccountText", "AccountText"), GlobalData.TapBarBlackAttrib), UIControlState.Normal);

			if (!isScanSave) {
				AnimateButtomBar ();
			}

			isScanSave = true;
			SetMySubscriptionView (false);
		}

		private bool _isVisible; 
		public bool IsButtomBarVisible 
		{
			get{ return _isVisible;}
			set 
			{
				_isVisible = value;
			}
		}

		private void SetButtomBar(CGRect frame)
		{
			ButtomBar = new UIImageView(new CGRect (0, frame.Height, frame.Width, frame.Height / 11));
			ButtomBar.UserInteractionEnabled = true;
			ButtomBar.Image = GlobalData.GrayBackGround;

			logInOutLabel = new UILabel (new CGRect(0, frame.Height / 50, frame.Width / 3, frame.Height / 11));
			if (GlobalData.IsLoggedIn) {
				#if DEBUG
				logInOutLabel.Text = NSBundle.MainBundle.LocalizedString ("LogOutText", "LogOutText");
				#endif
			} else {
				logInOutLabel.Text = NSBundle.MainBundle.LocalizedString ("LogInText", "LogInText");
			}
			logInOutLabel.TextAlignment = UITextAlignment.Center;
			logInOutLabel.Font = UIFont.FromName("Roboto-Regular", 10f);
			logInOutLabel.BackgroundColor = UIColor.Clear;
			logInOutLabel.TextColor = UIColor.White;
			ButtomBar.AddSubview (logInOutLabel);

			subscriptionLabel = new UILabel (new CGRect(frame.Width / 3, frame.Height / 50, frame.Width / 3, frame.Height / 11));
			subscriptionLabel.Text = NSBundle.MainBundle.LocalizedString ("SubscriptionText", "SubscriptionText");

			subscriptionLabel.TextAlignment = UITextAlignment.Center;
			subscriptionLabel.Font = UIFont.FromName("Roboto-Regular", 10f);
			subscriptionLabel.BackgroundColor = UIColor.Clear;
			subscriptionLabel.TextColor = UIColor.White;;
			ButtomBar.AddSubview (subscriptionLabel);

			editLabel = new UILabel (new CGRect(frame.Width / 1.5, frame.Height / 50, frame.Width / 3, frame.Height / 11));

			editLabel.TextAlignment = UITextAlignment.Center;
			editLabel.Font = UIFont.FromName("Roboto-Regular", 10f);
			editLabel.BackgroundColor = UIColor.Clear;
			editLabel.TextColor = UIColor.White;

			if (GlobalData.IsLoggedIn) {
				
				editLabel.Text = NSBundle.MainBundle.LocalizedString ("EditAccountText", "EditAccountText");

			} else {
				editLabel.Text = NSBundle.MainBundle.LocalizedString ("CreateAccountText", "CreateAccountText");
			}



			ButtomBar.AddSubview (editLabel);

			UIImageView image = new UIImageView (new CGRect ((frame.Width / 15) * 2, 9, frame.Width / 15, frame.Width / 15 ));
			if (GlobalData.IsLoggedIn) {
				image.Image = UIImage.FromBundle("Images/LogOut_128x128.png");
			} else {
				image.Image = UIImage.FromBundle("Images/Login_128x128.png");
			}
			logInOutTap = new UITapGestureRecognizer (() => 
			{
				HandleLogInOut();
			});
			image.UserInteractionEnabled = true;
			#if DEBUG
			image.AddGestureRecognizer (logInOutTap);
			ButtomBar.AddSubview (image);
			#else
			if (!GlobalData.IsLoggedIn) 
			{
			   image.AddGestureRecognizer (logInOutTap);
			   ButtomBar.AddSubview (image);
			}

			#endif

			image = new UIImageView (new CGRect ((frame.Width / 15) * 7, 6, frame.Width / 12, frame.Width / 12));
			if (GlobalData.IsLoggedIn) {
				image.Image  = UIImage.FromBundle("Images/MySub_128x128.png");
			} else {
				image.Image  = UIImage.FromBundle("Images/AddSub_128x128.png");
			}
			subScriptionTap = new UITapGestureRecognizer (() => 
			{
				if (GlobalData.IsLoggedIn)
				{
					HandleSubscription();
				}
				else
				{
					UIAlertView alert = new UIAlertView (NSBundle.MainBundle.LocalizedString ("LoginReqText", "LoginReqText"), "", null, //#New string
					NSBundle.MainBundle.LocalizedString ("Ok", "Ok")); 
					alert.Show ();
				}
			});
			image.UserInteractionEnabled = true;
			image.AddGestureRecognizer (subScriptionTap);
			ButtomBar.AddSubview (image);

			image = new UIImageView (new CGRect ((frame.Width / 15) * 12, 3, frame.Width / 10, 	frame.Width / 10));
			image.Image  = UIImage.FromBundle("Images/EditWhite.png");
			editTap = new UITapGestureRecognizer (() => 
				{
					HandleEdit();
				});
			image.UserInteractionEnabled = true;
			image.AddGestureRecognizer (editTap);
			ButtomBar.AddSubview (image);

			Add (ButtomBar);
			IsButtomBarVisible = false;
		}

		void HandleLogInOut()
		{
			if (GlobalData.IsLoggedIn) {
				if (LogOutEvent != null) {
					ViewModel.LogOut ();
					LogOutEvent (this, EventArgs.Empty);
				}
			} 
			else 
			{
				if (LogInEvent != null) {
					LogInEvent (this, true);
				}
			}
		}

		void HandleSubscription ()
		{
			if (!string.IsNullOrEmpty(ViewModel.Item.SubscriptionLink))
			{		
				try 
				{
					AddBckButton();
					webView = new UIWebView(new CGRect (0, Frame.Height / 10, Frame.Width, Frame.Height - Frame.Height / 10));
					this.AddSubview (webView);
					webView.ScalesPageToFit = true;

					webView.LoadRequest(new NSUrlRequest(new NSUrl(ViewModel.Item.SubscriptionLink)));

					var ctrl = viewcontroller as SavingsViewController;
					ctrl.StartSpinner ();

					webView.LoadFinished+= (object sender, EventArgs e) => 
					{
						ctrl.StopSpinner ();
					};

					webView.LoadError+= (object sender, UIWebErrorArgs e) => 
					{
						ctrl.StopSpinner ();

						UIAlertView alert = new UIAlertView (NSBundle.MainBundle.LocalizedString ("InvalidLinkText", "InvalidLinkText"), "", null, 
							NSBundle.MainBundle.LocalizedString ("Ok", "Ok")); 
						alert.Show ();

						alert.Clicked += (snd, buttonArgs) => {
							if (buttonArgs.ButtonIndex == 0) 
							{ // OK Clicked
								DropWebView();
							}
						};
					};
				}
				catch 
				{

				}
			}
			else
			{
				UIAlertView alert = new UIAlertView (NSBundle.MainBundle.LocalizedString ("NoSubscriptionLinkText", "NoSubscriptionLinkText"), "", null, //#New string
					NSBundle.MainBundle.LocalizedString ("Ok", "Ok")); 
				alert.Show ();	
			}
		}

		void HandleEdit()
		{
			if (SubScriptionView != null) {
				if ((editPhoneView != null) || 
					(editEmailView != null) || 
					(editSliderView != null) || 
					(editProfilePictureView != null) || 
					(editNameView != null))
				{
					editLabel.Text = NSBundle.MainBundle.LocalizedString ("DoneText", "DoneText");
					editLabel.BackgroundColor = UIColor.Black;
					if (userChanged) {
						editLabel.Text = NSBundle.MainBundle.LocalizedString ("SaveText", "SaveText");
						isSaving = true;
						UpdateFields (false);
					} else {
						AddEditIcons (false);
					}
					AnimateSubView();
				} else {
					if (isSaving) {
						AddEditIcons (false);
						BlurView (true);
						ViewModel.UpdateUser (GlobalData.UserItem);
					} else {
						if (!SubScriptionView.UserInteractionEnabled) {
							SubScriptionView.UserInteractionEnabled = true;
							editLabel.Text = NSBundle.MainBundle.LocalizedString ("DoneText", "DoneText");
							editLabel.BackgroundColor = UIColor.Black;
							AddEditIcons (true);
						} else {
							SubScriptionView.UserInteractionEnabled = false;
							editLabel.Text = NSBundle.MainBundle.LocalizedString ("EditAccountText", "EditAccountText");
							editLabel.BackgroundColor = UIColor.Clear;
							AddEditIcons (false);
						}
					}
				}
			}
		}

		private void SetMySubscriptionView(bool Show)
		{
			if (Show) 
			{
				if (SubScriptionView == null) 
				{
					SubScriptionView = new UIImageView (new CGRect (0, (this.viewcontroller.View.Frame.Height /
						2) + 25, this.viewcontroller.View.Frame.Width, (this.viewcontroller.View.Frame.Height / 2) - 25 - this.viewcontroller.View.Frame.Height / 11));
					SubScriptionView.Image = GlobalData.WhiteBackGround;

					// Phone
					var tmpFrame = new CGRect (this.viewcontroller.View.Frame.Width * 0.1f, this.viewcontroller.View.Frame.Height / 15, 
						               this.viewcontroller.View.Frame.Width / 2.5f, this.viewcontroller.View.Frame.Height / 12);
					UILabel name = new UILabel (tmpFrame);
					name.TextAlignment = UITextAlignment.Left;
					name.Font = UIFont.FromName ("Roboto-Regular", 12f);
					name.Text = NSBundle.MainBundle.LocalizedString ("PhoneText", "PhoneText");
					name.TextColor = UIColor.Gray;
					SubScriptionView.AddSubview (name);

					tmpFrame = new CGRect (this.viewcontroller.View.Frame.Width * 0.1f, this.viewcontroller.View.Frame.Height / 9, 
						this.viewcontroller.View.Frame.Width / 2.5f, this.viewcontroller.View.Frame.Height / 12);
					phoneLabel = new UILabel (tmpFrame);
					phoneLabel.TextAlignment = UITextAlignment.Left;
					phoneLabel.Font = UIFont.FromName ("Roboto-Regular", 12f);
					if (GlobalData.IsLoggedIn) {
						phoneLabel.Text = GlobalData.UserItem.Prefix + GlobalData.UserItem.Phone;
					} else {
						phoneLabel.Text = NSBundle.MainBundle.LocalizedString ("NotFilledText", "NotFilledText");
					}
					phoneLabel.TextColor = UIColor.Black;
					SubScriptionView.AddSubview (phoneLabel);

					// Email
					tmpFrame = new CGRect (this.viewcontroller.View.Frame.Width / 1.9f, this.viewcontroller.View.Frame.Height / 15, 
						this.viewcontroller.View.Frame.Width / 2.5f, this.viewcontroller.View.Frame.Height / 12);
					name = new UILabel (tmpFrame);
					name.TextAlignment = UITextAlignment.Left;
					name.Font = UIFont.FromName ("Roboto-Regular", 12f);
					name.Text = NSBundle.MainBundle.LocalizedString ("EmailText", "EmailText");
					name.TextColor = UIColor.Gray;
					SubScriptionView.AddSubview (name);

					tmpFrame = new CGRect (this.viewcontroller.View.Frame.Width / 1.9f, this.viewcontroller.View.Frame.Height / 9, 
						this.viewcontroller.View.Frame.Width / 2.5f, this.viewcontroller.View.Frame.Height / 12);
					emailLabel = new UILabel (tmpFrame);
					emailLabel.TextAlignment = UITextAlignment.Left;
					emailLabel.Font = UIFont.FromName ("Roboto-Regular", 12f);
					if (GlobalData.IsLoggedIn) {
						emailLabel.Text = GlobalData.UserItem.Email;
					} else {
						emailLabel.Text = NSBundle.MainBundle.LocalizedString ("NotFilledText", "NotFilledText");
					}
					emailLabel.TextColor = UIColor.Black;
					SubScriptionView.AddSubview (emailLabel);

					// Slider
				
					int mainSliderMax = GlobalData.SLIDERMAX;
					int mainSliderMin = GlobalData.SLIDERMIN;
					string mainSlider;
		
				
					mainSlider = mainSliderMax.ToString();

					if (GlobalData.IsLoggedIn) {
						if (GlobalData.UserItem.NotificationDistance > mainSliderMax) {
							mainSlider= mainSliderMax.ToString();
						} else {
							mainSlider = GlobalData.UserItem.NotificationDistance.ToString ();
						}
					} else {
						mainSlider = mainSliderMin.ToString ();
					}
						
			
					tmpFrame = new CGRect (this.viewcontroller.View.Frame.Width * 0.1f, this.viewcontroller.View.Frame.Height / 5, 
						this.viewcontroller.View.Frame.Width / 0.1f, this.viewcontroller.View.Frame.Height / 12);
					SliderName = new UILabel (tmpFrame);
					SliderName.TextAlignment = UITextAlignment.Left;
					SliderName.Font = UIFont.FromName ("Roboto-Regular", 12f);

					SliderName.Text = NSBundle.MainBundle.LocalizedString ("RadiusNotificationText", "RadiusNotificationText") + " : " + mainSlider + "M";
					SliderName.TextColor = UIColor.Gray;
					SubScriptionView.AddSubview (SliderName);

					Add (SubScriptionView);
				}
			}
			else 
			{
				if (SubScriptionView != null) 
				{
					SubScriptionView.RemoveFromSuperview ();
					SubScriptionView.Dispose ();
					SubScriptionView = null;
				}
			}
		}

		private void AddBckButton() 
		{
			// Disable main wheel
			SavingsViewController view = viewcontroller as SavingsViewController;
			view.WheelUserInteractionEnabled (false);

			if (_bckButton == null) {
				_bckButton = new UIImageView (new CGRect (5, 5, Frame.Height / 11, Frame.Height / 11));
				_bckButton.Image = UIImage.FromBundle ("Images/back.png");
				_bckButton.UserInteractionEnabled = true;

				UITapGestureRecognizer bckTap = new UITapGestureRecognizer((g) => 
					{
						DropWebView();
					});
				_bckButton.AddGestureRecognizer (bckTap);
			}
			Add (_bckButton);
		}

		private void DropWebView()
		{
			if (webView != null)
			{
				webView.RemoveFromSuperview();
				webView.Dispose();
				webView = null;

				_bckButton.RemoveFromSuperview();
				_bckButton.Dispose();
				_bckButton = null;

				// Disable main wheel
				SavingsViewController viewTmp  = viewcontroller as SavingsViewController;
				viewTmp.WheelUserInteractionEnabled (true);
			}
		}

		private void AddEditIcons(bool show)
		{
			if (show) 
			{
				CGRect tmpFrame;

				Random rnd = new Random (DateTime.UtcNow.Millisecond);

				// Phone
				if (editPhone == null) {
					tmpFrame = new CGRect (this.viewcontroller.View.Frame.Width * 0.35f, Frame.Height / 13, Frame.Height / 16, Frame.Height / 16);
					editPhone = new UIImageView (tmpFrame);
					editPhone.UserInteractionEnabled = true;
					editPhone.Image = UIImage.FromFile ("Images/EditDark.png");
					editPhoneTap = new UITapGestureRecognizer (() =>  
						{
							HandleEditPhoneTap ();
						});
					editPhone.AddGestureRecognizer (editPhoneTap);
							
				}
				editPhone.Layer.Opacity = 0;
				SubScriptionView.AddSubview (editPhone);

				float time = rnd.Next (10, 50);
				double dTime = time / 100;

				UIView.AnimateAsync (dTime, () => {
					editPhone.Layer.Opacity = 1.0f;
				});

				// Email
				if (editEmail == null) {
					tmpFrame = new CGRect (this.viewcontroller.View.Frame.Width * 0.83, Frame.Height / 13, Frame.Height / 16, Frame.Height / 16);
					editEmail = new UIImageView (tmpFrame);
					editEmail.Image = UIImage.FromFile ("Images/EditDark.png");
					editEmail.UserInteractionEnabled = true;
					editEmailTap = new UITapGestureRecognizer (() =>  
						{
							HandleEditEmailTap();
						});
					editEmail.AddGestureRecognizer (editEmailTap);
				}
				editEmail.Layer.Opacity = 0;
				SubScriptionView.AddSubview (editEmail);

				time = rnd.Next (10, 50);
				dTime = time / 100;

				UIView.AnimateAsync (dTime, () => {
					editEmail.Layer.Opacity = 1.0f;
				});

				// Slider
				if (editSlider == null) {
					tmpFrame = new CGRect (this.viewcontroller.View.Frame.Width * 0.83, Frame.Width / 2.7f, Frame.Height / 16, Frame.Height / 16);
					editSlider = new UIImageView (tmpFrame);
					editSlider.Image = UIImage.FromFile ("Images/EditDark.png");
					editSlider.UserInteractionEnabled = true;
					editSliderTap = new UITapGestureRecognizer (() =>  
						{
							HandleEditSliderTap();
						});
					editSlider.AddGestureRecognizer (editSliderTap);
				}
				editSlider.Layer.Opacity = 0;
				SubScriptionView.AddSubview (editSlider);

				time = rnd.Next (10, 50);
				dTime = time / 100;

				UIView.AnimateAsync (dTime, () => {
					editSlider.Layer.Opacity = 1.0f;
				});

				// Profile picture
				if (editProfilePicture == null) {
					tmpFrame = new CGRect (this.viewcontroller.View.Frame.Width * 0.69, Frame.Width / 5f, Frame.Height / 16, Frame.Height / 16);
					editProfilePicture = new UIImageView (tmpFrame);
					editProfilePicture.Image = UIImage.FromFile ("Images/EditWhite.png");
					editProfilePicture.UserInteractionEnabled = true;
					editProfilePictureTap = new UITapGestureRecognizer (() =>  
						{ 
							HandleEditProfilePictureTap();
						});
					editProfilePicture.AddGestureRecognizer (editProfilePictureTap);
				}
				editProfilePicture.Layer.Opacity = 0;
				Add (editProfilePicture);

				time = rnd.Next (10, 50);
				dTime = time / 100;

				UIView.AnimateAsync (dTime, () => {
					editProfilePicture.Layer.Opacity = 1.0f;
				});

				// Profile name
				if (editName == null) {
					tmpFrame = new CGRect (this.viewcontroller.View.Frame.Width * 0.83, Frame.Width / 1.6f, Frame.Height / 16, Frame.Height / 16);
					editName = new UIImageView (tmpFrame);
					editName.Image = UIImage.FromFile ("Images/EditWhite.png");
					editName.UserInteractionEnabled = true;
					editNameTap = new UITapGestureRecognizer (() => 
						{
						HandleEditNameTap();
						});
					editName.AddGestureRecognizer (editNameTap);
					//editName.SetTitleColor (UIColor.Clear, UIControlState.Normal);
					//editName.SetTitle ("profile", UIControlState.Normal);
				}
				editName.Layer.Opacity = 0;
				Add (editName);

				time = rnd.Next (10, 50);
				dTime = time / 100;

				UIView.AnimateAsync (dTime, () => {
					editName.Layer.Opacity = 1.0f;
				});
			} 
			else 
			{
				editPhone.RemoveFromSuperview ();
				editEmail.RemoveFromSuperview ();
				editSlider.RemoveFromSuperview ();
				editProfilePicture.RemoveFromSuperview ();
				editName.RemoveFromSuperview ();
			}
		}

		private void HandleEditPhoneTap()
		{
			editPhoneView = new UIImageView(new CGRect (Frame.Width, Frame.Height / 11, Frame.Width, Frame.Height - (Frame.Height / 11) * 2));
			editPhoneView.Image = GlobalData.GrayBackGround;

			var tmpFrame = new CGRect(0, Frame.Height / 9, Frame.Width, Frame.Height / 11);
			UILabel phoneHeaderLabel = new UILabel(tmpFrame);
			phoneHeaderLabel.TextAlignment = UITextAlignment.Center;

			phoneHeaderLabel.Text = NSBundle.MainBundle.LocalizedString ("EnterPhoneNumberText", "EnterPhoneNumberText");
			phoneHeaderLabel.TextColor = UIColor.White;
			phoneHeaderLabel.Font = UIFont.FromName("Roboto-Regular", 14f);
			editPhoneView.AddSubview (phoneHeaderLabel);

			tmpFrame = new CGRect(0, Frame.Height / 5, Frame.Width, Frame.Height / 15);
			phoneText = new UITextView(tmpFrame);
			SetDoneButton (true);
			phoneText.TextAlignment = UITextAlignment.Center;
			phoneText.TextColor = UIColor.White;
			phoneText.Text = GlobalData.UserItem.Phone;
			phoneText.BackgroundColor = UIColor.Gray;
			phoneText.Font = UIFont.FromName("Roboto-Regular", 16);
			phoneText.UserInteractionEnabled = true;
			phoneText.KeyboardType = UIKeyboardType.NumberPad;
			phoneText.SpellCheckingType = UITextSpellCheckingType.No;
			phoneText.DataDetectorTypes = UIDataDetectorType.PhoneNumber;
			phoneText.ReturnKeyType = UIReturnKeyType.Done;
			phoneText.ShouldChangeText = (text, range, replacementString) =>
			{
				if (replacementString.Equals("\n"))
				{
					phoneText.EndEditing(true);
					return false;
				}
				else
				{
					return true;
				}
			};
			phoneText.Changed += (object sender, EventArgs e) => {
				GlobalData.UserItem.Phone = phoneText.Text;
				HandleUserChanged();
			};

			editPhoneView.AddSubview (phoneText);

			if (!GlobalData.IsLoggedIn) {
				CountryInfo = new UITextView(new CGRect((Frame.Width / 2) - 148f, Frame.Height / 3, 296, 30f));
				CountryInfo.BackgroundColor = UIColor.White;
				CountryInfo.TextAlignment = UITextAlignment.Center;
				CountryInfo.AutocorrectionType = UITextAutocorrectionType.No;
				CountryInfo.AutoresizingMask = UIViewAutoresizing.FlexibleLeftMargin | UIViewAutoresizing.FlexibleWidth;
				CountryInfo.Font = UIFont.FromName ("Roboto-Regular", 18f);
				CountryInfo.TextColor = UIColor.DarkGray;
				CountryInfo.Layer.CornerRadius = 15;   
				CountryInfo.ContentInset = new UIEdgeInsets (-4, 0, 4, 0);
				if (ViewModel.PhoneData.Count > 0) {
					CountryInfo.Text = ViewModel.PhoneData [ViewModel.Index].Native;
				} else {
					CountryInfo.Text = "...";
				}
				CountryInfo.AddGestureRecognizer (new UITapGestureRecognizer(HandleTextGesture));
				CountryInfo.UserInteractionEnabled = true;
				editPhoneView.AddSubview (CountryInfo);

				EnterCountryCode = new MyUITextField (new CGRect ((Frame.Width / 2) - 148f, Frame.Height / 3, 48f, 30f));
				EnterCountryCode.TextColor = UIColor.DarkGray;
				EnterCountryCode.Enabled = true;
				EnterCountryCode.Font = UIFont.FromName ("Roboto-Regular", 18f);
				EnterCountryCode.AutoresizingMask = UIViewAutoresizing.FlexibleRightMargin;
				EnterCountryCode.UserInteractionEnabled = false;
				EnterCountryCode.TextAlignment = UITextAlignment.Center;
				EnterCountryCode.BackgroundColor = UIColor.White;
				EnterCountryCode.Layer.CornerRadius = 15;
				if (ViewModel.PhoneData.Count > 0) {
					EnterCountryCode.Text = ViewModel.PhoneData [ViewModel.Index].PhonePrefix;
					GlobalData.UserItem.Prefix = EnterCountryCode.Text;
				} else {
					EnterCountryCode.Text = "";
				}
				editPhoneView.AddSubview (EnterCountryCode);

				FlagImage = new UIImageView();
				FlagImage.Frame = new CoreGraphics.CGRect ((Frame.Width / 2) + 148f - 40f, Frame.Height / 3, 40f, 30f);
				FlagImage.Layer.CornerRadius = 15;
				FlagImage.Layer.MasksToBounds = true;

				int i = 0;
				flaglist = new List<UIImage>();
				foreach(SupportedPhoneDataItem item in ViewModel.PhoneData)
				{	
					var image =  Convert.FromBase64String (item.Base64Image);
					flaglist.Add (Extensions.ToImage (image));

					_countryList.Add(new DropDownListItem()
						{
							Id = (i+1).ToString(),
							DisplayText = item.Native+ " ("+item.Region+")",
							Image = flaglist[i]
						});
					i++;
				}

				if (flaglist.Count > 0) {
					FlagImage.Image = flaglist [ViewModel.Index];
				}
				editPhoneView.AddSubview (FlagImage);
			}
			editPhoneView.UserInteractionEnabled = true;
			Add (editPhoneView);
			AnimateSubView ();

			if(!string.IsNullOrEmpty(GlobalData.UserItem.Code))
			{
				AddSMSView ();
			}
		}

		private void AddSMSView()
		{
			var tmpFrame = new CGRect(0, Frame.Height / 4, Frame.Width, Frame.Height / 11);
			UILabel phoneHeaderLabel = new UILabel(tmpFrame);
			phoneHeaderLabel.TextAlignment = UITextAlignment.Center;
			phoneHeaderLabel.Text = NSBundle.MainBundle.LocalizedString ("EnterPinFromSmsText", "EnterPinFromSmsText");
			phoneHeaderLabel.TextColor = UIColor.White;
			phoneHeaderLabel.Font = UIFont.FromName("Roboto-Regular", 14f);
			editPhoneView.AddSubview (phoneHeaderLabel);

			tmpFrame = new CGRect(0, Frame.Height / 3, Frame.Width, Frame.Height / 15);
			pinText = new UITextView(tmpFrame);
			pinText.TextAlignment = UITextAlignment.Center;
			pinText.TextColor = UIColor.White;
			pinText.Text = "";
			pinText.BackgroundColor = UIColor.Gray;
			pinText.Font = UIFont.FromName("Roboto-Regular", 18f);
			pinText.UserInteractionEnabled = true;
			pinText.DataDetectorTypes = UIDataDetectorType.PhoneNumber;
			pinText.KeyboardType = UIKeyboardType.NumberPad;
			pinText.ReturnKeyType = UIReturnKeyType.Done;
			pinText.ShouldChangeText = (text, range, replacementString) =>
			{
				if (replacementString.Equals("\n"))
				{
					pinText.EndEditing(true);
					BlurView(true);
					ViewModel.Item.Code = pinText.Text;
					ViewModel.UpdateUser (ViewModel.Item);
					return false;
				}
				else
				{
					return true;
				}

			};
			pinText.Changed += (object sender, EventArgs e) => {
				
				//GlobalData.UserItem.Code = pinText.Text;
				//HandleUserChanged();
			};
			SetDoneButton (false);

			editPhoneView.UserInteractionEnabled = true;
			editPhoneView.AddSubview (pinText);
		}

		private void HandleEditEmailTap()
		{
			editEmailView = new UIImageView(new CGRect (Frame.Width, Frame.Height / 11, Frame.Width, Frame.Height - (Frame.Height / 11) * 2));
			editEmailView.Image = GlobalData.GrayBackGround;

			var tmpFrame = new CGRect(0, Frame.Height / 9, Frame.Width, Frame.Height / 11);
			UILabel emailHeaderLabel = new UILabel(tmpFrame);
			emailHeaderLabel.TextAlignment = UITextAlignment.Center;
			emailHeaderLabel.Text = NSBundle.MainBundle.LocalizedString ("EnterEmailText", "EnterEmailText"); 
			emailHeaderLabel.TextColor = UIColor.White;
			emailHeaderLabel.Font = UIFont.FromName("Roboto-Regular", 14f);
			editEmailView.AddSubview (emailHeaderLabel);

			tmpFrame = new CGRect(0, Frame.Height / 5, Frame.Width, Frame.Height / 15);
			UITextView emailText = new UITextView(tmpFrame);
			emailText.TextAlignment = UITextAlignment.Center;
			emailText.TextColor = UIColor.White;
			emailText.Text = GlobalData.UserItem.Email;
			emailText.BackgroundColor = UIColor.Gray;
			emailText.Font = UIFont.FromName("Roboto-Regular", 18f);
			emailText.UserInteractionEnabled = true;
			emailText.DataDetectorTypes = UIDataDetectorType.All;
			emailText.ReturnKeyType = UIReturnKeyType.Done;
			emailText.ShouldChangeText = (text, range, replacementString) =>
			{
				GlobalData.UserItem.Email = emailText.Text;
				HandleUserChanged();
				if (replacementString.Equals("\n"))
				{
					emailText.EndEditing(true);
					return false;
				}
				else
				{
					if (Regex.Match(emailText.Text, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$").Success)
					{
						emailText.TextColor = UIColor.White;
					}
					else
					{
						emailText.TextColor = UIColor.Red;
					}
					return true;
				}
			};
			editEmailView.UserInteractionEnabled = true;
			editEmailView.AddSubview (emailText);

			Add (editEmailView);
			AnimateSubView ();
		}

		private void HandleEditSliderTap()
		{
			editSliderView = new UIImageView(new CGRect (Frame.Width, Frame.Height / 11, Frame.Width, Frame.Height - (Frame.Height / 11) * 2));
			editSliderView.Image = GlobalData.GrayBackGround;

			var tmpFrame = new CGRect(0, Frame.Height / 9, Frame.Width, Frame.Height / 11);
			UILabel sliderHeaderLabel = new UILabel(tmpFrame);
			sliderHeaderLabel.TextAlignment = UITextAlignment.Center;
			sliderHeaderLabel.Text = NSBundle.MainBundle.LocalizedString ("SetNewRadiusText", "SetNewRadiusText"); 
			sliderHeaderLabel.TextColor = UIColor.White;
			sliderHeaderLabel.Font = UIFont.FromName("Roboto-Regular", 14f);
			editSliderView.AddSubview (sliderHeaderLabel);

			// Slider
			UISlider Slider = new UISlider(new CGRect (0, this.viewcontroller.View.Frame.Height / 5, 
				this.viewcontroller.View.Frame.Width, this.viewcontroller.View.Frame.Height / 15));
			Slider.MaxValue = GlobalData.SLIDERMAX;
			Slider.MinValue = GlobalData.SLIDERMIN;

			if (GlobalData.UserItem.NotificationDistance > (int)Slider.MaxValue) {
				Slider.Value = Slider.MaxValue;
			} else {
				Slider.Value = GlobalData.UserItem.NotificationDistance;
			}

			if ((int)Slider.MinValue < GlobalData.SLIDERMIN) {
				GlobalData.UserItem.NotificationDistance = (int)Slider.MinValue;
				Slider.Value = Slider.MinValue;
			} else {
				Slider.Value = GlobalData.UserItem.NotificationDistance;
			}


			Slider.ValueChanged += Slider_DidChangeValue;
			Slider.UserInteractionEnabled = true;
			Slider.TintColor = UIColor.White;
			Slider.BackgroundColor = UIColor.Gray;
			editSliderView.AddSubview (Slider);
			editSliderView.UserInteractionEnabled = true;

			tmpFrame = new CGRect(0, Frame.Height / 9, Frame.Width - 12, Frame.Height / 11);
			sliderValueLabel = new UILabel(tmpFrame);
			sliderValueLabel.TextAlignment = UITextAlignment.Right;
			sliderValueLabel.Text = Slider.Value.ToString() + "M";
			sliderValueLabel.Font = UIFont.FromName("Roboto-Regular", 14f);
			sliderValueLabel.TextColor = UIColor.White;
			editSliderView.AddSubview (sliderValueLabel);

			Add (editSliderView);
			AnimateSubView ();
		}

		private void HandleEditProfilePictureTap()
		{
			imagePicker = new UIImagePickerController ();
			imagePicker.SourceType = UIImagePickerControllerSourceType.PhotoLibrary;
			imagePicker.MediaTypes = UIImagePickerController.AvailableMediaTypes (UIImagePickerControllerSourceType.PhotoLibrary);
			imagePicker.FinishedPickingMedia += Handle_FinishedPickingMedia;
			imagePicker.Canceled += Handle_Canceled;
			viewcontroller.PresentModalViewController(imagePicker, true);
		}

		protected void Handle_FinishedPickingMedia (object sender, UIImagePickerMediaPickedEventArgs e)
		{
			// determine what was selected, video or image
			bool isImage = false;
			switch(e.Info[UIImagePickerController.MediaType].ToString()) {
			case "public.image":
				isImage = true;
				break;
			case "public.video":
				break;
			}

			// get common info (shared between images and video)
			NSUrl referenceURL = e.Info[new NSString("UIImagePickerControllerReferenceUrl")] as NSUrl;
	
		
			if (referenceURL != null)
				Console.WriteLine("Url:"+referenceURL.ToString ());
			
			// if it was an image, get the other image info
			if(isImage) {
				// get the original image
				UIImage originalImage = e.Info[UIImagePickerController.OriginalImage] as UIImage;





				if(originalImage != null) {

					var height = (originalImage.Size.Height > originalImage.Size.Width) ? originalImage.Size.Width : originalImage.Size.Height; 
					var width = height;

					var divider = (int)(width / 1200);


					byte[] myByteArray;
					using (NSData Imagedata =  (originalImage.Scale (new CGSize(width/divider, height/divider)).AsJPEG(0.25f)))
					{
						myByteArray = new byte[Imagedata.Length];
						System.Runtime.InteropServices.Marshal.Copy
						(Imagedata.Bytes, myByteArray, 0, Convert.ToInt32(Imagedata.Length));
					}

					var content = Convert.ToBase64String(myByteArray);


					// do something with the image
					profileImage.Image = originalImage; // display
					profileImage.Layer.CornerRadius = ((float)Frame.Height / 4.6f) / 2;
					profileImage.Layer.MasksToBounds = true;

					profileImageGhost.Layer.Opacity = 0.0f;

					GlobalData.UserItem.Picture.Uri = content;//originalImage.AsJPEG (0.23f).GetBase64EncodedString (NSDataBase64EncodingOptions.None);
					HandleUserChanged();
					isSaving = true;
					editLabel.Text = NSBundle.MainBundle.LocalizedString ("SaveText", "SaveText"); 
				}
			} else { 
				UIAlertView alert = new UIAlertView (NSBundle.MainBundle.LocalizedString ("OnlyPictureAllowedText", "OnlyPictureAllowedText") , "", null, NSBundle.MainBundle.LocalizedString ("OK", "OK"), null);
				alert.Clicked += (sender1, buttonArgs) => 
				{

				};
				alert.Show ();
			}
			// dismiss the picker
			imagePicker.DismissViewControllerAsync (true);
		}

		void Handle_Canceled (object sender, EventArgs e) {
			imagePicker.DismissViewControllerAsync(true);
		}

		private void HandleEditNameTap()
		{
			editNameView = new UIImageView(new CGRect (Frame.Width, Frame.Height / 11, Frame.Width, Frame.Height - (Frame.Height / 11) * 2));
			editNameView.Image = GlobalData.GrayBackGround;

			var tmpFrame = new CGRect(0, Frame.Height / 9, Frame.Width, Frame.Height / 11);
			UILabel nameHeaderLabel = new UILabel(tmpFrame);
			nameHeaderLabel.TextAlignment = UITextAlignment.Center;
			nameHeaderLabel.Text = NSBundle.MainBundle.LocalizedString ("EnterNewNameText", "EnterNewNameText");
			nameHeaderLabel.TextColor = UIColor.White;
			nameHeaderLabel.Font = UIFont.FromName("Roboto-Regular", 14f);
			editNameView.AddSubview (nameHeaderLabel);

			tmpFrame = new CGRect(0, Frame.Height / 5, Frame.Width, Frame.Height / 15);
			UITextView nameText = new UITextView(tmpFrame);
			nameText.TextAlignment = UITextAlignment.Center;
			nameText.TextColor = UIColor.White;
			nameText.Text = GlobalData.UserItem.Name;
			nameText.BackgroundColor = UIColor.Gray;
			nameText.Font = UIFont.FromName("Roboto-Regular", 18f);
			nameText.UserInteractionEnabled = true;
			nameText.DataDetectorTypes = UIDataDetectorType.All;
			nameText.ReturnKeyType = UIReturnKeyType.Done;
			nameText.ShouldChangeText = (text, range, replacementString) =>
			{
				GlobalData.UserItem.Name = nameText.Text;
				HandleUserChanged();
				if (replacementString.Equals("\n"))
				{
					nameText.EndEditing(true);
					return false;
				}
				else
				{
					return true;
				}
			};
			editNameView.UserInteractionEnabled = true;
			editNameView.AddSubview (nameText);
			Add (editNameView);

			tmpFrame = new CGRect(0, Frame.Height / 3, 2*Frame.Width/3, Frame.Height / 11);
			UILabel areaHeaderLabel = new UILabel(tmpFrame);
			areaHeaderLabel.TextAlignment = UITextAlignment.Right;
			areaHeaderLabel.Text = NSBundle.MainBundle.LocalizedString ("UpdateAreaText", "UpdateAreaText");
			areaHeaderLabel.TextColor = UIColor.White;
			areaHeaderLabel.Font = UIFont.FromName("Roboto-Regular", 14f);
			editNameView.AddSubview (areaHeaderLabel);

			tmpFrame = new CGRect(2*Frame.Width/3+20, Frame.Height / 3+ Frame.Height / 44, Frame.Height / 22, Frame.Height / 22);
			UIImageView refreshBtn = new UIImageView (tmpFrame);
			refreshBtn.Image = UIImage.FromBundle ("Images/refresh_128x128.png");
			refreshBtn.UserInteractionEnabled = true;
			refreshTap = new UITapGestureRecognizer ((g) => 
			{
					CABasicAnimation rotate = new CABasicAnimation();
					rotate.KeyPath = "transform.rotation.z";
					rotate.To = new NSNumber(Math.PI*2);

					rotate.Duration = 1;
					rotate.Cumulative = true;
					rotate.RepeatCount=5;

					refreshBtn.Layer.AddAnimation(rotate, "rotate");
			//		refreshBtn.Layer.RemoveAnimation("rotate");

			});
			refreshBtn.AddGestureRecognizer (refreshTap);


			editNameView.AddSubview (refreshBtn);



			
		

			tmpFrame = new CGRect(0, Frame.Height / 2.4, Frame.Width, Frame.Height / 15);
			UITextView bioText = new UITextView(tmpFrame);
			bioText.TextAlignment = UITextAlignment.Center;
			bioText.TextColor = UIColor.White;
			bioText.Text = GlobalData.UserItem.Bio;
			bioText.BackgroundColor = UIColor.Gray;
			bioText.Font = UIFont.FromName("Roboto-Regular", 18f);
			bioText.UserInteractionEnabled = true;
			bioText.DataDetectorTypes = UIDataDetectorType.All;
			bioText.ReturnKeyType = UIReturnKeyType.Done;
			bioText.ShouldChangeText = (text, range, replacementString) =>
			{
				GlobalData.UserItem.Bio = bioText.Text;
				HandleUserChanged();
				if (replacementString.Equals("\n"))
				{
					bioText.EndEditing(true);
					return false;
				}
				else
				{
					return true;
				}
			};
			editNameView.UserInteractionEnabled = true;
			editNameView.AddSubview (bioText);

			Add (editNameView);
			AnimateSubView ();
		}

		private void HandleUserChanged()
		{
			if (EditedUserItem != GlobalData.UserItem)
			{
				userChanged = true;
			}
			else
			{
				userChanged = false;
				isSaving = false;
			}
		}

		void Slider_DidChangeValue (object sender, EventArgs e)
		{
			var slider = sender as UISlider;
			if (slider != null)
			{
				// Get the closet "step" 
				double nextStep = Math.Round(slider.Value / 300);

				// Convert that step to a value used by the slider
				slider.Value = (int)(nextStep * 300);
				int dist = (int)(nextStep * 300);

				if (slider.Value <= GlobalData.SLIDERMIN)
				{	
					slider.Value = GlobalData.SLIDERMIN;
					dist = (int)GlobalData.SLIDERMIN;
				}

				sliderValueLabel.Text = dist + "M";
				SliderName.Text = "RADIUS-NOTIFICATIONS" + " : " + dist + "M";

				GlobalData.UserItem.NotificationDistance = dist;
				HandleUserChanged ();
			}
		}

		private void AnimateButtomBar()
		{
			CGRect startframe = ButtomBar.Frame;
			nfloat height = IsButtomBarVisible == true ? viewcontroller.View.Frame.Height : viewcontroller.View.Frame.Height - viewcontroller.View.Frame.Height / 11;  
			CGRect endframe = new CGRect (0, height, viewcontroller.View.Frame.Width, viewcontroller.View.Frame.Width);

			if (IsButtomBarVisible) {
				System.Threading.ThreadPool.QueueUserWorkItem (state => InvokeOnMainThread (() => 
				UIView.AnimateNotify (0.2, 0, UIViewAnimationOptions.TransitionCurlDown, () => {
					ButtomBar.Frame = endframe;
				}, finished => {
					IsButtomBarVisible = !IsButtomBarVisible;
				})));
			} 
			else
			{
				System.Threading.ThreadPool.QueueUserWorkItem (state => InvokeOnMainThread (() => 
					UIView.AnimateNotify (0.2, 0, UIViewAnimationOptions.TransitionCurlDown, () => {
						ButtomBar.Frame = endframe;
					}, finished => {
						IsButtomBarVisible = !IsButtomBarVisible;
					})));
			}
		}

		private void AnimateSubView()
		{
			UIImageView frameToAnimate = null;

			if (editPhoneView != null) {
				frameToAnimate = editPhoneView;;
			}
			else if (editEmailView != null) {
				frameToAnimate = editEmailView;
			}
			else if (editSliderView != null) {
				frameToAnimate = editSliderView;
			}
			else if (editProfilePictureView != null) {
				frameToAnimate = editProfilePictureView;
			}
			else if (editNameView != null) {
				frameToAnimate = editNameView;
			}

			if (frameToAnimate != null) {

				nfloat width = Visible == true ? Frame.Width : 0;  
				CGRect endframe = new CGRect (width, Frame.Height / 11, Frame.Width, Frame.Height - 2 * (Frame.Height / 11));

				if (Visible) {
					System.Threading.ThreadPool.QueueUserWorkItem (state => InvokeOnMainThread (() => 
						UIView.AnimateNotify (0.4, 0, 15.1f, 3f, UIViewAnimationOptions.TransitionCurlDown, () => {
							frameToAnimate.Frame = endframe;

					}, finished => {
						if (!userChanged)
						{
							SubScriptionView.UserInteractionEnabled = false;
						    editLabel.Text = NSBundle.MainBundle.LocalizedString ("EditAccountText", "EditAccountText");
							editLabel.BackgroundColor = UIColor.Clear;
						}
						Visible = !Visible;
						DropSubView ();
					})));
				} else {
					System.Threading.ThreadPool.QueueUserWorkItem (state => InvokeOnMainThread (() => 
					UIView.AnimateNotify (0.4, 0, 15.1f, 3f, UIViewAnimationOptions.TransitionCurlUp, () => {
							frameToAnimate.Frame = endframe;

					}, finished => {
						editLabel.Text = NSBundle.MainBundle.LocalizedString ("DoneText", "DoneText");
						Visible = !Visible;
					})));
				}
			}
		}

		private async void BlurView(bool isVisible)
		{
			if (isVisible) {
				if (blurOverlay == null) 
				{
					blurOverlay = new UIImageView (new CGRect (0, Frame.Height / 11, Frame.Width, Frame.Height - Frame.Height / 11));
					blurOverlay.Image = GlobalData.GrayBackGround;

					UILabel label = new UILabel(new CGRect (0, Frame.Height / 3, Frame.Width, Frame.Height / 11));
					label.Text = NSBundle.MainBundle.LocalizedString ("SavingText", "SavingText");
					label.TextColor = UIColor.White;
					label.TextAlignment = UITextAlignment.Center;
					label.Font = UIFont.FromName("Roboto-Regular", 18f);
					blurOverlay.AddSubview (label);

					UIActivityIndicatorView Spinner = new UIActivityIndicatorView (new CGRect (0, Frame.Height / 3 + 30, Frame.Width, Frame.Height / 11));
					Spinner.StartAnimating ();
					blurOverlay.Add (Spinner);

					Add (blurOverlay);
				}
				blurOverlay.Layer.Opacity = 0;

				await UIView.AnimateAsync (0.2f, () => {
					blurOverlay.Layer.Opacity = 0.7f;
				});
			} 
			else 
			{
				if (blurOverlay != null) 
				{
					blurOverlay.Layer.Opacity = 0.7f;
					await UIView.AnimateAsync (0.2f, () => {
						blurOverlay.Layer.Opacity = 0f;
					});

					blurOverlay.RemoveFromSuperview ();
					blurOverlay.Dispose ();
					blurOverlay = null;
				}
			}
		}

		void HandleTextGesture (UITapGestureRecognizer sender)
		{
			if (_countryList != null && _countryList.Count > 0) {
				UIView view = new UIView (new CGRect (0, 0, Frame.Width - 30f, Frame.Height));
				view.BackgroundColor = UIColor.FromRGBA (0x00, 0x00, 0x00, 0xAA);

				nfloat padding = 10.0f;

				UIScrollView ImageBackgroundView = new UIScrollView (new CGRect (padding, padding, Frame.Width - (2 * padding), (Frame.Height - (2 * padding))));
				ImageBackgroundView.BackgroundColor = UIColor.Clear;
				ImageBackgroundView.Layer.CornerRadius = 5f;

				ImageBackgroundView.ContentSize = new CGSize (Frame.Width - 30f, ViewModel.PhoneData.Count * 40.0f); 

				ddl = new DropDownList (ImageBackgroundView, _countryList.ToArray ()) {
					BackgroundColor = UIColor.FromCGColor (new CGColor (255f / 255f, 63f / 255f, 63f / 255f, 255f / 255f)), //FromRGB(255, 255, 255),
					Opacity = 0.85f,
					TintColor = UIColor.Black,
					NavigationBarAssumedHeight = 1
				};

				ddl.DropDownListChanged += (e, a) => {
					_index = e; // e is the index selected
					var strValue = a.DisplayText; //a is the dropdown list item object

					int id;
					if (!int.TryParse (a.Id, out id)) {	
						if (id < 1) {	
							id = 1;
						}
					}
					id = id - 1;

					CountryInfo.Text = ViewModel.PhoneData [id].Native + "(" + ViewModel.PhoneData [id].Region + ")";
					FlagImage.Image = flaglist [id];
					EnterCountryCode.Text = ViewModel.PhoneData [id].PhonePrefix;
					GlobalData.UserItem.Region = ViewModel.PhoneData [id].Region;
					ViewModel.Index = id;

					ddl.Toggle ();

					ImageBackgroundView.RemoveFromSuperview ();
				};

				ddl.ClipsToBounds = false;
				Console.WriteLine ("size = " + ddl.Bounds);

				ImageBackgroundView.Add (ddl);
				ImageBackgroundView.BringSubviewToFront (ddl);
				view.Add (ImageBackgroundView);

				Add (ImageBackgroundView);

				ddl.Toggle ();
			}
		}

		private void SetDoneButton(bool isPhone)
		{
			UIToolbar acc_toolbar = new UIToolbar (new CGRect(0.0f, 0.0f, Frame.Size.Width, 44.0f));
			acc_toolbar.TintColor = UIColor.White;
			acc_toolbar.BarStyle = UIBarStyle.Black;
			acc_toolbar.Translucent = true;

			UIBarButtonItem myDoneButton = new UIBarButtonItem(UIBarButtonSystemItem.Done, delegate
			{
				if (isPhone)
				{
					if (phoneText.CanResignFirstResponder)
					{
						phoneText.ResignFirstResponder();
					}
				}
				else
				{
					if (pinText.CanResignFirstResponder)
					{
						pinText.ResignFirstResponder();
					}

					pinText.EndEditing(true);
					BlurView(true);
					ViewModel.Item.Code = pinText.Text;
					ViewModel.UpdateUser (ViewModel.Item);
				}
			});

			myDoneButton.TintColor = UIColor.White;
			acc_toolbar.Items = new UIBarButtonItem[]{
				new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace),
				myDoneButton
			};

			if (isPhone) {
				this.phoneText.InputAccessoryView = acc_toolbar;
			} else {
				this.pinText.InputAccessoryView = acc_toolbar;
			}
		}
	}
}

