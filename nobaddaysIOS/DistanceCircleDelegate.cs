﻿using System;
using CDCircleLib;
using Utils;

namespace nobaddaysIOS
{

	public class DistanceCircleDelegate: CDCircleDelegate
	{
		
		public event EventHandler<int> WheelChanged;


		void RaiseWheelChanged(int index)
		{
			if (WheelChanged != null)
			{
				WheelChanged(this, index);
			}
		}

		public override void DidMoveToSegment (CDCircleLib.CDCircle circle, nint segment, CDCircleThumb thumb)
		{
			GlobalData.WheelSelection = (int)segment;
			RaiseWheelChanged ((int)segment);
		}
	}
}

