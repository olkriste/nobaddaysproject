﻿using System;
using CoreGraphics;
using UIKit;
using System.Timers;
using Foundation;
using System.Collections.Generic;
using NoBadDaysPCL;
using System.IO;
using SQLite.Net.Platform.XamarinIOS;
using Valore.IOSDropDown;
using Security;
using Utils;
using NoBadDaysPCL.Items;
using NoBadDaysPCL.ViewModel;
using System.Threading.Tasks;
using System.Net;
using System.Drawing;

namespace nobaddaysIOS
{
	public class AboutView: UIView
	{
		UIViewController viewcontroller;

		UIImageView _bckButton;
		UIWebView webView;

		UILabel header;
	
		public AboutView(object controller)
		{
			viewcontroller = controller as UIViewController;
			var frame = viewcontroller.View.Bounds;
			Frame = new CGRect(0, 0, frame.Width, frame.Height);

			SetHeaderText (frame);
			SetPictureView (frame);
			SetOptionsView (frame);
		}

		private void SetHeaderText(CGRect frame)
		{
			var tmpFrame = new CGRect(0, 0, frame.Width, frame.Height / 10);
			header = new UILabel(tmpFrame);
			header.TextAlignment = UITextAlignment.Center;
			header.AttributedText = new NSAttributedString(NSBundle.MainBundle.LocalizedString("InfoText", "InfoText"), GlobalData.HeaderAttrib);
			Add (header);
		}

		private void SetPictureView(CGRect frame)
		{
			UIImageView bckImage = new UIImageView(new CGRect (frame.Width / 2 - (frame.Width / 4) / 2, frame.Height / 8, frame.Width / 4, frame.Width / 4));
			bckImage.Image = GlobalData.BlackBackGround;

			bckImage.Layer.CornerRadius = (frame.Width / 4) / 2;
			Add (bckImage);

			bckImage = new UIImageView(new CGRect (frame.Width / 2 - (frame.Width / 3.5) / 2, frame.Height / 9, frame.Width / 3.5, frame.Width / 3.5));
			bckImage.Image = UIImage.FromBundle ("Images/LogoMark.png");
			Add (bckImage);

			var tmpFrame = new CGRect(frame.Width / 10, frame.Height / 7, frame.Width / 1.2, frame.Height / 2);
			UILabel firstText = new UILabel(tmpFrame);
			firstText.TextAlignment = UITextAlignment.Center;
			firstText.LineBreakMode = UILineBreakMode.WordWrap;
			firstText.Lines = 0;
			firstText.Font = UIFont.FromName("Roboto-Regular", 16f);
			firstText.AttributedText = 
				new NSAttributedString(NSBundle.MainBundle.LocalizedString("IsAllAboutText", "IsAllAboutText"), 
					GlobalData.CountryAttrib);
			Add (firstText);

			//   NDB wants to help you and make your life easier
		}

		private void SetOptionsView(CGRect frame)
		{
			UIImageView bckImage = new UIImageView(new CGRect (0, frame.Height / 2, frame.Width, frame.Height / 2));
			bckImage.Image = GlobalData.GrayBackGround;
			Add (bckImage);

			var tmpFrame = new CGRect(0, frame.Height / 2, frame.Width, frame.Height / 8);
			UILabel text = new UILabel(tmpFrame);
			text.TextAlignment = UITextAlignment.Center;
			text.AttributedText = new NSAttributedString(NSBundle.MainBundle.LocalizedString("OurStoryText", "OurStoryText"), GlobalData.CountryAttrib);
			Add (text);

			tmpFrame = new CGRect(0, (frame.Height / 2) + (frame.Height / 8), frame.Width, frame.Height / 8);
			text = new UILabel(tmpFrame);
			text.TextAlignment = UITextAlignment.Center;
			text.AttributedText = new NSAttributedString(NSBundle.MainBundle.LocalizedString("ContactInfoText", "ContactInfoText"), GlobalData.CountryAttrib);
			Add (text);

			tmpFrame = new CGRect(0, (frame.Height / 2) + ((frame.Height / 8) * 2), frame.Width, frame.Height / 8);
			text = new UILabel(tmpFrame);
			text.TextAlignment = UITextAlignment.Center;
			text.AttributedText = new NSAttributedString(NSBundle.MainBundle.LocalizedString("PrivacyText", "PrivacyText"), GlobalData.CountryAttrib);
			Add (text);

			tmpFrame = new CGRect(0, (frame.Height / 2) + ((frame.Height / 8) * 3), frame.Width, frame.Height / 8);
			text = new UILabel(tmpFrame);
			text.TextAlignment = UITextAlignment.Center;
			text.AttributedText = new NSAttributedString(NSBundle.MainBundle.LocalizedString("TermsText", "TermsText"), GlobalData.CountryAttrib);
			Add (text);

			UIButton btnFirst = new UIButton (new CGRect(frame.Width - 100, frame.Height / 2, 100, frame.Height / 8));
			btnFirst.Hidden = false;
			btnFirst.TintColor = UIColor.White;
			btnFirst.SetTitle("+", UIControlState.Normal);
			btnFirst.Font = UIFont.FromName ("Roboto-Regular", 18f);
			btnFirst.BackgroundColor = UIColor.Clear;
			btnFirst.SetTitleColor (UIColor.White, UIControlState.Normal);
			btnFirst.Layer.BorderColor = UIColor.Clear.CGColor;
			btnFirst.TouchUpInside+= (object sender, EventArgs e) => 
			{
		
				header.AttributedText = new NSAttributedString(NSBundle.MainBundle.LocalizedString("OurStoryText", "OurStoryText"), GlobalData.HeaderAttrib);

				LoadPage(NSBundle.MainBundle.LocalizedString("OurStoryLink", "OurStoryLink"));
			};
			Add (btnFirst);

			UIButton btnSecond = new UIButton (new CGRect(frame.Width - 100, (frame.Height / 2) + (frame.Height / 8), 100, frame.Height / 8));
			btnSecond.Hidden = false;
			btnSecond.TintColor = UIColor.White;
			btnSecond.SetTitle("+", UIControlState.Normal);
			btnSecond.Font = UIFont.FromName ("Roboto-Regular", 18f);
			btnSecond.BackgroundColor = UIColor.Clear;
			btnSecond.SetTitleColor (UIColor.White, UIControlState.Normal);
			btnSecond.Layer.BorderColor = UIColor.Clear.CGColor;
			btnSecond.TouchUpInside+= (object sender, EventArgs e) => 
			{
				header.AttributedText = new NSAttributedString(NSBundle.MainBundle.LocalizedString("ContactInfoText", "ContactInfoText"), GlobalData.HeaderAttrib);

				LoadPage(NSBundle.MainBundle.LocalizedString("ContactInfoLink", "ContactInfoLink"));
			};
			Add (btnSecond);

			UIButton btnThird = new UIButton (new CGRect(frame.Width - 100, (frame.Height / 2) + ((frame.Height / 8) * 2), 100, frame.Height / 8));
			btnThird.Hidden = false;
			btnThird.TintColor = UIColor.White;
			btnThird.SetTitle("+", UIControlState.Normal);
			btnThird.Font = UIFont.FromName ("Roboto-Regular", 18f);
			btnThird.BackgroundColor = UIColor.Clear;
			btnThird.SetTitleColor (UIColor.White, UIControlState.Normal);
			btnThird.Layer.BorderColor = UIColor.Clear.CGColor;
			btnThird.TouchUpInside += (object sender, EventArgs e) => 
			{
				
		    	header.AttributedText = new NSAttributedString(NSBundle.MainBundle.LocalizedString("PrivacyText", "PrivacyText"), GlobalData.HeaderAttrib);

				LoadPage(NSBundle.MainBundle.LocalizedString("PrivacyLink", "PrivacyLink"));
			};
			Add (btnThird);

			UIButton btnFourth = new UIButton (new CGRect(frame.Width - 100, (frame.Height / 2) + ((frame.Height / 8) * 3), 100, frame.Height / 8));
			btnFourth.Hidden = false;
			btnFourth.TintColor = UIColor.White;
			btnFourth.SetTitle("+", UIControlState.Normal);
			btnFourth.Font = UIFont.FromName ("Roboto-Regular", 18f);
			btnFourth.BackgroundColor = UIColor.Clear;
			btnFourth.SetTitleColor (UIColor.White, UIControlState.Normal);
			btnFourth.Layer.BorderColor = UIColor.Clear.CGColor;
			btnFourth.TouchUpInside+= (object sender, EventArgs e) => 
			{
				header.AttributedText = new NSAttributedString(NSBundle.MainBundle.LocalizedString("TermsText", "TermsText"), GlobalData.HeaderAttrib);

				LoadPage(NSBundle.MainBundle.LocalizedString("TermsLink", "TermsLink"));
			};
			Add (btnFourth);
		}


		private void LoadPage(string link)
		{
			if (!string.IsNullOrEmpty(link))
			{		
				try 
				{

					AddBckButton();
					webView = new UIWebView(new CGRect (0, Frame.Height / 10, Frame.Width, Frame.Height - Frame.Height / 10));

					this.AddSubview (webView);
					webView.ScalesPageToFit = true;

					webView.LoadRequest(new NSUrlRequest(new NSUrl(link)));

					var ctrl = viewcontroller as SavingsViewController;
					ctrl.StartSpinner ();

					webView.LoadFinished+= (object sender, EventArgs e) => 
					{
						ctrl.StopSpinner ();
					};

					webView.LoadError+= (object sender, UIWebErrorArgs e) => 
					{
						ctrl.StopSpinner ();

						UIAlertView alert = new UIAlertView (NSBundle.MainBundle.LocalizedString("InvalidLinkText", "InvalidLinkText"), "", null, 
							NSBundle.MainBundle.LocalizedString("OK", "OK")); 
						alert.Show ();

						alert.Clicked += (snd, buttonArgs) => {
							if (buttonArgs.ButtonIndex == 0) 
							{ // OK Clicked
								webView.RemoveFromSuperview ();
							}
						};
					};
				}
				catch 
				{

				}
			}
			else
			{
				UIAlertView alert = new UIAlertView (NSBundle.MainBundle.LocalizedString("NoWebLinkText", "NoWebLinkText"), "", null,
					NSBundle.MainBundle.LocalizedString ("OK", "OK")); 
				alert.Show ();	
			}

		}

		private void AddBckButton() 
		{
			// Disable main wheel
			SavingsViewController view = viewcontroller as SavingsViewController;
			view.WheelUserInteractionEnabled (false);

			if (_bckButton == null) {
				_bckButton = new UIImageView (new CGRect (5, 5, Frame.Height / 11, Frame.Height / 11));
				_bckButton.Image = UIImage.FromBundle ("Images/back.png");
				_bckButton.UserInteractionEnabled = true;

				UITapGestureRecognizer bckTap = new UITapGestureRecognizer((g) => 
					{
						DropWebView();
					});
				_bckButton.AddGestureRecognizer (bckTap);
			}
			Add (_bckButton);
		}


		private void DropWebView()
		{
			if (webView != null)
			{
				webView.RemoveFromSuperview();
				webView.Dispose();
				webView = null;

				_bckButton.RemoveFromSuperview();
				_bckButton.Dispose();
				_bckButton = null;

				// Disable main wheel
				SavingsViewController viewTmp  = viewcontroller as SavingsViewController;
				viewTmp.WheelUserInteractionEnabled (true);

				header.AttributedText = new NSAttributedString(NSBundle.MainBundle.LocalizedString("InfoText", "InfoText"), GlobalData.HeaderAttrib);
			}
		}


	}
}

