using System;
using Security;
using Foundation;
using System.Runtime.InteropServices;
using ObjCRuntime;

namespace nobaddaysIOS
{
	//https://github.com/Redth/PushSharp/blob/master/Client.Samples/PushSharp.ClientSample.MonoTouch/PushSharp.ClientSample.MonoTouch/AppDelegate.cs
	public class DeviceToken
	{

//		[DllImport(Constants.ObjectiveCLibrary, EntryPoint="objc_msgSend")]
//		static extern IntPtr intptr_objc_msgSend(IntPtr deviceHandle, IntPtr setterHandle, IntPtr strFormat, IntPtr devToken);

		[DllImport(Constants.ObjectiveCLibrary, EntryPoint="objc_msgSend")]
		static extern IntPtr intptr_objc_msgSend(IntPtr tokenHandle, IntPtr selectorHandle);

		public static void SetToken(NSData deviceToken)
		{
			//var strFormat = new NSString("%@");
			var oldDeviceToken = NSUserDefaults.StandardUserDefaults.StringForKey("PushDeviceToken");
			Selector selector = new Selector ("description"); 

			var dt = Runtime.GetNSObject(intptr_objc_msgSend(deviceToken.Handle, selector.Handle));
			var newDeviceToken = dt.ToString().Replace("<", "").Replace(">", "").Replace(" ", "");


			if (string.IsNullOrEmpty(oldDeviceToken) || !deviceToken.Equals(newDeviceToken))
			{
				Console.WriteLine("Device Token: " + newDeviceToken);
				//TODO: Put your own logic here to notify your server that the device token has changed/been created!
			}

			//Save device token now
			NSUserDefaults.StandardUserDefaults.SetString(newDeviceToken, "PushDeviceToken");

			Console.WriteLine("Device Token: " + newDeviceToken);

		}

		public int GetIntUnchecked(IntPtr value)
		{
			return IntPtr.Size == 8 ? unchecked((int)value.ToInt64()) : value.ToInt32();
		}

		public static string RetrieveToken()
		{
			string lastDeviceToken = NSUserDefaults.StandardUserDefaults.StringForKey("PushDeviceToken");

			return lastDeviceToken;
		}

	}
}