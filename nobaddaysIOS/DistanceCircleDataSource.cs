﻿using System;
using UIKit;
using CDCircleLib;
using Utils;

namespace nobaddaysIOS
{
	class DistanceCircleDataSource: CDCircleDataSource{
		public override UIImage IconForThumbAtRow (CDCircle circle, nint row)
		{
			switch (row) {
			case GlobalData.SAVINGSINDEX:
				return UIImage.FromBundle ("Wheel/Hjul_ikoner-04.png");  // Savings
			case GlobalData.WININDEX:
				return UIImage.FromBundle ("Wheel/Hjul_ikoner-02.png");  //Win
			case GlobalData.CHARITYINDEX:
				return UIImage.FromBundle ("Wheel/Hjul_ikoner-03.png");  //Charity
			case GlobalData.INFOINDEX:
				return UIImage.FromBundle ("Wheel/Hjul_ikoner-01.png");  //Info
			case GlobalData.OFFERSINDEX:
				return UIImage.FromBundle ("Wheel/Hjul_ikoner-05.png");   //Offers
			case GlobalData.MAPINDEX:
				return UIImage.FromBundle ("Wheel/Hjul_ikoner-06.png");   //Map
			case GlobalData.NBDINDEX:
				return UIImage.FromBundle ("Wheel/Hjul_ikoner-07.png");   //NBD
			default:
				return UIImage.FromBundle ("Wheel/Hjul_ikoner-08.png");   //Community
			}
		}
	}
}

