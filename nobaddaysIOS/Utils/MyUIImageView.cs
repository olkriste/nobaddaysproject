using System;
using UIKit;
using Foundation;
using CoreLocation;
using CoreGraphics;

namespace Utils
{
	[Register("MyUIImageView")]
	public class MyUIImageView : UIImageView
	{

		public UIActivityIndicatorView Wheel {
			get;
			private set;
		}


		public MyUIImageView (CGRect frame) : base(frame)
		{
			Wheel = new UIActivityIndicatorView (UIActivityIndicatorViewStyle.Gray);
			Wheel.Center = new CGPoint ( (Frame.Width / 2), (Frame.Height / 2));

			Add (Wheel);
		}

		public MyUIImageView (UIActivityIndicatorViewStyle style) : base()
		{
			Wheel = new UIActivityIndicatorView (style);
			Wheel.Center = new CGPoint ( (Frame.Width / 2), (Frame.Height / 2));

			Add (Wheel);
		}


		public MyUIImageView (UIImage image, UIActivityIndicatorViewStyle style) : base(image)
		{
			Wheel = new UIActivityIndicatorView (style);
			Wheel.Center = new CGPoint ( (Frame.Width / 2), (Frame.Height / 2));

			Add (Wheel);
		}

		[Export("initWithCoder:")]
		public MyUIImageView (NSCoder coder) : base(coder)
		{
			Wheel = new UIActivityIndicatorView (UIActivityIndicatorViewStyle.Gray);
			Wheel.Center = new CGPoint ( (Frame.Width / 2), (Frame.Height / 2));

			Add (Wheel);
		}

		public override CGRect Frame {
			get {
				return base.Frame;
			}
			set {
				base.Frame = value;
				if (Wheel != null)
					Wheel.Center = new CGPoint ( (Frame.Width / 2), (Frame.Height / 2));
			}
		}
	}
}

