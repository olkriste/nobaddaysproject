﻿using System;
using UIKit;
using Foundation;
using CoreGraphics;

namespace Utils
{
	[Register ("MyButton")]
	public class MyButton : UIButton
	{

		[Export("initWithCoder:")]
		public MyButton (NSCoder coder) : base(coder)
		{
		}

		public void SetupButton(string title, UIImage image)
		{
		//	UIButton button = new UIButton ();

			UIFont font = UIFont.FromName ("Helvetica", 12.0f);

			// Get size of target string using the label's font.
			NSString nsString = new NSString(title);
			UIStringAttributes attribs = new UIStringAttributes { Font = font };
			CGSize titleSize = nsString.GetSizeUsingAttributes(attribs);
		//	button.Frame = new CGRect (0, 0, size.Width, size.Height);
		///	CGSize buttonSize = button.Frame.Size;
			nfloat offsetBetweenImageAndText = 3; //vertical space between image and text

			Layer.CornerRadius = 3;

			TitleLabel.Font = font;
			SetTitle (title, UIControlState.Normal);

			HorizontalAlignment = UIControlContentHorizontalAlignment.Left;
			VerticalAlignment = UIControlContentVerticalAlignment.Top;

			SetImage (image, UIControlState.Normal);
			nfloat imageWidth = ImageView.Frame.Size.Width;

			UIImage buttonImage = ImageView.Image;
			CGSize buttonImageSize = buttonImage.Size;

			ImageEdgeInsets = new UIEdgeInsets ((Frame.Height - (titleSize.Height + buttonImageSize.Height)) / 2,
				(Frame.Width - buttonImageSize.Width) / 2,
				0,0);

			TitleEdgeInsets = new UIEdgeInsets((Frame.Height - (titleSize.Height + buttonImageSize.Height)) / 2 + buttonImageSize.Height /*+ offsetBetweenImageAndText */,
				titleSize.Width + ImageEdgeInsets.Left > Frame.Width ? - buttonImage.Size.Width  +  (Frame.Width - titleSize.Width) / 2 : (Frame.Width - titleSize.Width) / 2 - buttonImage.Size.Width,
				0,0);


			SetNeedsDisplay ();

		}
	}
}

