﻿using System;
using UIKit;
using Foundation;

namespace Utils
{
	public static class Extensions
	{

		public static byte[] ToNSData(this UIImage image){

			if (image == null) {
				return null;
			}
			NSData data = null;

			try {
				data = image.AsPNG();
				return data.ToArray ();
			} catch (Exception ) {
				return null;
			}
			finally
			{
				if (image != null) {
					image.Dispose ();
					image = null;
				}
				if (data != null) {
					data.Dispose ();
					data = null;
				}
			}
		}

		public static UIImage ToImage(this byte[] data)
		{
			if (data==null) {
				return null;
			}
			UIImage image = null;
			try {

				image = new UIImage(NSData.FromArray(data));
				data = null;
			} catch (Exception ) {
				return null;
			}
			return image;
		}

		/*
		public UIImage ByteArrayToImage(this byte[] _imageBuffer) 
		{ 
			if(_imageBuffer != null) 
			{ 
				if(_imageBuffer.Length != 0) 
				{ 
					NSData imageData = NSData.FromArray(_imageBuffer); 
					return UIImage.LoadFromData(imageData); 
				} 
				else 
					return new UIImage(); 
			} 
			else 
				return new UIImage(); 
		}

*/
	}
}