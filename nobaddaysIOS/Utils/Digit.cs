﻿using System;
using UIKit;
using CoreGraphics;

namespace Utils
{
	public class Digit : UITextView
	{
//		UIView circleView;

		CGRect _frame;

		bool borderHighlighted;
		public bool BorderHighlighted
		{
			
			get { return borderHighlighted; }
			set
			{
				if (borderHighlighted != value)
				{
					if (borderHighlighted == false)
					{
						Layer.BorderColor = UIColor.FromRGB(0x35, 0xB3, 0x9A).CGColor;
						Layer.BorderWidth = 2.0f;

					}
					else
					{
						Layer.BorderColor = UIColor.LightGray.CGColor;
						Layer.BorderWidth = 1.0f;

					}
				}
				borderHighlighted = value;
			}
		} 



		bool digitEntered;
		public bool DigitEntered
		{

			get { return digitEntered; }
			set
			{
					digitEntered = value;
			}
		} 




		public void Toggle()
		{
			BorderHighlighted = !BorderHighlighted;
		}

		public void Turnoff()
		{
			BorderHighlighted = false;
		}

		public Digit(CGRect frame) : base(frame)
		{
			AutoresizingMask = UIViewAutoresizing.None;
			Layer.BorderWidth = 1.0f;
			Layer.CornerRadius = 2.0f;
			Layer.BorderColor = UIColor.LightGray.CGColor;
			TextContainerInset = new UIEdgeInsets (1, 0, -1, 0);
			UserInteractionEnabled = false;
			TintColor = UIColor.Clear;
			KeyboardAppearance = UIKeyboardAppearance.Dark;
			KeyboardType = UIKeyboardType.NumberPad;
	//		SecureTextEntry = false;
			Font = UIFont.FromName ("OpenSans-Bold", 26f);
			TextAlignment = UITextAlignment.Center;
	//		SetupCircleView(frame);
			DigitEntered = false;
		}

		/*
		private void SetupCircleView(CGRect frame)
		{
			circleView = new UIView(new CGRect(frame.Width / 4, frame.Width / 4, frame.Width / 2, frame.Width / 2));
			circleView.Layer.CornerRadius = frame.Width / 4;
			Add(circleView);
		}
		*/
	}
	
}

