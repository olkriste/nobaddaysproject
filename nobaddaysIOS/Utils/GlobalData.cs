﻿using System;
using UIKit;
using NoBadDaysPCL.Items;
using CDCircleLib;
using CoreGraphics;
using nobaddaysIOS;
using System.Drawing;
using Foundation;
using System.Collections.Generic;

namespace Utils
{
	public class GlobalData
	{
		public const int NBDINDEX = 0;
		public const int MAPINDEX = 1;
		public const int COMMUNITYINDEX = 2;
		public const int INFOINDEX = 3;
		public const int WININDEX = 4;
		public const int CHARITYINDEX = 5;
		public const int SAVINGSINDEX = 6;
		public const int OFFERSINDEX = 7;

		public const int SLIDERMAX = 20000;   //Meters
		public const int SLIDERMIN = 20;


		public static List<string> categoryList;

		private static bool _iSLoggedIn;
		public static bool IsLoggedIn
		{
			get { return _iSLoggedIn; }
			set { _iSLoggedIn = value;}
		}

		private static UserItem _userItem;
		public static UserItem UserItem
		{
			get 
			{ 
				if (_userItem == null) 
				{
					_userItem = new UserItem ();
					_userItem.Picture = new ImageItem ();
				}
				return _userItem; 
			}
			set { _userItem = value;}
		}

		private static bool isPushRegistered;
		public static bool IsPushRegistered
		{
			get { return isPushRegistered; }
			set { isPushRegistered = value;}
		}

		private static NSData deviceToken;
		public static NSData DeviceToken
		{
			get { return deviceToken; }
			set { deviceToken = value;}
		}

		private static nfloat _viewFrameWidth;
		public static nfloat ViewFrameWidth
		{
			get { return _viewFrameWidth; }
			set { _viewFrameWidth = value;}
		}

		private static nfloat _viewFrameHeight;
		public static nfloat ViewFrameHeight
		{
			get { return _viewFrameHeight; }
			set { _viewFrameHeight = value;}
		}

		private static int _wheelSelection;
		public static int WheelSelection
		{
			get { return _wheelSelection; }
			set { _wheelSelection = value;}
		}

		private static int _wheelDiameter;
		public static int WheelDiameter
		{
			get { return _wheelDiameter; }
			set { _wheelDiameter = value;}
		}

		private static nfloat _containerWidth;
		public static nfloat ContainerWidth
		{
			get { return _containerWidth; }
			set { _containerWidth = value;}
		}

		private static nfloat _containerHeight;
		public static nfloat ContainerHeight
		{
			get { return _containerHeight; }
			set { _containerHeight = value;}
		}

		private static nfloat _wheelPosition;
		public static nfloat WheelPosition
		{
			get { return _wheelPosition; }
			set { _wheelPosition = value;}
		}

		private static float _springDampingRatio = 5.25f;
		public static float SpringDampRation
		{
			get { return _springDampingRatio; }
		}

		private static float _initialSpringVelocity = 0.50f;
		public static float SpringVelocity
		{
			get { return _initialSpringVelocity; }
		}

		static UIView _container = null;
		public static UIView WheelContainer
		{
			get
			{
				if(_container == null)
				{
					_wheelPosition = 20 + 2 * _viewFrameHeight / 3;
					_container = new UIView (new CGRect(0, _wheelPosition, _viewFrameWidth, _viewFrameWidth));
					_containerHeight = (float)_container.Frame.Height;
					_containerWidth = (float)_container.Frame.Width;

				}
				return _container;
			}
		}

		static CDCircle _circle = null;
		public static CDCircle Circle
		{
			get 
			{ 
				if (_circle == null) 
				{
					_wheelDiameter = (int)(5 * _containerWidth / 6);
					CGRect circleFrame = new CGRect((float)(_containerWidth -_wheelDiameter) / 2 , 
						(float)(_containerHeight -_wheelDiameter) / 2, _wheelDiameter, _wheelDiameter);
					_circle = new CDCircle (circleFrame, 8, 80);
					//_circle.CircleColor = new UIColor(255f/255f, 63f/255f, 63f/255f, 255f/255f);
					_circle.DataSource = new DistanceCircleDataSource ();
				}
				return _circle; 
			}
			set { _circle = value;}
		}

		static CDCircleOverlayView _overlay = null;
		public static CDCircleOverlayView Overlay
		{
			get 
			{ 
				if (_overlay == null) 
				{
					//circle.CircleColor = new UIColor (0.09f, 0.6f, 0.41f, 0.5f);
					_overlay = new CDCircleOverlayView (_circle);
				}
				return _overlay; 
			}
			set { _overlay = value;}
		}

		private static UIImage _redbackGround = null;
		public static UIImage RedBackGround
		{
			get 
			{ 
				if (_redbackGround == null) 
				{
					var imageSize = new SizeF(30, 30);
					var imageSizeRectF = new RectangleF(0, 0, 30, 30);
					UIGraphics.BeginImageContextWithOptions(imageSize, false, 0);
					var context = UIGraphics.GetCurrentContext();
					var red = new CGColor(255f/255f, 63f/255f, 63f/255f, 255f/255f);
					context.SetFillColor(red);
					context.FillRect(imageSizeRectF);
					_redbackGround = UIGraphics.GetImageFromCurrentImageContext();
					UIGraphics.EndImageContext(); 
				}
				return _redbackGround;
			}
		}

		private static UIImage _whiteBackGround = null;
		public static UIImage WhiteBackGround
		{
			get 
			{ 
				if (_whiteBackGround == null) 
				{
					var imageSize = new SizeF(30, 30);
					var imageSizeRectF = new RectangleF(0, 0, 30, 30);
					UIGraphics.BeginImageContextWithOptions(imageSize, false, 0);
					var context = UIGraphics.GetCurrentContext();
					var white = new CGColor(255f/255f, 255f/255f, 255f/255f, 255f/255f);
					context.SetFillColor(white);
					context.FillRect(imageSizeRectF);
					_whiteBackGround = UIGraphics.GetImageFromCurrentImageContext();
					UIGraphics.EndImageContext(); 
				}
				return _whiteBackGround;
			}
		}

		private static UIImage _blackBackGround = null;
		public static UIImage BlackBackGround
		{
			get 
			{ 
				if (_blackBackGround == null) 
				{
					var imageSize = new SizeF(30, 30);
					var imageSizeRectF = new RectangleF(0, 0, 30, 30);
					UIGraphics.BeginImageContextWithOptions(imageSize, false, 0);
					var context = UIGraphics.GetCurrentContext();
					var black = new CGColor(1f/255f, 1f/255f, 1f/255f, 255f/255f);
					context.SetFillColor(black);
					context.FillRect(imageSizeRectF);
					_blackBackGround = UIGraphics.GetImageFromCurrentImageContext();
					UIGraphics.EndImageContext(); 
				}
				return _blackBackGround;
			}
		}

		private static UIImage _grayGround = null;
		public static UIImage GrayBackGround
		{
			get 
			{ 
				if (_grayGround == null) 
				{
					var imageSize = new SizeF(30, 30);
					var imageSizeRectF = new RectangleF(0, 0, 30, 30);
					UIGraphics.BeginImageContextWithOptions(imageSize, false, 0);
					var context = UIGraphics.GetCurrentContext();
					var gray = new CGColor(40f/255f, 38f/255f, 40f/255f, 255f/255f);
					context.SetFillColor(gray);
					context.FillRect(imageSizeRectF);
					_grayGround = UIGraphics.GetImageFromCurrentImageContext();
					UIGraphics.EndImageContext(); 
				}
				return _grayGround;
			}
		}

		private static UIImage _lightGrayGround = null;
		public static UIImage LightGrayBackGround
		{
			get 
			{ 
				if (_lightGrayGround == null) 
				{
					var imageSize = new SizeF(30, 30);
					var imageSizeRectF = new RectangleF(0, 0, 30, 30);
					UIGraphics.BeginImageContextWithOptions(imageSize, false, 0);
					var context = UIGraphics.GetCurrentContext();
					var gray = new CGColor(192f/255f, 192f/255f, 192f/255f, 255f/255f);
					context.SetFillColor(gray);
					context.FillRect(imageSizeRectF);
					_lightGrayGround = UIGraphics.GetImageFromCurrentImageContext();
					UIGraphics.EndImageContext(); 
				}
				return _lightGrayGround;
			}
		}

		private static UIStringAttributes _headerAttrib = null;
		public static UIStringAttributes HeaderAttrib 
		{
			get 
			{
				if (_headerAttrib == null) 
				{
					_headerAttrib = new UIStringAttributes {
						ForegroundColor = UIColor.White, 
						Font = UIFont.PreferredSubheadline,
						StrokeWidth = 10f,
						StrokeColor = UIColor.White
					};
				}
				return _headerAttrib;
			}
		}

		private static UIStringAttributes _countryAttrib = null;
		public static UIStringAttributes CountryAttrib 
		{
			get 
			{
				if (_countryAttrib == null) 
				{
					_countryAttrib = new UIStringAttributes {
						ForegroundColor = UIColor.White, 
						Font = UIFont.PreferredCallout
					};
				}
				return _countryAttrib;
			}
		}

		private static UIStringAttributes _tapBarBlackAttrib = null;
		public static UIStringAttributes TapBarBlackAttrib 
		{
			get 
			{
				if (_tapBarBlackAttrib == null) 
				{
					_tapBarBlackAttrib = new UIStringAttributes {
						ForegroundColor = UIColor.White,
						BackgroundColor = UIColor.Black,
						Font = UIFont.PreferredSubheadline,
						StrokeWidth = 10f,
						StrokeColor = UIColor.White
					};
				}
				return _tapBarBlackAttrib;
			}
		}

		private static UIStringAttributes _tapBarWhiteAttrib = null;
		public static UIStringAttributes TapBarWhiteAttrib 
		{
			get 
			{
				if (_tapBarWhiteAttrib == null) 
				{
					_tapBarWhiteAttrib = new UIStringAttributes {
						ForegroundColor = UIColor.Black,
						BackgroundColor = UIColor.White,
						Font = UIFont.PreferredSubheadline,
						StrokeWidth = 10f,
						StrokeColor = UIColor.Black
					};
				}
				return _tapBarWhiteAttrib;
			}
		}

		private static UIStringAttributes _barAttrib = null;
		public static UIStringAttributes BarAttrib 
		{
			get 
			{
				if (_barAttrib == null) 
				{
					_barAttrib = new UIStringAttributes {
						ForegroundColor = UIColor.White, 
						Font = UIFont.PreferredFootnote
					};
				}
				return _barAttrib;
			}
		}

		#region winner
		private static UIImage _firstGrayGround = null;
		public static UIImage FirstGrayBackGround
		{
			get 
			{ 
				if (_lightGrayGround == null) 
				{
					var imageSize = new SizeF(30, 30);
					var imageSizeRectF = new RectangleF(0, 0, 30, 30);
					UIGraphics.BeginImageContextWithOptions(imageSize, false, 0);
					var context = UIGraphics.GetCurrentContext();
					var gray = new CGColor(191f/255f, 191f/255f, 191f/255f, 255f/255f);
					context.SetFillColor(gray);
					context.FillRect(imageSizeRectF);
					_firstGrayGround = UIGraphics.GetImageFromCurrentImageContext();
					UIGraphics.EndImageContext(); 
				}
				return _firstGrayGround;
			}
		}

		private static UIImage _secondGrayGround = null;
		public static UIImage SecondGrayBackGround
		{
			get 
			{ 
				if (_lightGrayGround == null) 
				{
					var imageSize = new SizeF(30, 30);
					var imageSizeRectF = new RectangleF(0, 0, 30, 30);
					UIGraphics.BeginImageContextWithOptions(imageSize, false, 0);
					var context = UIGraphics.GetCurrentContext();
					var gray = new CGColor(204f/255f, 204f/255f, 204f/255f, 255f/255f);
					context.SetFillColor(gray);
					context.FillRect(imageSizeRectF);
					_secondGrayGround = UIGraphics.GetImageFromCurrentImageContext();
					UIGraphics.EndImageContext(); 
				}
				return _secondGrayGround;
			}
		}

		private static UIImage _thirdGrayGround = null;
		public static UIImage ThirdGrayBackGround
		{
			get 
			{ 
				if (_lightGrayGround == null) 
				{
					var imageSize = new SizeF(30, 30);
					var imageSizeRectF = new RectangleF(0, 0, 30, 30);
					UIGraphics.BeginImageContextWithOptions(imageSize, false, 0);
					var context = UIGraphics.GetCurrentContext();
					var gray = new CGColor(227f/255f, 227f/255f, 227f/255f, 255f/255f);
					context.SetFillColor(gray);
					context.FillRect(imageSizeRectF);
					_thirdGrayGround = UIGraphics.GetImageFromCurrentImageContext();
					UIGraphics.EndImageContext(); 
				}
				return _thirdGrayGround;
			}
		}
		#endregion


		public static void initCategoryList()
		{
			categoryList = new List<string> { 
				NSBundle.MainBundle.LocalizedString ("AllCategoriesText", "AllCategoriesText"),
				NSBundle.MainBundle.LocalizedString ("FoodText", "FoodText"),
				NSBundle.MainBundle.LocalizedString ("EatAndDrinkText", "EatAndDrinkText"),
				NSBundle.MainBundle.LocalizedString ("HotelAndTravelText", "HotelAndTravelText"),
				NSBundle.MainBundle.LocalizedString ("TicketsAndExperienencesText", "TicketsAndExperienencesText"),
				NSBundle.MainBundle.LocalizedString ("SportText", "SportText"),
				NSBundle.MainBundle.LocalizedString ("ServicesText", "ServicesText"),
				NSBundle.MainBundle.LocalizedString ("HomeAndGardenText", "HomeAndGardenText"),
				NSBundle.MainBundle.LocalizedString ("ClothingText", "ClothingText"),
				NSBundle.MainBundle.LocalizedString ("KidsText", "KidsText"),
				NSBundle.MainBundle.LocalizedString ("ElectronicsText", "ElectronicsText")
			};
		}

	}
}

