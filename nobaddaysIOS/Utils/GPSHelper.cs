using System;
using CoreLocation;
using UIKit;
using System.ComponentModel;
using Foundation;

namespace nobaddaysIOS
{
	public class GPSHelper
	{
		public CLLocationManager locationManager;
		public static CLLocation mylocation;
		public event EventHandler GpsChanged;

		public enum GPSStatus {
			NotSettled,
			Enabled,
			Disabled
		}

		public GPSHelper()
		{
			locationManager = new CLLocationManager ();
		}

		private static GPSStatus _gpsenabled;
		public GPSStatus GPSEnabled
		{
			get { return _gpsenabled; }
			set
			{
				if (value != _gpsenabled)
				{
					_gpsenabled = value;

					if (GpsChanged != null) {
						GpsChanged (this, EventArgs.Empty);
					} else {

					}
	
				}
			}
		}



		/// <summary>
		/// Converts miles to latitude degrees
		/// </summary>
		public double MilesToLatitudeDegrees(double miles)
		{
			double earthRadius = 3960.0;
			double radiansToDegrees = 180.0/Math.PI;
			return (miles/earthRadius) * radiansToDegrees;
		}

		/// <summary>
		/// Converts miles to longitudinal degrees at a specified latitude
		/// </summary>
		public double MilesToLongitudeDegrees(double miles, double atLatitude)
		{
			double earthRadius = 3960.0;
			double degreesToRadians = Math.PI/180.0;
			double radiansToDegrees = 180.0/Math.PI;

			// derive the earth's radius at that point in latitude
			double radiusAtLatitude = earthRadius * Math.Cos(atLatitude * degreesToRadians);
			return (miles / radiusAtLatitude) * radiansToDegrees;
		}

		/*

		public  CLLocation MyLastKnownLocation 
		{
			get 
			{ 
				if (_mylocation == null) 
				{ 
					// Place you on Sankt Helena in the Atlantic Ocean :-)
					_mylocation = new CLLocation (0,0);
				} 
				return _mylocation; 
			}
			
			set
			{
				if (value != _mylocation)
				{
					_mylocation = value;
				}
			}
		}

*/
	}
}

