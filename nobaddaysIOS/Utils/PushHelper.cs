﻿using System;
using Foundation;
using UIKit;
using WindowsAzure.Messaging;
using SQLite.Net.Platform.XamarinIOS;
using NoBadDaysPCL.ViewModel;
using Utils;

namespace nobaddaysIOS
{
	public class PushHelper
	{
		private SBNotificationHub Hub {
			get;
			set;
		}

		private UserViewModel _viewModel = null;
		UserViewModel ViewModel 
		{ 
			get 
			{ 
				if (_viewModel == null) 
				{
					string libraryPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
					var path = System.IO.Path.Combine(libraryPath, nobaddaysIOS.Container.SQLitePath);

					_viewModel = new UserViewModel(new SQLitePlatformIOS(), path);
				}
				return _viewModel;
			}
		}

		public void RegisterPush()
		{
			if (!GlobalData.IsPushRegistered) {
				UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;
				GlobalData.UserItem = ViewModel.GetUser ();
				if (GlobalData.UserItem != null && GlobalData.UserItem.UserId > 0 &&
				   GlobalData.DeviceToken != null) {

					GlobalData.IsPushRegistered = true;
					Hub = new SBNotificationHub (AzureConstants.ConnectionString, AzureConstants.NotificationHubPath);

					Hub.UnregisterAllAsync (GlobalData.DeviceToken, (error) => {
						if (error != null) {
							//Console.WriteLine("Error calling Unregister: {0}", error.ToString());
							System.Diagnostics.Debug.WriteLine ("Error calling Unregister: {0}", error.ToString ());
							return;
						}

						NSSet tags = new NSSet (GlobalData.UserItem.UserId.ToString ()); // create tags if you want
						Hub.RegisterNativeAsync (GlobalData.DeviceToken, tags, (errorCallback) => {
							if (errorCallback != null) {
								System.Diagnostics.Debug.WriteLine ("RegisterNativeAsync error: " + errorCallback.ToString ());
							} 
						});
					});
				}
			}
		}
	}
}

