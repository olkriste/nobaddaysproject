﻿using System;
using CoreGraphics;
using UIKit;
using System.Timers;
using Foundation;
using System.Collections.Generic;
using NoBadDaysPCL;
using System.IO;
using SQLite.Net.Platform.XamarinIOS;
using Valore.IOSDropDown;
using Security;
using Utils;
using NoBadDaysPCL.Items;
using NoBadDaysPCL.ViewModel;
using System.Threading.Tasks;
using System.Net;
using System.Drawing;
using MapKit;
using CoreAnimation;
using CoreLocation;


namespace nobaddaysIOS
{
	public class StoreView: UIView
	{
		UIViewController viewcontroller;

		// Tap bar
		UIButton btnContact;
		UIButton btnRate;

		UIButton btnPlus;

		// Shop
		int _currentShop;

		// Views
		UIImageView blurOverlay = null;
		UIImageView storeBckImage = null;
		UIImageView bckImageRate = null;
		UIImageView bckImageContact = null;
		UIImageView storeImage = null;
		UIImageView buttomBarRate = null;
		UIImageView buttomBarContact = null;
		UIImageView plusImage = null;

		UIScrollView scrollView = null;

		RatingsView _ratingsView = null;

		// Text views
		UITextView ratingComment = null;

		// Stars
		UIImageView storeStarOne;
		UIImageView storeStarTwo;
		UIImageView storeStarThree;
		UIImageView storeStarFour;
		UIImageView storeStarFive;

		UIImageView myStarOne;
		UIImageView myStarTwo;
		UIImageView myStarThree;
		UIImageView myStarFour;
		UIImageView myStarFive;

		UIImageView imageWeb;
		UIImageView imageRoute;
		UIImageView imageCall;

		// Tap recognizers
		UITapGestureRecognizer starOne;
		UITapGestureRecognizer starTwo;
		UITapGestureRecognizer starThree;
		UITapGestureRecognizer starFour;
		UITapGestureRecognizer starFive;
		UITapGestureRecognizer bckGroundTap;

		UITapGestureRecognizer gestureRecognizer;
	
		// Labels 
		UILabel storeName;
		UILabel storeLocation;
		UILabel closedNowLabel;

		// Map
		MKMapView mapContact;

		MKCoordinateSpan span;
		BasicMapAnnotation annotation;

		CLLocation mycoor;

		MapView map;

		UIWebView webView;

		Random rnd = new Random (DateTime.UtcNow.Millisecond);
		int myRating = 0;


		private bool _ratingsVisible; 
		public bool IsRatingsVisible 
		{
			get{ return _ratingsVisible;}
			set 
			{
				_ratingsVisible = value;
			}
		}

		private bool _webVisible; 
		public bool IsWebVisible 
		{
			get{ return _webVisible;}
			set 
			{
				_webVisible = value;
			}
		}

			
		public StoreView (object controller, int shopId, MapView _map)
		{
			viewcontroller = controller as UIViewController;
			var frame = viewcontroller.View.Bounds;
			Frame = new CGRect(frame.Width, frame.Height / 10, frame.Width, frame.Height - frame.Height / 10);

			RegisterForKeyboardNotifications();
			_currentShop = shopId;

			mycoor = GPSHelper.mylocation;

			SetViews();
			map = _map;

	
		}

		ShopViewModel _viewModel = null;
		ShopViewModel ViewModel 
		{
			get 
			{ 
				if (_viewModel == null) 
				{
					string libraryPath = System.Environment.GetFolderPath (System.Environment.SpecialFolder.Personal);
					var path = Path.Combine (libraryPath, Container.SQLitePath);

					_viewModel = new ShopViewModel (new SQLitePlatformIOS (), path);
					_viewModel.PhoneDataSuccessful += DataSuccessful;
					_viewModel.PhoneDataErrorOccurred += DataError;
					_viewModel.RatingSuccessful += RatingSuccessful;
				} 
				return _viewModel;
			}
		}

		void DataSuccessful(object sender, ServerOutputItem e)
		{
			SetInfoText ();
			SetProfileImage ();
			SetRating ();
	
			var gpshelper = new GPSHelper ();

			var coords = new CLLocationCoordinate2D ( ViewModel.Item.Latitude, ViewModel.Item.Longitude); 
			span = new MKCoordinateSpan (gpshelper.MilesToLatitudeDegrees (0.5), gpshelper.MilesToLongitudeDegrees (0.5, coords.Latitude));
			annotation = new BasicMapAnnotation (new CLLocationCoordinate2D (ViewModel.Item.Latitude, ViewModel.Item.Longitude), "", "");		

			mapContact.Region = new MKCoordinateRegion (coords, span);
											
			mapContact.AddAnnotation (annotation);


			if (ViewModel.Item.IsOpen) {
				closedNowLabel.Text = NSBundle.MainBundle.LocalizedString ("OpenNowText", "OpenNowText");
			} else {
				closedNowLabel.Text = NSBundle.MainBundle.LocalizedString ("ClosedNowText", "ClosedNowText");
			}

			UILabel label = new UILabel (new CGRect (Frame.Width / 10, Frame.Height / 15f, Frame.Width / 2  - Frame.Width / 10, Frame.Height / 5));
			label.Text = ViewModel.Item.TextLeft;
			label.TextColor = UIColor.Black;
			label.TextAlignment = UITextAlignment.Left;
			label.Font = UIFont.FromName ("Roboto-Regular", 14f);
			label.LineBreakMode = UILineBreakMode.WordWrap;
			label.Lines = 0;
			label.SizeToFit ();
			bckImageContact.AddSubview (label);

			label = new UILabel (new CGRect (Frame.Width / 2 + Frame.Width / 10, Frame.Height / 15f, Frame.Width / 2 - Frame.Width / 10, Frame.Height / 5));
			label.Text = ViewModel.Item.TextRigth;
			label.TextColor = UIColor.Black;
			label.TextAlignment = UITextAlignment.Left;
			label.Font = UIFont.FromName ("Roboto-Regular", 14f);
			label.LineBreakMode = UILineBreakMode.WordWrap;
			label.Lines = 0;
			label.SizeToFit ();
			bckImageContact.AddSubview (label);
		}

		void DataError(object sender, ServerOutputItem e)
		{
			BlurView(false);
		}

		void RatingSuccessful(object sender, ServerOutputItem e)
		{
			BlurView(false);
			ratingComment.Text = "";
			SetContactRating (false, 0);
		}

		private void SetViews()
		{
			SetBackGrounds ();
			AddBckImageContact ();
			AddBckImage ();
			SetDefaultRating ();
			SetCommentView ();
			ViewModel.GetShop (_currentShop);
		}

		private async Task<UIImage> LoadImage (string imageUrl)
		{
			Task<byte[]> contentsTask = ViewModel.LoadImage (imageUrl);
			var contents = await contentsTask;
			return UIImage.LoadFromData (NSData.FromArray (contents));
		}

		public void SetBackGrounds()
		{
			storeBckImage = new UIImageView(new CGRect (0, 0, Frame.Width, Frame.Height / 2 - Frame.Height / 11));
			storeBckImage.Image = GlobalData.RedBackGround;
			Add (storeBckImage);

			float TopImageHeight = (float)Frame.Height / 4.6f;

			storeImage = new UIImageView (new CGRect ((Frame.Width - TopImageHeight) / 2, TopImageHeight / 1.8f - Frame.Height / 11f, TopImageHeight, TopImageHeight));
			storeImage.Layer.Opacity = 0.0f;
			Add(storeImage);

			var tmpFrame = new CGRect(0, Frame.Height / 3.0 - Frame.Height / 11f, Frame.Width, Frame.Height / 15);
			storeName = new UILabel(tmpFrame);
			Add (storeName);

			tmpFrame = new CGRect(0, Frame.Height / 2.7 - Frame.Height / 11f, Frame.Width, Frame.Height / 15);
			storeLocation = new UILabel(tmpFrame);
			Add (storeLocation);
		}

		private void AddBckImage()
		{
			if (bckImageRate == null)
			{
				bckImageRate = new UIImageView(new CGRect (0, Frame.Height / 2 + Frame.Height / 25 - Frame.Height / 11, 
					Frame.Width, Frame.Height / 2 + Frame.Height / 11));
			}
			bckImageRate.Image = GlobalData.WhiteBackGround;
	
	
			if (btnContact == null) {
				btnContact = new UIButton (new CGRect (0, Frame.Height / 2 - Frame.Height / 11, Frame.Width / 2, Frame.Height / 25));
			}
				

			btnContact.Hidden = false;
			btnContact.TintColor = UIColor.Black;
			btnContact.SetAttributedTitle(new NSAttributedString(NSBundle.MainBundle.LocalizedString ("ContactText", "ContactText"), GlobalData.TapBarBlackAttrib), UIControlState.Normal);
			btnContact.BackgroundColor = UIColor.Black;
			btnContact.SetTitleColor (UIColor.White, UIControlState.Normal);
			btnContact.Layer.BorderColor = UIColor.Clear.CGColor;
			btnContact.TouchUpInside += LeftBtn_TouchUpInside;
			Add (btnContact);

			if (btnRate == null) {
				btnRate = new UIButton (new CGRect (Frame.Width / 2, Frame.Height / 2 - Frame.Height / 11, 
					Frame.Width / 2, Frame.Height / 25));
			}
			btnRate.Hidden = false;
			btnRate.TintColor = UIColor.White;
			btnRate.SetAttributedTitle(new NSAttributedString(NSBundle.MainBundle.LocalizedString ("RateStoreText", "RateStoreText"), GlobalData.TapBarWhiteAttrib), UIControlState.Normal);
			btnRate.BackgroundColor = UIColor.White;
			btnRate.SetTitleColor (UIColor.Black, UIControlState.Normal);
			btnRate.Layer.BorderColor = UIColor.Clear.CGColor;
			btnRate.TouchUpInside += RightBtn_TouchUpInside;
			Add (btnRate);

			bckImageRate.UserInteractionEnabled = true;
			UITapGestureRecognizer bckGroundTap = new UITapGestureRecognizer ((g) => 
				{
					ratingComment.EndEditing(true);
		
				});
			bckImageRate.AddGestureRecognizer (bckGroundTap);
			SetButtomBarRate ();
			//bckImageRate.UserInteractionEnabled = true;
			Add (bckImageRate);
		}

		private void AddBckImageContact()
		{
			if (bckImageContact == null)
			{

				CGRect frame = new CGRect (0, Frame.Height / 2 + Frame.Height / 25 - Frame.Height / 11, 
					Frame.Width, Frame.Height / 2 + Frame.Height / 11);
				
				bckImageContact = new UIImageView(frame);
				bckImageContact.Image = GlobalData.WhiteBackGround;

		
				UILabel label = new UILabel (new CGRect (frame.Width / 10, frame.Height / 28, frame.Width / 2  - frame.Width / 10, frame.Height / 10));
				label.Text = NSBundle.MainBundle.LocalizedString ("OpeningHoursText", "OpeningHoursText");
				label.TextColor = UIColor.Black;
				label.TextAlignment = UITextAlignment.Left;
				label.Font = UIFont.FromName ("Roboto-Regular", 16F);
				bckImageContact.AddSubview (label);

				closedNowLabel = new UILabel (new CGRect (frame.Width / 2 + frame.Width / 10, frame.Height / 28, frame.Width / 2 - frame.Width / 10, frame.Height / 10));
				closedNowLabel.Text = "...";
				closedNowLabel.TextColor = UIColor.Red;
				closedNowLabel.TextAlignment = UITextAlignment.Left;
				closedNowLabel.Font = UIFont.FromName ("Roboto-Regular", 16F);
				bckImageContact.AddSubview (closedNowLabel);

				// Add map
				mapContact = new MKMapView (new CGRect (0, frame.Height / 3 + 20, frame.Width, frame.Height / 2));
							
				mapContact.ShowsPointsOfInterest = false;
				mapContact.ScrollEnabled = true;
				mapContact.ZoomEnabled = true;
				mapContact.ShowsUserLocation = true;
				mapContact.UserInteractionEnabled = true;
			
				bckImageContact.AddSubview (mapContact);
						
				SetButtomBarContact ();
			}
			bckImageContact.UserInteractionEnabled = true;
			Add (bckImageContact);
		}

		private async void SetProfileImage()
		{
			float TopImageHeight = (float)Frame.Height / 4.6f;

			try
			{
				storeImage.Image = await LoadImage (ViewModel.Item.ListPicture.Uri);

				Helper helper = new Helper ();
				storeImage.Image = helper.RounderCorners (storeImage.Image, TopImageHeight, TopImageHeight / 2);
				storeImage.Layer.Opacity = 0;

				await UIView.AnimateAsync (0.3f, () => {
					storeImage.Layer.Opacity = 1f;
				});
			}
			catch 
			{
			}
		}

		private void SetDefaultRating()
		{
			SetContactRating (false, 0);

			SetStoreRating (false, 1);
			SetStoreRating (false, 2);
			SetStoreRating (false, 3);
			SetStoreRating (false, 4);
			SetStoreRating (false, 5);
		}
			
		private void SetCommentView()
		{
			var tmpFrame = new CGRect (40, Frame.Height / 6, Frame.Width - 80, Frame.Height / 6);
			if (ratingComment == null) {
				ratingComment = new UITextView (tmpFrame);
			}
			ratingComment.TextAlignment = UITextAlignment.Center;
			ratingComment.Layer.CornerRadius = 15;   
			ratingComment.ClipsToBounds = true;
			ratingComment.BackgroundColor = UIColor.LightGray;
			ratingComment.DataDetectorTypes = UIDataDetectorType.None;
			ratingComment.KeyboardType = UIKeyboardType.Default;
			ratingComment.ReturnKeyType = UIReturnKeyType.Send;
			ratingComment.Editable = true;
			//ratingComment.UserInteractionEnabled = true;
			ratingComment.ShouldChangeText = (text, range, replacementString) =>
			{
				if (replacementString.Equals("\n"))
				{
					ratingComment.EndEditing(true);

					if (myRating > 0)
					{
						BlurView(true);
						RatingItem rate = new RatingItem();
						rate.Rating = myRating;
						rate.ShopId = _currentShop;
						rate.Text = ratingComment.Text;
						ViewModel.RateShop(rate);
					}
					return false;
				}
				else
				{
					return true;
				}
			};

			bckImageRate.AddSubview (ratingComment);
		}

		private void SetRating()
		{
			int rating = (int)ViewModel.Item.Rating;
			if (rating >= 1) {
				SetStoreRating (true, 1);
			}
			if (rating >= 2) {
				SetStoreRating (true, 2);
			}
			if (rating >= 3) {
				SetStoreRating (true, 3);
			}
			if (rating >= 4) {
				SetStoreRating (true, 4);
			}
			if (rating >= 5) {
				SetStoreRating (true, 5);
			}
		}

		private void SetStoreRating(bool animated, int rating)
		{
			var star = new CGRect ((Frame.Width / 15) * (rating + 4), Frame.Height / 2.9, Frame.Width / 20, Frame.Width / 20);
			if (!animated) {
				if (rating == 1) {
					storeStarOne = new UIImageView (star);
					storeStarOne.Image = UIImage.FromBundle ("Images/star_gray.png");
					storeBckImage.AddSubview (storeStarOne);
				} else if (rating == 2) {
					storeStarTwo = new UIImageView (star);
					storeStarTwo.Image = UIImage.FromBundle ("Images/star_gray.png");
					storeBckImage.AddSubview (storeStarTwo);
				} else if (rating == 3) {
					storeStarThree = new UIImageView (star);
					storeStarThree.Image = UIImage.FromBundle ("Images/star_gray.png");
					storeBckImage.AddSubview (storeStarThree);
				} else if (rating == 4) {
					storeStarFour = new UIImageView (star);
					storeStarFour.Image = UIImage.FromBundle ("Images/star_gray.png");
					storeBckImage.AddSubview (storeStarFour);
				} else if (rating == 5) {
					storeStarFive = new UIImageView (star);
					storeStarFive.Image = UIImage.FromBundle ("Images/star_gray.png");
					storeBckImage.AddSubview (storeStarFive);
				}
			} else {
				if (rating == 1) {
					storeStarOne.RemoveFromSuperview ();
				} else if (rating == 2) {
					storeStarTwo.RemoveFromSuperview ();
				} else if (rating == 3) {
					storeStarThree.RemoveFromSuperview ();
				} else if (rating == 4) {
					storeStarFour.RemoveFromSuperview ();
				} else if (rating == 5) {
					storeStarFive.RemoveFromSuperview ();
				}
				UIImageView stars = new UIImageView (star);
				stars.Image = UIImage.FromBundle ("Images/star_white.png");

				CABasicAnimation rotationAnimation = new CABasicAnimation();
				rotationAnimation.TimingFunction = CAMediaTimingFunction.FromName (CAMediaTimingFunction.EaseInEaseOut);
				rotationAnimation.KeyPath = "transform.rotation.z";
				rotationAnimation.To = new NSNumber(Math.PI * 2);

				float time = rnd.Next (50, 150);
				double dTime = time / 100;

				rotationAnimation.Duration = dTime;
				rotationAnimation.Cumulative = true;
				rotationAnimation.RepeatCount = 1;
				stars.Layer.AddAnimation(rotationAnimation, "rotationAnimation");
				storeBckImage.AddSubview (stars);
			}
		}
			
		private void SetContactRating(bool animated, int rating)
		{
			if (!animated) {
				
				// Add gray stars
				rating = 1;
				var star = new CGRect ((Frame.Width / 9) * (1 + rating), 36, Frame.Width / 10, Frame.Width / 10);
				if (myStarOne == null) {
					myStarOne = new UIImageView (star);
				}
				else {
					myStarOne.RemoveFromSuperview ();
				}
				myStarOne.Image = UIImage.FromBundle ("Images/star_gray.png");
				myStarOne.UserInteractionEnabled = true;
				starOne = new UITapGestureRecognizer ((g) => 
					{
						SetContactRating(true, 1);
					});
				
				myStarOne.AddGestureRecognizer (starOne);
				bckImageRate.AddSubview (myStarOne);

				rating++;
				star = new CGRect ((Frame.Width / 9) * (1 + rating), 36, Frame.Width / 10, Frame.Width / 10);
				if (myStarTwo == null) {
					myStarTwo = new UIImageView (star);
				}
				else {
					myStarTwo.RemoveFromSuperview ();
				}
				myStarTwo.Image = UIImage.FromBundle ("Images/star_gray.png");
				myStarTwo.UserInteractionEnabled = true;
				starTwo = new UITapGestureRecognizer ((g) => 
					{
						SetContactRating(true, 2);
					});
				myStarTwo.AddGestureRecognizer (starTwo);
				bckImageRate.AddSubview (myStarTwo);

				rating++;
				star = new CGRect ((Frame.Width / 9) * (1 + rating), 36, Frame.Width / 10, Frame.Width / 10);
				if (myStarThree == null) {
					myStarThree = new UIImageView (star);
				}
				else {
					myStarThree.RemoveFromSuperview ();
				}
				myStarThree.Image = UIImage.FromBundle ("Images/star_gray.png");
				myStarThree.UserInteractionEnabled = true;
				starThree = new UITapGestureRecognizer ((g) => 
					{
						SetContactRating(true, 3);
					});
				myStarThree.AddGestureRecognizer (starThree);
				bckImageRate.AddSubview (myStarThree);

				rating++;
				star = new CGRect ((Frame.Width / 9) * (1 + rating), 36, Frame.Width / 10, Frame.Width / 10);
				if (myStarFour == null) {
					myStarFour = new UIImageView (star);
				} else {
					myStarFour.RemoveFromSuperview ();
				}
				myStarFour.Image = UIImage.FromBundle ("Images/star_gray.png");
				myStarFour.UserInteractionEnabled = true;
				starFour = new UITapGestureRecognizer ((g) => 
					{
						SetContactRating(true, 4);
					});
				myStarFour.AddGestureRecognizer (starFour);
				bckImageRate.AddSubview (myStarFour);

				rating++;
				star = new CGRect ((Frame.Width / 9) * (1 + rating), 36, Frame.Width / 10, Frame.Width / 10);
				if (myStarFive == null) {
					myStarFive = new UIImageView (star);
				} else {
					myStarFive.RemoveFromSuperview ();
				}
				myStarFive.Image = UIImage.FromBundle ("Images/star_gray.png");
				myStarFive.UserInteractionEnabled = true;
				starFive = new UITapGestureRecognizer ((g) => 
					{
						SetContactRating(true, 5);
					});
				myStarFive.AddGestureRecognizer (starFive);
				bckImageRate.AddSubview (myStarFive);

			} else {

				// Set globalvariable
				myRating = rating;

				if (rating == 0) {
					myStarOne.RemoveFromSuperview ();
					myStarTwo.RemoveFromSuperview ();
					myStarThree.RemoveFromSuperview ();
					myStarFour.RemoveFromSuperview ();
					myStarFive.RemoveFromSuperview ();
				}

				// Remove gray stars
				if (rating >= 1) {
					myStarOne.RemoveFromSuperview ();
				} 
				if (rating >= 2) {
					myStarTwo.RemoveFromSuperview ();
				}  
				if (rating >= 3) {
					myStarThree.RemoveFromSuperview ();
				} 
				if (rating >= 4) {
					myStarFour.RemoveFromSuperview ();
				} 
				if (rating >= 5) {
					myStarFive.RemoveFromSuperview ();
				}

				CABasicAnimation rotationAnimation = new CABasicAnimation();
				rotationAnimation.TimingFunction = CAMediaTimingFunction.FromName (CAMediaTimingFunction.EaseInEaseOut);
				rotationAnimation.KeyPath = "transform.rotation.z";
				rotationAnimation.To = new NSNumber(Math.PI * 2);

				rotationAnimation.Cumulative = false;
				rotationAnimation.RepeatCount = 1;
				for (int i = 0; i <= rating; i++)
				{
					float time = rnd.Next (50, 100);
					double dTime = time / 100;

					rotationAnimation.Duration = dTime;

					if (i == 1) {
						myStarOne.Image = UIImage.FromBundle ("Images/star_black.png");
						myStarOne.Layer.AddAnimation(rotationAnimation, "rotationAnimation");
						bckImageRate.AddSubview (myStarOne);
					} 
					if (i == 2) {
						myStarTwo.Image = UIImage.FromBundle ("Images/star_black.png");
						myStarTwo.Layer.AddAnimation(rotationAnimation, "rotationAnimation");
						bckImageRate.AddSubview (myStarTwo);
					}  
					if (i == 3) {
						myStarThree.Image = UIImage.FromBundle ("Images/star_black.png");
						myStarThree.Layer.AddAnimation(rotationAnimation, "rotationAnimation");
						bckImageRate.AddSubview (myStarThree);
					} 
					if (i == 4) {
						myStarFour.Image = UIImage.FromBundle ("Images/star_black.png");
						myStarFour.Layer.AddAnimation(rotationAnimation, "rotationAnimation");
						bckImageRate.AddSubview (myStarFour);
					} 
					if (i == 5) {
						myStarFive.Image = UIImage.FromBundle ("Images/star_black.png");
						myStarFive.Layer.AddAnimation(rotationAnimation, "rotationAnimation");
						bckImageRate.AddSubview (myStarFive);
					}
				}

				// Add missing gray stars
				if (rating < 1)
				{
					myStarOne.Image = UIImage.FromBundle ("Images/star_gray.png");
					bckImageRate.AddSubview (myStarOne);
				}

				if (rating < 2)
				{
					myStarTwo.Image = UIImage.FromBundle ("Images/star_gray.png");
					bckImageRate.AddSubview (myStarTwo);
				}

				if (rating < 3)
				{
					myStarThree.Image = UIImage.FromBundle ("Images/star_gray.png");
					bckImageRate.AddSubview (myStarThree);
				}

				if (rating < 4)
				{
					myStarFour.Image = UIImage.FromBundle ("Images/star_gray.png");
					bckImageRate.AddSubview (myStarFour);
				}

				if (rating < 5)
				{
					myStarFive.Image = UIImage.FromBundle ("Images/star_gray.png");
					bckImageRate.AddSubview (myStarFive);
				}
			}
		}

		private void SetInfoText()
		{
			storeName.TextAlignment = UITextAlignment.Center;
			storeName.AttributedText = new NSAttributedString(ViewModel.Item.Name, GlobalData.HeaderAttrib);

			storeLocation.TextAlignment = UITextAlignment.Center;
			storeLocation.AttributedText = new NSAttributedString(ViewModel.Item.Street.ToUpper() + ", " + 
			ViewModel.Item.PostalCode + " " + ViewModel.Item.City.ToUpper(), GlobalData.CountryAttrib);
		}
			
		private void SetButtomBarRate()
		{
			if (buttomBarRate == null) {
				buttomBarRate = new UIImageView (new CGRect (0, Frame.Height / 2 - Frame.Height / 25, Frame.Width, Frame.Height / 11));
				buttomBarRate.Image = GlobalData.GrayBackGround;

				UITapGestureRecognizer gestureRecognizer = new UITapGestureRecognizer (() => 
				{
					if (buttomBarRate != null)
					{
						if (_ratingsView == null) {
							_ratingsView = new RatingsView (viewcontroller, _currentShop);
						}
						
							map._headerShop.AttributedText = new NSAttributedString(NSBundle.MainBundle.LocalizedString ("RatingsText", "RatingsText"), GlobalData.HeaderAttrib);

						this.Add (_ratingsView);
						AnimateSubView ();
					}
				});
						
				UILabel label = new UILabel (new CGRect (0, 0, Frame.Width-(Frame.Height / 44+Frame.Width / 11), Frame.Height / 11));
				label.Text = NSBundle.MainBundle.LocalizedString ("UserReviewsText", "UserReviewsText");
				label.TextColor = UIColor.White;
				label.TextAlignment = UITextAlignment.Center;
				label.Font = UIFont.FromName ("Roboto-Regular", 18f);

				buttomBarRate.Add (label);
									
				UIImageView plusImage = new UIImageView (new CGRect (Frame.Width - (Frame.Height / 44+Frame.Width / 11), 
					Frame.Height / 44 , Frame.Width / 10, Frame.Width / 10));
				plusImage.Image = UIImage.FromBundle ("Images/plus.png");
				plusImage.UserInteractionEnabled = true;
		
				buttomBarRate.AddGestureRecognizer (gestureRecognizer); 
				buttomBarRate.Add (plusImage);
			}
			buttomBarRate.UserInteractionEnabled = true;
			bckImageRate.AddSubview (buttomBarRate);
		}

		public void AnimateSubView()
		{
			if (_ratingsView != null) {

				nfloat width = IsRatingsVisible == true ? Frame.Width : 0;  
				CGRect endframe = new CGRect(width, 0 - Frame.Height / 10, Frame.Width, Frame.Height + Frame.Height / 10 * 2);

				if (IsRatingsVisible) {
					System.Threading.ThreadPool.QueueUserWorkItem (state => InvokeOnMainThread (() => 
						UIView.AnimateNotify (0.4, 0, 10.1f, 2f, UIViewAnimationOptions.TransitionCurlDown, () => {
							_ratingsView.Frame = endframe;

						}, finished => {
							IsRatingsVisible = !IsRatingsVisible;
							DropSubView ();
						})));
				} else {
					System.Threading.ThreadPool.QueueUserWorkItem (state => InvokeOnMainThread (() => 
						UIView.AnimateNotify (0.4, 0, 15.1f, 3f, UIViewAnimationOptions.TransitionFlipFromRight, () => {
							_ratingsView.Frame = endframe;

						}, finished => {
							IsRatingsVisible = !IsRatingsVisible;
						})));
				}
			}
		}
			
		void DropSubView()
		{
			if (_ratingsView != null) {
				_ratingsView.RemoveFromSuperview ();
				_ratingsView.Dispose ();
				_ratingsView = null;
			}
		}
			
		private void SetButtomBarContact()
		{
			if (buttomBarContact == null) {
				buttomBarContact = new UIImageView (new CGRect (0, Frame.Height / 2 - Frame.Height / 25, Frame.Width, Frame.Height / 11));
				buttomBarContact.Image = GlobalData.GrayBackGround;

				UILabel label = new UILabel (new CGRect (0, Frame.Height / 20, Frame.Width / 3, Frame.Height / 20));
				label.Text = NSBundle.MainBundle.LocalizedString ("CALL", "CALL");
				label.TextColor = UIColor.White;
				label.TextAlignment = UITextAlignment.Center;
				label.Font = UIFont.FromName ("Roboto-Regular", 12f);
				buttomBarContact.AddSubview (label);

				label = new UILabel (new CGRect (Frame.Width / 3, Frame.Height / 20, Frame.Width / 3, Frame.Height / 20));
				label.Text = NSBundle.MainBundle.LocalizedString ("ROUTE", "ROUTE");
				label.TextColor = UIColor.White;
				label.TextAlignment = UITextAlignment.Center;
				label.Font = UIFont.FromName ("Roboto-Regular", 12f);
				buttomBarContact.AddSubview (label);

				label = new UILabel (new CGRect (Frame.Width / 3 * 2, Frame.Height / 20, 
					Frame.Width / 3, Frame.Height / 20));
				label.Text = NSBundle.MainBundle.LocalizedString ("WebSiteText", "WebSiteText");
				label.TextColor = UIColor.White;
				label.TextAlignment = UITextAlignment.Center;
				label.Font = UIFont.FromName ("Roboto-Regular", 12f);
				buttomBarContact.AddSubview (label);

				imageCall = new UIImageView (new CGRect (Frame.Width / 30 * 4, Frame.Height / 100, Frame.Width / 15, Frame.Width / 7));
				imageCall.Image = UIImage.FromBundle ("Images/Phone_128x128.png");

				UITapGestureRecognizer callTap = new UITapGestureRecognizer (() => 
					{
						if ((ViewModel.Item.Phone != "")&&(ViewModel.Item.Phone  != null))
						{	

						    UIAlertView alert = new UIAlertView (NSBundle.MainBundle.LocalizedString ("Call", "Call") + " " + ViewModel.Item.Phone + " ?", "", null, 
								NSBundle.MainBundle.LocalizedString ("Ok", "Ok"), 
								NSBundle.MainBundle.LocalizedString ("Cancel", "Cancel")); 
						        alert.Show ();

						    alert.Clicked += (sender, buttonArgs) => {
							if (buttonArgs.ButtonIndex == 0) { // OK Clicked

								NSUrl url = new NSUrl ("tel://" + ViewModel.Item.Phone);
						
								if (!UIApplication.SharedApplication.OpenUrl (url)) {
										var av = new UIAlertView (NSBundle.MainBundle.LocalizedString ("NotSupportedText", "NotSupportedText")
										, NSBundle.MainBundle.LocalizedString ("OnThisDeviceText", "OnThisDeviceText")
										, null
										, NSBundle.MainBundle.LocalizedString ("OK", "OK")  
										, null);
									av.Show ();
								}
							}
						};
					  }
				      else
					  {
							UIAlertView alert = new UIAlertView (NSBundle.MainBundle.LocalizedString ("MissingNumberText", "MissingNumberText"), "", null, 
								NSBundle.MainBundle.LocalizedString ("Ok", "Ok")); 
						 alert.Show ();

						 alert.Clicked += (snd, buttonArgs) => {
								if (buttonArgs.ButtonIndex == 0) 
								{ // OK Clicked
							
								}
						 };
					  }
				  });
				imageCall.UserInteractionEnabled = true;
				imageCall.AddGestureRecognizer (callTap);
				buttomBarContact.Add (imageCall);

				imageRoute = new UIImageView (new CGRect (Frame.Width / 30 * 14, Frame.Height / 100, Frame.Width / 15, Frame.Width / 7));
				imageRoute.Image = UIImage.FromBundle ("Images/Pin_128x128.png");

				UITapGestureRecognizer routeTap = new UITapGestureRecognizer (() => 
				{
					if (GPSHelper.mylocation != null)
					{
						CLLocationCoordinate2D mycoord2D = new CLLocationCoordinate2D(GPSHelper.mylocation.Coordinate.Latitude, GPSHelper.mylocation.Coordinate.Longitude);

						var emptyDict = new NSDictionary();

						var conferenceMapItem = new MKMapItem(new MKPlacemark(mycoord2D, emptyDict));
							conferenceMapItem.Name = "Me"; 

						var conferenceHotel = new MKMapItem(new MKPlacemark(new CLLocationCoordinate2D(ViewModel.Item.Latitude, ViewModel.Item.Longitude), emptyDict));
							conferenceHotel.Name = "End"; 

						var mapItems = new MKMapItem[] {conferenceMapItem,  conferenceHotel};
						MKMapItem.OpenMaps(mapItems, new MKLaunchOptions() { 
						DirectionsMode = MKDirectionsMode.Driving });
					}
					else
					{
						UIAlertView alert = new UIAlertView (NSBundle.MainBundle.LocalizedString ("GPSNotAvailableText", "GPSNotAvailableText"), "", null, 
						NSBundle.MainBundle.LocalizedString ("Ok", "Ok")); 
						alert.Show ();
					}
				});
				
				imageRoute.UserInteractionEnabled = true;
				imageRoute.AddGestureRecognizer (routeTap);
				buttomBarContact.Add (imageRoute);

				imageWeb = new UIImageView (new CGRect (Frame.Width / 30 * 24, Frame.Height / 100, Frame.Width / 15, Frame.Width / 7));
				imageWeb.Image = UIImage.FromBundle ("Images/Web_128x128.png");

				UITapGestureRecognizer websiteTap = new UITapGestureRecognizer (() => 
				{
					if (!string.IsNullOrEmpty(ViewModel.Item.Link))
					{		
						try 
						{
							webView = new UIWebView(this.Bounds);
							this.AddSubview (webView);
							webView.ScalesPageToFit = true;
				
							webView.LoadRequest(new NSUrlRequest(new NSUrl(ViewModel.Item.Link)));

							var ctrl = viewcontroller as SavingsViewController;
							ctrl.StartSpinner ();
												
							webView.LoadFinished+= (object sender, EventArgs e) => 
							{
								map._headerShop.AttributedText = new NSAttributedString(NSBundle.MainBundle.LocalizedString ("WebSiteText", "WebSiteText"), GlobalData.HeaderAttrib);
								IsWebVisible = true;
								ctrl.StopSpinner ();
							};

							webView.LoadError+= (object sender, UIWebErrorArgs e) => 
							{
								ctrl.StopSpinner ();
							
								UIAlertView alert = new UIAlertView (NSBundle.MainBundle.LocalizedString ("InvalidLinkText", "InvalidLinkText"), "", null,
								NSBundle.MainBundle.LocalizedString ("Ok", "Ok")); 
								alert.Show ();

								alert.Clicked += (snd, buttonArgs) => {
								  if (buttonArgs.ButtonIndex == 0) 
								  { // OK Clicked
								   	IsWebVisible = false;
									webView.RemoveFromSuperview ();
								  }
						    	};
							};
						}
						catch 
						{
							
						}
				   	}
					else
					{
						UIAlertView alert = new UIAlertView (NSBundle.MainBundle.LocalizedString ("NoWebLinkText", "NoWebLinkText"), "", null, 
						NSBundle.MainBundle.LocalizedString ("Ok", "Ok")); 
						alert.Show ();	
					}
				});
				imageWeb.UserInteractionEnabled = true;
				imageWeb.AddGestureRecognizer (websiteTap);
				buttomBarContact.Add (imageWeb);
			}
			buttomBarContact.UserInteractionEnabled = true;
			bckImageContact.AddSubview (buttomBarContact);
		}

		public void CancelWeb()
		{
			IsWebVisible = false;
			webView.RemoveFromSuperview ();

		}

		void LeftBtn_TouchUpInside (object sender, EventArgs e)
		{
			if (ratingComment != null) {
				ratingComment.EndEditing (true);
				SetStoreRating (false, 0);
				myRating = 0;
			}

			btnContact.BackgroundColor = UIColor.White;
			btnContact.SetAttributedTitle(new NSAttributedString("CONTACT", GlobalData.TapBarWhiteAttrib), UIControlState.Normal);

			btnRate.BackgroundColor = UIColor.Black;
			btnRate.SetAttributedTitle(new NSAttributedString("RATE STORE", GlobalData.TapBarBlackAttrib), UIControlState.Normal);

			bckImageRate.RemoveFromSuperview ();

			Add (bckImageContact);
		}

		void RightBtn_TouchUpInside (object sender, EventArgs e)
		{
			if (ratingComment != null) {
				ratingComment.EndEditing (true);
				SetStoreRating (false, 0);
				myRating = 0;
			}

			btnContact.BackgroundColor = UIColor.Black;
			btnContact.SetAttributedTitle(new NSAttributedString(NSBundle.MainBundle.LocalizedString ("ContactText", "ContactText"), GlobalData.TapBarBlackAttrib), UIControlState.Normal);

			btnRate.BackgroundColor = UIColor.White;
			btnRate.SetAttributedTitle(new NSAttributedString(NSBundle.MainBundle.LocalizedString ("RateStoreText", "RateStoreText"), GlobalData.TapBarWhiteAttrib), UIControlState.Normal);

			bckImageContact.RemoveFromSuperview ();
			Add (bckImageRate);

		}
			
		private bool _visible; 
		public bool Visible 
		{
			get{ return _visible;}
			set 
			{
				_visible = value;
			}
		}

		private bool _isVisible; 
		public bool IsButtomBarVisible 
		{
			get{ return _isVisible;}
			set 
			{
				_isVisible = value;
			}
		}

		private async void BlurView(bool isVisible)
		{
			if (isVisible) {
				if (blurOverlay == null) 
				{
					blurOverlay = new UIImageView (new CGRect (0, 0, 
						Frame.Width, Frame.Height - Frame.Height / 11));
					blurOverlay.Image = GlobalData.GrayBackGround;

					UILabel label = new UILabel(new CGRect (0, Frame.Height / 3, 
						Frame.Width, Frame.Height / 11));
					label.Text = "Saving...";
					label.TextColor = UIColor.White;
					label.TextAlignment = UITextAlignment.Center;
					label.Font = UIFont.FromName("Roboto-Regular", 18f);
					blurOverlay.AddSubview (label);

					UIActivityIndicatorView Spinner = new UIActivityIndicatorView (new CGRect (0, Frame.Height / 3 + 30, 
						Frame.Width, Frame.Height / 11));
					Spinner.StartAnimating ();
					blurOverlay.Add (Spinner);

					Add (blurOverlay);
				}
				blurOverlay.Layer.Opacity = 0;

				await UIView.AnimateAsync (0.2f, () => {
					blurOverlay.Layer.Opacity = 0.7f;
				});
			} 
			else 
			{
				if (blurOverlay != null) 
				{
					blurOverlay.Layer.Opacity = 0.7f;
					await UIView.AnimateAsync (0.2f, () => {
						blurOverlay.Layer.Opacity = 0f;
					});

					blurOverlay.RemoveFromSuperview ();
					blurOverlay.Dispose ();
					blurOverlay = null;
				}
			}
		}

		private void MoveRating(CGRect r, bool show)
		{
			nfloat height = show == false ?  Frame.Height / 2 - Frame.Height / 11 + Frame.Height / 25 : 
				Frame.Height / 2 + Frame.Height / 11 - r.Height;  
			CGRect endframe = new CGRect (0, height, Frame.Width, Frame.Height / 2);

			System.Threading.ThreadPool.QueueUserWorkItem (state => InvokeOnMainThread (() => 
			UIView.AnimateNotify (0.5, 0.0, GlobalData.SpringDampRation, GlobalData.SpringVelocity, 0, () => 
			{
				bckImageRate.Frame = endframe;

			}, finished => {
				Visible = !Visible;
			})));
		}


		NSObject _keyboardObserverWillShow;
		NSObject _keyboardObserverWillHide;

		protected virtual void RegisterForKeyboardNotifications ()
		{
			_keyboardObserverWillShow = NSNotificationCenter.DefaultCenter.AddObserver (UIKeyboard.WillShowNotification, KeyboardWillShowNotification);
			_keyboardObserverWillHide = NSNotificationCenter.DefaultCenter.AddObserver (UIKeyboard.WillHideNotification, KeyboardWillHideNotification);
		}

		protected virtual void UnregisterKeyboardNotifications()
		{
			NSNotificationCenter.DefaultCenter.RemoveObserver(_keyboardObserverWillShow);
			NSNotificationCenter.DefaultCenter.RemoveObserver(_keyboardObserverWillHide);
		}

		protected virtual void KeyboardWillShowNotification (NSNotification notification)
		{
			CGRect r = UIKeyboard.BoundsFromNotification (notification);
			MoveRating (r, true);
		}

		protected virtual void KeyboardWillHideNotification (NSNotification notification)
		{
			CGRect r = UIKeyboard.BoundsFromNotification (notification);
			MoveRating (r, false);
		}
	}

	class BasicMapAnnotation : MKAnnotation
	{
		private CLLocationCoordinate2D _Coordinate;
		string title, subtitle;
		public override string Title { get{ return title; }}
		public override string Subtitle { get{ return subtitle; }}
		public BasicMapAnnotation (CLLocationCoordinate2D coordinate, string title, string subtitle) {
			_Coordinate = coordinate;
			this.title = title;
			this.subtitle = subtitle;
		}

		public override CLLocationCoordinate2D Coordinate {
			get {

				return _Coordinate;
			}
		}


	}
}

