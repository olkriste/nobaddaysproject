﻿using System;
using CoreGraphics;
using UIKit;
using System.Timers;
using Foundation;
using System.Collections.Generic;
using NoBadDaysPCL;
using System.IO;
using SQLite.Net.Platform.XamarinIOS;
using Valore.IOSDropDown;
using Security;
using Utils;
using NoBadDaysPCL.Items;
using NoBadDaysPCL.ViewModel;
using System.Threading.Tasks;
using System.Net;
using System.Drawing;
using MapKit;
using CoreLocation;

namespace nobaddaysIOS
{
	public class MapView: UIView
	{
		UIViewController viewcontroller;
	//	UITextView header;
		UIImageView bckImage;

		MKMapView map = null;

		// Views
		StoreView _storeView;

		UILabel _header;
		public UILabel _headerShop;

		UIImageView _bckButton;
		UIImageView _searchImage = null;
	
		// Gesture 
		UITapGestureRecognizer bckTap;
		UITapGestureRecognizer gestureRecognizer;

		int _shopId;

		List<DropDownListItem> _categoryDropDownList = new List<DropDownListItem>();

	
		DropDownList _ddl;

		MainClass.eShopType _shopType = MainClass.eShopType.None;


		public MapView (object controller)
		{
			viewcontroller = controller as UIViewController;
			var frame = viewcontroller.View.Bounds;
			Frame = new CGRect(0, 0, frame.Width, frame.Height);

			SetHeaderText (frame);
			SetMap (frame);
			SetSearchImage (frame);
		}

		private MapViewModel _viewModel = null;
		private MapViewModel ViewModel 
		{
			get 
			{ 
				if (_viewModel == null) 
				{
					string libraryPath = System.Environment.GetFolderPath (System.Environment.SpecialFolder.Personal);
					var path = Path.Combine (libraryPath, Container.SQLitePath);

					_viewModel = new MapViewModel (new SQLitePlatformIOS (), path);
					_viewModel.PhoneDataSuccessful += DataSuccessful;
					_viewModel.PhoneDataErrorOccurred += DataError;
				} 
				return _viewModel;
			}
		}

		void DataSuccessful(object sender, ServerOutputItem e)
		{
			string mapPin = null;;
			bool isOnMap = false;
			foreach (MapItem item in ViewModel.Items) 
			{
				isOnMap = false;
				foreach (ShopAnnotation s in map.Annotations) 
				{
					if(s.ShopId == item.ShopId && s.Latitude == item.Latitude && s.Longitude == item.Longitude)
					{
						isOnMap = true;
						break;
					}
				}
				if (!isOnMap) 
				{
					map.AddAnnotations (new ShopAnnotation (item.Name, 
						new CLLocationCoordinate2D (item.Latitude, item.Longitude), item));
				}
			}

			foreach (ShopAnnotation s in map.Annotations) 
			{
				isOnMap = false;
				foreach (MapItem item in ViewModel.Items) 
				{
					if (item.ShopId == s.ShopId && s.Latitude == item.Latitude && s.Longitude == item.Longitude) {
						isOnMap = true;
						break;
					}
				}

				if (!isOnMap) 
				{
					map.RemoveAnnotation (s);
				}
			}
		}
			
		void DataError(object sender, ServerOutputItem e)
		{
		}


			
		private void SetHeaderText(CGRect frame)
		{
	
			var tmpFrame = new CGRect(0, 0, frame.Width, frame.Height / 10);
			_header = new UILabel(tmpFrame);
			_header.TextAlignment = UITextAlignment.Center;
			_header.AttributedText = new NSAttributedString(NSBundle.MainBundle.LocalizedString ("MapText", "MapText"), GlobalData.HeaderAttrib);

			Add (_header);
		}

		private void SetSearchImage(CGRect frame)
		{
			if (_searchImage == null) {
				_searchImage = new UIImageView (new CGRect (frame.Width-40, 20, frame.Height / 18, frame.Height / 18));
				_searchImage.Image = UIImage.FromBundle ("Images/search.png");

				gestureRecognizer = new UITapGestureRecognizer ((g) => 
				{
					if ((_searchImage != null)&&(_ddl == null))
					{
						GlobalData.initCategoryList();
						_categoryDropDownList.Clear();

							for (int i=0;i<GlobalData.categoryList.Count;i++)
						{	
							_categoryDropDownList.Add(new DropDownListItem()
							{
								Id = (i+1).ToString(),
								DisplayText = GlobalData.categoryList[i],
								Image = null
							});
								
						}
								
						_ddl = new DropDownList(map ,_categoryDropDownList.ToArray())
						{
							BackgroundColor = UIColor.FromCGColor(new CGColor(255f/255f, 63f/255f, 63f/255f, 255f/255f)), //FromRGB(255, 255, 255),
							Opacity = 0.85f,
							TintColor = UIColor.Black,
							NavigationBarAssumedHeight = 1
						};
					}
							
					_ddl.DropDownListChanged += (e, a) =>
					{
						var strValue = a.DisplayText; //a is the dropdown list item object

						int id;
						if (!int.TryParse(a.Id, out id))
						{	
							if (id < 1)
							{	
								id = 1;
							}
						}
						id = id-1;
				
						_ddl.Toggle();

						_ddl.RemoveFromSuperview();

						_ddl = null;

					   	MKMapRect mRect = map.VisibleMapRect;
					    MKMapPoint neMapPoint = new MKMapPoint (mRect.MaxX, mRect.Origin.Y);
					    MKMapPoint swMapPoint = new MKMapPoint (mRect.Origin.X, mRect.MaxY);
					    CLLocationCoordinate2D neCoord = MKMapPoint.ToCoordinate (neMapPoint);
					    CLLocationCoordinate2D swCoord = MKMapPoint.ToCoordinate (swMapPoint);

					    MapSearchItem item = new MapSearchItem ();
					    item.Gridx = 10;
					    item.Gridy = 10;
					    item.Nelat = neCoord.Latitude;
					    item.Nelon = neCoord.Longitude;
					    item.Swlat = swCoord.Latitude;
					    item.Swlon = swCoord.Longitude;
					    item.Zoomlevel = CalculateZoomLevel (map);
					    item.ZoomlevelClusterStop = 16;
					    _shopType = item.ShopType = (MainClass.eShopType)e;
					    ViewModel.GetShops (item);
					};
							
					map.Add(_ddl);
					map.BringSubviewToFront(_ddl);
					
		    		_ddl.Toggle ();
				});
			
				_searchImage.AddGestureRecognizer (gestureRecognizer);
				_searchImage.UserInteractionEnabled = true;

				Add (_searchImage);
			}
		}

	
		private void SetMap(CGRect frame)
		{
			map = new MKMapView (new CGRect(0, frame.Height / 10, frame.Width, frame.Height - frame.Height / 10));

			var customDelegate = new CustomMapViewDelegate(this);
			customDelegate.OnRegionChanged += MapView_OnRegionChanged;
			map.Delegate = customDelegate;
			Add (map);
		}

		public void MapView_OnRegionChanged(object sender, MKMapViewChangeEventArgs e)
		{
			MKMapRect mRect = map.VisibleMapRect;
			MKMapPoint neMapPoint = new MKMapPoint (mRect.MaxX, mRect.Origin.Y);
			MKMapPoint swMapPoint = new MKMapPoint (mRect.Origin.X, mRect.MaxY);
			CLLocationCoordinate2D neCoord = MKMapPoint.ToCoordinate (neMapPoint);
			CLLocationCoordinate2D swCoord = MKMapPoint.ToCoordinate (swMapPoint);

			MapSearchItem item = new MapSearchItem ();
			item.Gridx = 10;
			item.Gridy = 10;
			item.Nelat = neCoord.Latitude;
			item.Nelon = neCoord.Longitude;
			item.Swlat = swCoord.Latitude;
			item.Swlon = swCoord.Longitude;
			item.Zoomlevel = CalculateZoomLevel (map);
			item.ZoomlevelClusterStop = 16;
			item.ShopType = _shopType;
			ViewModel.GetShops (item);
		}

		public class CustomMapViewDelegate : MKMapViewDelegate
		{
			MapView mapViewTmp;

			static string annotationId = "ShopAnnotation";

			public event EventHandler<MKMapViewChangeEventArgs> OnRegionChanged;

			public override void RegionChanged(MKMapView mapView, bool animated)
			{
				if (OnRegionChanged != null)
				{
					OnRegionChanged(mapView, new MKMapViewChangeEventArgs(animated));
				}
			}

			public CustomMapViewDelegate(MapView view)
			{
				mapViewTmp = view;
			}
				
			public override void DidAddAnnotationViews (MKMapView mapView, MKAnnotationView[] views)
			{
				Random rnd = new Random (DateTime.UtcNow.Millisecond);
				foreach (MKAnnotationView v in views) 
				{
					CGRect endframe = v.Frame;
					CGRect startframe = endframe;
					startframe.X = v.Frame.X + (v.Frame.Width / 2);
					startframe.Y = v.Frame.Y + (v.Frame.Height / 2);
					startframe.Size = new CGSize (0, 0);
					v.Frame = startframe;

					float time = rnd.Next (50, 200);
					double dTime = time / 100;
					UIView.AnimateNotify (dTime, 0, 0.8f, 5f, UIViewAnimationOptions.CurveEaseInOut, () => 
					{
						v.Frame = endframe;

					}, finished => {

					});
				}
			}

			public override MKAnnotationView GetViewForAnnotation (MKMapView mapView, IMKAnnotation annotation)
			{
				MKAnnotationView annotationView = mapView.DequeueReusableAnnotation (annotationId);

				CLLocationCoordinate2D currentLocation = mapView.UserLocation.Coordinate;
				CLLocationCoordinate2D annotationLocation = annotation.Coordinate;

				if (currentLocation.Latitude == annotationLocation.Latitude & currentLocation.Longitude == annotationLocation.Longitude)
					return null;

				annotationView = (MKAnnotationView)mapView.DequeueReusableAnnotation (annotationId);

				if (annotationView == null) {
					annotationView = new MKAnnotationView (annotation, annotationId);
				} else {
					annotationView.Annotation = annotation;
				}

				annotationView.CanShowCallout = true;
				annotationView.SetSelected (true, false);

				ShopAnnotation anno = annotation as ShopAnnotation;
				UIButton _annotationDetailButton = UIButton.FromType (UIButtonType.DetailDisclosure);
				_annotationDetailButton.SetTitle(anno.ShopId.ToString(), UIControlState.Normal);
				_annotationDetailButton.TouchUpInside += (sender, e) => 
				{
					mapViewTmp.HandleTapEvent(anno.ShopId);
				};

				annotationView.RightCalloutAccessoryView = _annotationDetailButton;

				UIImageView pin = new UIImageView (new CGRect(0,0, 10,10));

				if (anno.ClusterCount <= 1) {
					if (anno.ShopType == NoBadDaysPCL.MainClass.eShopType.ClothingAndbeauty) {
						pin.Image = UIImage.FromBundle ("Map/ClothingMarker.png");
					} else if (anno.ShopType == NoBadDaysPCL.MainClass.eShopType.EatAndDrink) {
						pin.Image = UIImage.FromBundle ("Map/EatDrinkMarker.png");
					} else if (anno.ShopType == NoBadDaysPCL.MainClass.eShopType.Electronics) {
						pin.Image = UIImage.FromBundle ("Map/ElectronicsMarker.png");
					} else if (anno.ShopType == NoBadDaysPCL.MainClass.eShopType.Food) {
						pin.Image = UIImage.FromBundle ("Map/FoodMarker.png");
					} else if (anno.ShopType == NoBadDaysPCL.MainClass.eShopType.HomeAndGarden) {
						pin.Image = UIImage.FromBundle ("Map/HomeGardenMarker.png");
					} else if (anno.ShopType == NoBadDaysPCL.MainClass.eShopType.HotelsAndTravel) {
						pin.Image = UIImage.FromBundle ("Map/HotelTravelMarker.png");
					} else if (anno.ShopType == NoBadDaysPCL.MainClass.eShopType.KidsAndPlay) {
						pin.Image = UIImage.FromBundle ("Map/KidsMarker.png");
					} else if (anno.ShopType == NoBadDaysPCL.MainClass.eShopType.None) {
						pin.Image = UIImage.FromBundle ("Map/UnknownMarker.png");
					} else if (anno.ShopType == NoBadDaysPCL.MainClass.eShopType.Services) {
						pin.Image = UIImage.FromBundle ("Map/ServicesMarker.png");
					} else if (anno.ShopType == NoBadDaysPCL.MainClass.eShopType.SportFitnessAndOutdoor) {
						pin.Image = UIImage.FromBundle ("Map/SportMarker.png");
					}
				} else {
					if (anno.ClusterCount >= 2) {
						pin.Image = UIImage.FromBundle ("Map/two.png");
					} 
					if (anno.ClusterCount >= 3) {
						pin.Image = UIImage.FromBundle ("Map/three.png");
					}
					if (anno.ClusterCount >= 5) {
						pin.Image = UIImage.FromBundle ("Map/five.png");
					}
					if (anno.ClusterCount >= 10) {
						pin.Image = UIImage.FromBundle ("Map/ten.png");
					}
					if (anno.ClusterCount >= 20) {
						pin.Image = UIImage.FromBundle ("Map/twenty.png");
					}
					if (anno.ClusterCount >= 50) {
						pin.Image = UIImage.FromBundle ("Map/fifty.png");
					}
					if (anno.ClusterCount >= 100) {
						pin.Image = UIImage.FromBundle ("Map/hundred.png");
					}
				}

				UILabel label = new UILabel(new CGRect(0,0, 10,10));
				label.Text = "3";
				pin.AddSubview (label);

				pin.Layer.CornerRadius = 15.0f;

				annotationView.Image = pin.Image;
				annotationView.Opaque = true;

				return annotationView;
			}
				

			public override void DidSelectAnnotationView (MKMapView mapView, MKAnnotationView view)
			{
			}

			public override void DidDeselectAnnotationView (MKMapView mapView, MKAnnotationView view)
			{
			}

			public override MKOverlayView GetViewForOverlay (MKMapView mapView, IMKOverlay overlay)
			{
				// return a view for the polygon
				MKPolygon polygon = overlay as MKPolygon;
				MKPolygonView polygonView = new MKPolygonView (polygon);
				polygonView.FillColor = UIColor.Blue;
				polygonView.StrokeColor = UIColor.Red;
				return polygonView;
			}
		}

		private void HandleTapEvent(int shopId)
		{
			_shopId = shopId;

			//if (_storeView == null) {
				_storeView = new StoreView (viewcontroller, shopId, this);
			//}
		
			_searchImage.Hidden = true;
			_header.Hidden = true;
			map.UserInteractionEnabled = false;

			var tmpFrame = new CGRect(0, 0, Frame.Width, Frame.Height / 10);
			_headerShop = new UILabel(tmpFrame);
			_headerShop.TextAlignment = UITextAlignment.Center;
			_headerShop.AttributedText = new NSAttributedString("SHOP INFO", GlobalData.HeaderAttrib);

			Add (_headerShop);
			AddBckButton();

			this.Add (_storeView);
			AnimateSubView ();
		}

		private void AddBckButton() 
		{
			// Disable main wheel
			SavingsViewController view = viewcontroller as SavingsViewController;
			view.WheelUserInteractionEnabled (false);

			if (_bckButton == null) {
				_bckButton = new UIImageView (new CGRect (5, 5, Frame.Height / 11, Frame.Height / 11));
				_bckButton.Image = UIImage.FromBundle ("Images/back.png");
				_bckButton.UserInteractionEnabled = true;

				bckTap = new UITapGestureRecognizer((g) => 
					{
						if (_storeView != null && _storeView.IsRatingsVisible)
						{
							_storeView.AnimateSubView();
							_headerShop.AttributedText = new NSAttributedString(NSBundle.MainBundle.LocalizedString ("ShopInfoText", "ShopInfoText"), GlobalData.HeaderAttrib);
						}
						else if (_storeView != null && _storeView.IsWebVisible)
						{
							_headerShop.AttributedText = new NSAttributedString(NSBundle.MainBundle.LocalizedString ("ShopInfoText", "ShopInfoText"), GlobalData.HeaderAttrib);

							_storeView.CancelWeb();
						}
						else
						{
							AnimateSubView ();
						}
					});
				_bckButton.AddGestureRecognizer (bckTap);
			}
			Add (_bckButton);
		}

		static int CalculateZoomLevel(MKMapView mapView)
		{
			var zoomLevel = 20; // max zoom level
			var zoomScale = mapView.VisibleMapRect.Size.Width / mapView.Bounds.Size.Width;
			double zoomExponent = Math.Log(zoomScale);
			zoomLevel = (int)(20 - Math.Ceiling(zoomExponent));

			return zoomLevel;
		}

		void Changed(object sender, EventArgs e)
		{
			var barButtonItem = sender as UITextView;
			if (barButtonItem != null) 
			{
				
			}
		}

		private bool _visible; 
		public bool Visible 
		{
			get{ return _visible;}
			set 
			{
				_visible = value;
			}
		}

		private bool _storeVisible; 
		public bool IsStoreVisible 
		{
			get{ return _storeVisible;}
			set 
			{
				_storeVisible = value;
			}
		}

		void DropSubView()
		{
			if (_storeView != null) {
				_storeView.RemoveFromSuperview ();
				//_storeView.Dispose ();
				//_storeView = null;
			}
			SavingsViewController view = viewcontroller as SavingsViewController;
			view.WheelUserInteractionEnabled (true);
		}

		private void AnimateSubView()
		{
			if (_storeView != null) {

				nfloat width = IsStoreVisible == true ? Frame.Width : 0;  
				CGRect endframe = new CGRect(width, Frame.Height / 10, Frame.Width, Frame.Height - Frame.Height / 10);

				if (IsStoreVisible) {

					if (_bckButton != null) {
						_bckButton.RemoveFromSuperview ();
						_bckButton.Dispose ();
						_bckButton = null;
					}	

					if (_searchImage != null) {
						_searchImage.Hidden = false;
					}

					if (_headerShop != null) {
					    _headerShop.RemoveFromSuperview ();
					}

					if (_header != null) {
					    _header.Hidden = false;
					}

					System.Threading.ThreadPool.QueueUserWorkItem (state => InvokeOnMainThread (() => 
						UIView.AnimateNotify (0.4, 0, 10.1f, 2f, UIViewAnimationOptions.TransitionCurlDown, () => {
							_storeView.Frame = endframe;

						}, finished => {
							IsStoreVisible = !IsStoreVisible;
							DropSubView ();
							map.UserInteractionEnabled = true;
						})));
				} else {
					System.Threading.ThreadPool.QueueUserWorkItem (state => InvokeOnMainThread (() => 
						UIView.AnimateNotify (0.4, 0, 15.1f, 3f, UIViewAnimationOptions.TransitionCurlUp, () => {
							_storeView.Frame = endframe;

						}, finished => {
							IsStoreVisible = !IsStoreVisible;
						})));
				}
			}
		}

		/*
		private void initCategoryList()
		{
			_categoryList = new List<string> { 
				NSBundle.MainBundle.LocalizedString ("AllCategoriesText", "AllCategoriesText"),
				NSBundle.MainBundle.LocalizedString ("FoodText", "FoodText"),
				NSBundle.MainBundle.LocalizedString ("EatAndDrinkText", "EatAndDrinkText"),
				NSBundle.MainBundle.LocalizedString ("HotelAndTravelText", "HotelAndTravelText"),
				NSBundle.MainBundle.LocalizedString ("TicketsAndExperienencesText", "TicketsAndExperienencesText"),
				NSBundle.MainBundle.LocalizedString ("SportText", "SportText"),
				NSBundle.MainBundle.LocalizedString ("ServicesText", "ServicesText"),
				NSBundle.MainBundle.LocalizedString ("HomeAndGardenText", "HomeAndGardenText"),
				NSBundle.MainBundle.LocalizedString ("ClothingText", "ClothingText"),
				NSBundle.MainBundle.LocalizedString ("KidsText", "KidsText"),
				NSBundle.MainBundle.LocalizedString ("ElectronicsText", "ElectronicsText")
			};
		}*/
	}
}

