﻿using System;
using UIKit;
using Foundation;
using CoreGraphics;
using System.Threading.Tasks;
using NoBadDaysPCL.ViewModel;

namespace nobaddaysIOS
{
	public class RatingCell : UITableViewCell  {
		UILabel _commentLabel, _userLabel;
		UIImageView _ratingImage;
		public RatingCell (NSString cellId) : base (UITableViewCellStyle.Default, cellId)
		{
			SelectionStyle = UITableViewCellSelectionStyle.Gray;
			ContentView.BackgroundColor = UIColor.White;
		
			_ratingImage = new UIImageView();
		
			_commentLabel = new UILabel () {
				Font = UIFont.FromName("Roboto-Regular", 20f),
				TextColor = UIColor.Black,
				TextAlignment = UITextAlignment.Center,
				BackgroundColor = UIColor.Clear
			};

			_userLabel = new UILabel () {
				Font = UIFont.FromName("Roboto-Regular", 18f),
				TextColor = UIColor.DarkGray,
				TextAlignment = UITextAlignment.Center,
				BackgroundColor = UIColor.Clear,
				Text = NSBundle.MainBundle.LocalizedString ("NotSpecifiedText", "NotSpecifiedText")
			};

			ContentView.AddSubviews(new UIView[] {_commentLabel, _userLabel, _ratingImage});

		}

		public void UpdateCell (string comment, string name, int rating)
		{
			if ((comment != "")&&(comment != null))
			{	
				_commentLabel.Text = comment;
			}
			if ((name != "")&&(name != null))
			{	
				_userLabel.Text = name;
				_userLabel.TextColor = UIColor.Black;
			}



			if (rating == 1)
			{	
				_ratingImage.Image = UIImage.FromBundle("Images/OneStar");
			}
			else if (rating == 2)
			{	
				_ratingImage.Image = UIImage.FromBundle("Images/TwoStars");
			}
			else if (rating == 3)
			{	
				_ratingImage.Image = UIImage.FromBundle("Images/ThreeStars");
			}
			else if (rating == 4)
			{	
				_ratingImage.Image = UIImage.FromBundle("Images/FourStars");
			}
			else if (rating == 5)
			{	
				_ratingImage.Image = UIImage.FromBundle("Images/FiveStars");
			}

		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();

			var ratingWidth = ContentView.Bounds.Width / 2;
			var ratingHeight = ratingWidth * 60 / 320;

			_ratingImage.Frame = new CGRect (ContentView.Bounds.Width/4, 10, ratingWidth, ratingHeight);
			_commentLabel.Frame = new CGRect (0, ratingHeight+20, ContentView.Bounds.Width - 20, 30);
			_userLabel.Frame = new CGRect (0, ratingHeight+30, ContentView.Bounds.Width - 50, 80);

		}




	}
}

