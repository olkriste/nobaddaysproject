﻿using System;
using CoreGraphics;
using UIKit;
using System.Timers;
using Foundation;
using System.Collections.Generic;
using NoBadDaysPCL;
using System.IO;
using SQLite.Net.Platform.XamarinIOS;
using Valore.IOSDropDown;
using Security;
using Utils;
using NoBadDaysPCL.Items;
using NoBadDaysPCL.ViewModel;
using System.Threading.Tasks;
using System.Net;
using System.Drawing;
using System.Linq;
using CoreLocation;

namespace nobaddaysIOS
{
	public class RatingsView : UIView
	{
		public static NSString RatingCellId = new NSString ("RatingCell");

		GPSHelper _gpshelper = new GPSHelper();

		UIViewController _viewcontroller;

		UITableView _table;

		UILabel _noRatingsLabel;

		// Views
	
		UIImageView _bckButton;


		// Gesture 
		UITapGestureRecognizer _bckTap;
		UITapGestureRecognizer _gestureRecognizer;
	
		private bool _visible; 
		public bool Visible 
		{
			get{ return _visible;}
			set 
			{
				_visible = value;
			}
		}

		private bool _offerVisible; 
		public bool IsOfferVisible 
		{
			get{ return _offerVisible;}
			set 
			{
				_offerVisible = value;
			}
		}




		private static UIRefreshControl _myfresh;
		public static UIRefreshControl refreshControl
		{
			get { return _myfresh; }
			set { _myfresh = value; }
		}


		UserViewModel _viewModelUser = null;

		ShopViewModel _viewModelShop = null;



		public RatingsView (object controller, int shopId)
		{
			_viewcontroller = controller as UIViewController;
			var frame = _viewcontroller.View.Bounds;
			Frame = new CGRect(frame.Width, 0 - frame.Height / 11, frame.Width, frame.Height + frame.Height / 11);

			string libraryPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
			var path = System.IO.Path.Combine(libraryPath, nobaddaysIOS.Container.SQLitePath);

			_viewModelShop = new ShopViewModel(new SQLitePlatformIOS(), path);

			_viewModelShop.PhoneDataSuccessful += _viewModelShop_PhoneDataSuccessful;
			_viewModelShop.PhoneDataErrorOccurred += _viewModelShop_PhoneDataErrorOccurred;

		

			CLLocation mycoor = GPSHelper.mylocation;

	

			SetTable (_viewcontroller);

	

			_viewModelShop.GetShopRatings (shopId);


		}

		void _viewModelShop_PhoneDataErrorOccurred (object sender, ServerOutputItem e)
		{
	
			if (_viewModelShop.Items.Count > 0)
			{	
				_table.Source = new RatingTableSource (_viewModelShop);
				_table.ReloadData ();

				refreshControl.EndRefreshing();

			}
			else 
			{	
				_table.RemoveFromSuperview ();

			}	

		
		
			UIAlertView alert = new UIAlertView (e.ResultString, "", null, NSBundle.MainBundle.LocalizedString ("OK", "OK"), null);
			alert.Clicked += (sender1, buttonArgs) => 
			{

			};
			alert.Show ();
		}

		void _viewModelShop_PhoneDataSuccessful (object sender, ServerOutputItem e)
		{
	

			if (_viewModelShop.Items.Count > 0)
			{	
				_table.Source = new RatingTableSource (_viewModelShop);
				_table.ReloadData ();

				refreshControl.EndRefreshing();

				//    TicketsTable.Hidden = false;
				_table.SeparatorStyle = UITableViewCellSeparatorStyle.SingleLine;


			}
			else
			{
				var frame = _viewcontroller.View.Bounds;

				var tmpFrame = new CGRect(0, frame.Height/2, frame.Width, frame.Height / 10);
				_noRatingsLabel = new UILabel (tmpFrame) {
				Font = UIFont.FromName("Roboto-Regular", 18f),
				TextColor = UIColor.Black,
				BackgroundColor = UIColor.Clear,
				Text= NSBundle.MainBundle.LocalizedString ("NoRatingsText", "NoRatingsText")
				};

				Add(_noRatingsLabel);

			}
		}



		private void SetTable(UIViewController viewcontroller)
		{
			var height = viewcontroller.View.Frame.Height-viewcontroller.View.Frame.Height/11+2;

			var tmpFrame = new CGRect(0, viewcontroller.View.Frame.Height-height, viewcontroller.View.Frame.Width, height);

			_table = new UITableView(tmpFrame);

			_table.Source = new RatingTableSource (_viewModelShop);

			_table.SeparatorStyle = UITableViewCellSeparatorStyle.None;
			_table.ClearsContextBeforeDrawing = true;
			_table.AutoresizingMask = UIViewAutoresizing.All;

			_table.RowHeight = 120f;

			_table.Bounces = true;
			_table.AllowsSelection = false;
			_table.UserInteractionEnabled = true; 
			_table.ScrollEnabled = true;

			refreshControl = new UIRefreshControl ();
			refreshControl.TintColor = UIColor.LightGray;
			refreshControl.ValueChanged += HandleValueChanged;
			_table.AddSubview (refreshControl);
					
			Add (_table);
		}

		void HandleValueChanged (object sender, EventArgs e)
		{


			refreshControl.EndRefreshing ();

			if (_viewModelShop != null)
			{	
				
			
		//		_viewModelShop.GetOffers (_offerSearchItem);


			}

		}




		void DropSubView()
		{
			
			SavingsViewController view = _viewcontroller as SavingsViewController;
			view.WheelUserInteractionEnabled (true);

		}





	}

	public class RatingTableSource : UITableViewSource {



		ShopViewModel _viewModel;
	

		public RatingTableSource (ShopViewModel viewModelShop)
		{
			_viewModel = viewModelShop;

		}

		public override nint RowsInSection (UITableView tableview, nint section)
		{
			return _viewModel.Items.Count;
		}

		/// <summary>
		/// Called by the table view to determine whether or not the row is editable
		/// </summary>
		public override bool CanEditRow (UITableView tableView, NSIndexPath indexPath)
		{
			return false;
		}

		/// <summary>
		/// Called by the table view to determine whether or not the row is moveable
		/// </summary>
		public override bool CanMoveRow (UITableView tableView, NSIndexPath indexPath)
		{
			return false;
		}

		public override nint NumberOfSections (UITableView tableView)
		{
			return 1;
		}


		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			
			var cellid = RatingsView.RatingCellId;

			var cell = (RatingCell)tableView.DequeueReusableCell (cellid) as RatingCell;

			if (cell == null)
				cell = new RatingCell (cellid); 

		
					

			if (_viewModel.Items.Count>0)
			{  
				var comment = _viewModel.Items.ElementAt (indexPath.Row).Text;
				var user = _viewModel.Items.ElementAt (indexPath.Row).Name;
				var rating = _viewModel.Items.ElementAt (indexPath.Row).Rating;

				cell.UpdateCell (comment, user, rating);

			
			}

			return cell;
		}

		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{

			tableView.DeselectRow (indexPath, true);  


		}
	}
}

