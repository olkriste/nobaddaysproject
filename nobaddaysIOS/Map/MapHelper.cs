﻿
using System;
using MapKit;
using CoreLocation;
using NoBadDaysPCL.Items;
using NoBadDaysPCL;

namespace nobaddaysIOS
{
	public class ShopAnnotation : MKAnnotation
	{
		static string annotationId = "ShopAnnotation";

		string title;
		CLLocationCoordinate2D coord;
		int shopId;
		int type;
		string mapPin;
		string picture;
		int clusterCount;
		double latitude;
		double longitude;
		MainClass.eShopType shopType;

		public ShopAnnotation (string title,
			CLLocationCoordinate2D coord, MapItem item)
		{
			this.title = item.Name;
			this.coord = coord;
			this.shopId = item.ShopId;
			//this.mapPin = mapPin;
			this.picture = item.ListPicture;
			this.clusterCount = item.ClusterCount;
			this.latitude = item.Latitude;
			this.longitude = item.Longitude;
			this.shopType = item.ShopType;
		}

		public int ClusterCount {
			get {
				return clusterCount;
			}
		}

		public MainClass.eShopType ShopType {
			get {
				return shopType;
			}
		}

		public double Latitude {
			get {
				return latitude;
			}
		}

		public double Longitude {
			get {
				return longitude;
			}
		}

		public string Picture {
			get {
				return picture;
			}
		}

		public string MapPin {
			get {
				return mapPin;
			}
		}

		public int ShopId {
			get {
				return shopId;
			}
		}

		public override string Title {
			get {
				return title;
			}
		}

		public override CLLocationCoordinate2D Coordinate {
			get {
				return coord;
			}
		}

		public void Remove()
		{
			
		}
	}

	public class MapHelper
	{
		public double KilometresToLatitudeDegrees(double kms)
		{
			double earthRadius = 6371.0; // in kms
			double radiansToDegrees = 180.0/Math.PI;
			return (kms/earthRadius) * radiansToDegrees;
		}

		/// <summary>Converts kilometres to longitudinal degrees at a specified latitude</summary>
		public double KilometresToLongitudeDegrees(double kms, double atLatitude)
		{
			double earthRadius = 6371.0; // in kms
			double degreesToRadians = Math.PI/180.0;
			double radiansToDegrees = 180.0/Math.PI;
			// derive the earth's radius at that point in latitude
			double radiusAtLatitude = earthRadius * Math.Cos(atLatitude * degreesToRadians);
			return (kms / radiusAtLatitude) * radiansToDegrees;
		}
	}
}

