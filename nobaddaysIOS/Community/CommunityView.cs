﻿using System;
using CoreGraphics;
using UIKit;
using System.Timers;
using Foundation;
using System.Collections.Generic;
using NoBadDaysPCL;
using System.IO;
using SQLite.Net.Platform.XamarinIOS;
using Valore.IOSDropDown;
using Security;
using Utils;
using NoBadDaysPCL.Items;
using NoBadDaysPCL.ViewModel;
using System.Threading.Tasks;
using System.Net;
using System.Drawing;

namespace nobaddaysIOS
{
	public class CommunityView: UIView
	{
		UIViewController viewcontroller;

		UIImageView _bckButton;
		UIWebView webView;
		UITapGestureRecognizer gestureRecognizerFb;
		UITapGestureRecognizer gestureRecognizerInstagram;

		UILabel header;

		public CommunityView(object controller)
		{
			viewcontroller = controller as UIViewController;
			var frame = viewcontroller.View.Bounds;
			Frame = new CGRect(0, 0, frame.Width, frame.Height);

			SetHeaderText (frame);
			SetPictureView (frame);
		}

		private void SetHeaderText(CGRect frame)
		{
			var tmpFrame = new CGRect(0, 0, frame.Width, frame.Height / 10);
			header = new UILabel(tmpFrame);
			header.TextAlignment = UITextAlignment.Center;
			header.AttributedText = new NSAttributedString(NSBundle.MainBundle.LocalizedString("CommunityText", "CommunityText"), GlobalData.HeaderAttrib);
			Add (header);
		}

		private void SetPictureView(CGRect frame)
		{
			UIImageView fb = new UIImageView(new CGRect (frame.Width / 4, frame.Height / 5, frame.Width / 5, frame.Width / 5));
			fb.Image = UIImage.FromBundle ("Images/Facebook_256x256.png");

			gestureRecognizerFb = new UITapGestureRecognizer ((g) => 
			{
		
				header.AttributedText = new NSAttributedString(NSBundle.MainBundle.LocalizedString("FacebookText", "FacebookText"), GlobalData.HeaderAttrib);
				LoadPage(NSBundle.MainBundle.LocalizedString("FacebookLink", "FacebookLink"));
			});
			
			fb.AddGestureRecognizer (gestureRecognizerFb);
			fb.UserInteractionEnabled = true;
	
			Add (fb);

			UIImageView instagram = new UIImageView(new CGRect ((frame.Width / 4) * 2.3, frame.Height / 5, frame.Width / 5, frame.Width / 5));
			instagram.Image = UIImage.FromBundle ("Images/Instagram_256x256.png");

			gestureRecognizerInstagram = new UITapGestureRecognizer ((g) => 
			{
				header.AttributedText = new NSAttributedString(NSBundle.MainBundle.LocalizedString("InstagramText", "InstagramText"), GlobalData.HeaderAttrib);
				LoadPage(NSBundle.MainBundle.LocalizedString("InstagramLink", "InstagramLink"));
			});

			instagram.AddGestureRecognizer (gestureRecognizerInstagram);
			instagram.UserInteractionEnabled = true;

			Add (instagram);

			var tmpFrame = new CGRect(frame.Width / 6, frame.Height / 3.5, frame.Width / 1.5, frame.Height / 3);
			UILabel firstText = new UILabel(tmpFrame);
			firstText.TextAlignment = UITextAlignment.Center;
			firstText.LineBreakMode = UILineBreakMode.WordWrap;
			firstText.Lines = 0;
			firstText.TextColor = UIColor.White;
			firstText.Text = NSBundle.MainBundle.LocalizedString("JoinUsText", "JoinUsText");
			firstText.Font = UIFont.FromName("Roboto-Regular", 24f);
			Add (firstText);

			tmpFrame = new CGRect(frame.Width / 10, frame.Height / 2.2, frame.Width / 1.2, frame.Height / 3);
			firstText = new UILabel(tmpFrame);
			firstText.TextAlignment = UITextAlignment.Center;
			firstText.LineBreakMode = UILineBreakMode.WordWrap;
			firstText.Lines = 0;
			firstText.TextColor = UIColor.White;
			firstText.Text = NSBundle.MainBundle.LocalizedString("IsAllAboutText", "IsAllAboutText");
			firstText.Font = UIFont.FromName("Roboto-Regular", 16f);
			Add (firstText);

			tmpFrame = new CGRect(frame.Width / 10, frame.Height / 1.6, frame.Width / 1.2, frame.Height / 3);
			firstText = new UILabel(tmpFrame);
			firstText.TextAlignment = UITextAlignment.Center;
			firstText.LineBreakMode = UILineBreakMode.WordWrap;
			firstText.Lines = 0;
			firstText.TextColor = UIColor.White;
			firstText.Text = NSBundle.MainBundle.LocalizedString("NBDText", "NBDText");
			firstText.Font = UIFont.FromName("Roboto-Regular", 16f);
			Add (firstText);
			// 
		}


		private void LoadPage(string link)
		{
			if (!string.IsNullOrEmpty(link))
			{		
				try 
				{
					AddBckButton();
					webView = new UIWebView(new CGRect (0, Frame.Height / 10, Frame.Width, Frame.Height - Frame.Height / 10));

					this.AddSubview (webView);
					webView.ScalesPageToFit = true;

					webView.LoadRequest(new NSUrlRequest(new NSUrl(link)));

					var ctrl = viewcontroller as SavingsViewController;
					ctrl.StartSpinner ();

					webView.LoadFinished+= (object sender, EventArgs e) => 
					{
						ctrl.StopSpinner ();
					};

					webView.LoadError+= (object sender, UIWebErrorArgs e) => 
					{
						ctrl.StopSpinner ();

						UIAlertView alert = new UIAlertView (NSBundle.MainBundle.LocalizedString("InvalidLinkText", "InvalidLinkText"), "", null, 
							NSBundle.MainBundle.LocalizedString("OK", "OK")); 
						alert.Show ();

						alert.Clicked += (snd, buttonArgs) => {
							if (buttonArgs.ButtonIndex == 0) 
							{ // OK Clicked
								webView.RemoveFromSuperview ();
							}
						};
					};
				}
				catch 
				{

				}
			}
			else
			{
				UIAlertView alert = new UIAlertView (NSBundle.MainBundle.LocalizedString("NoWebLinkText", "NoWebLinkText"), "", null,
					NSBundle.MainBundle.LocalizedString ("OK", "OK")); 
				alert.Show ();	
			}

		}

		private void AddBckButton() 
		{
			// Disable main wheel
			SavingsViewController view = viewcontroller as SavingsViewController;
			view.WheelUserInteractionEnabled (false);

			if (_bckButton == null) {
				_bckButton = new UIImageView (new CGRect (5, 5, Frame.Height / 11, Frame.Height / 11));
				_bckButton.Image = UIImage.FromBundle ("Images/back.png");
				_bckButton.UserInteractionEnabled = true;

				UITapGestureRecognizer bckTap = new UITapGestureRecognizer((g) => 
					{
						DropWebView();
					});
				_bckButton.AddGestureRecognizer (bckTap);
			}
			Add (_bckButton);

		}


		private void DropWebView()
		{
			if (webView != null)
			{
				webView.RemoveFromSuperview();
				webView.Dispose();
				webView = null;

				_bckButton.RemoveFromSuperview();
				_bckButton.Dispose();
				_bckButton = null;

				// Disable main wheel
				SavingsViewController viewTmp  = viewcontroller as SavingsViewController;
				viewTmp.WheelUserInteractionEnabled (true);

				header.AttributedText = new NSAttributedString(NSBundle.MainBundle.LocalizedString("CommunityText", "CommunityText"), GlobalData.HeaderAttrib);

				var ctrl = viewcontroller as SavingsViewController;
				ctrl.StopSpinner ();
			}
		}
	}
}

