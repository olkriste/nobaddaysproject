﻿using System;
using CoreGraphics;
using UIKit;
using System.Timers;
using Foundation;
using System.Collections.Generic;
using NoBadDaysPCL;
using System.IO;
using SQLite.Net.Platform.XamarinIOS;
using Valore.IOSDropDown;
using Security;
using Utils;
using NoBadDaysPCL.Items;
using NoBadDaysPCL.ViewModel;
using System.Threading.Tasks;
using System.Net;
using System.Drawing;
using Alliance.Carousel;

namespace nobaddaysIOS
{
	public class WinView: UIView
	{
		UIViewController viewcontroller;
		CarouselView carousel;

		UILabel bandLabel;
		UILabel previousNameLabel;
		UILabel previousBioLabel;

		UIImageView prizeImage;
		UIImageView imgViewPrevious;
		UIImageView imgViewCurrent;
		UIImageView imgViewNext;

		UIImageView firstSection;
		UIImageView secondSection;
		UIImageView thirdSection;
		UIImageView fifthSection;

		UILabel prizeName;
		UILabel valueLabel;

		UIImageView profileImageGhost;
		UIImageView profileImage;

		int index = 1;

		public WinView(object controller)
		{
			viewcontroller = controller as UIViewController;
			var frame = viewcontroller.View.Bounds;
			Frame = new CGRect(0, 0, frame.Width, frame.Height);

			SetHeaderText (frame);
			SetPrizeView ();
			SetBand (frame);
			SetCarousel (frame);

			ViewModel.GetWin ();
		}

		private WinViewModel _viewModel = null;
		private WinViewModel ViewModel 
		{
			get 
			{ 
				if (_viewModel == null) 
				{
					string libraryPath = System.Environment.GetFolderPath (System.Environment.SpecialFolder.Personal);
					var path = Path.Combine (libraryPath, Container.SQLitePath);

					_viewModel = new WinViewModel (new SQLitePlatformIOS (), path);
					_viewModel.PhoneDataSuccessful += DataSuccessful;
					_viewModel.PhoneDataErrorOccurred += DataError;
				} 
				return _viewModel;
			}
		}

		void DataSuccessful(object sender, ServerOutputItem e)
		{
			SetPrizeImage (index);

			prizeName.Text = ViewModel.Item.PreviousPrize.Title;
			valueLabel.Text = ViewModel.Item.PreviousPrize.Description;

			previousNameLabel.Text = ViewModel.Item.PreviousWinner.Name;
			previousBioLabel.Text = ViewModel.Item.PreviousWinner.Bio;
			SetProfileImage ();

			SetCurrentUsers ();
		}

		void DataError(object sender, ServerOutputItem e)
		{
			
		}

		private void SetHeaderText(CGRect frame)
		{
			var tmpFrame = new CGRect(0, 0, frame.Width, frame.Height / 11);
			UILabel header = new UILabel(tmpFrame);
			header.TextAlignment = UITextAlignment.Center;
			header.AttributedText = new NSAttributedString(NSBundle.MainBundle.LocalizedString ("WinText", "WinText"), GlobalData.HeaderAttrib);
			Add (header);
		}

		private void SetPrizeView()
		{
			prizeImage = new UIImageView (new CGRect (Frame.Width / 3, Frame.Height / 7, Frame.Width / 3, Frame.Width / 3));
			prizeImage.Layer.Opacity = 0.0f;
			Add(prizeImage);

			var tmpFrame = new CGRect(0, Frame.Height / 2.2 - Frame.Height / 11f, Frame.Width, Frame.Height / 15);
			prizeName = new UILabel(tmpFrame);
			prizeName.TextAlignment = UITextAlignment.Center;
			prizeName.TextColor = UIColor.White;
			Add (prizeName);

			tmpFrame = new CGRect(0, Frame.Height / 2.0 - Frame.Height / 11f, Frame.Width, Frame.Height / 15);
			valueLabel = new UILabel(tmpFrame);
			valueLabel.TextAlignment = UITextAlignment.Center;
			valueLabel.TextColor = UIColor.White;
			Add (valueLabel);
		}

		private void SetBand(CGRect frame)
		{
			var tmpFrame = new CGRect(0, frame.Height / 2, frame.Width, frame.Height / 15);
			bandLabel = new UILabel(tmpFrame);
			bandLabel.Text = "THIS MONTH";
			bandLabel.TextColor = UIColor.White;
			bandLabel.TextAlignment = UITextAlignment.Center;
			bandLabel.BackgroundColor = UIColor.Black;
			bandLabel.Font = UIFont.FromName ("Roboto-Regular", 16F);
			Add (bandLabel);
		}
			
		private void SetCarousel(CGRect frame)
		{
			carousel = new CarouselView(new CGRect(0, frame.Height / 2 + frame.Height / 15, frame.Width, frame.Height / 2 - frame.Height / 15));
			carousel.DataSource = new LinearDataSource(this);
			carousel.Delegate = new LinearDelegate(this);
			carousel.ConfigureView();
			carousel.CurrentItemIndex = 1;
			carousel.CarouselType = CarouselType.Linear;
			carousel.Bounces = false;
			AddSubview(carousel);
		}

		public class LinearDataSource : CarouselViewDataSource
		{
			WinView winViewTmp;

			public LinearDataSource(WinView viewTmp)
			{
				this.winViewTmp = viewTmp;
			}

			public override nint NumberOfItems(CarouselView carousel)
			{
				return 3;
			}

			public override UIView ViewForItem(CarouselView carousel, nint index, UIView reusingView)
			{
				if (index == 0) {
					reusingView = winViewTmp.GetPreviousWinner ();
				} else if (index == 1) {
					reusingView = winViewTmp.SetCurrentWinnerDefaults ();
				} else if (index == 2) {
					reusingView = winViewTmp.SetNextWinner ();
				}
				return reusingView;
			}
		}

		public class LinearDelegate : CarouselViewDelegate
		{
			WinView winViewTmp;

			public LinearDelegate(WinView winView)
			{
				this.winViewTmp = winView;
			}

			public override nfloat ValueForOption(CarouselView carousel, CarouselOption option, nfloat aValue)
			{
				if (option == CarouselOption.Spacing)
				{
					return aValue * 2.1f;
				}
				return aValue;
			}

			public override void DidSelectItem(CarouselView carousel, nint index)
			{
				Console.WriteLine("Selected: " + ++index);
			}

			public override void CurrentItemIndexDidChange (CarouselView carouselView)
			{
				base.CurrentItemIndexDidChange (carouselView);
				winViewTmp.HandleSelect (carouselView.CurrentItemIndex);
			}

			public override void DidEndScrollingAnimation (CarouselView carouselView)
			{
				base.DidEndScrollingAnimation (carouselView);
			}
		}

		private void HandleSelect(nint indexTmp)
		{
			if (indexTmp == 0) {
				index = 0;
				this.bandLabel.Text = NSBundle.MainBundle.LocalizedString ("PreviousMonthText", "PreviousMonthText");
				if (ViewModel.Item.PreviousPrize != null) {
					prizeName.Text = ViewModel.Item.PreviousPrize.Title;
					valueLabel.Text = ViewModel.Item.PreviousPrize.Description;
					SetPrizeImage (index);
				}
			}
			else if (indexTmp == 1) {
				index = 1;
				this.bandLabel.Text = NSBundle.MainBundle.LocalizedString ("ThisMonthText", "ThisMonthText");
				if (ViewModel.Item.Prize != null) {
					prizeName.Text = ViewModel.Item.Prize.Title;
					valueLabel.Text = ViewModel.Item.Prize.Description;
					SetPrizeImage (index);
				}
			}
			else if (indexTmp == 2) {
				index = 2;
				this.bandLabel.Text = NSBundle.MainBundle.LocalizedString ("NextMonthText", "NextMonthText");
				if (ViewModel.Item.NextPrize != null) {
					prizeName.Text = ViewModel.Item.NextPrize.Title;
					valueLabel.Text = ViewModel.Item.NextPrize.Description;
					SetPrizeImage (index);
				}
			}
		}

		private UIImageView GetPreviousWinner()
		{
			if (imgViewPrevious == null) {
				var tmpFrame = new CGRect (0, 0, Frame.Width, Frame.Height / 2 - Frame.Height / 15);
				imgViewPrevious = new UIImageView (tmpFrame);
				imgViewPrevious.Image = GlobalData.RedBackGround;

				var tmpRect = new CGRect (Frame.Width / 3, Frame.Height / 20, Frame.Width / 3, Frame.Width / 3);

				profileImage = new UIImageView (tmpRect);
				imgViewPrevious.AddSubview (profileImage);

				profileImageGhost = new UIImageView (tmpRect);
				profileImageGhost.Image = UIImage.FromBundle ("Images/GhostProfile_128x128.png");
				imgViewPrevious.AddSubview (profileImageGhost);

				UIImageView badge = new UIImageView (new CGRect (Frame.Width / 1.7, Frame.Height / 7, Frame.Width / 5, Frame.Width / 5));
				badge.Image = UIImage.FromBundle ("Images/WinBadge_256x256.png");
				imgViewPrevious.AddSubview (badge);

				UILabel label = new UILabel (new CGRect (0, Frame.Height / 4, Frame.Width, Frame.Height / 15));
				label.Text = "WINNER";
				label.TextColor = UIColor.White;
				label.TextAlignment = UITextAlignment.Center;
				label.Font = UIFont.FromName ("Roboto-Regular", 18F);
				imgViewPrevious.AddSubview (label);

				previousNameLabel = new UILabel (new CGRect (0, Frame.Height / 3.5f, Frame.Width, Frame.Height / 15));
				previousNameLabel.Text = "...";
				previousNameLabel.TextColor = UIColor.White;
				previousNameLabel.TextAlignment = UITextAlignment.Center;
				previousNameLabel.Font = UIFont.FromName ("Roboto-Regular", 18F);
				imgViewPrevious.AddSubview (previousNameLabel);

				previousBioLabel = new UILabel (new CGRect (0, Frame.Height / 3.2f, Frame.Width, Frame.Height / 15));
				previousBioLabel.TextColor = UIColor.White;
				previousBioLabel.TextAlignment = UITextAlignment.Center;
				previousBioLabel.Font = UIFont.FromName ("Roboto-Regular", 18F);
				imgViewPrevious.AddSubview (previousBioLabel);
			}
			return imgViewPrevious;
		}

		private UIImageView SetCurrentWinnerDefaults()
		{
			if (imgViewCurrent == null) {
				var tmpFrame = new CGRect (0, 0, Frame.Width, Frame.Height / 2 - Frame.Height / 15);
				imgViewCurrent = new UIImageView (tmpFrame);

				var tmpRect = new CGRect (0, 0, Frame.Width, Frame.Height / 10);
				firstSection = new UIImageView (tmpRect);
				firstSection.Image = GlobalData.FirstGrayBackGround;
				imgViewCurrent.AddSubview (firstSection);

				tmpRect = new CGRect (0, Frame.Height / 12, Frame.Width, Frame.Height / 10);
				secondSection = new UIImageView (tmpRect);
				secondSection.Image = GlobalData.SecondGrayBackGround;
				imgViewCurrent.AddSubview (secondSection);

				tmpRect = new CGRect (0, (Frame.Height / 12) * 2, Frame.Width, Frame.Height / 10);
				thirdSection = new UIImageView (tmpRect);
				thirdSection.Image = GlobalData.ThirdGrayBackGround;
				imgViewCurrent.AddSubview (thirdSection);

				tmpRect = new CGRect (0, (Frame.Height / 12) * 3, Frame.Width, Frame.Height / 10);
				var fourth = new UIImageView (tmpRect);
				fourth.Image = GlobalData.WhiteBackGround;
				imgViewCurrent.AddSubview (fourth);

				tmpRect = new CGRect (0, (Frame.Height / 12) * 4, Frame.Width, Frame.Height / 10);
				fifthSection = new UIImageView (tmpRect);
				fifthSection.Image = GlobalData.RedBackGround;
				imgViewCurrent.AddSubview (fifthSection);
			}
			return imgViewCurrent;
		}

		private async void SetCurrentUsers()
		{
			if (ViewModel.Item.Users != null) {
				int index = 0;
				foreach(UserListItem user in ViewModel.Item.Users)
				{
					if (index == 0) {
						var tmpRect = new CGRect (Frame.Height / 150, Frame.Height / 150, Frame.Height / 15, Frame.Height / 15);
						var image = new UIImageView (tmpRect);
						image.Image = await LoadImage (user.Picture.Uri);
						firstSection.AddSubview (image);

						tmpRect = new CGRect (Frame.Width / 5, Frame.Height / 150, Frame.Width / 1.5f, Frame.Height / 15);
						var label = new UILabel (tmpRect);
						label.Text = user.Name;
						label.Font = UIFont.FromName ("Roboto-Regular", 16F);
						label.TextColor = UIColor.Red;
						label.TextAlignment = UITextAlignment.Left;
						firstSection.AddSubview (label);

						tmpRect = new CGRect (Frame.Width - Frame.Width / 4, Frame.Height / 150, Frame.Width / 5, Frame.Height / 15);
						label = new UILabel (tmpRect);
						label.Text = user.Points;
						label.Font = UIFont.FromName ("Roboto-Regular", 22F);
						label.TextColor = UIColor.Red;
						label.TextAlignment = UITextAlignment.Right;
						firstSection.AddSubview (label);
					} else if (index == 1) {
						var tmpRect = new CGRect (Frame.Height / 150, Frame.Height / 150, Frame.Height / 15, Frame.Height / 15);
						var image = new UIImageView (tmpRect);
						image.Image = await LoadImage (user.Picture.Uri);
						secondSection.AddSubview (image);

						tmpRect = new CGRect (Frame.Width / 5, Frame.Height / 150, Frame.Width / 1.5f, Frame.Height / 15);
						var label = new UILabel (tmpRect);
						label.Text = user.Name;
						label.Font = UIFont.FromName ("Roboto-Regular", 16F);
						label.TextColor = UIColor.Gray;
						label.TextAlignment = UITextAlignment.Left;
						secondSection.AddSubview (label);

						tmpRect = new CGRect (Frame.Width - Frame.Width / 4, Frame.Height / 150, Frame.Width / 5, Frame.Height / 15);
						label = new UILabel (tmpRect);
						label.Text = user.Points;
						label.Font = UIFont.FromName ("Roboto-Regular", 22F);
						label.TextColor = UIColor.Gray;
						label.TextAlignment = UITextAlignment.Right;
						secondSection.AddSubview (label);
					} else if (index == 2) {
						var tmpRect = new CGRect (Frame.Height / 150, Frame.Height / 150, Frame.Height / 15, Frame.Height / 15);
						var image = new UIImageView (tmpRect);
						image.Image = await LoadImage (user.Picture.Uri);
						thirdSection.AddSubview (image);

						tmpRect = new CGRect (Frame.Width / 5, Frame.Height / 150, Frame.Width / 1.5f, Frame.Height / 15);
						var label = new UILabel (tmpRect);
						label.Text = user.Name;
						label.Font = UIFont.FromName ("Roboto-Regular", 16F);
						label.TextColor = UIColor.Gray;
						label.TextAlignment = UITextAlignment.Left;
						thirdSection.AddSubview (label);

						tmpRect = new CGRect (Frame.Width - Frame.Width / 4, Frame.Height / 150, Frame.Width / 5, Frame.Height / 15);
						label = new UILabel (tmpRect);
						label.Text = user.Points;
						label.Font = UIFont.FromName ("Roboto-Regular", 22F);
						label.TextColor = UIColor.Gray;
						label.TextAlignment = UITextAlignment.Right;
						thirdSection.AddSubview (label);
					} else if (index == 3) {
						//var tmpRect = new CGRect (Frame.Height / 150, Frame.Height / 150, Frame.Height / 15, Frame.Height / 15);
						//var image = new UIImageView (tmpRect);
						//image.Image = await LoadImage (user.Picture.Uri);
						//fifthSection.AddSubview (image);

						var tmpRect = new CGRect (Frame.Width / 5, Frame.Height / 150, Frame.Width / 1.5f, Frame.Height / 15);
						var label = new UILabel (tmpRect);
						label.Text = user.Name;
						label.Font = UIFont.FromName ("Roboto-Regular", 16F);
						label.TextColor = UIColor.White;
						label.TextAlignment = UITextAlignment.Left;
						fifthSection.AddSubview (label);

						tmpRect = new CGRect (Frame.Width - Frame.Width / 4, Frame.Height / 150, Frame.Width / 5, Frame.Height / 15);
						label = new UILabel (tmpRect);
						label.Text = user.Points;
						label.Font = UIFont.FromName ("Roboto-Regular", 22F);
						label.TextColor = UIColor.White;
						label.TextAlignment = UITextAlignment.Right;
						fifthSection.AddSubview (label);
					}

					index++;
				}
			}
		}

		private UIImageView SetNextWinner()
		{
			if (imgViewNext == null) {
				var tmpFrame = new CGRect (0, 0, Frame.Width, Frame.Height / 2 - Frame.Height / 15);
				imgViewNext = new UIImageView (tmpFrame);
				imgViewNext.Image = GlobalData.WhiteBackGround;

				UILabel label = new UILabel (new CGRect (Frame.Width / 4, Frame.Height / 8, Frame.Width / 2, Frame.Height / 5));
				label.Text = NSBundle.MainBundle.LocalizedString ("CompetitionNotStartText", "CompetitionNotStartText");
				label.TextColor = UIColor.Gray;
				label.TextAlignment = UITextAlignment.Center;
				label.LineBreakMode = UILineBreakMode.WordWrap;
				label.Lines = 2;
				//label.SizeToFit ();
				label.Font = UIFont.FromName ("Roboto-Regular", 22F);
				imgViewNext.AddSubview (label);
			}
			return imgViewNext;
		}

		private async void SetProfileImage()
		{
			if (!string.IsNullOrEmpty (ViewModel.Item.PreviousWinner.Picture.Uri)) {
				profileImage.Image = await LoadImage (ViewModel.Item.PreviousWinner.Picture.Uri);

				profileImage.Layer.CornerRadius = Frame.Width / 6;
				profileImage.Layer.MasksToBounds = true;

				await UIView.AnimateAsync (0.1f, () => {
					profileImageGhost.Layer.Opacity = 0.0f;
				});
			}
		}

		private async void SetPrizeImage(int index)
		{
			try
			{
				if (index == 0)
				{
					if (!string.IsNullOrEmpty(ViewModel.Item.PreviousPrize.Picture.Uri))
					{
						prizeImage.Image = await LoadImage (ViewModel.Item.PreviousPrize.Picture.Uri);
					}
					else
					{
						prizeImage.Image = UIImage.FromBundle ("Images/GhostSquareContentBig.png");
					}
				}
				else if (index == 1)
				{
					if (!string.IsNullOrEmpty(ViewModel.Item.Prize.Picture.Uri))
					{
						prizeImage.Image = await LoadImage (ViewModel.Item.Prize.Picture.Uri);
					}
					else
					{
						prizeImage.Image = UIImage.FromBundle ("Images/GhostSquareContentBig.png");
					}
				}
				else if (index == 2)
				{
					if (!string.IsNullOrEmpty(ViewModel.Item.NextPrize.Picture.Uri))
					{
						prizeImage.Image = await LoadImage (ViewModel.Item.NextPrize.Picture.Uri);
					}
					else
					{
						prizeImage.Image = UIImage.FromBundle ("Images/GhostSquareContentBig.jpg");
					}
				}
					
				prizeImage.Layer.CornerRadius = Frame.Width / 6;
				prizeImage.Layer.MasksToBounds = true;
				prizeImage.Layer.Opacity = 0;

				await UIView.AnimateAsync (0.3f, () => {
					prizeImage.Layer.Opacity = 1f;
				});
			}
			catch 
			{
			}
		}

		private async Task<UIImage> LoadImage (string imageUrl)
		{
			Task<byte[]> contentsTask = ViewModel.LoadImage (imageUrl);
			var contents = await contentsTask;
			return UIImage.LoadFromData (NSData.FromArray (contents));
		}
	}
}

