﻿using System;
using CoreGraphics;
using UIKit;
using System.Timers;
using Foundation;
using System.Collections.Generic;
using NoBadDaysPCL;
using System.IO;
using SQLite.Net.Platform.XamarinIOS;
using Valore.IOSDropDown;
using Security;
using Utils;
using NoBadDaysPCL.Items;
using NoBadDaysPCL.ViewModel;
using System.Threading.Tasks;
using System.Net;
using System.Drawing;
using System.Linq;
using CoreLocation;

namespace nobaddaysIOS
{
	public class MyOffersView: UIView
	{

		public static NSString OfferCellId = new NSString ("OfferCell");

		UIViewController _viewcontroller;

		UITableView _table;

		UILabel _noOffersLabel;

		// Views
		OfferView offerView;

		UIImageView _bckButton;
		UIImageView _searchImage = null;


		// Gesture 
		UITapGestureRecognizer _bckTap;

	
		private bool _visible; 
		public bool Visible 
		{
			get{ return _visible;}
			set 
			{
				_visible = value;
			}
		}

		private bool _offerVisible; 
		public bool IsOfferVisible 
		{
			get{ return _offerVisible;}
			set 
			{
				_offerVisible = value;
			}
		}
			
		private static UIRefreshControl _myfresh;
		public static UIRefreshControl refreshControl
		{
			get { return _myfresh; }
			set { _myfresh = value; }
		}

		OfferViewModel _viewModelOffers = null;
		OfferSearchItem _offerSearchItem;
	
		UITapGestureRecognizer gestureRecognizer;


		List<DropDownListItem> _categoryDropDownList = new List<DropDownListItem>();

		DropDownList _ddl;

		List<string> _categoryList;

		MainClass.eShopType _shopType = MainClass.eShopType.None;
	

	
		public MyOffersView (object controller)
		{
			_viewcontroller = controller as UIViewController;
			var frame = _viewcontroller.View.Bounds;
			Frame = new CGRect(0, 0, frame.Width, frame.Height);

			string libraryPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
			var path = System.IO.Path.Combine(libraryPath, nobaddaysIOS.Container.SQLitePath);

			_viewModelOffers = new OfferViewModel(new SQLitePlatformIOS(), path);

			_viewModelOffers.PhoneDataSuccessful += _viewModelOffer_PhoneDataSuccessful;
			_viewModelOffers.PhoneDataErrorOccurred += _viewModelOffer_PhoneDataErrorOccurred;

			_offerSearchItem = new OfferSearchItem ();
			_offerSearchItem.Count = 10;
			_offerSearchItem.Index = 0;
			_offerSearchItem.Search = "";
			_offerSearchItem.NotificationDistance = GlobalData.UserItem.NotificationDistance;


			CLLocation mycoor = GPSHelper.mylocation;
			if (mycoor != null) {
	
				_offerSearchItem.Latitude = (float)mycoor.Coordinate.Latitude;
				_offerSearchItem.Longitude = (float)mycoor.Coordinate.Longitude;
			} 

			SetHeaderText (frame);
			SetTable (_viewcontroller);

			var ctrl = _viewcontroller as SavingsViewController;
			ctrl.StartSpinner ();

			_viewModelOffers.GetOffers (_offerSearchItem);
		}

		void _viewModelOffer_PhoneDataErrorOccurred (object sender, ServerOutputItem e)
		{
			var ctrl = _viewcontroller as SavingsViewController;
			ctrl.StopSpinner ();

			if (_viewModelOffers.Items.Count > 0)
			{	
				_table.Source = new OffersTableSource (_viewModelOffers, this);
				_table.ReloadData ();

				refreshControl.EndRefreshing();
			}
			else 
			{	
				_table.RemoveFromSuperview ();

			}	
		
			UIAlertView alert = new UIAlertView (e.ResultString, "", null, NSBundle.MainBundle.LocalizedString ("OK", "OK"), null);
			alert.Clicked += (sender1, buttonArgs) => 
			{

			};
			alert.Show ();
		}

		void _viewModelOffer_PhoneDataSuccessful (object sender, ServerOutputItem e)
		{
			var ctrl = _viewcontroller as SavingsViewController;
			ctrl.StopSpinner ();

			if (_viewModelOffers.Items.Count > 0)
			{	
				_table.Source = new OffersTableSource (_viewModelOffers, this);
				_table.ReloadData ();

				refreshControl.EndRefreshing();

				//    TicketsTable.Hidden = false;
				_table.SeparatorStyle = UITableViewCellSeparatorStyle.SingleLine;

				_table.Hidden = false;
			}
			else
			{
				_table.Hidden = true;
				var frame = _viewcontroller.View.Bounds;

				var tmpFrame = new CGRect(0, frame.Height/2, frame.Width, frame.Height / 10);
				_noOffersLabel = new UILabel (tmpFrame) {
					Font = UIFont.FromName("Roboto-Regular", 18f),
					TextColor = UIColor.Black,
					TextAlignment = UITextAlignment.Center,
					BackgroundColor = UIColor.Clear,
					Text= NSBundle.MainBundle.LocalizedString ("NoOffersText", "NoOffersText") 
				};

				Add(_noOffersLabel);
			}
		}

		private void SetHeaderText(CGRect frame)
		{
			var tmpFrame = new CGRect(0, 0, frame.Width, frame.Height / 10);
			UILabel header = new UILabel(tmpFrame);
			header.TextAlignment = UITextAlignment.Center;
			header.AttributedText = new NSAttributedString(NSBundle.MainBundle.LocalizedString ("OffersText", "OffersText") , GlobalData.HeaderAttrib);
			Add (header);
		}

		private void SetSearchImage(CGRect frame)
		{
			if (_searchImage == null) 
			{
				_searchImage = new UIImageView (new CGRect (frame.Width-40, 20, frame.Height / 18, frame.Height / 18));
				_searchImage.Image = UIImage.FromBundle ("Images/search.png");


				gestureRecognizer = new UITapGestureRecognizer ((g) => 
					{
					if ((_searchImage != null)&&(_ddl == null))
						{
							GlobalData.initCategoryList();
							_categoryDropDownList.Clear();

							for (int i=0;i<GlobalData.categoryList.Count;i++)
							{	
								_categoryDropDownList.Add(new DropDownListItem()
									{
										Id = (i+1).ToString(),
										DisplayText = GlobalData.categoryList[i],
										Image = null
									});

							}

							_ddl = new DropDownList(this ,_categoryDropDownList.ToArray())
							{
								BackgroundColor = UIColor.FromCGColor(new CGColor(255f/255f, 63f/255f, 63f/255f, 255f/255f)), 
								Opacity = 0.85f,
								TintColor = UIColor.Black,
								NavigationBarAssumedHeight = 1
							};  
						} 

					_ddl.DropDownListChanged += (e, a) =>
						{
							var strValue = a.DisplayText;

							int id;
							if (!int.TryParse(a.Id, out id))
							{	
								if (id < 1)
								{	
									id = 1;
								}
							}
							id = id-1;

							_ddl.Toggle();

							_ddl.RemoveFromSuperview();

							_ddl = null;

						
						};

						Add(_ddl);
						BringSubviewToFront(_ddl);

						_ddl.Toggle ();  
					});

				_searchImage.AddGestureRecognizer (gestureRecognizer);
				_searchImage.UserInteractionEnabled = true;

				Add (_searchImage);
			}
		}
	





		private void SetTable(UIViewController viewcontroller)
		{
			var height = viewcontroller.View.Frame.Height * 9 / 10;

			var tmpFrame = new CGRect(0, viewcontroller.View.Frame.Height-height, viewcontroller.View.Frame.Width, height);

			_table = new UITableView(tmpFrame);

			_table.Source = new OffersTableSource (_viewModelOffers, this);

			_table.SeparatorStyle = UITableViewCellSeparatorStyle.None;
			_table.ClearsContextBeforeDrawing = true;
			_table.AutoresizingMask = UIViewAutoresizing.All;

			_table.RowHeight = 120f;

			_table.Bounces = true;
			_table.AllowsSelection = true;
			_table.UserInteractionEnabled = true; 
			_table.ScrollEnabled = true;

			refreshControl = new UIRefreshControl ();
			refreshControl.TintColor = UIColor.LightGray;
			refreshControl.ValueChanged += HandleValueChanged;
			_table.AddSubview (refreshControl);
					
			Add (_table);
		}

		void HandleValueChanged (object sender, EventArgs e)
		{
			var ctrl = _viewcontroller as SavingsViewController;
			ctrl.StartSpinner ();

			refreshControl.EndRefreshing ();

			if (_viewModelOffers != null)
			{	
				_viewModelOffers.GetOffers (_offerSearchItem);
			}
		}

		private void AddBckButton(bool show)
		{
			if (show) {

				// Disable main wheel
				SavingsViewController view = _viewcontroller as SavingsViewController;
				view.WheelUserInteractionEnabled (false);

				if (_bckButton == null) {
					_bckButton = new UIImageView (new CGRect (5, 5, Frame.Height / 11, Frame.Height / 11));
					_bckButton.Image = UIImage.FromBundle ("Images/back.png");
					_bckButton.UserInteractionEnabled = true;

					_bckTap = new UITapGestureRecognizer((g) => 
						{
							// Disable search
				
							AnimateSubView ();
						});
					_bckButton.AddGestureRecognizer (_bckTap);
				
				}
				Add (_bckButton);
			} else {

			}
		}

		private void AnimateSubView()
		{
			if (offerView != null) {

				nfloat width = IsOfferVisible == true ? Frame.Width : 0;  
				CGRect endframe = new CGRect (width, Frame.Height / 11, Frame.Width, Frame.Height - (Frame.Height / 11));

				if (IsOfferVisible) {

					if (_bckButton != null) {
						_bckButton.RemoveFromSuperview ();
						_bckButton.Dispose ();
						_bckButton = null;
					}

					System.Threading.ThreadPool.QueueUserWorkItem (state => InvokeOnMainThread (() => 
						UIView.AnimateNotify (0.4, 0, 10.1f, 2f, UIViewAnimationOptions.TransitionCurlDown, () => {
							offerView.Frame = endframe;

						}, finished => {
							IsOfferVisible = !IsOfferVisible;
							DropSubView ();
						})));
				} else {
					System.Threading.ThreadPool.QueueUserWorkItem (state => InvokeOnMainThread (() => 
						UIView.AnimateNotify (0.4, 0, 15.1f, 3f, UIViewAnimationOptions.TransitionCurlUp, () => {
							offerView.Frame = endframe;

						}, finished => {
							IsOfferVisible = !IsOfferVisible;
						})));
				}
			}
		}
			
		void DropSubView()
		{
			if (offerView != null) {
				offerView.RemoveFromSuperview ();
				offerView.Dispose ();
				offerView = null;
			}
			SavingsViewController view = _viewcontroller as SavingsViewController;
			view.WheelUserInteractionEnabled (true);
		}


		public void GotoOffer(int offerId)
		{
			if (offerView == null) {
				offerView = new OfferView (_viewcontroller, offerId);
			}
			AddBckButton(true);
			this.Add (offerView);
			AnimateSubView ();
		}
	}

	public class OffersTableSource : UITableViewSource {


		OfferViewModel _viewModel;
		MyOffersView _parentView;

		public OffersTableSource (OfferViewModel viewModelOffers, MyOffersView parentView)
		{
			_viewModel = viewModelOffers;
			_parentView = parentView;
		}

		public override nint RowsInSection (UITableView tableview, nint section)
		{
			return _viewModel.Items.Count;
		}

		/// <summary>
		/// Called by the table view to determine whether or not the row is editable
		/// </summary>
		public override bool CanEditRow (UITableView tableView, NSIndexPath indexPath)
		{
			return false;
		}

		/// <summary>
		/// Called by the table view to determine whether or not the row is moveable
		/// </summary>
		public override bool CanMoveRow (UITableView tableView, NSIndexPath indexPath)
		{
			return false;
		}

		public override nint NumberOfSections (UITableView tableView)
		{
			return 1;
		}


		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			var cellid = MyOffersView.OfferCellId;

			var cell = (OfferCell)tableView.DequeueReusableCell (cellid) as OfferCell;

			if (cell == null)
				cell = new OfferCell (cellid); 

			if (_viewModel.Items.Count>0)
			{  
				var title = _viewModel.Items.ElementAt (indexPath.Row).Title;
				var desc = _viewModel.Items.ElementAt (indexPath.Row).Description;
				var image = _viewModel.Items.ElementAt (indexPath.Row).ListPicture.Uri;

				cell.UpdateCell (title, desc, image, _viewModel);
			}

			return cell;
		}

		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			tableView.DeselectRow (indexPath, true);  

			var id = _viewModel.Items.ElementAt (indexPath.Row).OfferId;
			_parentView.GotoOffer (id);

		}
	}
}

