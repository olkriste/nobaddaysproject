﻿using System;
using CoreGraphics;
using UIKit;
using System.Timers;
using Foundation;
using System.Collections.Generic;
using NoBadDaysPCL;
using System.IO;
using SQLite.Net.Platform.XamarinIOS;
using Valore.IOSDropDown;
using Security;
using Utils;
using NoBadDaysPCL.Items;
using NoBadDaysPCL.ViewModel;
using System.Threading.Tasks;
using System.Net;
using System.Drawing;
using MapKit;
using CoreAnimation;
using CoreLocation;


namespace nobaddaysIOS
{
	public class OfferView: UIView
	{
		UIViewController _viewcontroller;

	
		// Shop
		int _currentShop;

		// Views
		UIImageView blurOverlay = null;
		UIImageView _offerBckImage = null;
		UIImageView _offerImage = null;
		UILabel _offerTitle;




		// Tap recognizers

		UITapGestureRecognizer bckGroundTap;



		OfferViewModel _viewModelOffer = null;



		public OfferView (object controller, int shopId)
		{
			_viewcontroller = controller as UIViewController;
			var frame = _viewcontroller.View.Bounds;
			Frame = new CGRect(frame.Width, frame.Height / 10, frame.Width, frame.Height);

			RegisterForKeyboardNotifications();
			_currentShop = shopId;
		

			var ctrl = _viewcontroller as SavingsViewController;

			ctrl.StartSpinner ();


			SetViews(frame);


			string libraryPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
			var path = System.IO.Path.Combine(libraryPath, nobaddaysIOS.Container.SQLitePath);


			_viewModelOffer = new OfferViewModel(new SQLitePlatformIOS(), path);

			_viewModelOffer.PhoneDataSuccessful += _viewModelOffer_PhoneDataSuccessful;
			_viewModelOffer.PhoneDataErrorOccurred += _viewModelOffer_PhoneDataErrorOccurred;

			_viewModelOffer.GetOffer (shopId);
		}



		void _viewModelOffer_PhoneDataErrorOccurred (object sender, ServerOutputItem e)
		{
			var ctrl = _viewcontroller as SavingsViewController;
			ctrl.StopSpinner ();


			UIAlertView alert = new UIAlertView (e.ResultString, "", null, NSBundle.MainBundle.LocalizedString ("OK", "OK"), null);
			alert.Clicked += (sender1, buttonArgs) => 
			{

			};
			alert.Show ();
		}

		void _viewModelOffer_PhoneDataSuccessful (object sender, ServerOutputItem e)
		{
			var ctrl = _viewcontroller as SavingsViewController;
			ctrl.StopSpinner ();

			var frame = _viewcontroller.View.Bounds;
			SetItems (frame);



		}


		private void SetViews(CGRect frame)
		{
			AutoresizingMask = UIViewAutoresizing.All;
			SetHeaderText (frame);
			SetBackGround (frame);
		
		
		
		
	//		ViewModel.GetShop (_currentShop);
		}
		/*
		private async Task<UIImage> LoadImage (string imageUrl)
		{
			Task<byte[]> contentsTask = ViewModel.LoadImage (imageUrl);
			var contents = await contentsTask;
			return UIImage.LoadFromData (NSData.FromArray (contents));
		}
*/
		private void SetHeaderText(CGRect frame)
		{
			/*var tmpFrame = new CGRect(0, 0, frame.Width, frame.Height / 11);
			UILabel header = new UILabel(tmpFrame);
			header.TextAlignment = UITextAlignment.Center;
			header.AttributedText = new NSAttributedString("MAP", GlobalData.HeaderAttrib);
			Add (header);*/
		}

		public void SetBackGround(CGRect frame)
		{
			_offerBckImage = new UIImageView(new CGRect (0, 0, frame.Width, frame.Height - frame.Height / 11));
			_offerBckImage.Image = GlobalData.RedBackGround;
			Add (_offerBckImage);

			var margin = 20;
			float TopImageWidth = (float)frame.Width-2*margin;
			float TopImageHeight = 16 * TopImageWidth / 9;

			_offerImage = new UIImageView (new CGRect (margin, margin, TopImageWidth, TopImageHeight));
			_offerImage.Layer.Opacity = 0.0f;
			Add(_offerImage);

			var tmpFrame = new CGRect(margin, TopImageHeight+2*margin, TopImageWidth, TopImageHeight);
			_offerTitle = new UILabel(tmpFrame);
			Add (_offerTitle);

		}

		public void SetItems(CGRect frame)
		{

			var margin = 20;
			var TopImageWidth = frame.Width-2*margin;
				var TopImageHeight = _viewModelOffer.Item.ContentPicture.Height;
			var xpos = frame.Width / 2 - TopImageWidth / 2;


			_offerImage = new UIImageView (new CGRect (xpos, margin, TopImageWidth, TopImageHeight));
			_offerImage.ContentMode = UIViewContentMode.ScaleAspectFit;
			_offerImage.Image = UIImage.FromBundle("Images/GhostSquareContentBig.jpg");
			Add(_offerImage);

			if (_viewModelOffer.Item.ContentPicture.Uri != "")
			{	
			   SetImage (_viewModelOffer.Item.ContentPicture.Uri);
			}

			_offerTitle = new UILabel(new CGRect(0, TopImageHeight+margin, frame.Width-2*margin, 100));
			Add (_offerTitle);

			_offerTitle.Text = _viewModelOffer.Item.Description;
			_offerTitle.TextAlignment = UITextAlignment.Center;
			_offerTitle.Font = UIFont.FromName ("Roboto-Regular", 20f);
		}

		private async void SetImage(string url)
		{
	    	_offerImage.Image = await LoadImage (url);
		}

		private async Task<UIImage> LoadImage (string imageUrl)
		{
			Task<byte[]> contentsTask = _viewModelOffer.LoadImage (imageUrl);
			var contents = await contentsTask;
			return UIImage.LoadFromData (NSData.FromArray (contents));  
		}


		private async void SetOfferImage(CGRect frame)
		{
			float TopImageHeight = (float)frame.Height / 4.6f;

			try
			{

			}
			catch 
			{
			}
		}
			
		private void SetInfoText(CGRect frame)
		{
			
		}

		private void SetButtomBarRate(CGRect frame)
		{
			
		}
			
		void LeftBtn_TouchUpInside (object sender, EventArgs e)
		{
			
		}

		void RightBtn_TouchUpInside (object sender, EventArgs e)
		{
		
		}
			
		private bool _visible; 
		public bool Visible 
		{
			get{ return _visible;}
			set 
			{
				_visible = value;
			}
		}

		private bool _isVisible; 
		public bool IsButtomBarVisible 
		{
			get{ return _isVisible;}
			set 
			{
				_isVisible = value;
			}
		}

		NSObject _keyboardObserverWillShow;
		NSObject _keyboardObserverWillHide;

		protected virtual void RegisterForKeyboardNotifications ()
		{
			_keyboardObserverWillShow = NSNotificationCenter.DefaultCenter.AddObserver (UIKeyboard.WillShowNotification, KeyboardWillShowNotification);
			_keyboardObserverWillHide = NSNotificationCenter.DefaultCenter.AddObserver (UIKeyboard.WillHideNotification, KeyboardWillHideNotification);
		}

		protected virtual void UnregisterKeyboardNotifications()
		{
			NSNotificationCenter.DefaultCenter.RemoveObserver(_keyboardObserverWillShow);
			NSNotificationCenter.DefaultCenter.RemoveObserver(_keyboardObserverWillHide);
		}

		protected virtual void KeyboardWillShowNotification (NSNotification notification)
		{
			CGRect r = UIKeyboard.BoundsFromNotification (notification);
		}

		protected virtual void KeyboardWillHideNotification (NSNotification notification)
		{
			CGRect r = UIKeyboard.BoundsFromNotification (notification);
		}
	}
}

