﻿using System;
using UIKit;
using Foundation;
using CoreGraphics;
using System.Threading.Tasks;
using NoBadDaysPCL.ViewModel;

namespace nobaddaysIOS
{
	public class OfferCell : UITableViewCell  {
		UILabel _titleLabel, _descriptionLabel, _moreLabel;
		UIImageView _offerImage, _arrowImage;
		public OfferCell (NSString cellId) : base (UITableViewCellStyle.Default, cellId)
		{
			SelectionStyle = UITableViewCellSelectionStyle.Gray;
			ContentView.BackgroundColor = UIColor.White;
		
			_offerImage = new UIImageView();
			_offerImage.Image = UIImage.FromBundle("Images/GhostCircle.png");

			_arrowImage = new UIImageView();
			_arrowImage.Image = UIImage.FromBundle ("Images/arrow.png");


			_titleLabel = new UILabel () {
				Font = UIFont.FromName("Roboto-Regular", 20f),
				TextColor = UIColor.Black,
				BackgroundColor = UIColor.Clear
			};
			_descriptionLabel = new UILabel () {
				Font = UIFont.FromName("Roboto-Regular", 18f),
				TextColor = UIColor.DarkGray,
				BackgroundColor = UIColor.Clear,
				Text = NSBundle.MainBundle.LocalizedString ("NotSpecifiedText", "NotSpecifiedText")
			};

			_moreLabel = new UILabel () {
				Font = UIFont.FromName("Roboto-Regular", 20f),
				TextColor = UIColor.DarkGray,
				TextAlignment = UITextAlignment.Center,
				BackgroundColor = UIColor.Clear
			};
	
			ContentView.AddSubviews(new UIView[] {_titleLabel, _descriptionLabel, _offerImage, _arrowImage, _moreLabel});

		}

		public void UpdateCell (string title, string desc, string url, OfferViewModel vm)
		{
			SetImage (url, vm);
			_titleLabel.Text = title;
			if ((desc != "")&&(desc != null))
			{	
			     _descriptionLabel.Text = desc;
				_descriptionLabel.TextColor = UIColor.Black;
			}
			_moreLabel.Text = "SEE AD";  //#string
			_arrowImage.Image = UIImage.FromBundle ("Images/arrow.png");

		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			_offerImage.Frame = new CGRect (10, 20, 60, 60);
			_titleLabel.Frame = new CGRect (80, 10, ContentView.Bounds.Width - 80, 25);
			_descriptionLabel.Frame = new CGRect (80, 50, ContentView.Bounds.Width - 80, 80);
			_moreLabel.Frame = new CGRect (ContentView.Bounds.Width - 100, 40, 80, 20);
			_arrowImage.Frame = new CGRect (ContentView.Bounds.Width - 20, 40, 15, 20);
		}


		private async void SetImage(string url, OfferViewModel vm)
		{
						
			_offerImage.Image = await LoadImage (url, vm);
			_offerImage.Layer.CornerRadius = 30;
			_offerImage.Layer.MasksToBounds = true;
		//	_offerImage.Layer.BorderColor = UIColor.Black.CGColor;
		//	_offerImage.Layer.BorderWidth = 1f;

		}

		private async Task<UIImage> LoadImage (string imageUrl, OfferViewModel vm)
		{
			Task<byte[]> contentsTask = vm.LoadImage (imageUrl);
			var contents = await contentsTask;
			return UIImage.LoadFromData (NSData.FromArray (contents));  
		}


	}
}

