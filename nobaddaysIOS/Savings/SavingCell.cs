﻿using System;
using UIKit;
using Foundation;
using CoreGraphics;
using System.Threading.Tasks;
using NoBadDaysPCL.ViewModel;

namespace nobaddaysIOS
{
	public class SavingCell : UITableViewCell  {
		UILabel _titleLabel, _amountLabel;
		UIImageView _shopImage;
		public SavingCell (NSString cellId) : base (UITableViewCellStyle.Default, cellId)
		{
			SelectionStyle = UITableViewCellSelectionStyle.Gray;
			ContentView.BackgroundColor = UIColor.LightGray;
		
			_shopImage = new UIImageView();
			_shopImage.Image = UIImage.FromBundle("Images/GhostCircle.png");

			_titleLabel = new UILabel () {
				Font = UIFont.FromName("Roboto-Regular", 20f),
				TextColor = UIColor.Black,
				BackgroundColor = UIColor.Clear
			};
			_amountLabel = new UILabel () {
				Font = UIFont.FromName("Roboto-Regular", 18f),
				TextColor = UIColor.Black,
				BackgroundColor = UIColor.Clear
			};
					
			ContentView.AddSubviews(new UIView[] {_titleLabel, _amountLabel, _shopImage});
		}

		public void UpdateCell (string title, string amount, string url, SavingsViewModel vm)
		{
			SetImage (url, vm);
			_titleLabel.Text = title;
			if (!string.IsNullOrEmpty(amount))
			{	
				_amountLabel.Text = amount;
			}
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			_shopImage.Frame = new CGRect (10, 20, 60, 60);
			_titleLabel.Frame = new CGRect (80, 40, ContentView.Bounds.Width - 120, 25);
			_amountLabel.Frame = new CGRect (ContentView.Bounds.Width - 60, 40, 60, 25);
		}


		private async void SetImage(string url, SavingsViewModel vm)
		{
			_shopImage.Image = await LoadImage (url, vm);
			_shopImage.Layer.CornerRadius = 30;
			_shopImage.Layer.MasksToBounds = true;
		}

		private async Task<UIImage> LoadImage (string imageUrl, SavingsViewModel vm)
		{
			Task<byte[]> contentsTask = vm.LoadImage (imageUrl);
			var contents = await contentsTask;
			return UIImage.LoadFromData (NSData.FromArray (contents));  
		}


	}
}

