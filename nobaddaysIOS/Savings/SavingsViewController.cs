using System;
using UIKit;
using CDCircleLib;
using CoreGraphics;
using System.Timers;
using System.Drawing;
using NoBadDaysPCL.ViewModel;
using NoBadDaysPCL.Items;
using SQLite.Net.Platform.XamarinIOS;
using Utils;
using System.Net;
using Foundation;
using System.Threading.Tasks;
using CoreLocation;
using System.Collections.Generic;
using Alliance.Carousel;

namespace nobaddaysIOS
{
	public partial class SavingsViewController : UIViewController
	{

		public static NSString SavingCellId = new NSString ("SavingCell");


		Timer timer = new Timer(1000);
		bool _timerRunning = false;

		UITableView _currentMonthTable;
		UITableView _previousMonthTable;

		CDCircle _circle = null;
		CDCircleOverlayView _overlay = null;
		DistanceCircleDelegate _events = null;
		int _currentWheelSelection = -1;//GlobalData.NBDINDEX;

		// Images
		UIImageView wheelImage = null;
		UIImageView blurOverlay = null;
		UIImageView SavingsImage;

		// Profile Image 
		UIImagePickerController imagePicker;

		UIScrollView imgViewPrevious;
		UIScrollView imgViewCurrent;
		UIImageView piggyImage;

		UILabel CountLabel;
		UILabel BandLabel;

		// Views
		MyNBDView myNbdView = null;
		AboutView aboutView = null;
		CharityView charityView = null;
		MapView mapView = null;
		CommunityView communityView = null;
		WinView winView = null;
		MyOffersView myOffersView = null;
		StoreView storeView = null;
		LoginView loginView = null;
		UIImageView _wheelLogo = null;
		UILabel _wheelLabel;

		UILabel HeaderText;
		UIView Wheel;

		UIImageView shadeImage;

		// Gestures
		UITapGestureRecognizer bckTap;
		UITapGestureRecognizer gestureRecognizer;

		UIActivityIndicatorView _spinner;
		CarouselView carousel;


		UIStringAttributes _wheelAttributes;


		private static UIRefreshControl _myfresh;
		public static UIRefreshControl RefreshControl
		{
			get { return _myfresh; }
			set { _myfresh = value; }
		}

		private UserViewModel _viewModel = null;
		UserViewModel ViewModel 
		{ 
			get 
			{ 
				if (_viewModel == null) 
				{
					string libraryPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
					var path = System.IO.Path.Combine(libraryPath, nobaddaysIOS.Container.SQLitePath);

					_viewModel = new UserViewModel(new SQLitePlatformIOS(), path);
				}
				return _viewModel;
			}
		}
			
		private SavingsViewModel _savingsViewModel = null;
		SavingsViewModel SavingsViewModel 
		{ 
			get 
			{ 
				if (_savingsViewModel == null) 
				{
					string libraryPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
					var path = System.IO.Path.Combine(libraryPath, nobaddaysIOS.Container.SQLitePath);

					_savingsViewModel = new SavingsViewModel(new SQLitePlatformIOS(), path);
					_savingsViewModel.PhoneDataSuccessful += DataSuccessful;
					_savingsViewModel.PhoneDataErrorOccurred += DataError;
				} 
				return _savingsViewModel;
			}
		}

		void DataSuccessful(object sender, ServerOutputItem e)
		{
			if (SavingsViewModel.Item != null) {

				CountLabel.Text = NSBundle.MainBundle.LocalizedString ("CalculatingText", "CalculatingText");
				// Animate pig
				double factorHeight = (double)SavingsViewModel.Item.ThisMonth.Amount / 100.0;
				double factorPos = 1 - factorHeight;
				if (factorHeight > 1) {
					factorHeight = 1.0;
					factorPos = 0.0;
				}

				CGRect endframe = new CGRect (View.Frame.Width / 3, (View.Frame.Height / 15) + ((View.Frame.Width / 3) * factorPos), View.Frame.Width / 3, (View.Frame.Width / 3) * factorHeight);
				System.Threading.ThreadPool.QueueUserWorkItem (state => InvokeOnMainThread (() => 
				UIView.AnimateNotify (1.0, 0.7, UIViewAnimationOptions.CurveEaseOut, () => {
					shadeImage.Frame = endframe;
				}, finished => {
					IncreaseCount ();
				})));

				// Fill current table
				if (SavingsViewModel.Item.ThisMonth.Savings.Count > 0)
				{	
					//_currentMonthTable.Hidden = false;
					_currentMonthTable.Source = new SavingsCurrentTableSource (_savingsViewModel);
					_currentMonthTable.ReloadData ();
					_currentMonthTable.SeparatorStyle = UITableViewCellSeparatorStyle.SingleLine;
					RefreshControl.EndRefreshing();
				}
				else
				{
					CountLabel.Text = "0" + SavingsViewModel.Item.ThisMonth.Unit; ;
					
					//_currentMonthTable.Hidden = true;
					var tmpFrame = new CGRect(0, View.Frame.Height / 10, View.Frame.Width, View.Frame.Height / 10);
					UILabel _noSavingsCurrentLabel = new UILabel (tmpFrame) {
						Font = UIFont.FromName("Roboto-Regular", 18f),
						TextColor = UIColor.Black,
						TextAlignment = UITextAlignment.Center,
						BackgroundColor = UIColor.Clear,
						Text = NSBundle.MainBundle.LocalizedString ("NoSavingsThisMonthText", "NoSavingsThisMonthText")
					};

					imgViewCurrent.Add(_noSavingsCurrentLabel);
				}

				// Fill previous table
				if (SavingsViewModel.Item.PreviousMonth.Savings.Count > 0)
				{	
					//_previousMonthTable.Hidden = false;
					_previousMonthTable.Source = new SavingsPreviousTableSource (_savingsViewModel);
					_previousMonthTable.ReloadData ();
					_previousMonthTable.SeparatorStyle = UITableViewCellSeparatorStyle.SingleLine;
				}
				else
				{
					//_previousMonthTable.Hidden = true;
					var tmpFrame = new CGRect(0, View.Frame.Height / 10, View.Frame.Width, View.Frame.Height / 10);
					UILabel _noSavingsPreviousLabel = new UILabel (tmpFrame) {
						Font = UIFont.FromName("Roboto-Regular", 18f),
						TextColor = UIColor.Black,
						TextAlignment = UITextAlignment.Center,
						BackgroundColor = UIColor.Clear,
						Text = NSBundle.MainBundle.LocalizedString ("NoSavingsPrevMonthText", "NoSavingsPrevMonthText")
					};
					imgViewPrevious.Add(_noSavingsPreviousLabel);
				}
			}
		}

		void DataError(object sender, ServerOutputItem e)
		{

		}

		private bool _visible; 
		public bool Visible 
		{
			get{ return _visible;}
			set 
			{
				_visible = value;
			}
		}

		public static LocationManager Manager { get; set;}
			
		public SavingsViewController (IntPtr handle) : base (handle)
		{
			_events = new DistanceCircleDelegate ();
			_events.WheelChanged += wheelChanged;

			Manager = new LocationManager();
			Manager.StartLocationUpdates();

			timer.Elapsed += Timer_Elapsed;
			timer.Enabled = false;
			_timerRunning = false;
		}

		void Timer_Elapsed (object sender, ElapsedEventArgs e)
		{
			timer.Stop ();
			InvokeOnMainThread (() => {
				
			    if (_currentWheelSelection != GlobalData.WheelSelection) 
			    {
			    	AnimateWheel ();
				    Navigate(true);
				    BlurView ();
			    }

			});
		}

		public void HandleLocationChanged (object sender, LocationUpdatedEventArgs e)
		{
			// Handle foreground updates
			CLLocation location = e.Location;
			GPSHelper.mylocation = e.Location;
		}

		private void SetCarousel()
		{
			if (carousel == null) {
				carousel = new CarouselView (new CGRect (0, View.Frame.Height / 2 - View.Frame.Height / 10, 
					View.Frame.Width, View.Frame.Height / 2));
				carousel.DataSource = new LinearDataSource (this);
				carousel.Delegate = new LinearDelegate (this);
				carousel.ConfigureView ();
				carousel.ClipsToBounds = true;
				carousel.CurrentItemIndex = 1;
				carousel.CarouselType = CarouselType.Linear;
				carousel.Bounces = false;
			}
			SavingsImage.AddSubview (carousel);
			SavingsImage.UserInteractionEnabled = true;
		}

		public class LinearDataSource : CarouselViewDataSource
		{
			SavingsViewController savViewTmp;

			public LinearDataSource(SavingsViewController viewTmp)
			{
				this.savViewTmp = viewTmp;
			}

			public override nint NumberOfItems(CarouselView carousel)
			{
				return 2;
			}

			public override UIView ViewForItem(CarouselView carousel, nint index, UIView reusingView)
			{
				if (index == 0) {
					reusingView = savViewTmp.GetPreviousSavings ();
				} else if (index == 1) {
					reusingView = savViewTmp.SetCurrentSavings ();
				}
				return reusingView;
			}
		}

		public class LinearDelegate : CarouselViewDelegate
		{
			SavingsViewController savViewTmp;

			public LinearDelegate(SavingsViewController winView)
			{
				this.savViewTmp = winView;
			}

			public override nfloat ValueForOption(CarouselView carousel, CarouselOption option, nfloat aValue)
			{
				if (option == CarouselOption.Spacing)
				{
					return aValue * 2.1f;
				}
				return aValue;
			}

			public override void DidSelectItem(CarouselView carousel, nint index)
			{

			}

			public override void CurrentItemIndexDidChange (CarouselView carouselView)
			{
				base.CurrentItemIndexDidChange (carouselView);
				savViewTmp.HandleSelect (carouselView.CurrentItemIndex);
			}

			public override void DidEndScrollingAnimation (CarouselView carouselView)
			{
				base.DidEndScrollingAnimation (carouselView);
			}
		}

		private UIScrollView GetPreviousSavings()
		{
			if (imgViewPrevious == null) {
				var tmpFrame = new CGRect (0, 0, View.Frame.Width, View.Frame.Height / 2);
				imgViewPrevious = new UIScrollView (tmpFrame);
				//imgViewPrevious.Image = GlobalData.GrayBackGround;

				_previousMonthTable = new UITableView (tmpFrame);
				_previousMonthTable.Source = new SavingsPreviousTableSource (_savingsViewModel);
				_previousMonthTable.SeparatorStyle = UITableViewCellSeparatorStyle.None;
				_previousMonthTable.ClearsContextBeforeDrawing = true;
				_previousMonthTable.AutoresizingMask = UIViewAutoresizing.All;
				_previousMonthTable.RowHeight = 120f;
				_previousMonthTable.Bounces = true;
				_previousMonthTable.AllowsSelection = false;
				_previousMonthTable.UserInteractionEnabled = true; 
				_previousMonthTable.ScrollEnabled = true;

				RefreshControl = new UIRefreshControl ();
				RefreshControl.TintColor = UIColor.LightGray;
				RefreshControl.ValueChanged += HandleValueChanged;
				_previousMonthTable.AddSubview (RefreshControl);

				imgViewPrevious.Add (_previousMonthTable);
			}
		
			return imgViewPrevious;
		}

		private UIScrollView SetCurrentSavings()
		{
			if (imgViewCurrent == null) {
				var tmpFrame = new CGRect (0, 0, View.Frame.Width, View.Frame.Height / 2);
				imgViewCurrent = new UIScrollView (tmpFrame);

				_currentMonthTable = new UITableView (tmpFrame);
				_currentMonthTable.Source = new SavingsCurrentTableSource (_savingsViewModel);
				_currentMonthTable.SeparatorStyle = UITableViewCellSeparatorStyle.None;
				_currentMonthTable.ClearsContextBeforeDrawing = true;
				_currentMonthTable.AutoresizingMask = UIViewAutoresizing.All;
				_currentMonthTable.RowHeight = 120f;
				_currentMonthTable.Bounces = true;
				_currentMonthTable.AllowsSelection = false;
				_currentMonthTable.UserInteractionEnabled = true; 
				_currentMonthTable.ScrollEnabled = true;

				RefreshControl = new UIRefreshControl ();
				RefreshControl.TintColor = UIColor.LightGray;
				RefreshControl.ValueChanged += HandleValueChanged;
				_currentMonthTable.AddSubview (RefreshControl);
				imgViewCurrent.Add (_currentMonthTable);
			}

			return imgViewCurrent;
		}

		void HandleValueChanged (object sender, EventArgs e)
		{
			RefreshControl.EndRefreshing ();
			SavingsViewModel.GetSavings ();
		}

		private void HandleSelect(nint indexTmp)
		{
			if (indexTmp == 0) {
				BandLabel.Text = NSBundle.MainBundle.LocalizedString ("PreviousMonthText", "PreviousMonthText");
				if (SavingsViewModel.Item.PreviousMonth != null) {
					CountLabel.Text = SavingsViewModel.Item.PreviousMonth.Amount.ToString () +
					SavingsViewModel.Item.PreviousMonth.Unit;
					shadeImage.Hidden = true;
				}
				piggyImage.Image = UIImage.FromBundle ("Images/Piggybank_NotActive_512x512.png");
			} else {
				BandLabel.Text = NSBundle.MainBundle.LocalizedString ("ThisMonthText", "ThisMonthText");
				if (SavingsViewModel.Item.ThisMonth != null) {
					CountLabel.Text = SavingsViewModel.Item.ThisMonth.Amount.ToString () +
					SavingsViewModel.Item.ThisMonth.Unit;
					shadeImage.Hidden = false;
				}
				piggyImage.Image = UIImage.FromBundle ("Images/PiggyBank_Active_512x512.png");
			}
		}

		void wheelChanged (object sender, int e)
		{
			
			timer.Stop ();
			timer.Start ();


			if (GlobalData.WheelSelection == GlobalData.SAVINGSINDEX) 
			{
				_wheelLabel.AttributedText = new NSAttributedString(NSBundle.MainBundle.LocalizedString ("SAVINGSText", "SAVINGSText"), _wheelAttributes);
			} 
			else if (GlobalData.WheelSelection == GlobalData.WININDEX) 
			{
				_wheelLabel.AttributedText = new NSAttributedString(NSBundle.MainBundle.LocalizedString ("WinText", "WinText"), _wheelAttributes);
			}
			else if (GlobalData.WheelSelection == GlobalData.CHARITYINDEX) 
			{
				_wheelLabel.AttributedText = new NSAttributedString(NSBundle.MainBundle.LocalizedString ("CharityText", "CharityText"), _wheelAttributes);
			}
			else if (GlobalData.WheelSelection == GlobalData.INFOINDEX) 
			{
				_wheelLabel.AttributedText = new NSAttributedString(NSBundle.MainBundle.LocalizedString ("InfoText", "InfoText"), _wheelAttributes);
			}
			else if (GlobalData.WheelSelection == GlobalData.OFFERSINDEX) 
			{
				_wheelLabel.AttributedText = new NSAttributedString(NSBundle.MainBundle.LocalizedString ("OffersText", "OffersText"), _wheelAttributes);
			}
			else if (GlobalData.WheelSelection == GlobalData.MAPINDEX) 
			{
				_wheelLabel.AttributedText = new NSAttributedString(NSBundle.MainBundle.LocalizedString ("MapText", "MapText"), _wheelAttributes);
			}  
			else if (GlobalData.WheelSelection == GlobalData.NBDINDEX) 
			{
				_wheelLabel.AttributedText = new NSAttributedString(NSBundle.MainBundle.LocalizedString ("MyNoBadDaysText", "MyNoBadDaysText"), _wheelAttributes);
			}  
			else if (GlobalData.WheelSelection == GlobalData.COMMUNITYINDEX) 
			{
				_wheelLabel.AttributedText = new NSAttributedString(NSBundle.MainBundle.LocalizedString ("CommunityText", "CommunityText"), _wheelAttributes);
			}  

		}



		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);
		}
			
		public override void ViewDidLoad ()
		{

			UIApplication.Notifications.ObserveDidBecomeActive ((sender, args) => {
				Manager.LocationUpdated += HandleLocationChanged;
			});

			// Whenever the app enters the background state, we unsubscribe from the event 
			// so we no longer perform foreground updates
			UIApplication.Notifications.ObserveDidEnterBackground ((sender, args) => {
				Manager.LocationUpdated -= HandleLocationChanged;
			});
				
			InitializeView ();
			HeaderText.RemoveFromSuperview ();
	
		    GlobalData.WheelSelection = GlobalData.NBDINDEX;
			if (_wheelLabel != null)
			{	
				_wheelLabel.AttributedText = new NSAttributedString(NSBundle.MainBundle.LocalizedString ("MyNoBadDaysText", "MyNoBadDaysText"), _wheelAttributes);
			
		    	_wheelLabel.Hidden = true;
			}
	    	Navigate (false);
			
			base.ViewDidLoad ();
		}

		private void InitializeView()
		{
			GlobalData.UserItem = ViewModel.GetUser ();
			if (GlobalData.UserItem != null && GlobalData.UserItem.UserId > 0) {
				GlobalData.IsLoggedIn = true;
			} else {
				GlobalData.IsLoggedIn = false;
			}

			// BackGround
			UIImageView bck = new UIImageView(new CGRect (0, 0, View.Frame.Width, View.Frame.Height));
			bck.Image = GlobalData.RedBackGround;
			View.Add (bck);

			UIImageView header = new UIImageView(new CGRect (0, 0, View.Frame.Width, View.Frame.Height / 10));
			header.Image = GlobalData.BlackBackGround;
			Add (header);

			AddHeader ();
			AddSavingsPicture ();

			// Add blur effect
			if (blurOverlay == null) {
				blurOverlay = new UIImageView (new CGRect (0, View.Frame.Height / 11, View.Frame.Width, View.Frame.Height - View.Frame.Height / 11));
				blurOverlay.Image = GlobalData.GrayBackGround;
			}
			blurOverlay.Layer.Opacity = 0;
			blurOverlay.UserInteractionEnabled = false;

			gestureRecognizer = new UITapGestureRecognizer ((g) => {
				if (Visible) {
					AnimateWheel ();
					BlurView ();
				}
			});

			blurOverlay.AddGestureRecognizer (gestureRecognizer);

			Add (blurOverlay);

			GlobalData.ViewFrameWidth = View.Frame.Width;
			GlobalData.ViewFrameHeight = View.Frame.Height;

			AddWheel ();
		}

		private void AddHeader()
		{
			var tmpFrame = new CGRect(0, 0, View.Frame.Width, View.Frame.Height / 10);
			HeaderText = new UILabel(tmpFrame);
			HeaderText.TextAlignment = UITextAlignment.Center;
			HeaderText.AttributedText = new NSAttributedString(NSBundle.MainBundle.LocalizedString ("SAVINGSText", "SAVINGSText"), GlobalData.HeaderAttrib);
			Add (HeaderText);
		}

		private void AddBand()
		{
			UIImageView bck = new UIImageView(new CGRect (0, View.Frame.Height / 3, View.Frame.Width, View.Frame.Height / 15));
			bck.Image = GlobalData.BlackBackGround;

			BandLabel = new UILabel(new CGRect (0, 0, View.Frame.Width, View.Frame.Height / 15));
			BandLabel.Text = NSBundle.MainBundle.LocalizedString ("ThisMonthText", "ThisMonthText");
			BandLabel.TextAlignment = UITextAlignment.Center;
			BandLabel.TextColor = UIColor.White;
			bck.Add (BandLabel);

			SavingsImage.AddSubview (bck);
		}

		private void AddSavingsPicture()
		{
			if (SavingsImage == null) {
				SavingsImage = new UIImageView (new CGRect (0, View.Frame.Height / 11, View.Frame.Width, View.Frame.Height - View.Frame.Height / 11));
												
				shadeImage = new UIImageView (new CGRect (View.Frame.Width / 3, View.Frame.Height / 15 + View.Frame.Width / 3, View.Frame.Width / 3, 0));
				shadeImage.Image = GlobalData.BlackBackGround;
				SavingsImage.AddSubview (shadeImage);

				piggyImage = new UIImageView (new CGRect (View.Frame.Width / 3, View.Frame.Height / 17, View.Frame.Width / 2.8, View.Frame.Width / 2.8));
				piggyImage.Image = UIImage.FromBundle ("Images/PiggyBank_Active_512x512.png");
				SavingsImage.AddSubview (piggyImage);

				CountLabel = new UILabel (new CGRect (View.Frame.Width / 3, View.Frame.Height / 15, View.Frame.Width / 3, View.Frame.Width / 3));
				CountLabel.TextAlignment = UITextAlignment.Center;
				CountLabel.TextColor = UIColor.White;
				SavingsImage.AddSubview (CountLabel);

				Add (SavingsImage);
			}

			AddBand ();
			SetCarousel ();
			SavingsViewModel.GetSavings ();
		}
			
		public async void IncreaseCount()
		{
			if (CountLabel != null) {
				int index = 0;
				while (index < SavingsViewModel.Item.ThisMonth.Amount) {
					index++;
					CountLabel.Text = index.ToString () + SavingsViewModel.Item.ThisMonth.Unit;
					await Task.Delay (10);
				}
			}
		}

		private void AddMonthText()
		{
		}


		public void StartSpinner()
		{
			var frame = View.Bounds;
			_spinner = new UIActivityIndicatorView (new CGRect ((frame.Height / 11) + 10, 15, 30, 30));
			_spinner.StartAnimating ();
			Add(_spinner);
		}

		public void StopSpinner()
		{
			_spinner.RemoveFromSuperview ();
		}



		public void WheelUserInteractionEnabled(bool enable)
		{
			if (enable) {
				AddWheel ();
			} else {
				wheelImage.RemoveFromSuperview ();
				wheelImage.UserInteractionEnabled = false;
				wheelImage.Hidden = true;
			}
			//wheelImage.UserInteractionEnabled = enable;
		}

		private void AddWheel()
		{
			if (wheelImage == null) {
				wheelImage = new UIImageView (new CGRect (5, 5, View.Frame.Height / 11, View.Frame.Height / 11));
				wheelImage.Image = UIImage.FromBundle ("Images/menubutton.png");
				newCircleView ();
				Visible = false;
			}

			bckTap = new UITapGestureRecognizer ((g) => 
				{
					AnimateWheel ();
					BlurView();
				});
			wheelImage.AddGestureRecognizer (bckTap);

			wheelImage.UserInteractionEnabled = true;
			Add(wheelImage);
		}

		private void newCircleView ()
		{
			GlobalData.WheelPosition = 20 + 2 * View.Frame.Height / 3;
			
			Wheel = new UIView (new CGRect(0, View.Frame.Height, View.Frame.Width, View.Frame.Width));

			GlobalData.ContainerHeight = (float)Wheel.Frame.Height;
			GlobalData.ContainerWidth = (float)Wheel.Frame.Width;

			_circle = GlobalData.Circle;
			_circle.CircleColor = new UIColor(255f/255f, 63f/255f, 63f/255f, 255f/255f);
			_circle.Delegate = _events;
			_overlay = GlobalData.Overlay;
		
			var diameter = GlobalData.WheelDiameter;
			var radius = diameter/2;
			int imageRadius = 54;

			_wheelAttributes = new UIStringAttributes {
				ForegroundColor = UIColor.FromRGB(32,32,32),
				BackgroundColor = UIColor.Clear,
				Font = UIFont.FromName("Roboto-Regular", 20f)
			};
					

			var tmpFrame = new CGRect(0, (float)(GlobalData.ContainerHeight-diameter)/2+radius-imageRadius - 130, View.Frame.Width, 40);
			_wheelLabel = new UILabel(tmpFrame);
			_wheelLabel.TextAlignment = UITextAlignment.Center;
							

			_wheelLogo = new UIImageView();
			_wheelLogo.Image = UIImage.FromBundle ("Wheel/wheellogo.png");
			_wheelLogo.Frame = new CGRect ((float)(GlobalData.ContainerWidth-diameter)/2+ radius-imageRadius, (float)(GlobalData.ContainerHeight-diameter) / 2+ radius-imageRadius, imageRadius*2, imageRadius*2);

			Wheel.Add(_wheelLabel);
			Wheel.Add(_wheelLogo);
			Wheel.Add (_circle);
			Wheel.Add (_overlay);
			Wheel.BringSubviewToFront (_wheelLogo);


			View.Add (Wheel);
		}
			
		public override void DidReceiveMemoryWarning ()
		{
			base.DidReceiveMemoryWarning ();
		}

		private void Navigate(bool delay)
		{
			if (delay)
				System.Threading.Thread.Sleep (3 * 300);


			if (blurOverlay != null) {
				blurOverlay.RemoveFromSuperview ();
			}
			if (Wheel != null) {
				Wheel.RemoveFromSuperview ();
			}
			if (wheelImage != null) {
				wheelImage.RemoveFromSuperview ();
			}

			if (GlobalData.WheelSelection != GlobalData.NBDINDEX) 
			{
				HeaderText.RemoveFromSuperview ();
			}
				
			if (GlobalData.WheelSelection == GlobalData.SAVINGSINDEX) 
			{
				View.Add (HeaderText);
				AddSavingsPicture ();
			} 
			else if (GlobalData.WheelSelection == GlobalData.WININDEX) 
			{
				if (winView == null) {
					winView = new WinView (this);
				}
				View.Add (winView);
			}
			else if (GlobalData.WheelSelection == GlobalData.CHARITYINDEX) 
			{
				if (charityView == null) {
					charityView = new CharityView (this);
				}
				View.Add (charityView);
			}
			else if (GlobalData.WheelSelection == GlobalData.INFOINDEX) 
			{
				if (aboutView == null) {
					aboutView = new AboutView (this);
				}
				View.Add (aboutView);
			}
			else if (GlobalData.WheelSelection == GlobalData.OFFERSINDEX) 
			{
				if (myOffersView == null) {
					myOffersView = new MyOffersView (this);
				}
				View.Add (myOffersView);
			}
			else if (GlobalData.WheelSelection == GlobalData.MAPINDEX) 
			{
				if (mapView == null) {
					mapView = new MapView (this);
				}
				View.Add (mapView);
			}  
			else if (GlobalData.WheelSelection == GlobalData.NBDINDEX) 
			{
				if (myNbdView == null) {
					myNbdView = new MyNBDView (this);
					myNbdView.LogOutEvent += (object sender, EventArgs e) =>
					{
						HandleLogOut ();
					};
					myNbdView.LogInEvent += (object sender, bool e) =>
					{
						HandleLogIn (e);
					};
				}
				View.Add (myNbdView);
			}  
			else if (GlobalData.WheelSelection == GlobalData.COMMUNITYINDEX) 
			{
				if (communityView == null) {
					communityView = new CommunityView (this);
				}
				View.Add (communityView);
			}  

			CleanViews ();

			Add (blurOverlay);
			Add (Wheel);
			AddWheel();

			_currentWheelSelection = GlobalData.WheelSelection;
		}

		private void CleanViews()
		{
			// Clean
			if ((_currentWheelSelection == GlobalData.SAVINGSINDEX)||(_currentWheelSelection == -1))  
			{
				if (SavingsImage != null) 
				{
					SavingsImage.RemoveFromSuperview ();
					SavingsImage.Dispose();
					SavingsImage = null;
				}
			}
			else if (_currentWheelSelection == GlobalData.WININDEX) 
			{
				if (winView != null) 
				{
					winView.RemoveFromSuperview ();
					winView.Dispose();
					winView = null;
				}
			}
			else if (_currentWheelSelection == GlobalData.CHARITYINDEX) 
			{
				if (charityView != null) 
				{
					charityView.RemoveFromSuperview ();
					charityView.Dispose();
					charityView = null;
				}
			}
			else if (_currentWheelSelection == GlobalData.INFOINDEX) 
			{
				if (aboutView != null) 
				{
					aboutView.RemoveFromSuperview ();
					aboutView.Dispose();
					aboutView = null;
				}
			}
			else if (_currentWheelSelection == GlobalData.OFFERSINDEX) 
			{
				if (myOffersView != null) 
				{
					myOffersView.RemoveFromSuperview ();
					myOffersView.Dispose();
					myOffersView = null;
				}
			}
			else if (_currentWheelSelection == GlobalData.MAPINDEX) 
			{
				if (mapView != null) 
				{
					mapView.RemoveFromSuperview ();
					mapView.Dispose();
					mapView = null;
				}

				if (storeView != null) 
				{
					storeView.RemoveFromSuperview ();
					storeView.Dispose();
					storeView = null;
				}
			}
			else if (_currentWheelSelection == GlobalData.NBDINDEX) 
			{
				if (myNbdView != null) 
				{
					myNbdView.RemoveFromSuperview ();
					myNbdView.Dispose();
					myNbdView = null;
				}
			}
			else if (_currentWheelSelection == GlobalData.COMMUNITYINDEX) 
			{
				if (communityView != null) 
				{
					communityView.RemoveFromSuperview ();
					communityView.Dispose();
					communityView = null;
				}
			}

			if (loginView != null) {
				loginView.RemoveFromSuperview ();
				loginView.Dispose ();
				loginView = null;
			}
		}

		private void HandleLogOut()
		{
			GlobalData.IsLoggedIn = false;
			GlobalData.UserItem = null;

			wheelImage.RemoveFromSuperview ();
			wheelImage.Dispose ();
			wheelImage = null;
			CleanViews ();

			GlobalData.WheelSelection = GlobalData.NBDINDEX;
			_currentWheelSelection = -1;

			Navigate (false);
		}

		private void HandleLogIn(bool login)
		{
			CleanViews ();
			if (loginView == null) {
				loginView = new LoginView (this, login);
				loginView.LogInEvent += (object sender, bool e) =>
				{
					HandleLogIn (e);
				};

				loginView.LoginViewDone += (object sender, EventArgs e) =>
				{
					GlobalData.WheelSelection = GlobalData.NBDINDEX;
					_currentWheelSelection = -1;
					Navigate (false);
				};
			}
			View.Add (loginView);
			Add (blurOverlay);
			Add (Wheel);
			AddWheel();
		}

		private void AnimateWheel()
		{
			nfloat height = Visible == true ?  View.Frame.Height : GlobalData.WheelPosition;  
			CGRect endframe = new CGRect(0, height, View.Frame.Width, View.Frame.Width);

			if (Visible) {
				timer.Stop ();
				System.Threading.ThreadPool.QueueUserWorkItem (state => InvokeOnMainThread (() => 
					UIView.AnimateNotify (3*0.3, 3*0.1, UIViewAnimationOptions.TransitionCurlDown, () => {
						Wheel.Frame = endframe;

					}, finished => {
						Visible = !Visible;
						_wheelLabel.Hidden = true;
					})));
			} else {
				timer.Stop ();
				timer.Start ();	
				_wheelLabel.Hidden = false;

				System.Threading.ThreadPool.QueueUserWorkItem (state => InvokeOnMainThread (() => 
					UIView.AnimateNotify (3*0.3, 0, UIViewAnimationOptions.TransitionCurlUp, () => {
					Wheel.Frame = endframe;

				}, finished => {
					Visible = !Visible;
				})));
			}
		}

		private async void BlurView()
		{
			if (!Visible) {
				if (blurOverlay == null) 
				{
					blurOverlay = new UIImageView (new CGRect (0, View.Frame.Height / 11, View.Frame.Width, View.Frame.Height - View.Frame.Height / 11));
					blurOverlay.Image = GlobalData.GrayBackGround;
					Add (blurOverlay);
				}
				blurOverlay.Layer.Opacity = 0;
				blurOverlay.UserInteractionEnabled = true;

				await UIView.AnimateAsync (0.4f, () => {
					blurOverlay.Layer.Opacity = 0.7f;
				});
			} 
			else 
			{
				if (blurOverlay != null) 
				{
					blurOverlay.Layer.Opacity = 0.7f;
					await UIView.AnimateAsync (0.4f, () => {
						blurOverlay.Layer.Opacity = 0f;
					});

					blurOverlay.UserInteractionEnabled = false;
				}
			}
		}


	}
}



