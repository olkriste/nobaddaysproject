﻿using System;
using CoreGraphics;
using UIKit;
using System.Timers;
using Foundation;
using System.Collections.Generic;
using NoBadDaysPCL;
using System.IO;
using SQLite.Net.Platform.XamarinIOS;
using Valore.IOSDropDown;
using Security;
using Utils;
using NoBadDaysPCL.Items;
using NoBadDaysPCL.ViewModel;
using System.Threading.Tasks;
using System.Net;
using System.Linq;


namespace nobaddaysIOS
{
	public class SavingsPreviousTableSource: UITableViewSource 
	{
		SavingsViewModel _viewModel;

		public SavingsPreviousTableSource (SavingsViewModel viewModelSavings)
		{
			_viewModel = viewModelSavings;
		}

		public override nint RowsInSection (UITableView tableview, nint section)
		{
			if (_viewModel != null) {
				return _viewModel.Item.PreviousMonth.Savings.Count;
			} else {
				return 0;
			}
		}

		/// <summary>
		/// Called by the table view to determine whether or not the row is editable
		/// </summary>
		public override bool CanEditRow (UITableView tableView, NSIndexPath indexPath)
		{
			return false;
		}

		/// <summary>
		/// Called by the table view to determine whether or not the row is moveable
		/// </summary>
		public override bool CanMoveRow (UITableView tableView, NSIndexPath indexPath)
		{
			return false;
		}

		public override nint NumberOfSections (UITableView tableView)
		{
			return 1;
		}

		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			var cellid = SavingsViewController.SavingCellId;
			var cell = (SavingCell)tableView.DequeueReusableCell (cellid) as SavingCell;

			if (cell == null) {
				cell = new SavingCell (cellid); 
			}

			if (_viewModel.Item.ThisMonth.Savings.Count > 0)
			{  
				var title = _viewModel.Item.PreviousMonth.Savings.ElementAt (indexPath.Row).Name;
				var desc = _viewModel.Item.PreviousMonth.Savings.ElementAt (indexPath.Row).Amount;
				var image = _viewModel.Item.PreviousMonth.Savings.ElementAt (indexPath.Row).Picture.Uri;

				cell.UpdateCell (title, desc, image, _viewModel);
			}

			return cell;
		}

		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
		}
	}
}

