﻿using Newtonsoft.Json;
using NoBadDaysPCL.Items;
using NoBadDaysPCL.SQLite;
using SQLite.Net.Interop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace NoBadDaysPCL.ViewModel
{
    public class MapViewModel
    {
        public HashSet<MapItem> Items { get; private set; }

        NBDDatabase dataBase;

        public event EventHandler<ServerOutputItem> PhoneDataErrorOccurred;
        public event EventHandler<ServerOutputItem> PhoneDataSuccessful;

        ServerInputItem input;
        ServerOutputItem output;
        HttpClient client;

        public MapViewModel(ISQLitePlatform platform, string path)
        {
            this.Items = new HashSet<MapItem>();
            this.input = new ServerInputItem();
            this.output = new ServerOutputItem();
            this.client = new HttpClient();

            if (!string.IsNullOrEmpty(path))
            {
                if (platform != null)
                {
                    dataBase = new NBDDatabase(platform, path);
                }
            }
        }

        void RaiseDataErrorOccurred(ServerOutputItem result)
        {
            if (PhoneDataErrorOccurred != null)
            {
                PhoneDataErrorOccurred(this, result);
            }
        }

        void RaiseDataSuccessful(ServerOutputItem result)
        {
            if (PhoneDataSuccessful != null)
            {
                PhoneDataSuccessful(this, result);
            }
        }

        public async Task GetShops(MapSearchItem map)
        {
            try
            {
                if (map != null)
                {

                    if (map.Gridx <= 0)
                    {
                        output.Result = MainClass.eServerResult.InvalidPhone;
                        output.ResultString = "Gridx must be > 0";
                        RaiseDataErrorOccurred(output);
                        return;
                    }

                    if (map.Gridy <= 0)
                    {
                        output.Result = MainClass.eServerResult.InvalidPhone;
                        output.ResultString = "Gridy must be > 0";
                        RaiseDataErrorOccurred(output);
                        return;
                    }

                    if (map.Zoomlevel <= 0)
                    {
                        output.Result = MainClass.eServerResult.InvalidPhone;
                        output.ResultString = "Zoomlevel must be > 0";
                        RaiseDataErrorOccurred(output);
                        return;
                    }

                    if (map.ZoomlevelClusterStop <= 0)
                    {
                        output.Result = MainClass.eServerResult.InvalidPhone;
                        output.ResultString = "ZoomlevelClusterStop must be > 0";
                        RaiseDataErrorOccurred(output);
                        return;
                    }

                    if(string.IsNullOrEmpty(map.Search))
                    {
                        map.Search = "";
                    }

                    input.Function = MainClass.eServerFunction.AppGetMapShops;
                    input.Parameters = JsonConvert.SerializeObject(map);

                    string inputString = JsonConvert.SerializeObject(input);
                    string uri = MainClass.ServerUri + inputString;

                    var res = await client.GetStringAsync(new Uri(uri));

                    if (res != null)
                    {
                        output = JsonConvert.DeserializeObject<ServerOutputItem>(res.ToString());

                        Items = JsonConvert.DeserializeObject<HashSet<MapItem>>(output.ServerData);
						RaiseDataSuccessful(output);
                    }
                    else
                    {
                        output.Result = MainClass.eServerResult.NetWorkError;
                        output.ResultString = "Cuould not reach server";
                        RaiseDataErrorOccurred(output);
                    }
                }
                else
                {
                    output.Result = MainClass.eServerResult.IncorrectInputParameter;
                    output.ResultString = "Parameter not valid";
                    RaiseDataErrorOccurred(output);
                }
            }
            catch (Exception ex)
            {
                output.Result = MainClass.eServerResult.NetWorkError;
                output.ResultString = "Offline";
                RaiseDataErrorOccurred(output);
            }
        }

		public async Task<byte[]> LoadImage (string imageUrl)
		{
			try
			{
				return await client.GetByteArrayAsync(imageUrl);
			}
			catch
			{
				return null;
			}
		}
    }
}
