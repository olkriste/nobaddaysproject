﻿using Newtonsoft.Json;
using NoBadDaysPCL.Items;
using NoBadDaysPCL.SQLite;
using SQLite.Net.Interop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace NoBadDaysPCL.ViewModel
{
    public class ShopViewModel
    {
        public AppShopItem Item { get; private set; }
        public HashSet<RatingItem> Items { get; private set; }

        NBDDatabase dataBase;

        public event EventHandler<ServerOutputItem> PhoneDataErrorOccurred;
        public event EventHandler<ServerOutputItem> PhoneDataSuccessful;
		public event EventHandler<ServerOutputItem> RatingSuccessful;

        ServerInputItem input;
        ServerOutputItem output;
        HttpClient client;

        public ShopViewModel(ISQLitePlatform platform, string path)
        {
            this.Item = new AppShopItem();
            this.Items = new HashSet<RatingItem>();
            this.input = new ServerInputItem();
            this.output = new ServerOutputItem();
            this.client = new HttpClient();

            if (!string.IsNullOrEmpty(path))
            {
                if (platform != null)
                {
                    dataBase = new NBDDatabase(platform, path);
                }
            }
        }

        void RaiseDataErrorOccurred(ServerOutputItem result)
        {
            if (PhoneDataErrorOccurred != null)
            {
                PhoneDataErrorOccurred(this, result);
            }
        }

        void RaiseDataSuccessful(ServerOutputItem result)
        {
            if (PhoneDataSuccessful != null)
            {
                PhoneDataSuccessful(this, result);
            }
        }

		void RaiseRatingSuccessful(ServerOutputItem result)
		{
			if (RatingSuccessful != null)
			{
				RatingSuccessful(this, result);
			}
		}

        public async void GetShop(int shopId)
        {
            try
            {
                if (shopId > 0)
                {
                    AppShopItem shop = new AppShopItem();
                    shop.ShopId = shopId;

                    input.Function = MainClass.eServerFunction.AppGetShop;
                    input.Parameters = JsonConvert.SerializeObject(shop);

                    string inputString = JsonConvert.SerializeObject(input);
                    string uri = MainClass.ServerUri + inputString;

                    var res = await client.GetStringAsync(new Uri(uri));
                    if (res != null)
                    {
                        output = JsonConvert.DeserializeObject<ServerOutputItem>(res.ToString());

                        if (output.Result == MainClass.eServerResult.OK)
                        {
                            this.Item = JsonConvert.DeserializeObject<AppShopItem>(output.ServerData);
                            RaiseDataSuccessful(output);
                        }
                        else
                        {
                            RaiseDataErrorOccurred(output);
                        }
                    }
                    else
                    {
                        output.Result = MainClass.eServerResult.NetWorkError;
                        output.ResultString = "Cuould not reach server";
                        RaiseDataErrorOccurred(output);
                    }
                }
                else
                {
                    output.Result = MainClass.eServerResult.IncorrectInputParameter;
                    output.ResultString = "Parameter not valid";
                    RaiseDataErrorOccurred(output);
                }
            }
            catch (Exception e)
            {
                output.Result = MainClass.eServerResult.NetWorkError;
                output.ResultString = "Offline";
                RaiseDataErrorOccurred(output);
            }
        }

        public async void GetShopRatings(int shopId)
        {
            try
            {
                if (shopId > 0)
                {
                    //AppShopItem shop = new AppShopItem();
                    //shop.ShopId = shopId;

                    input.Function = MainClass.eServerFunction.AppGetShopRating;
                    input.Parameters = JsonConvert.SerializeObject(shopId);

                    string inputString = JsonConvert.SerializeObject(input);
                    string uri = MainClass.ServerUri + inputString;

                    var res = await client.GetStringAsync(new Uri(uri));
                    if (res != null)
                    {
                        output = JsonConvert.DeserializeObject<ServerOutputItem>(res.ToString());

                        if (output.Result == MainClass.eServerResult.OK)
                        {
                            this.Items = JsonConvert.DeserializeObject<HashSet<RatingItem>>(output.ServerData);
                            RaiseDataSuccessful(output);
                        }
                        else
                        {
                            RaiseDataErrorOccurred(output);
                        }
                    }
                    else
                    {
                        output.Result = MainClass.eServerResult.NetWorkError;
                        output.ResultString = "Cuould not reach server";
                        RaiseDataErrorOccurred(output);
                    }
                }
                else
                {
                    output.Result = MainClass.eServerResult.IncorrectInputParameter;
                    output.ResultString = "Parameter not valid";
                    RaiseDataErrorOccurred(output);
                }
            }
            catch (Exception e)
            {
                output.Result = MainClass.eServerResult.NetWorkError;
                output.ResultString = "Offline";
                RaiseDataErrorOccurred(output);
            }
        }

        public async void RateShop(RatingItem rating)
        {
            try
            {
                if (rating == null)
                {
                    output.Result = MainClass.eServerResult.IncorrectInputParameter;
                    output.ResultString = "RatingItem cannot be null";
                    RaiseDataErrorOccurred(output);
                    return;
                }

                if (rating.ShopId <= 0)
                {
                    output.Result = MainClass.eServerResult.IncorrectInputParameter;
                    output.ResultString = "ShopId must be > 0";
                    RaiseDataErrorOccurred(output);
                    return;
                }

                if (rating.Rating <= 0)
                {
                    output.Result = MainClass.eServerResult.IncorrectInputParameter;
                    output.ResultString = "Rating must be > 0";
                    RaiseDataErrorOccurred(output);
                    return;
                }

                if (dataBase == null)
                {
                    output.Result = MainClass.eServerResult.DatabaseError;
                    output.ResultString = "DatabaseError cannot be null";
                    RaiseDataErrorOccurred(output);
                    return;
                }

                UserItem user = dataBase.GetUser();

                if (user == null)
                {
                    output.Result = MainClass.eServerResult.UserNotFound;
                    output.ResultString = "You are not logged in";
                    RaiseDataErrorOccurred(output);
                    return;
                }

                if (string.IsNullOrEmpty(rating.Text))
                {
                    rating.Text = "";
                }

                rating.UserId = user.UserId;
                input.AdminToken = user.Token;

                input.Function = MainClass.eServerFunction.AppRateShop;
                input.Parameters = JsonConvert.SerializeObject(rating);

                string inputString = JsonConvert.SerializeObject(input);
                string uri = MainClass.ServerUri + inputString;

                var res = await client.GetStringAsync(new Uri(uri));
                if (res != null)
                {
                    output = JsonConvert.DeserializeObject<ServerOutputItem>(res.ToString());

                    if (output.Result == MainClass.eServerResult.OK)
                    {
                        //this.Item = JsonConvert.DeserializeObject<AppShopItem>(output.ServerData);
						RaiseRatingSuccessful(output);
                    }
                    else
                    {
                        RaiseDataErrorOccurred(output);
                    }
                }
                else
                {
                    output.Result = MainClass.eServerResult.NetWorkError;
                    output.ResultString = "Cuould not reach server";
                    RaiseDataErrorOccurred(output);
                }
            }
            catch (Exception e)
            {
                output.Result = MainClass.eServerResult.NetWorkError;
                output.ResultString = "Offline";
                RaiseDataErrorOccurred(output);
            }
        }

		public async Task<byte[]> LoadImage (string imageUrl)
		{
            try
            { 
			    return await client.GetByteArrayAsync (imageUrl);
            }
            catch
            {
                return null;
            }
		}
    }
}
