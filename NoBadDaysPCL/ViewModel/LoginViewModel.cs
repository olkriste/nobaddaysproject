﻿using Newtonsoft.Json;
using NoBadDaysPCL.Items;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Json;
using NoBadDaysPCL.SQLite;
using SQLite.Net.Interop;
using NoBadDaysPCL.Parameters;

namespace NoBadDaysPCL.ViewModel
{
    public class LoginViewModel
    {
        public ObservableCollection<SupportedPhoneDataItem> Items { get; private set; }
        public UserItem Item { get; private set; }

        NBDDatabase dataBase;

        public event EventHandler<string> PhoneDataErrorOccurred;
        public event EventHandler RequestSuccessful;
		public event EventHandler<ServerOutputItem> RequestNotSuccessful;

        public event EventHandler ConfirmSuccessful;
        public event EventHandler<ServerOutputItem> ConfirmNotSuccessful;

        public event EventHandler<int> PhoneDataSuccessfulWithIndex;
        public event EventHandler PhoneDataSuccessful;

        ServerInputItem input = new ServerInputItem();
        ServerOutputItem output = new ServerOutputItem();
        HttpClient client = new HttpClient();

        public LoginViewModel(ISQLitePlatform platform, string path)
        {
            this.Items = new ObservableCollection<SupportedPhoneDataItem>();
            this.Item = new UserItem();

            if (!string.IsNullOrEmpty(path))
            {
                if (platform != null)
                {
                    dataBase = new NBDDatabase(platform, path);
                }
            }
        }

        void RaiseCountryIndex(int index)
        {
            if (PhoneDataSuccessfulWithIndex != null)
            {
                PhoneDataSuccessfulWithIndex(this, index);
            }
        }

        void RaiseDataErrorOccurred(string message)
        {
            if (PhoneDataErrorOccurred != null)
            {
                PhoneDataErrorOccurred(this, message);
            }
        }

        void RaiseDataSuccessful()
        {
            if (PhoneDataSuccessful != null)
            {
                PhoneDataSuccessful(this, null);
            }
        }

        void RaiseConfirmSuccessful()
        {
            if (ConfirmSuccessful != null)
            {
                ConfirmSuccessful(this, null);
            }
        }

        void RaiseConfirmNotSuccessful(ServerOutputItem result)
        {
            if (ConfirmNotSuccessful != null)
            {
                ConfirmNotSuccessful(this, result);
            }
        }

        void RaiseRequestSuccessful()
        {
            if (RequestSuccessful != null)
            {
                RequestSuccessful(this, null);
            }
        }

		void RaiseRequestNotSuccessful(ServerOutputItem message)
        {
            if (RequestNotSuccessful != null)
            {
                RequestNotSuccessful(this, message);
            }
        }

        public async Task GetSupportedPhoneData()
        {
            try
            {
                input.Function = MainClass.eServerFunction.GetSupportedPhoneDate;
                //input.Token = "DROPSTER";

                string inputString = JsonConvert.SerializeObject(input);
                string uri = MainClass.ServerUri + inputString;

                var res = await client.GetStringAsync(new Uri(uri));

                if (res != null)
                {
                    output = JsonConvert.DeserializeObject<ServerOutputItem>(res.ToString());

                    HashSet<SupportedPhoneDataItem> items = JsonConvert.DeserializeObject<HashSet<SupportedPhoneDataItem>>(output.ServerData);

                    foreach (SupportedPhoneDataItem item in items)
                    {
                        this.Items.Add(new SupportedPhoneDataItem()
                        {
                            PhonePrefix = item.PhonePrefix,
                            Region = item.Region,
                            Base64Image = item.Base64Image,
                            ImageHeight = item.ImageHeight,
                            ImageWidth = item.ImageWidth,
                            MaxLength = item.MaxLength,
                            MinLength = item.MinLength,
                            Danish = item.Danish,
                            English = item.English,
                            Native = item.Native
                        });
                    }

                    RaiseDataSuccessful();
                }
                else
                {
                    RaiseDataErrorOccurred("Network error");
                }
            }
            catch (Exception ex)
            {
                RaiseDataErrorOccurred("Offline");
            }
        }

        public async Task GetSupportedPhoneData(string region)
        {
            try
            {
                if (!string.IsNullOrEmpty(region))
                {
                    if (region.Length == 5)
                    {
                        region = region.Substring(3, 2).ToLower();
                    }

                    input.Function = MainClass.eServerFunction.GetSupportedPhoneDate;
                    input.AdminToken = "";

                    string inputString = JsonConvert.SerializeObject(input);
                    string uri = MainClass.ServerUri + inputString;

                    var res = await client.GetStringAsync(new Uri(uri));

                    if (res != null)
                    {

                        output = JsonConvert.DeserializeObject<ServerOutputItem>(res.ToString());

                        HashSet<SupportedPhoneDataItem> items = JsonConvert.DeserializeObject<HashSet<SupportedPhoneDataItem>>(output.ServerData);

                        int index = 0;
                        int count = 0;
                        foreach (SupportedPhoneDataItem item in items)
                        {
                            this.Items.Add(new SupportedPhoneDataItem()
                            {
                                PhonePrefix = item.PhonePrefix,
                                Region = item.Region,
                                Base64Image = item.Base64Image,
                                ImageHeight = item.ImageHeight,
                                ImageWidth = item.ImageWidth,
                                MaxLength = item.MaxLength,
                                MinLength = item.MinLength,
                                Danish = item.Danish,
                                English = item.English,
                                Native = item.Native
                            });

                            if (item.Region == region)
                            {
                                index = count;
                            }

                            count++;
                        }

                        RaiseCountryIndex(index);
                    }
                    else
                    {
                        RaiseDataErrorOccurred("Network error");
                    }
                }
                else
                {
                    RaiseDataErrorOccurred("Region cannot be null or empty");
                }
            }
            catch (Exception ex)
            {
				RaiseDataErrorOccurred("Offline");
            }
        }

        public async Task Request(UserItem user)
        {
            try
            {
                if (user != null)
                {
                    if (!string.IsNullOrEmpty(user.Phone))
                    {
                        if (!string.IsNullOrEmpty(user.Prefix))
                        {
                            if (!string.IsNullOrEmpty(user.Region))
                            {
                                input.Function = MainClass.eServerFunction.AppRequestPhoneVerification;
                                input.AdminToken = "";
                                input.Parameters = JsonConvert.SerializeObject(user);

                                string inputString = JsonConvert.SerializeObject(input);
                                string uri = MainClass.ServerUri + inputString;

                                var res = await client.GetStringAsync(new Uri(uri));
                                if (res != null)
                                {
                                    output = JsonConvert.DeserializeObject<ServerOutputItem>(res.ToString());

                                    if (output.Result == MainClass.eServerResult.OK)
                                    {
                                        this.Item = JsonConvert.DeserializeObject<UserItem>(output.ServerData);
                                        RaiseRequestSuccessful();
                                    }
                                    else
                                    {
                                        RaiseRequestNotSuccessful(output);
                                    }
                                }
                                else
                                {
                                    RaiseDataErrorOccurred("Could not reach server");
                                }
                            }
                            else
                            {
                                RaiseDataErrorOccurred("Region cannot be null or empty");
                            }
                        }
                        else
                        {
                            RaiseDataErrorOccurred("PhonePrefix cannot be null or empty");
                        }
                    }
                    else
                    {
                        RaiseDataErrorOccurred("Phone cannot be null or empty");
                    }
                }
                else
                {
                    RaiseDataErrorOccurred("Input cannot be null");
                }
            }
            catch (Exception ex)
            {
                RaiseDataErrorOccurred("Could not reach server");
            }
        }

        public async Task Confirm(UserItem user, string code)
        {
            try
            {
                if (!string.IsNullOrEmpty(code))
                {
                    if (user != null)
                    {
                        if (!string.IsNullOrEmpty(user.Phone))
                        {
                            input.Function = MainClass.eServerFunction.AppConfirmPhoneVerification;
                            input.AdminToken = "";

                            ConfirmUserParameter parameter = new ConfirmUserParameter();
                            parameter.Code = code;
                            parameter.User = user;
                            input.Parameters = JsonConvert.SerializeObject(parameter);

                            string inputString = JsonConvert.SerializeObject(input);
                            string uri = MainClass.ServerUri + inputString;

                            var res = await client.GetStringAsync(new Uri(uri));
                            if (res != null)
                            {
                                output = JsonConvert.DeserializeObject<ServerOutputItem>(res.ToString());

                                if (output.Result == MainClass.eServerResult.OK)
                                {
                                    this.Item = JsonConvert.DeserializeObject<UserItem>(output.ServerData);
                                    if (dataBase != null)
                                    {
                                        UserItem userTmp = dataBase.GetUser();
                                        if (userTmp != null && userTmp.UserId > 0)
                                        {
                                            if (userTmp.UserId == this.Item.UserId)
                                            {
                                                // Update
                                                dataBase.UpdateUser(this.Item);
                                            }
                                            else
                                            {
                                                // Delete old user and insert new user
                                                dataBase.DeleteUser();
                                                dataBase.InsertUser(this.Item);
                                            }
                                        }
                                        else
                                        {
                                            // Insert new user
                                            dataBase.InsertUser(this.Item);
                                        }
                                    }
                                    RaiseConfirmSuccessful();
                                }
                                else
                                {
                                    RaiseConfirmNotSuccessful(output);
                                }
                            }
                            else
                            {
                                RaiseDataErrorOccurred("Cuould not reach server");
                            }
                        }
                        else
                        {
                            RaiseDataErrorOccurred("PhoneNumber cannot be null or empty");
                        }
                    }
                    else
                    {
                        RaiseDataErrorOccurred("UserItem is null");
                    }
                }
                else
                {
                    RaiseDataErrorOccurred("Code cannot be null or empty");
                }
            }
            catch
            {
                RaiseDataErrorOccurred("Exception occured");
            }
        }
    }
}
