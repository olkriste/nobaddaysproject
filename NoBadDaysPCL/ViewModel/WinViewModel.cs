﻿using Newtonsoft.Json;
using NoBadDaysPCL.Items;
using NoBadDaysPCL.SQLite;
using SQLite.Net.Interop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace NoBadDaysPCL.ViewModel
{
    public class WinViewModel
    {
        public WinItem Item { get; private set; }

        NBDDatabase dataBase;

        public event EventHandler<ServerOutputItem> PhoneDataErrorOccurred;
        public event EventHandler<ServerOutputItem> PhoneDataSuccessful;

        ServerInputItem input;
        ServerOutputItem output;
        HttpClient client;

        public WinViewModel(ISQLitePlatform platform, string path)
        {
            this.Item = new WinItem();
            this.input = new ServerInputItem();
            this.output = new ServerOutputItem();
            this.client = new HttpClient();

            if (!string.IsNullOrEmpty(path))
            {
                if (platform != null)
                {
                    dataBase = new NBDDatabase(platform, path);
                }
            }
        }

        void RaiseDataErrorOccurred(ServerOutputItem result)
        {
            if (PhoneDataErrorOccurred != null)
            {
                PhoneDataErrorOccurred(this, result);
            }
        }

        void RaiseDataSuccessful(ServerOutputItem result)
        {
            if (PhoneDataSuccessful != null)
            {
                PhoneDataSuccessful(this, result);
            }
        }

        public async Task GetWin()
        {
            try
            {
                input.Function = MainClass.eServerFunction.AppGetWin;

                UserItem item = new UserItem();

                if (dataBase != null)
                {
                    UserItem itemTmp = dataBase.GetUser();
					if (itemTmp != null && itemTmp.UserId > 0)
					{
						item = itemTmp;
					}
                }

                input.Parameters = JsonConvert.SerializeObject(item);

                string inputString = JsonConvert.SerializeObject(input);
                string uri = MainClass.ServerUri + inputString;

                var res = await client.GetStringAsync(new Uri(uri));
                if (res != null)
                {
                    output = JsonConvert.DeserializeObject<ServerOutputItem>(res.ToString());

                    if (output.Result == MainClass.eServerResult.OK)
                    {
                        this.Item = JsonConvert.DeserializeObject<WinItem>(output.ServerData);
                        RaiseDataSuccessful(output);
                    }
                    else
                    {
                        RaiseDataErrorOccurred(output);
                    }
                }
                else
                {
                    output.Result = MainClass.eServerResult.NetWorkError;
                    output.ResultString = "Cuould not reach server";
                    RaiseDataErrorOccurred(output);
                }
            }
            catch (Exception e)
            {
                output.Result = MainClass.eServerResult.NetWorkError;
                output.ResultString = "Offline";
                RaiseDataErrorOccurred(output);
            }
        }

		public async Task<byte[]> LoadImage (string imageUrl)
		{
			try
			{
				return await client.GetByteArrayAsync(imageUrl);
			}
			catch
			{
				return null;
			}
		}
    }
}
