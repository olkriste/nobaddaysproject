﻿using Newtonsoft.Json;
using NoBadDaysPCL.Items;
using NoBadDaysPCL.SQLite;
using SQLite.Net.Interop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace NoBadDaysPCL.ViewModel
{
    public class OfferViewModel
    {
        public HashSet<OfferItem> Items { get; private set; }
        public OfferItem Item { get; private set; }

        NBDDatabase dataBase = null;

        public event EventHandler<ServerOutputItem> PhoneDataErrorOccurred;
        public event EventHandler<ServerOutputItem> PhoneDataSuccessful;

        ServerInputItem input;
        ServerOutputItem output;
        HttpClient client;

        public OfferViewModel(ISQLitePlatform platform, string path)
        {
            this.Items = new HashSet<OfferItem>();
            this.Item = new OfferItem();

            this.input = new ServerInputItem();
            this.output = new ServerOutputItem();
            this.client = new HttpClient();

            if (!string.IsNullOrEmpty(path))
            {
                if (platform != null)
                {
                    dataBase = new NBDDatabase(platform, path);
                }
            }
        }

        void RaiseDataErrorOccurred(ServerOutputItem result)
        {
            if (PhoneDataErrorOccurred != null)
            {
                PhoneDataErrorOccurred(this, result);
            }
        }

        void RaiseDataSuccessful(ServerOutputItem result)
        {
            if (PhoneDataSuccessful != null)
            {
                PhoneDataSuccessful(this, result);
            }
        }

        public async void GetOffers(OfferSearchItem search)
        {
            try
            {
                if (dataBase != null)
                {
                    UserItem user = dataBase.GetUser();
                    if (user != null)
                    { 
                        search.NotificationDistance = user.NotificationDistance;
                        input.AdminToken = user.Token;
                    }
                }

                if (search == null)
                {
                    output.Result = MainClass.eServerResult.IncorrectInputParameter;
                    output.ResultString = "Parameter not valid";
                    RaiseDataErrorOccurred(output);
                    return;
                }

                if (search.Count <= 0)
                {
                    output.Result = MainClass.eServerResult.IncorrectInputParameter;
                    output.ResultString = "Count must be > 0";
                    RaiseDataErrorOccurred(output);
                    return;
                }

                if (search.Index < 0)
                {
                    output.Result = MainClass.eServerResult.IncorrectInputParameter;
                    output.ResultString = "Count must be >= 0";
                    RaiseDataErrorOccurred(output);
                    return;
                }

                if ((search.Latitude == 0 && search.Longitude == 0) ||
                    (search.Latitude < -90 || search.Latitude > 90 || search.Longitude < -180 || search.Longitude > 180))
                {
                    output.Result = MainClass.eServerResult.IncorrectInputParameter;
                    output.ResultString = "Enable location services in settings";
                    RaiseDataErrorOccurred(output);
                    return;
                }

                if (string.IsNullOrEmpty(search.Search))
                {
                    search.Search = "";
                }

                input.Function = MainClass.eServerFunction.AppGetOffers;
                input.Parameters = JsonConvert.SerializeObject(search);

                string inputString = JsonConvert.SerializeObject(input);
                string uri = MainClass.ServerUri + inputString;

                var res = await client.GetStringAsync(new Uri(uri));
                if (res != null)
                {
                    output = JsonConvert.DeserializeObject<ServerOutputItem>(res.ToString());

                    if (output.Result == MainClass.eServerResult.OK)
                    {
                        this.Items = JsonConvert.DeserializeObject<HashSet<OfferItem>>(output.ServerData);
                        RaiseDataSuccessful(output);
                    }
                    else
                    {
                        RaiseDataErrorOccurred(output);
                    }
                }
                else
                {
                    output.Result = MainClass.eServerResult.NetWorkError;
                    output.ResultString = "Cuould not reach server";
                    RaiseDataErrorOccurred(output);
                }
            }
            catch (Exception e)
            {
                output.Result = MainClass.eServerResult.NetWorkError;
                output.ResultString = "Offline";
                RaiseDataErrorOccurred(output);
            }
        }

        public async void GetOffer(int offerId)
        {
            try
            {
                if (dataBase != null)
                {
                    UserItem user = dataBase.GetUser();
                    if (user != null)
                    {
                        input.AdminToken = user.Token;
                    }
                }

                if (offerId <= 0)
                {
                    output.Result = MainClass.eServerResult.IncorrectInputParameter;
                    output.ResultString = "offerid be > 0";
                    RaiseDataErrorOccurred(output);
                    return;
                }

                input.Function = MainClass.eServerFunction.AppGetOffer;
                input.Parameters = JsonConvert.SerializeObject(offerId);

                string inputString = JsonConvert.SerializeObject(input);
                string uri = MainClass.ServerUri + inputString;

                var res = await client.GetStringAsync(new Uri(uri));
                if (res != null)
                {
                    output = JsonConvert.DeserializeObject<ServerOutputItem>(res.ToString());

                    if (output.Result == MainClass.eServerResult.OK)
                    {
                        this.Item = JsonConvert.DeserializeObject<OfferItem>(output.ServerData);
                        RaiseDataSuccessful(output);
                    }
                    else
                    {
                        RaiseDataErrorOccurred(output);
                    }
                }
                else
                {
                    output.Result = MainClass.eServerResult.NetWorkError;
                    output.ResultString = "Cuould not reach server";
                    RaiseDataErrorOccurred(output);
                }
            }
            catch (Exception e)
            {
                output.Result = MainClass.eServerResult.NetWorkError;
                output.ResultString = "Offline";
                RaiseDataErrorOccurred(output);
            }
        }

		public async Task<byte[]> LoadImage (string imageUrl)
		{
			try
			{ 
				return await client.GetByteArrayAsync (imageUrl);
			}
			catch
			{
				return null;
			}
		}
    }
}
