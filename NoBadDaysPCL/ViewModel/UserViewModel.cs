﻿using Newtonsoft.Json;
using NoBadDaysPCL.Items;
using NoBadDaysPCL.SQLite;
using SQLite.Net.Interop;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace NoBadDaysPCL.ViewModel
{
    public class UserViewModel
    {
        public UserItem Item { get; private set; }
		public ObservableCollection<SupportedPhoneDataItem> PhoneData { get; private set; }
		public int Index = 0;

        NBDDatabase dataBase;

        public event EventHandler<ServerOutputItem> PhoneDataErrorOccurred;
        public event EventHandler<ServerOutputItem> PhoneDataSuccessful;
        public event EventHandler<ServerOutputItem> SMSIsSent;
		public event EventHandler<int> SupportedPhoneDataSuccessful;

        ServerInputItem input;
        ServerOutputItem output;
        HttpClient client;

        public UserViewModel(ISQLitePlatform platform, string path)
        {
			this.PhoneData = new ObservableCollection<SupportedPhoneDataItem>();
            this.Item = new UserItem();
            this.input = new ServerInputItem();
            this.output = new ServerOutputItem();
            this.client = new HttpClient();

            if (!string.IsNullOrEmpty(path))
            {
                if (platform != null)
                {
                    dataBase = new NBDDatabase(platform, path);
                }
            }
        }

        void RaiseDataErrorOccurred(ServerOutputItem result)
        {
            if (PhoneDataErrorOccurred != null)
            {
                PhoneDataErrorOccurred(this, result);
            }
        }

        void RaiseSMSSentOccurred(ServerOutputItem result)
        {
            if (SMSIsSent != null)
            {
                SMSIsSent(this, result);
            }
        }

        void RaiseDataSuccessful(ServerOutputItem result)
        {
            if (PhoneDataSuccessful != null)
            {
                PhoneDataSuccessful(this, result);
            }
        }

		void RaiseSupportedPhoneDataSuccessful(int index)
		{
			if (SupportedPhoneDataSuccessful != null)
			{
				SupportedPhoneDataSuccessful(this, index);
			}
		}

        public UserItem GetUser()
        {
            if (dataBase != null)
            {
                Item = dataBase.GetUser();
                if (Item != null && Item.UserId > 0)
                {
                    return Item;
                }
            }
            return null;
        }

		public void LogOut()
		{
			if (dataBase != null)
			{
				dataBase.DeleteTables ();
			}
		}

        public async Task UpdateUser(UserItem user)
        {
            try
            {
                if (user != null)
                {
                    if (dataBase != null)
                    {
                        UserItem userDB = new UserItem();
                        userDB = dataBase.GetUser();

                        if(string.IsNullOrEmpty(user.Phone))
                        {
                            output.Result = MainClass.eServerResult.InvalidPhone;
                            output.ResultString = "Invalid phone number";
                            RaiseDataErrorOccurred(output);
                            return;
                        }

                        if (string.IsNullOrEmpty(user.Prefix))
                        {
                            output.Result = MainClass.eServerResult.InvalidPrefix;
                            output.ResultString = "Invalid phone prefix";
                            RaiseDataErrorOccurred(output);
                            return;
                        }

                        if (string.IsNullOrEmpty(user.Region))
                        {
                            output.Result = MainClass.eServerResult.InvalidRegion;
                            output.ResultString = "Invalid phone region";
                            RaiseDataErrorOccurred(output);
                            return;
                        }

                        if (string.IsNullOrEmpty(user.Email))
                        {
                            output.Result = MainClass.eServerResult.InvalidEmail;
                            output.ResultString = "Invalid email";
                            RaiseDataErrorOccurred(output);
                            return;
                        }

                        if (string.IsNullOrEmpty(user.Name))
                        {
                            output.Result = MainClass.eServerResult.InvalidLoginName;
                            output.ResultString = "Invalid Name";
                            RaiseDataErrorOccurred(output);
                            return;
                        }

                        input.Function = MainClass.eServerFunction.AppUpdateUser;
                        input.Parameters = JsonConvert.SerializeObject(user);

                        string inputString = JsonConvert.SerializeObject(input);

                        string function = "NBDServerStream";
                        HttpContent queryString = new StringContent(inputString); 
                        HttpClient client = new HttpClient();

                        client.BaseAddress = new Uri(MainClass.ServerUri);
                        var res = await client.PostAsync(function, queryString);

                        res.EnsureSuccessStatusCode();
                        string content = await res.Content.ReadAsStringAsync();

                        if (content != null)
                        {
                            output = JsonConvert.DeserializeObject<ServerOutputItem>(content.ToString());

                            if (output.Result == MainClass.eServerResult.OK)
                            {
                                this.Item = JsonConvert.DeserializeObject<UserItem>(output.ServerData);
                                if (userDB != null && userDB.UserId > 0)
                                {
                                    if (userDB.UserId == this.Item.UserId)
                                    {
                                        // Update
                                        dataBase.UpdateUser(this.Item);
                                    }
                                    else
                                    {
                                        // Delete old user and insert new user
                                        dataBase.DeleteUser();
                                        dataBase.InsertUser(this.Item);
                                    }
                                }
                                else
                                {
                                    // Insert new user
                                    dataBase.InsertUser(this.Item);
                                }
                                RaiseDataSuccessful(output);
                            }
                            else if (output.Result == MainClass.eServerResult.SMSIsSent)
                            {
                                this.Item = JsonConvert.DeserializeObject<UserItem>(output.ServerData);
                                RaiseSMSSentOccurred(output);
                            }
                            else
                            {
                                RaiseDataErrorOccurred(output);
                            }
                        }
                        else
                        {
                            output.Result = MainClass.eServerResult.NetWorkError;
                            output.ResultString = "Could not reach server";
                            RaiseDataErrorOccurred(output);
                        }
                    }
                    else
                    {
                        output.Result = MainClass.eServerResult.DatabaseError;
                        output.ResultString = "Database not valid";
                        RaiseDataErrorOccurred(output);
                    }
                }
                else
                {
                    output.Result = MainClass.eServerResult.IncorrectInputParameter;
                    output.ResultString = "Parameter not valid";
                    RaiseDataErrorOccurred(output);
                }
            }
            catch(Exception e)
            {
                output.Result = MainClass.eServerResult.NetWorkError;
                output.ResultString = "Offline";
                RaiseDataErrorOccurred(output);
            }
        }

        public async Task GetUserFromServer(float latitude, float longitude)
        {
            try
            {
                if (dataBase != null)
                {
                    if (Item == null || Item.UserId <= 0)
                    {
                        Item = dataBase.GetUser();
                        if (Item == null)
                        {
							Item = new UserItem();
                        }
						if (Item.UserId <= 0)
						{
							Item.Region = System.Globalization.CultureInfo.CurrentUICulture.Name;
						}
                    }

                    Item.Latitude = latitude;
                    Item.Longitude = longitude;

                    input.Function = MainClass.eServerFunction.AppGetUser;
                    input.Parameters = JsonConvert.SerializeObject(Item);

                    string inputString = JsonConvert.SerializeObject(input);

                    string function = "NBDServerStream";
                    HttpContent queryString = new StringContent(inputString);
                    HttpClient client = new HttpClient();

                    client.BaseAddress = new Uri(MainClass.ServerUri);
                    var res = await client.PostAsync(function, queryString);

                    res.EnsureSuccessStatusCode();
                    string content = await res.Content.ReadAsStringAsync();

                    if (content != null)
                    {
                        output = JsonConvert.DeserializeObject<ServerOutputItem>(content.ToString());

                        if (output.Result == MainClass.eServerResult.OK)
                        {
                            this.Item = JsonConvert.DeserializeObject<UserItem>(output.ServerData);
                            if (Item != null && Item.UserId > 0)
                            {
                                if (Item.UserId == this.Item.UserId)
                                {
                                    // Update
                                    dataBase.UpdateUser(this.Item);
                                    RaiseDataSuccessful(output);
                                }
                            }                             
						} 
						else if (output.Result == MainClass.eServerResult.PhoneData)
						{
							if (Item != null && !string.IsNullOrEmpty(Item.Region))
							{
								this.PhoneData = JsonConvert.DeserializeObject<ObservableCollection<SupportedPhoneDataItem>>(output.ServerData);

								int index = 0;
								int count = 0;
								foreach (SupportedPhoneDataItem item in this.PhoneData)
								{
									if (item.Region == Item.Region)
									{
										index = count;
										break;
									}

									count++;
								}

								this.Index = index;
								RaiseSupportedPhoneDataSuccessful(index);
							}
						}
                    }
                }
            }
            catch (Exception e)
            {
            }
        }

        public async Task<byte[]> LoadImage (string imageUrl)
		{
            try
            {
                return await client.GetByteArrayAsync(imageUrl);
            }
            catch
            {
                return null;
            }
		}
    }
}
