﻿using NoBadDaysPCL.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoBadDaysPCL.Parameters
{
    public class ConfirmUserParameter
    {
        public UserItem User { get; set; }

        public string Code { get; set; }
    }
}
