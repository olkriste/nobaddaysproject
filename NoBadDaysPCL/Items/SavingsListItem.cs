﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoBadDaysPCL.Items
{
    public class SavingsListItem
    {
        public ImageItem Picture { get; set; }

        public string Name { get; set; }

        public string Amount { get; set; }
    }
}
