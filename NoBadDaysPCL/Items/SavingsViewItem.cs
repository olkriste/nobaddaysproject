﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoBadDaysPCL.Items
{
    public class SavingsViewItem
    {
        public SavingsItem ThisMonth { get; set; }

        public SavingsItem PreviousMonth { get; set; }
    }
}
