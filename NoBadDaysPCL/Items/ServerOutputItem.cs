﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoBadDaysPCL.Items
{
    public class ServerOutputItem
    {
        public NoBadDaysPCL.MainClass.eServerResult Result { get; set; }

        public string ResultString { get; set; }

        public NoBadDaysPCL.MainClass.eServerItem ServerItem { get; set; }

        public string ServerData { get; set; }
    }
}
