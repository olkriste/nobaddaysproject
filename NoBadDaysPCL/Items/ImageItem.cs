﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoBadDaysPCL.Items
{
    public class ImageItem
    {
        public int Type { get; set; }

        public int Format { get; set; }

        public string Uri { get; set; }

        public int Height { get; set; }

        public int Width { get; set; }
    }
}
