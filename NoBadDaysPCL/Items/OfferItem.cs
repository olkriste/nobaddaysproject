﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoBadDaysPCL.Items
{
    public class OfferItem
    {
        public int OfferId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public ImageItem ContentPicture { get; set; }

        public ImageItem ListPicture { get; set; }

        public string StartDate { get; set; }

        public string EndDate { get; set; }

        public int Distance { get; set; }
    }
}
