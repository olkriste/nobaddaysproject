﻿using SQLite.Net.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoBadDaysPCL.Items
{
    public class UserItem
    {
        public int UserId { get; set; }

        public string PushChannel { get; set; }

        public string Phone { get; set; }

        public string Prefix { get; set; }

        public string Culture { get; set; }

        public string Region { get; set; }

		public string Bio { get; set; }

        public string Token { get; set; }

        public string Name { get; set; }

        public ImageItem Picture { get; set; }

        public ImageItem BarCode { get; set; }

        public string DeviceId { get; set; }

        public string Email { get; set; }

        public int NotificationDistance { get; set; }

        public string Code { get; set; }

        public string Guid { get; set; }

        public bool IsActive { get; set; }

        public float Latitude { get; set; }

        public float Longitude { get; set; }

        public string SubscriptionLink { get; set; }
    }
}
