﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoBadDaysPCL.Items
{
    public class SavingsItem
    {
        public int Amount { get; set; }

        public int MaxAmount { get; set; }

        public string Unit { get; set; }

        public HashSet<SavingsListItem> Savings { get; set; }
    }
}
