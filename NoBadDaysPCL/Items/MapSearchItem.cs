﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoBadDaysPCL.Items
{
    public class MapSearchItem
    {
        public double Nelat { get; set; }

        public double Nelon { get; set; }

        public double Swlat { get; set; }

        public double Swlon { get; set; }

        public int Zoomlevel { get; set; }

        public int Gridx { get; set; }

        public int Gridy { get; set; }

        public int ZoomlevelClusterStop { get; set; }

        public string Search { get; set; }

        public MainClass.eShopType ShopType { get; set; }
    }
}
