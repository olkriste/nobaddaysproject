﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoBadDaysPCL.Items
{
    public class WinItem
    {
        public PrizeItem Prize { get; set; }

        public PrizeItem NextPrize { get; set; }

        public PrizeItem PreviousPrize { get; set; }

        public UserItem PreviousWinner { get; set; }

        public HashSet<UserListItem> Users { get; set; }
    }
}
