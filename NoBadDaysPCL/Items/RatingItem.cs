﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoBadDaysPCL.Items
{
    public class RatingItem
    {
        public int ShopId { get; set; }

        public int UserId { get; set; }

        public int Rating { get; set; }

        public string Text { get; set; }

        public string Name { get; set; }
    }
}
