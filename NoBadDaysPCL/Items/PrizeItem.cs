﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoBadDaysPCL.Items
{
    public class PrizeItem
    {
        public int PrizeId { get; set; }

        public ImageItem Picture { get; set; }

        public int Month { get; set; }

        public int Year { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }
}
}
