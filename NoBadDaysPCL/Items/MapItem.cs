﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoBadDaysPCL.Items
{
    public class MapItem
    {
        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public int ShopId { get; set; }

        public string ListPicture { get; set; }

        public string Name { get; set; }

        public HashSet<int> ShopIds { get; set; }

        public MainClass.eShopType ShopType { get; set; }

        public int ClusterCount { get; set; }
    }
}
