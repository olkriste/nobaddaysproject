﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoBadDaysPCL.Items
{
    public class ServerInputItem
    {
        public string AdminToken { get; set; }

        public NoBadDaysPCL.MainClass.eServerFunction Function { get; set; }

        public string Parameters { get; set; }
    }
}
