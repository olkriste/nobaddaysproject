﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoBadDaysPCL.Items
{
    public class AppShopItem
    {
        public int ShopId { get; set; }

        public bool IsOpen { get; set; }

        public MainClass.eShopType ShopType { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string City { get; set; }

        public string Street { get; set; }

        public string PostalCode { get; set; }

        public string Country { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public ImageItem ListPicture { get; set; }

        public ImageItem ContentPicture { get; set; }

        public string Link { get; set; }

        public string MonFrom { get; set; }

        public string MonTo { get; set; }

        public string TueFrom { get; set; }

        public string TueTo { get; set; }

        public string WedFrom { get; set; }

        public string WedTo { get; set; }

        public string ThuFrom { get; set; }

        public string ThuTo { get; set; }

        public string FriFrom { get; set; }

        public string FriTo { get; set; }

        public string SatFrom { get; set; }

        public string SatTo { get; set; }

        public string SunFrom { get; set; }

        public string SunTo { get; set; }

        public float Rating { get; set; }

        public int Ratings { get; set; }

		public float Latitude { get; set; }

		public float Longitude { get; set; }

        public string TextLeft { get; set; }

        public string TextRigth { get; set; }
    }
}
