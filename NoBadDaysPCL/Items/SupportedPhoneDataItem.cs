﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoBadDaysPCL.Items
{
    public class SupportedPhoneDataItem
    {
        public string Base64Image { get; set; }

        public string PhonePrefix { get; set; }

        public string Region { get; set; }

        public int MaxLength { get; set; }

        public int MinLength { get; set; }

        public int ImageHeight { get; set; }

        public int ImageWidth { get; set; }

        public string Danish { get; set; }

        public string English { get; set; }

        public string Native { get; set; }
    }
}
