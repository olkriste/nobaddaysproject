﻿using NoBadDaysPCL.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoBadDaysPCL.SQLite
{
    public interface IDatabase
    {
        Task<int> InsertOrUpdate<TData>(TData[] informations);
        int InsertOrUpdate<TData>(TData data);

        int InsertUser(UserItem user);
        void DeleteUser();
        int UpdateUser(UserItem user);
        UserItem GetUser();

        void DeleteTables();
    }
}
