﻿using NoBadDaysPCL.Items;
using SQLite.Net;
using SQLite.Net.Attributes;
using SQLite.Net.Interop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoBadDaysPCL.SQLite
{
    public class NBDDatabase : IDatabase
    {
        readonly SQLiteConnection _conn;

        public NBDDatabase(ISQLitePlatform platform, string path)
        {
            _conn = new SQLiteConnection (platform, path);
            CreateTables();
        }

        public NBDDatabase(ISQLitePlatform platform, string path, SQLiteOpenFlags flags)
        {
            _conn = new SQLiteConnection(platform, path, flags);
            CreateTables();
        }

        void CreateTables()
        {
            _conn.CreateTable<DBUserItem>();
        }

        public void DeleteTables()
        {
            _conn.DropTable<DBUserItem>();
        }

        public Task<int> InsertOrUpdate<TData>(TData[] informations)
        {
            return Task.Run<int>(() => {
                int count = 0;
                foreach(TData i in informations) {
                    count += _conn.InsertOrReplace(i);
                }
                return count;
            });
        }

        public int InsertOrUpdate<TData>(TData data)
        {
            return _conn.InsertOrReplace(data);
        }

        #region User
        public int InsertUser(UserItem user)
        {
            DBUserItem item = ParseUserItem(user);
            return _conn.Insert(item);
        }

        public void DeleteUser()
        {
            _conn.DropTable<DBUserItem>();
            _conn.CreateTable<DBUserItem>();
        }

        public int UpdateUser(UserItem user)
        {
            DBUserItem item = ParseUserItem(user);
            return _conn.Update(item);
        }

        public UserItem GetUser()
        {
            UserItem item = ParseUserItem(_conn.Table<DBUserItem>().FirstOrDefault());
            return item; 
        }
        #endregion

        #region parsers
        private DBUserItem ParseUserItem(UserItem user)
        {
            if (user.BarCode == null)
            {
                user.BarCode = new ImageItem();
            }
            if (user.Picture == null)
            {
                user.Picture = new ImageItem();
            }

            DBUserItem DBIten = new DBUserItem();
            DBIten.BarCode = user.BarCode.Uri;
            DBIten.BarcodeHeight = user.BarCode.Height;
            DBIten.BarcodeWidth = user.BarCode.Width;
            DBIten.Code = user.Code;
            DBIten.Culture = user.Culture;
            DBIten.DeviceId = user.DeviceId;
            DBIten.Email = user.Email;
            DBIten.Guid = user.Guid;
            DBIten.Name = user.Name;
            DBIten.NotificationDistance = user.NotificationDistance;
            DBIten.Phone = user.Phone;
            DBIten.Picture = user.Picture.Uri;
            DBIten.PictureHeight = user.Picture.Height;
            DBIten.PictureWidth = user.Picture.Width;
            DBIten.Prefix = user.Prefix;
            DBIten.PushChannel = user.PushChannel;
			DBIten.Region = user.Region;
            DBIten.Token = user.Token;
            DBIten.UserId = user.UserId;
            DBIten.Bio = user.Bio;
            DBIten.IsActive = user.IsActive;
			DBIten.SubscriptionLink = user.SubscriptionLink;

            return DBIten;
        }

        private UserItem ParseUserItem(DBUserItem DBIten)
        {
            if (DBIten == null)
            {
                return null;
            }

            UserItem user = new UserItem();
            if (user.BarCode == null)
            {
                user.BarCode = new ImageItem();
            }
            if (user.Picture == null)
            {
                user.Picture = new ImageItem();
            }

            user.BarCode.Uri = DBIten.BarCode;
            user.BarCode.Height = DBIten.BarcodeHeight;
            user.BarCode.Width = DBIten.BarcodeWidth;
            user.Code = DBIten.Code;
            user.Culture = DBIten.Culture;
            user.DeviceId = DBIten.DeviceId;
            user.Email = DBIten.Email;
            user.Guid = DBIten.Guid;
            user.Name = DBIten.Name;
            user.NotificationDistance = DBIten.NotificationDistance;
            user.Phone = DBIten.Phone;
            user.Picture.Uri = DBIten.Picture;
            user.Picture.Height = DBIten.PictureHeight;
            user.Picture.Width = DBIten.PictureWidth;
            user.Prefix = DBIten.Prefix;
            user.PushChannel = DBIten.PushChannel;
			user.Region = DBIten.Region;
            user.Token = DBIten.Token;
            user.UserId = DBIten.UserId;
            user.Bio = DBIten.Bio;
            user.IsActive = DBIten.IsActive;
			user.SubscriptionLink = DBIten.SubscriptionLink;

            return user;
        }
        #endregion

        #region private classes
        private class DBUserItem
        {
            [PrimaryKey]
            public int UserId { get; set; }

            public string PushChannel { get; set; }

            public string Phone { get; set; }

            public string Prefix { get; set; }

            public string Culture { get; set; }

            public string Region { get; set; }

            public string Token { get; set; }

            public string Name { get; set; }

            public string Picture { get; set; }

            public int PictureHeight { get; set; }

            public int PictureWidth { get; set; }

            public string BarCode { get; set; }

            public int BarcodeHeight { get; set; }

            public int BarcodeWidth { get; set; }

            public string DeviceId { get; set; }

            public string Email { get; set; }

            public int NotificationDistance { get; set; }

            public string Code { get; set; }

            public string Guid { get; set; }

            public string Bio { get; set; }

            public bool IsActive { get; set; }

			public string SubscriptionLink { get; set; }
        }
        #endregion
    }
}
