﻿using System;

namespace NoBadDaysPCL
{
    public class MainClass
    {
        private static string _serverUri = "http://nobaddaystest.cloudapp.net/NoBadDaysService.svc/NBDServer?Json=";
        //private static string _serverUri = "http://localhost:56476/NoBadDaysService.svc/NBDServer?Json=";

        public static string ServerUri
        {
            get { return _serverUri; }
            set { _serverUri = value; }
        }

        public enum eServerFunction
        {
            None = 0,
            WebAdminLogin,
            GetSupportedPhoneDate,
            AppRequestPhoneVerification,
            AppConfirmPhoneVerification,
            AppUpdateUser,
            WebAdminUpdate,
            WebUpdateUser,
            WebUpdateShop,
            WebDeleteUser,
            WebDeleteShop,
            WebUpdateOffer,
            WebDeleteOffer,
            WebGetShop,
            WebSearchAdmin,
            WebIsLoginAvailable,
            WebGetCountryList,
            WebGetCityList,
            WebGetAreaList,
            WebUploadImage,
            WebGetOffer,
            AppGetShop,
            AppRateShop,
            WebGetAreaDump,
            AppGetMapShops,
            WebSearchShops,
            AppGetOffers,
            WebGetOffers,
            AppGetOffer,
            AppGetWin,
            AppGetSavings,
            WebGetAdmin,
            WebUpdatePrize,
            WebDeletePrize,
            WebGetPrizes,
            WebUpdateAreaDump,
            WebMakeTransaction,
            WebSearchUsers,
            WebSetWinner,
            WebGetWinnerList,
            AppGetShopRating,
            WebGetStatistics,
            AppGetUser,
            WebUpdateSubscription,
            WebSendMail
        }

        public enum eServerItem
        {
            None = 0,
            SupportedPhoneDataItem,
            UserVerificationCode,
            UserItem,
            AdminItem,
            ShopItem,
            AreaItem,
            ImageItem,
            OfferItem,
            PrizeItem
        }

        public enum eServerResult
        {
            OK,
            Fail,
            IncorrectInputParameter,
            VerifyExceeded,
            PhoneNumberDoesNotMatchRegion,
            ConfirmCodeNotCorrect,
            UserNotFound,
            IncorrectToken,
            InvalidLoginName,
            InvalidPasswordFormat,
            IncorrectAdminToken,
            Unautherized,
            UnautherizedToChangeRole,
            InvalidPrefix,
            InvalidPhone,
            InvalidRegion,
            PhoneNumberRegisteredOnOtherUser,
            ShopNotFound,
            InvalidEmail,
            SMSIsSent,
            InvalidCode,
            NetWorkError,
            DatabaseError,
            IncorrectDateTimeFormat,
            OfferNotFound,
            UnautherizedToCreateSuperUser,
            LoginNameIsInUse,
            PositionInvalid,
            IncorrectAdmin,
            PrizeNotFound,
            RepeatedTransaction,
            CouldNotFindGeoCoordinates,
            AdminIdDontMatchRole,
            StatusCannotBeNone,
            ProfileIdCannotBeEmpty,
            SubscriptionTypeCannotBeNone,
			PhoneData,
            ProfileNotActive
        }

        public enum eShopType
        {
            None = 0,
            Food,
            EatAndDrink,
            HotelsAndTravel,
            TicketsAndexperiences,
            SportFitnessAndOutdoor,
            Services,
            HomeAndGarden,
            ClothingAndbeauty,
            KidsAndPlay,
            Electronics
        }
    }
}

