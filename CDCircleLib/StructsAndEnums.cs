﻿using System;

namespace CDCircleLib
{
	public enum CDCircleThumbsSeparator : uint {
		CDCircleThumbsSeparatorNone,
		CDCircleThumbsSeparatorBasic
	}

	public enum CGGradientPosition : uint {
		CGGradientPositionVertical = 1,
		CGGradientPositionHorizontal
	}
}

