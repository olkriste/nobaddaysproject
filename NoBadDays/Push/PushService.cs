using System.Text;
using Android.App;
using Android.Content;
using Android.Util;
using Gcm.Client;
using Android.Support.V4.App;
//using DropsterAndroid.SplashHandling;

//VERY VERY VERY IMPORTANT NOTE!!!!
// Your package name MUST NOT start with an uppercase letter.
// Android does not allow permissions to start with an upper case letter
// If it does you will get a very cryptic error in logcat and it will not be obvious why you are crying!
// So please, for the love of all that is kind on this earth, use a LOWERCASE first letter in your Package Name!!!!

using System;
using Android.OS;
using Android.Media;
using Android.Graphics;
using System.Threading;
using NoBadDays.Splash;
using WindowsAzure.Messaging;
using System.Collections.Generic;
using Java.Net;
using NoBadDaysPCL.ViewModel;
using SQLite.Net.Platform.XamarinAndroid;

[assembly: Permission(Name = "com.ondodots.nobaddays.permission.C2D_MESSAGE")]
[assembly: UsesPermission(Name = "com.ondodots.nobaddays.permission.C2D_MESSAGE")]
[assembly: UsesPermission(Name = "com.google.android.c2dm.permission.RECEIVE")]


namespace NoBadDays.Push
{
    //You must subclass this!
	[BroadcastReceiver( Permission=Gcm.Client.Constants.PERMISSION_GCM_INTENTS )]
	[IntentFilter(new string[] { Gcm.Client.Constants.INTENT_FROM_GCM_MESSAGE }, Categories = new string[] { "com.ondodots.nobaddays" })]
	[IntentFilter(new string[] { Gcm.Client.Constants.INTENT_FROM_GCM_REGISTRATION_CALLBACK }, Categories = new string[] { "com.ondodots.nobaddays" })]
	[IntentFilter(new string[] { Gcm.Client.Constants.INTENT_FROM_GCM_LIBRARY_RETRY }, Categories = new string[] { "com.ondodots.nobaddays" })]
	[IntentFilter(new[] { Android.Content.Intent.ActionBootCompleted } )]
    public class GcmBroadcastReceiver : GcmBroadcastReceiverBase<PushHandlerService>
    {
        //IMPORTANT: Change this to your own Sender ID!
        //The SENDER_ID is your Google API Console App Project ID.
        //  Be sure to get the right Project ID from your Google APIs Console.  It's not the named project ID that appears in the Overview,
        //  but instead the numeric project id in the url: eg: https://code.google.com/apis/console/?pli=1#project:785671162406:overview
        //  where 785671162406 is the project id, which is the SENDER_ID to use!
	//	public static string[] SENDER_IDS = new string[] {Constants.SenderID};
		public static string[] SENDER_IDS = new string[] {"1042473096934"};

		                                                                                              
                                                           
        public const string TAG = "PushService-GCM";
    }

    [Service] //Must use the service tag
	public class PushHandlerService :GcmServiceBase
    {
		public static string RegistrationID { get; private set; }
		private NotificationHub Hub { get; set; }



		public PushHandlerService() : base(GcmBroadcastReceiver.SENDER_IDS) { }

        private static int NOTIFICATION_ID = 1;

        const string TAG = "GCM-NOBADDAYS";

        protected override void OnRegistered (Context context, string registrationId)
        {
		
			string libraryPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
			var path = System.IO.Path.Combine(libraryPath, Constants.SQLitePath);

			UserViewModel userViewModel = new UserViewModel (new SQLitePlatformAndroid(), path);

			if (string.IsNullOrEmpty(registrationId))
			{
				Container.registered = false;
				return;
			}
			else
			{
				Container.registered = true;
			}


			RegistrationID = registrationId;

	
			Hub = new NotificationHub(Constants.NotificationHubName, Constants.ListenConnectionString/*cs*/, context);

			try
			{
				Hub.UnregisterAll(registrationId);
			}
			catch (Exception ex)
			{
				Log.Error(PushHandlerService.TAG, ex.Message);
			}

			if (userViewModel.GetUser() == null) return;


			//var tags = new List<string>() { "falcons" }; // create tags if you want
			var tags = new List<string>() {};

			tags.Add (Container.userItem.UserId.ToString ());


			try
			{
				var hubRegistration = Hub.Register(registrationId, tags.ToArray());

			}
			catch (Exception ex)
			{
				Log.Error(PushHandlerService.TAG, ex.Message);
			}



        }

        protected override void OnUnRegistered (Context context, string registrationId)
        {
			string t = "";
        }

        protected override bool OnRecoverableError (Context context, string errorId)
        {
           
            return base.OnRecoverableError (context, errorId);
        }

        protected override void OnError (Context context, string errorId)
        {
			string t = "";
        }


        protected override void OnMessage (Context context, Intent intent)
        {
            Log.Info(TAG, "GCM Message Received!");

	//		intent.Extras	{Bundle[{selection=3, from=1042473096934, type=2, badge=5, 
	//			message={"title":"NoBadDays","body":"Ny notification"},   //26

			string notification = "";

			string msg = "";

			int selection = 0;
			int badge = 0;
			int type = 0;

			try
			{

			   msg = URLDecoder.Decode(intent.GetStringExtra("message"), "UTF-8");
			  
			   var tmp = "\"body\":";
			   var index = msg.IndexOf (tmp)+tmp.Length;

			   notification = msg.Substring (index).Replace ("\"", "").Replace ("}", "");

			   selection =  int.Parse(URLDecoder.Decode(intent.GetStringExtra("selection"), "UTF-8"));
			   type =   int.Parse(URLDecoder.Decode(intent.GetStringExtra("type"), "UTF-8"));
			   badge  =  int.Parse(URLDecoder.Decode(intent.GetStringExtra("badge"), "UTF-8"));

			}
			catch {}

	
            //Store the message
            var prefs = GetSharedPreferences(context.PackageName, FileCreationMode.Private);
            var edit = prefs.Edit();
            edit.PutString("last_msg", msg.ToString());
            edit.Commit();

			createNotification("NoBadDays", notification, badge, selection, type);
        }

     
        void createNotification(string title, string text, int badge, int selection, int type)
        {
            text = text.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
              Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122");


            Bundle valuesForActivity = new Bundle();

            valuesForActivity.PutInt("badge", badge);    
            valuesForActivity.PutInt("selection", selection);  
			valuesForActivity.PutInt("type", type);  

            Intent intent = new Intent(this, typeof(SplashActivity));
            intent.PutExtras(valuesForActivity);


            Android.App.TaskStackBuilder stackBuilder = Android.App.TaskStackBuilder.Create(this);
			stackBuilder.AddParentStack(Java.Lang.Class.FromType(typeof(SplashActivity)));
            stackBuilder.AddNextIntent(intent);

            PendingIntent resultPendingIntent = stackBuilder.GetPendingIntent(0, PendingIntentFlags.UpdateCurrent);

            int version = Int32.Parse(Android.OS.Build.VERSION.Sdk);

			Bitmap bm = BitmapFactory.DecodeResource (this.Resources, Resource.Drawable.NoBadDaysLogoSmall);

            int resource;

            if (version < 22) {
           
				resource = Resource.Drawable.NoBadDaysLogoSmall;
            } else
				resource = Resource.Drawable.NoBadDaysPush;

            NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .SetAutoCancel(true) 
                .SetContentIntent(resultPendingIntent) 
                .SetContentTitle(title)
                .SetContentText(text)
               .SetLargeIcon(bm)
                //          .SetSubText(GetString(Resource.String.NotificationTotalText) + " " + badge.ToString())
         
                .SetSmallIcon(resource); 


            builder.SetSound(RingtoneManager.GetDefaultUri(Android.Media.RingtoneType.Notification));


            NotificationManager notificationManager = (NotificationManager)GetSystemService(NotificationService);
            notificationManager.Notify(NOTIFICATION_ID, builder.Build());
        }
       
     
    }

  
}

