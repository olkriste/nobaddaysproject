﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;

using CustomTextView = NoBadDays.helpers.CustomTextView;

namespace NoBadDays
{
    [Activity (Label = "", Theme = "@style/NoBadDaysTheme")]            
    public class Info : Activity
    {
       
        private float _density;
        int _screenHeight;
        int _screenWidth;

        ImageView _menuButton;

        WheelMenu _wheelMenu;


        protected override void OnCreate (Bundle bundle)
        {
            base.OnCreate (bundle);

            // Create your application here

		
            SetContentView (Resource.Layout.Info);

        }

		protected override void OnStart()
		{

			base.OnStart();

		
		}
       
        protected override void OnResume()
        {
            base.OnResume ();

			int version = Int32.Parse(Android.OS.Build.VERSION.Sdk);

			if (version != 19)   //KitKat 4.4.2
			{
				View decorView = Window.DecorView;
				// Hide the status bar.
				int uiOptions = (int)View.SystemUiFlagFullscreen;
				decorView.SystemUiVisibility = (StatusBarVisibility)uiOptions;
			}

			ActionBar actionBar = ActionBar;
			actionBar.SetDisplayShowHomeEnabled(false);
			//displaying custom ActionBar
			View actionBarView = LayoutInflater.Inflate(Resource.Layout.actionbarLayout, null);
			actionBar.SetCustomView(actionBarView, new Android.App.ActionBar.LayoutParams(ActionBar.LayoutParams.WrapContent, ActionBar.LayoutParams.WrapContent,
				GravityFlags.Center|GravityFlags.Left));
			actionBar.SetDisplayOptions(ActionBarDisplayOptions.ShowCustom, ActionBarDisplayOptions.ShowCustom);

			_menuButton = actionBarView.FindViewById<ImageView>(Resource.Id.wheelBut);

			actionBarView.FindViewById<CustomTextView> (Resource.Id.actionbarTitle).Text = GetString (Resource.String.infoText);

			RequestedOrientation = Android.Content.PM.ScreenOrientation.Portrait;

			Display d = WindowManager.DefaultDisplay;

			Android.Util.DisplayMetrics m = new Android.Util.DisplayMetrics();
			d.GetMetrics(m);

			_density = m.Density;

			Point size = new Point();
			d.GetSize(size);
			_screenWidth = size.X;
			_screenHeight = size.Y;



            var _skinOverlay = FindViewById<View>(Resource.Id.skinOverlay);

            _wheelMenu = new WheelMenu(this, 0, _density, _menuButton, false, _skinOverlay);

			var plus1 = FindViewById<CustomTextView> (Resource.Id.plus1);
			plus1.Click+= (object sender, EventArgs e) => 
			{
				try
				{
					var uri = Android.Net.Uri.Parse(GetString(Resource.String.OurStoryLink));
					var intent = new Intent(Intent.ActionView, uri);
					StartActivity(intent);
				}
				catch 
				{

				}
			};
               
			var plus2 = FindViewById<CustomTextView> (Resource.Id.plus2);
			plus2.Click+= (object sender, EventArgs e) => 
			{
				try
				{
					var uri = Android.Net.Uri.Parse(GetString(Resource.String.ContactInfoLink));
					var intent = new Intent(Intent.ActionView, uri);
					StartActivity(intent);
				}
				catch 
				{

				}
			};

			var plus3 = FindViewById<CustomTextView> (Resource.Id.plus3);
			plus3.Click+= (object sender, EventArgs e) => 
			{
				try
				{
					var uri = Android.Net.Uri.Parse(GetString(Resource.String.PrivacyLink));
					var intent = new Intent(Intent.ActionView, uri);
					StartActivity(intent);
				}
				catch 
				{

				}
			};

			var plus4 = FindViewById<CustomTextView> (Resource.Id.plus4);
			plus4.Click+= (object sender, EventArgs e) => 
			{
				try
				{
					var uri = Android.Net.Uri.Parse(GetString(Resource.String.TermsLink));
					var intent = new Intent(Intent.ActionView, uri);
					StartActivity(intent);
				}
				catch 
				{

				}
			};
        }


		protected override void OnPause()
		{
			base.OnPause();

		

			GC.Collect (0);

		}


        public override bool OnKeyDown(Keycode keyCode, KeyEvent e)
        {

            if (keyCode == Keycode.Back)
            {
                return _wheelMenu.CloseActivity(this);
            }

            return base.OnKeyDown(keyCode, e);
        }

        public override void OnLowMemory()
        {
            Console.WriteLine("Info:OnLowMemory");
            GC.Collect();
        }

		protected override void OnDestroy()
		{
			base.OnDestroy();

			if (_wheelMenu != null)_wheelMenu.Dispose ();

		}
    }
}

