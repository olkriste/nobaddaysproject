using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using CustomTextView = NoBadDays.helpers.CustomTextView; 

namespace NoBadDays
{
    class OffersViewHolder : Java.Lang.Object
    {
        public ImageView offerimage { get; set; }
		public CustomTextView title { get; set; }
		public CustomTextView description { get; set; }
		public CustomTextView seeadd { get; set; }
        public int position { get; set; }
    }
}