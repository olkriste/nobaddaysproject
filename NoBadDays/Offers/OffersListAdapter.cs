﻿using System;
using Android.Widget;
using Android.Views;
using System.Collections.Generic;

using CustomTextView = NoBadDays.helpers.CustomTextView;
using NoBadDays.helpers;
using Android.Graphics;
using NoBadDaysPCL.Items;
using System.Linq;
using Koush; 

namespace NoBadDays
{
	public class OffersListAdapter: BaseAdapter, IListAdapter, Koush.IUrlImageViewCallback 
	{

		List<OfferItemTest> list = new List<OfferItemTest>();

		public static ListView BackGroundListView = null;

		Offers _activity;


		LayoutInflater _inflater;
			
		int count = 30;

		Bitmap tmpBitmap;

		public OffersListAdapter (Offers activity)
		{

			_inflater = LayoutInflater.From(activity);


			_activity = activity;

			var amount = 10;

			#if TEST

		
			for (int i = 0; i < count; i++) {
				list.Add (new OfferItemTest ("Store"+i.ToString(), (amount+i).ToString()+_activity.GetString(Resource.String.OffText)));
			}

			tmpBitmap = BitmapFactory.DecodeResource (_activity.Resources, Resource.Drawable.sparegrissquareSmall);

			#endif


		}

		public override long GetItemId(int position)
		{

			return 0;
		}

		public override Java.Lang.Object GetItem(int position)
		{
			return null; // could wrap a Contact in a Java.Lang.Object to return it here if needed
		}

		#if TEST
		public OfferItemTest this[int position]
		#else
		public OfferItem this[int position]
		#endif
		{
			get
			{
				#if TEST
				return list [position];
				#else
				return _activity.offerViewModel.Items.ElementAt(position);
				#endif

			}  
		}


		public override int Count
		{
			get 
			{
				#if TEST
				return list.Count;
				#else
				return _activity.offerViewModel.Items.Count;
				#endif

			}
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			OffersViewHolder holder;

			if (convertView == null)
			{
				
				convertView = _inflater.Inflate(Resource.Layout.OfferItem, parent, false);
			
				holder = new OffersViewHolder();
				holder.offerimage = convertView.FindViewById<ImageView>(Resource.Id.offerImage);
				holder.title = convertView.FindViewById<CustomTextView>(Resource.Id.title);
				holder.description = convertView.FindViewById<CustomTextView>(Resource.Id.description);
				holder.seeadd = convertView.FindViewById<CustomTextView>(Resource.Id.SeeAd);


				convertView.SetTag(Resource.Layout.OfferItem, holder);
			}
			else
			{
				holder = (OffersViewHolder)convertView.GetTag(Resource.Layout.OfferItem);
			}

			holder.position = position;


			_activity.RunOnUiThread(() =>
			{
				if (holder.offerimage != null)
				{
					try
					{

						#if TEST
						holder.offerimage.SetImageDrawable(new CircleDrawable(tmpBitmap));
						#else

			

						UrlImageViewHelper.SetUrlDrawable(holder.offerimage,  _activity.offerViewModel.Items.ElementAt(position).ListPicture.Uri,  Resource.Drawable.GhostCircle, 60000, this);
	                   #endif
					}
					catch { }
				}

					#if TEST
					holder.location.Text = list[position].location;
					holder.amount.Text =  list[position].amount;
					#else



			     	holder.title.Text = _activity.offerViewModel.Items.ElementAt(position).Title;

					var desc = _activity.offerViewModel.Items.ElementAt(position).Description;

					if ((desc != null)&&(desc != ""))
					{
					    holder.description.Text = desc;
				    }

			        #endif


			});


			return convertView;
		}



		public void OnLoaded(ImageView imageView, Bitmap loadedBitmap, string url, bool loadedFromCache)
		{
			_activity.RunOnUiThread(() =>
			{
				if (loadedBitmap != null)
				{
			        imageView.SetImageDrawable(new CircleDrawable(loadedBitmap));
				}
									
			    this.NotifyDataSetChanged();

			});

		}

		private OffersViewHolder getViewHolder(View v)
		{
			if (v.GetTag(Resource.Layout.OfferItem) == null)
			{
				return getViewHolder((View)v.Parent);
			}
			return (OffersViewHolder)v.GetTag(Resource.Layout.OfferItem);
		}
	}



	public class OfferItemTest
	{
		public string location;
		public string amount;

		public OfferItemTest(string location, string amount)
		{
			this.location = location;
			this.amount = amount;
		}
	}


}

