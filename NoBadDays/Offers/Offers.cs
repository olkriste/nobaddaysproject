﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using NoBadDays.helpers;
using NoBadDaysPCL.ViewModel;
using SQLite.Net.Platform.XamarinAndroid;
using NoBadDaysPCL.Items;

using CustomTextView = NoBadDays.helpers.CustomTextView; 

using PullToRefresharp;
using PullToRefresharp.Android.Views;

namespace NoBadDays
{
    [Activity (Label = "", Theme = "@style/NoBadDaysTheme")]            
	public class Offers : Activity
    {

		public OfferViewModel offerViewModel {get; set;}
		public UserViewModel userViewModel { get; set; }
		OfferSearchItem _offerSearchItem;

		private float _density;
        int _screenHeight;
        int _screenWidth;


        ImageView _menuButton;

        WheelMenu _wheelMenu;

		OffersListAdapter _adapter = null;
		public ListView _list = null;

		DialogHelper diag = new DialogHelper ();

		GeoHelper _geo = null;

		UserItem _user = new UserItem ();


		ProgressBar _progressSpinner;
		CustomTextView _emptyInfo;


		int _visiblePos = 0;

		private IPullToRefresharpView ptr_view;

		bool _refreshing = false;


		ListHelper _listHelper = new ListHelper ();
		AlertDialog _dlgAlert;
		ListView _popUpList;
		AlertDialog.Builder _builder = null;

	



        protected override void OnCreate (Bundle bundle)
        {
            base.OnCreate (bundle);

            // Create your application here
			       

            SetContentView (Resource.Layout.Offers);


			int version = Int32.Parse(Android.OS.Build.VERSION.Sdk);

			if (version != 19)   //KitKat 4.4.2
			{
				View decorView = Window.DecorView;
				// Hide the status bar.
				int uiOptions = (int)View.SystemUiFlagFullscreen;
				decorView.SystemUiVisibility = (StatusBarVisibility)uiOptions;
			}

			ActionBar actionBar = ActionBar;
			actionBar.SetDisplayShowHomeEnabled(false);
			//displaying custom ActionBar
			View actionBarView = LayoutInflater.Inflate(Resource.Layout.actionbarMapLayout, null);
			actionBar.SetCustomView(actionBarView, new Android.App.ActionBar.LayoutParams(ActionBar.LayoutParams.WrapContent, ActionBar.LayoutParams.WrapContent,
				GravityFlags.Center|GravityFlags.Left));
			actionBar.SetDisplayOptions(ActionBarDisplayOptions.ShowCustom, ActionBarDisplayOptions.ShowCustom);

			_menuButton = actionBarView.FindViewById<ImageView>(Resource.Id.wheelBut);

			actionBarView.FindViewById<CustomTextView> (Resource.Id.actionbarTitle).Text = GetString (Resource.String.offersText);

			var searchBtn = actionBarView.FindViewById<ImageView> (Resource.Id.search);
			searchBtn.Click+= SearchBtn_Click;

					
			RequestedOrientation = Android.Content.PM.ScreenOrientation.Portrait;

			Display d = WindowManager.DefaultDisplay;

			Android.Util.DisplayMetrics m = new Android.Util.DisplayMetrics();
			d.GetMetrics(m);

			_density = m.Density;

			Point size = new Point();
			d.GetSize(size);
			_screenWidth = size.X;
			_screenHeight = size.Y;

			var _skinOverlay = FindViewById<View>(Resource.Id.skinOverlay);

			_wheelMenu = new WheelMenu(this, 0, _density, _menuButton, false, _skinOverlay);

			string libraryPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
			var path = System.IO.Path.Combine(libraryPath, Constants.SQLitePath);

			userViewModel = new UserViewModel (new SQLitePlatformAndroid(), path);
			offerViewModel = new OfferViewModel (new SQLitePlatformAndroid(), path);

			offerViewModel.PhoneDataErrorOccurred += OfferViewModel_PhoneDataErrorOccurred;
			offerViewModel.PhoneDataSuccessful+= OfferViewModel_PhoneDataSuccessful;

			_geo = new GeoHelper(this);


	    	_list = FindViewById<ListView>(Resource.Id.Offerslist);

			_list.ItemClick += (object sender, AdapterView.ItemClickEventArgs e) =>
			{

				Bundle dataBundle = new Bundle();
				dataBundle.PutInt("OfferId", offerViewModel.Items.ElementAt(e.Position).OfferId);
				dataBundle.PutInt("Index", e.Position);
				var page = new Intent(this, typeof(Offer));
				page.PutExtras(dataBundle);
				StartActivity(page); 
			};



			if (ptr_view == null && _list is IPullToRefresharpView) {
				ptr_view = (IPullToRefresharpView)_list;
				// LOOK HERE!
				// Hookup a handler to the RefreshActivated event
				ptr_view.RefreshActivated += ptr_view_RefreshActivated;
			}

		


			_adapter = new OffersListAdapter(this);
		
			_progressSpinner = FindViewById<ProgressBar>(Resource.Id.ProgressSpinner);
			_emptyInfo = FindViewById<CustomTextView>(Resource.Id.emptyInfo);

			_progressSpinner.Visibility = ViewStates.Visible;
			_list.Visibility = ViewStates.Gone;
				

		}


		void SearchBtn_Click (object sender, EventArgs e)
		{


			_listHelper.add_categories(this);

			_popUpList = new ListView(this);

			_popUpList.Adapter = new AlertListViewAdapter(this, Container.listDataItem);
			_popUpList.ItemClick += _popUpList_ItemClick;



			_builder = new AlertDialog.Builder(new ContextThemeWrapper(this, Resource.Style.CustomDialogTheme));


			_dlgAlert = _builder.Create();
			_dlgAlert.SetTitle(GetString(Resource.String.SelectCategoryText));
			_dlgAlert.SetView(_popUpList);


			_dlgAlert.Show();
		}

		void _popUpList_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
		{
			_popUpList.ItemClick -= _popUpList_ItemClick;

			if (_dlgAlert != null) _dlgAlert.Dismiss();

			/*

			Container.latitudeStore = _mapPosition.Latitude.ToString();
			Container.longitudeStore = _mapPosition.Longitude.ToString();

			_mapSearchItem.Nelat = _map.Projection.VisibleRegion.FarRight.Latitude;
			_mapSearchItem.Nelon = _map.Projection.VisibleRegion.FarRight.Longitude;
			_mapSearchItem.Swlat = _map.Projection.VisibleRegion.NearLeft.Latitude;
			_mapSearchItem.Swlon = _map.Projection.VisibleRegion.NearLeft.Longitude;
			_mapSearchItem.Gridx = _gridx;
			_mapSearchItem.Gridy = _gridy;
			_mapSearchItem.ZoomlevelClusterStop = _zoomlevelClusterStop;
			_mapSearchItem.Zoomlevel = (int)_zoomLevel;
			_mapSearchItem.Search = "";

			Container.shoptypeStore = _mapSearchItem.ShopType = (eShopType)e.Position;

			mapViewModel.GetShops (_mapSearchItem);
*/
		}


		private void ptr_view_RefreshActivated(object sender, EventArgs args)
		{
			// LOOK HERE!
			// Refresh your content when PullToRefresharp informs you that a refresh is needed

			_refreshing = true;	
			offerViewModel.GetOffers (_offerSearchItem);
	

		
		}


		protected override void OnStart()
		{
			base.OnStart();

	       _offerSearchItem = new OfferSearchItem ();
					
			_offerSearchItem.Latitude = (float)Container.Latitude;
			_offerSearchItem.Longitude = (float)Container.Longitude;
			_offerSearchItem.Count = 10;
			_offerSearchItem.Index = 0;
			_offerSearchItem.NotificationDistance = Container.userItem.NotificationDistance;
			_offerSearchItem.Search = "";

			offerViewModel.GetOffers (_offerSearchItem);
		}

		void OfferViewModel_PhoneDataSuccessful (object sender, NoBadDaysPCL.Items.ServerOutputItem e)
		{

			_progressSpinner.Visibility = ViewStates.Gone;


			if (offerViewModel.Items.Count == 0) {
				_emptyInfo.Visibility = ViewStates.Visible;
				_list.Visibility = ViewStates.Gone;
			} else {
				_emptyInfo.Visibility = ViewStates.Gone;
				_list.Visibility = ViewStates.Visible;
			}

			if ((_refreshing)&& (ptr_view != null)) {
				// When you are done refreshing your content, let PullToRefresharp know you're done.
				ptr_view.OnRefreshCompleted();
				_refreshing = false;
			}


			#if !TEST
			_list.Invalidate ();
			_adapter.NotifyDataSetChanged ();    
			#endif

		}

		void OfferViewModel_PhoneDataErrorOccurred (object sender, NoBadDaysPCL.Items.ServerOutputItem e)
		{
			_progressSpinner.Visibility = ViewStates.Gone;


			if ((_refreshing)&& (ptr_view != null)) {
				// When you are done refreshing your content, let PullToRefresharp know you're done.
				ptr_view.OnRefreshCompleted();
				_refreshing = false;
			}

		
			Android.Widget.Toast.MakeText(this, e.ResultString, ToastLength.Long).Show();
		}
	

        protected override void OnResume()
        {
            base.OnResume ();


			_list.Adapter = _adapter;

		
			#if TEST
			_list.Invalidate ();
			_adapter.NotifyDataSetChanged (); 
			#endif

			if (!_geo.checkLocationSettings()) {

		
				ShowDialog (Constants.DIALOG_NO_GPS_MESSAGE);
			}
				
        }

		#region dialog

		protected override void OnPrepareDialog(int id, Dialog dialog)
		{
			diag.SetDialogButtons(dialog);
		}

		protected override Dialog OnCreateDialog(int id)
		{
			AlertDialog.Builder builder;
			View customView;

			diag.SetDialog(out customView, out builder, this);

			switch (id)
			{
			case Constants.DIALOG_NO_GPS_MESSAGE:
				{
					customView.FindViewById<TextView>(Resource.Id.dialogText).Text = GetString(Resource.String.ActivateGPSText);

					builder.SetPositiveButton(Resource.String.OkText, OkClicked);
					builder.SetNegativeButton(Resource.String.CancelText, CancelClicked);

					return builder.Create();
				}

			}
			return null;
		}

		private void OkClicked(object sender, DialogClickEventArgs e)
		{
			Intent intent = new Intent(Android.Provider.Settings.ActionLocationSourceSettings);
			StartActivity(intent);

		}

		private void CancelClicked(object sender, DialogClickEventArgs e)
		{

		}

		#endregion

	
		protected override void OnPause()
		{
			base.OnPause();

			GC.Collect (0);

		}

		protected override void OnDestroy()
		{
			base.OnDestroy();

			if (_wheelMenu != null)_wheelMenu.Dispose ();

			if (_geo != null)
			{
				_geo.SuspendLocationUpdates();
			}
		}


        public override bool OnKeyDown(Keycode keyCode, KeyEvent e)
        {
            if (keyCode == Keycode.Back)
            {
                return _wheelMenu.CloseActivity(this);
            }

            return base.OnKeyDown(keyCode, e);
        }

        public override void OnLowMemory()
        {
            Console.WriteLine("Offers:OnLowMemory");
            GC.Collect();
        }
    }
}

