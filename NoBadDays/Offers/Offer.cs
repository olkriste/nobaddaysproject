﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using NoBadDays.helpers;

using CustomTextView = NoBadDays.helpers.CustomTextView;
using NoBadDaysPCL.ViewModel;
using SQLite.Net.Platform.XamarinAndroid;
using NoBadDaysPCL.Items;
using Koush;

namespace NoBadDays
{
	[Activity (Label = "",Theme = "@android:style/Theme.Holo")]			
	public class Offer : Activity
	{
	
		OfferViewModel _offerViewModel {get; set;}


		private float _density;
		int _screenHeight;
		int _screenWidth;

	//	ImageView _menuButton;

		ImageView _backKey;

//		WheelMenu _wheelMenu;

		CustomTextView _offerText;

		ImageView _image;

		int _offerId;
		int _index;

		View _actionBarView;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Create your application here


			SetContentView (Resource.Layout.Offer);

			Bundle extras = Intent.Extras;

			try
			{
				_offerId = extras.GetInt("OfferId");
				_index = extras.GetInt("Index");
			}
			catch (Exception exc)
			{

			}


			int version = Int32.Parse(Android.OS.Build.VERSION.Sdk);

			if (version != 19)   //KitKat 4.4.2
			{
				View decorView = Window.DecorView;
				// Hide the status bar.
				int uiOptions = (int)View.SystemUiFlagFullscreen;
				decorView.SystemUiVisibility = (StatusBarVisibility)uiOptions;
			}

			ActionBar actionBar = ActionBar;
			actionBar.SetDisplayShowHomeEnabled(false);
			//displaying custom ActionBar
			_actionBarView = LayoutInflater.Inflate(Resource.Layout.actionbarOfferLayout, null);
			actionBar.SetCustomView(_actionBarView, new Android.App.ActionBar.LayoutParams(ActionBar.LayoutParams.WrapContent, ActionBar.LayoutParams.WrapContent,
				GravityFlags.Center|GravityFlags.Left));
			actionBar.SetDisplayOptions(ActionBarDisplayOptions.ShowCustom, ActionBarDisplayOptions.ShowCustom);

		//	_menuButton = actionBarView.FindViewById<ImageView>(Resource.Id.wheelBut);
			_backKey = _actionBarView.FindViewById<ImageView>(Resource.Id.backkey);
			_backKey.Click+= (object sender, EventArgs e) => 
			{
				this.Finish();
			};

		
			RequestedOrientation = Android.Content.PM.ScreenOrientation.Portrait;

			Display d = WindowManager.DefaultDisplay;

			Android.Util.DisplayMetrics m = new Android.Util.DisplayMetrics();
			d.GetMetrics(m);

			_density = m.Density;

			Point size = new Point();
			d.GetSize(size);
			_screenWidth = size.X;
			_screenHeight = size.Y;

			string libraryPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
			var path = System.IO.Path.Combine(libraryPath, Constants.SQLitePath);

			_offerViewModel = new OfferViewModel (new SQLitePlatformAndroid(), path);

			_offerViewModel.PhoneDataErrorOccurred += OfferViewModel_PhoneDataErrorOccurred;
			_offerViewModel.PhoneDataSuccessful += OfferViewModel_PhoneDataSuccessful;

			_offerViewModel.GetOffer (_offerId);


		}

		void OfferViewModel_PhoneDataSuccessful (object sender, NoBadDaysPCL.Items.ServerOutputItem e)
		{

//			_progressSpinner.Visibility = ViewStates.Gone;


			_offerText = FindViewById<CustomTextView>(Resource.Id.offerText);
			_image  = FindViewById<ImageView>(Resource.Id.OfferImage);
/*"Spis alle de burgers du kan for 10.000 $ !";*/

			_actionBarView.FindViewById<CustomTextView> (Resource.Id.actionbarTitle).Text = _offerViewModel.Item.Title;

			if ((_offerViewModel.Item.Description != null)&&(_offerViewModel.Item.Description != ""))
			{
				_offerText.Text = _offerViewModel.Item.Description;
			}

		
			UrlImageViewHelper.SetUrlDrawable(_image,  _offerViewModel.Item.ContentPicture.Uri,  Resource.Drawable.GhostSquarePictureBig, 60000);  
				
		

		}

		void OfferViewModel_PhoneDataErrorOccurred (object sender, NoBadDaysPCL.Items.ServerOutputItem e)
		{
	//		_progressSpinner.Visibility = ViewStates.Gone;


			Android.Widget.Toast.MakeText(this, e.ResultString, ToastLength.Long).Show();
		}


		protected override void OnStart()
		{
			base.OnStart ();

		
		}




		protected override void OnResume()
		{
			base.OnResume ();


		}

		public override bool OnKeyDown(Keycode keyCode, KeyEvent e)
		{
			if (keyCode == Keycode.Back)
			{
				Finish ();

			}

			return base.OnKeyDown(keyCode, e);
		}

		protected override void OnDestroy()
		{
			base.OnDestroy();

		}


		public override void OnLowMemory()
		{
			Console.WriteLine("Offers:OnLowMemory");
			GC.Collect();
		}
	}
}

