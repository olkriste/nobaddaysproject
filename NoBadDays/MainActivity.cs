﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

using Com.Szugyi.Circlemenu;
using Android.Views.Animations;
using Com.Szugyi.Circlemenu.View;
using Android.Graphics;
using System.Timers;

namespace NoBadDays
{
    [Activity (Label = "", Theme = "@style/NoBadDaysTheme")]
    public class MainActivity : Activity
    {
       
       
        Timer _fadeTimer = new Timer(5);

     
        private float _density;
        int _screenHeight;
        int _screenWidth;

     

        TextView _text;

        int _currentMargin;

        FrameLayout.LayoutParams _param;
   

        RelativeLayout _rel;

        int _marginMin;
        int _marginMax;
        int _diameter;
     
        bool _visible;

        ImageView _menuButton;

        protected override void OnCreate (Bundle bundle)
        {
            base.OnCreate (bundle);

            // Set our view from the "main" layout resource
   
            SetContentView (Resource.Layout.Main);
       
            ActionBar actionBar = ActionBar;
            actionBar.SetDisplayShowHomeEnabled(false);
           //displaying custom ActionBar
            View actionBarView = LayoutInflater.Inflate(Resource.Layout.actionbarLayout, null);
            actionBar.SetCustomView(actionBarView, new Android.App.ActionBar.LayoutParams(ActionBar.LayoutParams.WrapContent, ActionBar.LayoutParams.WrapContent,
                GravityFlags.Center|GravityFlags.Left));
            actionBar.SetDisplayOptions(ActionBarDisplayOptions.ShowCustom, ActionBarDisplayOptions.ShowCustom);

            _menuButton = actionBarView.FindViewById<ImageView>(Resource.Id.wheelBut);
            _menuButton.Visibility = ViewStates.Gone;

            RequestedOrientation = Android.Content.PM.ScreenOrientation.Portrait;

            Display d = WindowManager.DefaultDisplay;

            Android.Util.DisplayMetrics m = new Android.Util.DisplayMetrics();
            d.GetMetrics(m);

            _density = m.Density;

            Point size = new Point();
            d.GetSize(size);
            _screenWidth = size.X;
            _screenHeight = size.Y;

         
   
        }


        protected override void OnResume()
        {
            base.OnResume();
       
           
            WheelMenu wheel = new WheelMenu (this, 0, _density, _menuButton, true, null);
                

          

        }

        public override void OnLowMemory()
        {
            Console.WriteLine("Main:OnLowMemory");
            GC.Collect();
        }


    }
}


