using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Fragment = Android.Support.V4.App.Fragment;

using CustomTextView = NoBadDays.helpers.CustomTextView;
using CustomEditText = NoBadDays.helpers.CustomEditText;


using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using NoBadDaysPCL.Items;
using Koush;
using Android.Graphics;
using Android.Graphics.Drawables;

namespace NoBadDays.Profile
{
       
    
	public class ProfilePageSubPages : Fragment, View.IOnTouchListener, Koush.IUrlImageViewCallback 
    {
        int _pagenumber;
        View _view = null;

        ProfileMainPage _activity;

        NoBadDays.helpers.CustomTextView _radiusValue;

		Button _editaccount1;
	    Button _doneButton;

		//ofk		ScrollView _scrollView;
		CustomTextView _notRegistered;
		CustomTextView _notLoggedIn;
		Button _login;
		Button _logout;
		Button _subscription;

		CustomTextView _phoneEdit;
		CustomTextView _emailEdit;


		ImageView _blyant1;
		ImageView _blyant2;
		ImageView _blyant3;



		bool _editing = false;

	
		double _slidervalue;

		string editstate = "";

		int _lastMotionY;
	
		NoBadDays.Helpers.CustomRelative _relative1;
		NoBadDays.Helpers.CustomRelative _relative2;

		ImageView _barcode;




        public ProfilePageSubPages(int page, ProfileMainPage context)
        {
            _pagenumber = page;
            _activity = context;
			_activity.DataReady+= _activity_DataReady;

        }


        #region - inflate fragments
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
          
            if (_pagenumber == 0)
            {

			
				_view = inflater.Inflate(Resource.Layout.ProfilePageOne, null);
			

				_activity.PopUpDismissed += _activity_PopUpDismissed;
	
				_editaccount1 = _view.FindViewById<Button>(Resource.Id.editaccount1);
				_editaccount1.Click += _editaccount_Click;
		
				_doneButton = _view.FindViewById<Button>(Resource.Id.doneBtn);
				_doneButton.Click += _doneButton_Click; 
				_doneButton.Visibility = ViewStates.Gone;

		//		_activity.userViewModel.PhoneDataSuccessful += _activity_userViewModel_PhoneDataSuccessful;
		//		_activity.userViewModel.PhoneDataErrorOccurred += _activity_userViewModel_PhoneDataErrorOccurred;
				_activity.userViewModel.SMSIsSent += _activity_userViewModel_SMSIsSent;

				_subscription = _view.FindViewById<Button>(Resource.Id.addsubscription);
				_subscription.Click += (object sender, EventArgs e) => 
				{
					if (_activity.userViewModel.GetUser () == null) {
						Android.Widget.Toast.MakeText(_activity, _activity.GetString(Resource.String.LoginRequestText), ToastLength.Long).Show();
					}
					else
					{
					    try
					    {
						     var uri = Android.Net.Uri.Parse(Container.userItem.SubscriptionLink);
						     var intent = new Intent(Intent.ActionView, uri);
						     StartActivity(intent);
     					}
					    catch 
					    {

					    }
					}
				};

				_login = _view.FindViewById<Button>(Resource.Id.login);


				_login.Click+= (object sender, EventArgs e) => 
				{
					if (_activity.userViewModel.GetUser () == null) {

						_activity.Finish();
						_activity.StartActivity (typeof(LoginPhoneNumber));

					}
					else
					{
						var toolbar = _view.FindViewById<LinearLayout>(Resource.Id.toolbar1);
						_login.SetBackgroundColor (Android.Graphics.Color.ParseColor("#404040"));
						toolbar.SetBackgroundColor (Android.Graphics.Color.ParseColor("#404040"));
						_login.Text = _activity.GetString(Resource.String.LoginText);
						_activity.userViewModel.LogOut();
						Container.userItem.Phone = "";
						Container.userItem.Email = "";
						Container.userItem.Bio = "";
						Container.userItem.Name = "";
						Container.userItem.NotificationDistance = 0;
						Container.userItem.Picture.Uri = "";
						Android.Widget.Toast.MakeText(_activity, _activity.GetString(Resource.String.LoggedOutText), ToastLength.Long).Show();
						_activity.Finish();
					}

				};

		

            }
            else if (_pagenumber == 1)
            {

                _view = inflater.Inflate(Resource.Layout.ProfilePageTwo, null);

				_barcode = _view.FindViewById<ImageView>(Resource.Id.BarcodeImg);


           

            }


	        return _view;

        }

        #endregion



        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here

			_editing = false;

	
        }

        public override void OnStart()
        {

            base.OnStart();

			if (_pagenumber == 0) {

			
				//ofk		_scrollView = _view.FindViewById<ScrollView>(Resource.Id.scroll);

				_notRegistered = _view.FindViewById<CustomTextView> (Resource.Id.notregistered);
				_notLoggedIn = _view.FindViewById<CustomTextView> (Resource.Id.notloggedin);


			
				_radiusValue = _view.FindViewById<CustomTextView> (Resource.Id.radiusValue);
			
				_phoneEdit = _view.FindViewById<CustomTextView> (Resource.Id.phoneEdit);
				_emailEdit = _view.FindViewById<CustomTextView> (Resource.Id.emailEdit);

		    	_relative1 = _view.FindViewById<NoBadDays.Helpers.CustomRelative> (Resource.Id.relativeOne);
			    _relative1.SetOnTouchListener (this);


				if (_activity.userViewModel.GetUser () == null) {
			
					if (!Container.editing) {
						_notLoggedIn.Visibility = ViewStates.Visible;
						_relative1.Visibility = ViewStates.Gone;
						_editaccount1.Text = _activity.GetString (Resource.String.CreateAccountText);
					
					} else {
						_notRegistered.Visibility = ViewStates.Gone;
						_notLoggedIn.Visibility = ViewStates.Gone;
					
						_blyant1.Visibility = ViewStates.Visible;
						_blyant2.Visibility = ViewStates.Visible;
						_blyant3.Visibility = ViewStates.Visible;

						var toolbar = _view.FindViewById<LinearLayout> (Resource.Id.toolbar3);
						toolbar.SetBackgroundColor (Android.Graphics.Color.Black);
						_relative1.Visibility = ViewStates.Visible;
						_editaccount1.Text = _activity.GetString (Resource.String.EditAccountText);

						Container.editing = false;
					}

				} else {
		
				
					#if DEBUG
					var toolbar = _view.FindViewById<LinearLayout> (Resource.Id.toolbar1);
					_login.SetBackgroundColor (Android.Graphics.Color.Black);
					toolbar.SetBackgroundColor (Android.Graphics.Color.Black);
					_login.Text = _activity.GetString (Resource.String.LogoutText);

					Drawable img = Context.Resources.GetDrawable( Resource.Drawable.LogOut );
		    		_login.SetCompoundDrawablesWithIntrinsicBounds (null, img, null, null);
					#else
					_login.Visibility = ViewStates.Gone;
					var toolbar = _view.FindViewById<LinearLayout>(Resource.Id.toolbar1);
					toolbar.Visibility = ViewStates.Invisible;
					#endif

					var user = _activity.userViewModel.GetUser ();
					_phoneEdit.Text = Container.userItem.Prefix + Container.userItem.Phone;
					_emailEdit.Text = Container.userItem.Email;
					_radiusValue.Text = ((float)Container.userItem.NotificationDistance).ToString () + " " + GetString(Resource.String.MetersText);;
			
					_slidervalue = (double)Container.userItem.NotificationDistance;



			
					_notRegistered.Visibility = ViewStates.Gone;
					_notLoggedIn.Visibility = ViewStates.Gone;
				}


				editstate = "";

				_blyant1 = _view.FindViewById<ImageView> (Resource.Id.blyant1);
				_blyant1.Click += (sender, e) => {
					_activity.popupRequest (Container.userItem.Phone, -1, false, true, false);
					editstate = "PHONE";
				
				};

				_blyant2 = _view.FindViewById<ImageView> (Resource.Id.blyant2);
				_blyant2.Click += (sender, e) => {
					_activity.popupRequest (Container.userItem.Email, -1, true, false, false);
					editstate = "EMAIL";

				};


				_blyant3 = _view.FindViewById<ImageView> (Resource.Id.blyant3);
				_blyant3.Click += (sender, e) => {
					
					_activity.popupRequest (_activity.GetString (Resource.String.EditRadiusText), _slidervalue, false, false, false);
					editstate = "SLIDER";

				};

			} else {

				_relative2 = _view.FindViewById<NoBadDays.Helpers.CustomRelative> (Resource.Id.relativeTwo);
				_relative2.SetOnTouchListener (this);

			}
       }


		void _activity_DataReady (object sender, bool e)
		{
			if (_pagenumber == 0)
			{
				_phoneEdit.Text = Container.userItem.Prefix + Container.userItem.Phone;
				_emailEdit.Text = Container.userItem.Email;
				_radiusValue.Text = ((float)Container.userItem.NotificationDistance).ToString ()+ " " + GetString(Resource.String.MetersText);;
			_slidervalue = (double)Container.userItem.NotificationDistance;

			}
			else 
			{

				if ((_barcode != null)&&(Container.userItem.BarCode != null)) {
					try {
						UrlImageViewHelper.SetUrlDrawable (_barcode, Container.userItem.BarCode.Uri, null, 30000, null);
					} catch {
					}
				}
			}
		}


		public void OnLoaded(ImageView imageView, Bitmap loadedBitmap, string url, bool loadedFromCache)
		{
			_activity.RunOnUiThread(() =>
			{
				if (loadedBitmap != null)
				{
			    	imageView.SetImageBitmap(loadedBitmap);   
				}
			});

		}
  

        void _activity_SpinnerSelected (object sender, string e)
        {
			_editaccount1.Visibility = ViewStates.Gone;
		
			_doneButton.Visibility = ViewStates.Visible;
        }

        void _activity_PopUpDismissed (object sender, string e)
        {
			if (e != null) 
			{
				_editaccount1.Visibility = ViewStates.Gone;
			
				_doneButton.Visibility = ViewStates.Visible;
				_doneButton.SetBackgroundColor (Android.Graphics.Color.ParseColor("#404040"));
				var toolbar = _view.FindViewById<LinearLayout>(Resource.Id.toolbar3);
				toolbar.SetBackgroundColor (Android.Graphics.Color.ParseColor("#404040"));

				if (editstate == "EMAIL") 
				{

					 _emailEdit.Text = e;


				} else if (editstate == "PHONE") 
				{
					_phoneEdit.Text = Container.userItem.Prefix + e;

				} else if (editstate == "SLIDER") 
				{
					
					try 
					{
						_radiusValue.Text = e + " " + GetString(Resource.String.MetersText);

						_slidervalue = Double.Parse(e);

						Container.userItem.NotificationDistance = Int32.Parse(e);
											
				
					}
					catch{}

				}
				editstate = "";
			} 
			else
			{
				_editaccount1.Visibility = ViewStates.Visible;
	
				_doneButton.Visibility = ViewStates.Gone;
			}
        }

        void _doneButton_Click (object sender, EventArgs e)
        {
			
			Container.userItem.Culture = System.Globalization.CultureInfo.CurrentUICulture.Name;

	//		Container.userItem.Picture.Type = 0;

			_activity.userViewModel.PhoneDataSuccessful += _activity_userViewModel_PhoneDataSuccessful;
			_activity.userViewModel.PhoneDataErrorOccurred += _activity_userViewModel_PhoneDataErrorOccurred;

			_activity.SetProgress (true);
					
			_activity.userViewModel.UpdateUser(Container.userItem);

			Container.editing = false;
	
        }

        void _activity_userViewModel_PhoneDataErrorOccurred (object sender, NoBadDaysPCL.Items.ServerOutputItem e)
        {
			
			_activity.userViewModel.PhoneDataErrorOccurred -= _activity_userViewModel_PhoneDataErrorOccurred;

			_activity.RunOnUiThread(() =>
			{
				_activity.SetProgress (false);
			});
			


			Android.Widget.Toast.MakeText(_activity, e.ResultString, ToastLength.Long).Show();
			_activity.progressSpinner.Visibility = ViewStates.Invisible;
        }

        void _activity_userViewModel_SMSIsSent (object sender, NoBadDaysPCL.Items.ServerOutputItem e)
        {
					
			_activity.RunOnUiThread(() =>
			{
				_activity.SetProgress (false);
			});

			_editaccount1.Visibility = ViewStates.Visible;
		
			_doneButton.Visibility = ViewStates.Gone;

			_activity.progressSpinner.Visibility = ViewStates.Invisible;
	
			Container.userItem.Guid = _activity.userViewModel.Item.Guid;
			Container.userItem.Region = _activity.userViewModel.Item.Region;


			var logincode = new Intent(_activity, typeof(LoginCode));
			logincode.PutExtra("phonenumber", Container.userItem.Phone);
			logincode.PutExtra("context", "UserUpdate");
		
			_activity.StartActivity(logincode);

        }

        void _activity_userViewModel_PhoneDataSuccessful (object sender, NoBadDaysPCL.Items.ServerOutputItem e)
        {
			_activity.userViewModel.PhoneDataSuccessful -= _activity_userViewModel_PhoneDataSuccessful;

			_activity.RunOnUiThread(() =>
			{
				_activity.SetProgress (false);
			});

			_editaccount1.Visibility = ViewStates.Visible;
			_editaccount1.SetBackgroundColor (Android.Graphics.Color.ParseColor("#404040"));
		
			_doneButton.Visibility = ViewStates.Gone;

			_activity.progressSpinner.Visibility = ViewStates.Invisible;
	
			Android.Widget.Toast.MakeText(_activity, GetString(Resource.String.UserDataUpdatedText), ToastLength.Long).Show();
        }

        void _editaccount_Click (object sender, EventArgs e)
        {
			var toolbar = _view.FindViewById<LinearLayout>(Resource.Id.toolbar3);

			if (_editing)
			{

				if (_activity.userViewModel.GetUser () == null) {
		
					_notLoggedIn.Visibility = ViewStates.Visible;
				}

				_blyant1.Visibility = ViewStates.Invisible;
				_blyant2.Visibility = ViewStates.Invisible;
				_blyant3.Visibility = ViewStates.Invisible;
		//		_relative1.Visibility = ViewStates.Gone;
			
				_editing = false;

				_activity.EditProfile (false);

				_editaccount1.SetBackgroundColor (Android.Graphics.Color.ParseColor("#404040"));
				toolbar.SetBackgroundColor (Android.Graphics.Color.ParseColor("#404040"));


			
			}
			else
			{

				//ofk			_scrollView.Visibility = ViewStates.Visible;
				_notRegistered.Visibility = ViewStates.Gone;
				_notLoggedIn.Visibility = ViewStates.Gone;
				_editing = true;

				_blyant1.Visibility = ViewStates.Visible;
				_blyant2.Visibility = ViewStates.Visible;
				_blyant3.Visibility = ViewStates.Visible;

				_editaccount1.SetBackgroundColor (Android.Graphics.Color.Black);
				toolbar.SetBackgroundColor (Android.Graphics.Color.Black);

				_relative1.Visibility = ViewStates.Visible;
			

				_activity.EditProfile (true);

			}






        }


		#region - touch screen events

		public bool OnTouch(View v, MotionEvent e)
		{

		
			var scrollViewMax = _activity.scrollViewMax; 

			var scroll = _activity.scrollView;


			RelativeLayout.LayoutParams parameters;

			float Y = e.GetY();

			if (e.RawY < scroll.Height && e.Action == MotionEventActions.Move)return true;

			switch (e.Action)
			{
			case MotionEventActions.Cancel:
				break;


			case MotionEventActions.Down:

				_lastMotionY = (int)Y;

				break;
			case MotionEventActions.Up:
				
				break;
			case MotionEventActions.Move:

				// Finger move up
				if (Y < _lastMotionY)           
				{

					var newHeigt = scroll.Height;
					var moveMent = (_lastMotionY - Y);
					int reduction = (int)(newHeigt - moveMent);


					if ((reduction > 0) && ((int)newHeigt > 0))
					{

						parameters = new RelativeLayout.LayoutParams (RelativeLayout.LayoutParams.MatchParent, reduction);
						scroll.LayoutParameters = parameters;

						scroll.SmoothScrollTo (0, scroll.Bottom); 
						scroll.FullScroll (FocusSearchDirection.Down);

					}

				}
				// Finger move down
				else if (Y >= _lastMotionY)
				{

					scroll.Visibility = ViewStates.Visible;

					var newHeigt = scroll.Height;
					var moveMent = (Y - _lastMotionY);
					int increase = (int)(newHeigt + moveMent);

					if (increase > scrollViewMax) increase = scrollViewMax;

					if (((increase) > 0) && (newHeigt < scrollViewMax))
					{
						parameters = new RelativeLayout.LayoutParams (RelativeLayout.LayoutParams.MatchParent, increase);
						scroll.LayoutParameters = parameters;
						scroll.SmoothScrollTo (0, scroll.Bottom); 
						scroll.FullScroll (FocusSearchDirection.Down);
					}

				}

				break;
			}

			v.OnTouchEvent (e); 

			return true;

		}
		#endregion
       



    }
}