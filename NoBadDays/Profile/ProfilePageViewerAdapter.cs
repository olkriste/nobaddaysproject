﻿using Android.Content;
using NoBadDays.Profile;
using System;

namespace NoBadDays
{
    public class ProfilePageViewerAdapter : Android.Support.V4.App.FragmentPagerAdapter
    {
        int _count;
        ProfileMainPage _context;

        private readonly string[] Content;


        public ProfilePageViewerAdapter(Android.Support.V4.App.FragmentManager fm, ProfileMainPage context)
            : base(fm)
        {
            _context = context;


            Content = new string[2];

            Content[0] = _context.GetString(Resource.String.AccountText);
            Content[1] = _context.GetString(Resource.String.ScanAndSaveText);

            _count = Content.Length;

            _count = Content.Length; ;

        }

        public override Android.Support.V4.App.Fragment GetItem(int position)
        {
            return new ProfilePageSubPages(position, _context);
        }

        public override int Count
        {
            get
            {
                return _count;
            }
        }

        public override Java.Lang.ICharSequence GetPageTitleFormatted(int p0)
        {
            return new Java.Lang.String(Content[p0 % Content.Length]);
        }



        public void SetCount(int Count)
        {
            if (Count > 0 && Count <= 10)
            {
                _count = Count;
                NotifyDataSetChanged();
            }
        }
    }
}

