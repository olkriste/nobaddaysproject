﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using NoBadDays.helpers;
using Android.Graphics;
using Android.Support.V4.App;
using Android.Support.V4.View;
using DK.Ostebaronen.Droid.ViewPagerIndicator;
using System.Text.RegularExpressions;
using Android.Media;
using NoBadDaysPCL.ViewModel;
using SQLite.Net.Platform.XamarinAndroid;
using NoBadDaysPCL.Items;
using Android.Views.InputMethods;
using System.Timers;
using Koush;
using Android.Views.Animations;


namespace NoBadDays
{
    [Activity (Label = "", Theme = "@style/NoBadDaysTheme")]
	public class ProfileMainPage : FragmentActivity, Koush.IUrlImageViewCallback 
    {

		public event EventHandler<bool> DataReady;

		Timer timer = new Timer();

		public UserViewModel userViewModel { get; set; }
		UserItem user = new UserItem ();

		public LoginViewModel viewLoginModel { get; set; }


		public int index { get; set; }

		static int RESULT_LOAD_IMAGE = 1;

		public event EventHandler<string> PopUpDismissed;
	
		const string MatchEmailPattern = @"^\w[\w.-]*@(\w[\w-]*\.)+[a-z]{2,4}$";
		const string MatchPhonePattern = @"^\+?(\d[\d-. ]+)?(\([\d-. ]+\))?[\d-. ]+\d$";

		ViewPager _pager;
        ProfilePageViewerAdapter _adapter;
        protected IPageIndicator _indicator;

        ImageView _userImage; 
        
        
        private float _density;
        int _screenHeight;
        int _screenWidth;


        ImageView _menuButton;

        WheelMenu _wheelMenu;

		ImageView _blyant1;
		ImageView _blyant2;
//		ImageView _blyant3;




		PopupWindow _popupWindow;

		string _editedValue = "";

		CustomTextView _name;
		CustomTextView _area;

		public ScrollView scrollView  { set; get; }
		public int scrollViewMax { set; get; }

	

		bool _changes = false;

		Spinner _spinner;
		ProgressBar _progressPopup;

		public ProgressBar progressSpinner;

		int _max = -1;
		int _min = -1;

		GeoHelper _geo = null;
			
		public ProfileMainPage()
		{

		}

        protected override void OnCreate (Bundle bundle)
        {
            base.OnCreate (bundle);

            // Create your application here

			RequestWindowFeature(WindowFeatures.IndeterminateProgress);
          
            SetContentView (Resource.Layout.ProfileMainPage);


			int version = Int32.Parse(Android.OS.Build.VERSION.Sdk);

			if (version != 19)   //KitKat 4.4.2
			{
			    View decorView = Window.DecorView;
			// Hide the status bar.
			    int uiOptions = (int)View.SystemUiFlagFullscreen;
			    decorView.SystemUiVisibility = (StatusBarVisibility)uiOptions;
		    }

			ActionBar actionBar = ActionBar;
			actionBar.SetDisplayShowHomeEnabled(false);
			//displaying custom ActionBar
			View actionBarView = LayoutInflater.Inflate(Resource.Layout.actionbarLayout, null);
			actionBar.SetCustomView(actionBarView, new Android.App.ActionBar.LayoutParams(ActionBar.LayoutParams.WrapContent, ActionBar.LayoutParams.WrapContent,
				GravityFlags.Center|GravityFlags.Left));
			actionBar.SetDisplayOptions(ActionBarDisplayOptions.ShowCustom, ActionBarDisplayOptions.ShowCustom);

			actionBarView.FindViewById<CustomTextView> (Resource.Id.actionbarTitle).Text = GetString (Resource.String.mynobaddaysText);

			RequestedOrientation = Android.Content.PM.ScreenOrientation.Portrait;

			Display d = WindowManager.DefaultDisplay;

			Android.Util.DisplayMetrics m = new Android.Util.DisplayMetrics();
			d.GetMetrics(m);

			_density = m.Density;

			Point size = new Point();
			d.GetSize(size);
			_screenWidth = size.X;
			_screenHeight = size.Y;


			_name = FindViewById<CustomTextView>(Resource.Id.name);

			_area = FindViewById<CustomTextView>(Resource.Id.area);

			progressSpinner = FindViewById<ProgressBar>(Resource.Id.ProgressSpinner);

			_geo = new GeoHelper(this);
				
			string libraryPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
			var path = System.IO.Path.Combine(libraryPath, Constants.SQLitePath);

			userViewModel = new UserViewModel (new SQLitePlatformAndroid(), path);

		
			viewLoginModel = new LoginViewModel (new SQLitePlatformAndroid(), path);

			viewLoginModel.PhoneDataErrorOccurred += _viewModel_PhoneDataErrorOccurred;
			viewLoginModel.PhoneDataSuccessfulWithIndex += _viewModel_PhoneDataSuccessfulWithIndex;

			if (userViewModel.GetUser () == null) {

			} else 
			{
				var user = userViewModel.GetUser();
				Container.userItem.BarCode = user.BarCode;



				Container.userItem.Email = user.Email;
				if (( user.Name == "") || ( user.Name == null)) {
					_name.Hint = GetString (Resource.String.HintNameText);
				}
				else _name.Text = Container.userItem.Name = user.Name;

				if ((user.Bio == "") || (user.Bio == null)) {
					_area.Hint = "AREAHINT";//GetString (Resource.String.HintBiographyText);
				} else {
					_area.Text = Container.userItem.Bio = user.Bio; 

				}

				Container.userItem.NotificationDistance = user.NotificationDistance;
				Container.userItem.Phone = user.Phone;
				Container.userItem.Picture = user.Picture;
				Container.userItem.Prefix = user.Prefix;
				Container.userItem.Region = user.Region;
				Container.userItem.UserId = user.UserId;
				Container.userItem.Token = user.Token;

				userViewModel.PhoneDataSuccessful+= UserViewModel_PhoneDataSuccessful;
				userViewModel.PhoneDataErrorOccurred+= UserViewModel_PhoneDataErrorOccurred;

				userViewModel.GetUserFromServer ((float)Container.Latitude, (float)Container.Longitude);


				SetProgress (true);

			}



			_adapter = new ProfilePageViewerAdapter(SupportFragmentManager, this);

			_pager = FindViewById<ViewPager>(Resource.Id.pager);

			_pager.SetPageTransformer(true, new PageTransformer());

			_pager.Adapter = _adapter;


			_indicator = FindViewById<TabPageIndicator>(Resource.Id.indicator);


			#region custom fonts
			Typeface face = Typeface.CreateFromAsset(Assets, "Fonts/Roboto/Roboto-Bold.ttf");
			_indicator.SetViewPager(_pager, face);
			#endregion



			var _skinOverlay = FindViewById<View>(Resource.Id.skinOverlay);

			_menuButton = actionBarView.FindViewById<ImageView>(Resource.Id.wheelBut);

			_wheelMenu = new WheelMenu(this, 0, _density, _menuButton, false, _skinOverlay);


			_userImage = FindViewById<ImageView>(Resource.Id.profileImage);

			_blyant1 = FindViewById<ImageView>(Resource.Id.blyant1);
			_blyant2 = FindViewById<ImageView>(Resource.Id.blyant2);

			scrollView = FindViewById<ScrollView>(Resource.Id.header);
	

        }

        void UserViewModel_PhoneDataErrorOccurred (object sender, ServerOutputItem e)
        {
			Android.Widget.Toast.MakeText(this, GetString(Resource.String.ServerErrorText), ToastLength.Long).Show();

			SetProgress (false);

        }

        void UserViewModel_PhoneDataSuccessful (object sender, ServerOutputItem e)
        {
			SetProgress (false);

			var user = userViewModel.Item;

			if (user == null) {

			} else 
			{

				Container.userItem.BarCode = user.BarCode;

				Container.userItem.Email = user.Email;
				if (( user.Name == "") || ( user.Name == null)) {
					_name.Text = GetString (Resource.String.HintNameText);
				}
				else _name.Text = Container.userItem.Name = user.Name;

				if ((user.Bio == "") || (user.Bio == null)) {
					_area.Text = GetString (Resource.String.AreaTitleText);
				} else {
					 Container.userItem.Bio = user.Bio; 
					_area.Text = GetString (Resource.String.AreaTitleText)+ "" + Container.userItem.Bio;
				}

				Container.userItem.NotificationDistance = user.NotificationDistance;
				Container.userItem.Phone = user.Phone;
				Container.userItem.Picture = user.Picture;
				Container.userItem.Prefix = user.Prefix;
				Container.userItem.Region = user.Region;
				Container.userItem.UserId = user.UserId;
				Container.userItem.Token = user.Token;
				Container.userItem.BarCode = user.BarCode;
				Container.userItem.Bio = user.Bio;
				Container.userItem.SubscriptionLink = user.SubscriptionLink;

				RaiseDataReady();

				if ((Container.userItem.Picture != null) &&  (Container.userItem.Picture.Uri.Contains("http")))
				{
					UrlImageViewHelper.SetUrlDrawable (_userImage, Container.userItem.Picture.Uri, Resource.Drawable.GhostProfile, 60000, this);  
				}

			}

        }

		void RaiseDataReady()
		{
			if (DataReady != null)
			{
				DataReady(this, true);
			}
		}

		public void SetProgress(bool state)
		{
 
		       SetProgressBarIndeterminateVisibility(state);
	
		}


		void _spinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
		{
			index = e.Position;
			_max = viewLoginModel.Items[index].MaxLength;
			_min = viewLoginModel.Items[index].MinLength;

	    	Container.userItem.Prefix = viewLoginModel.Items [index].PhonePrefix;
			Container.userItem.Region = viewLoginModel.Items [index].Region;

	
		}



        protected override void OnResume()
        {
            base.OnResume ();

			scrollView.Measure(RelativeLayout.LayoutParams.WrapContent, RelativeLayout.LayoutParams.WrapContent);

			scrollViewMax = scrollView.MeasuredHeight;


			if (userViewModel.GetUser () == null)
				return;


			if ((Container.userItem.Picture != null) &&  (Container.userItem.Picture.Uri.Contains("http")))
			{
				UrlImageViewHelper.SetUrlDrawable (_userImage, Container.userItem.Picture.Uri, Resource.Drawable.GhostProfile, 60000, this);  
			}


	    }


		public void OnLoaded(ImageView imageView, Bitmap loadedBitmap, string url, bool loadedFromCache)
		{
			RunOnUiThread(() =>
			{
				if (loadedBitmap != null)
				{
				   	imageView.SetImageDrawable(new CircleDrawable(loadedBitmap));
				}
			});

		}

		#region fetch supported phones

		void subscribeData()
		{

			fetchSupportedPhones();

			_progressPopup.Visibility = ViewStates.Visible;

		}


	
		void _viewModel_PhoneDataSuccessfulWithIndex(object sender, int e)
		{
			_spinner.SetSelection(e);

			_spinner.Adapter = new SpinnerAdapter(this, viewLoginModel);

			_progressPopup.Visibility = ViewStates.Invisible;

		}

		void fetchSupportedPhones()
		{
			viewLoginModel.GetSupportedPhoneData(System.Globalization.CultureInfo.CurrentUICulture.Name);
		}



		void _viewModel_PhoneDataErrorOccurred(object sender, string e)
		{
			Android.Widget.Toast.MakeText(this, GetString(Resource.String.ServerErrorText), ToastLength.Long).Show();

			_progressPopup.Visibility = ViewStates.Invisible;


		}

		void _viewModel_PhoneDataSuccessful(object sender, EventArgs e)
		{

			_progressPopup.Visibility = ViewStates.Invisible;


		}

		#endregion


        public override bool OnKeyDown(Keycode keyCode, KeyEvent e)
        {

            if (keyCode == Keycode.Back)
            {
                return _wheelMenu.CloseActivity(this);
            }

            return base.OnKeyDown(keyCode, e);
        }

        public override void OnLowMemory()
        {
            Console.WriteLine("Profile:OnLowMemory");
            GC.Collect();
        }


		protected override void OnPause()
		{
			base.OnPause();

			GC.Collect (0);

		}


		public void EditProfile(bool edit)
		{
		
			if (edit) {
				_blyant1.Visibility = ViewStates.Visible;
				_blyant1.Click+= (sender, e) => 
				{
					Container.editing = true;
					gallery_req ();
				};
			
				_blyant2.Visibility = ViewStates.Visible;
				_blyant2.Click+= (sender, e) => 
				{
					popupRequest (Container.userItem.Name, -1, false, false, true);
				};

	

			} else
			{
				_blyant1.Visibility = ViewStates.Invisible;
				_blyant2.Visibility = ViewStates.Invisible;
				 Container.editing = false;
			}

		}

		public void popupRequest(string text1, double radius, bool email, bool phone, bool nameandarea)
		{
			if (_popupWindow != null) return;


			_editedValue = "";


			ImageView anchor = _userImage as ImageView;

			LayoutInflater layoutInflater = (LayoutInflater)GetSystemService(Context.LayoutInflaterService);
			View popupView = layoutInflater.Inflate(Resource.Layout.PopUpEditFields, null);
			_popupWindow = new PopupWindow(popupView,(int)(300*_density), (int)(400*_density));

			var close = (ImageView)popupView.FindViewById(Resource.Id.closeButton);
			close.Click += (sender, e) => 
			{
				_popupWindow.Dismiss();
				_popupWindow = null;
				RaisePopUpDismissed(null);

			};


			var textbox = (CustomTextView)popupView.FindViewById(Resource.Id.heading);

			var update =  (ImageView)popupView.FindViewById(Resource.Id.updateBtn);
			update.Click+= (sender, e) => 
			{
				Animation rotateAnimation;
				rotateAnimation = AnimationUtils.LoadAnimation (this, Resource.Animation.rotate);

				update.StartAnimation (rotateAnimation);
			};
	
			var editbox1 = (CustomEditText)popupView.FindViewById(Resource.Id.editfield1);
	//		var editbox2 = (CustomEditText)popupView.FindViewById(Resource.Id.editfield2);
	//		editbox2.Visibility = ViewStates.Invisible;
			var area = (CustomTextView)popupView.FindViewById(Resource.Id.area);
			area.Visibility = ViewStates.Gone;
		
			timer.Elapsed+= (sender, e) => 
			{
				RunOnUiThread(() =>
				{
					InputMethodManager imm = (InputMethodManager)GetSystemService(Context.InputMethodService);
					imm.ShowSoftInput(editbox1, ShowFlags.Implicit);
				});

				timer.Enabled = false;
		        timer.Stop();
			};
			timer.Enabled = true;
			timer.Interval = 200;
			timer.Start();


			editbox1.TextChanged += (sender, e) => 
			{
				editbox1.SetTextColor (Android.Graphics.Color.White);
			};


			_spinner = (Spinner)popupView.FindViewById(Resource.Id.countrySelection);
			_spinner.ItemSelected += _spinner_ItemSelected;
			_spinner.Enabled = false;
		
			_progressPopup = (ProgressBar)popupView.FindViewById(Resource.Id.ProgressPopup);



			_spinner.Visibility = ViewStates.Invisible;
			_progressPopup.Visibility = ViewStates.Invisible;


			if (phone) {
				editbox1.SetRawInputType (Android.Text.InputTypes.ClassPhone);
				textbox.Text = GetString (Resource.String.EnterPhoneText);
				if ((text1 == "")||(text1 == null)) {
					editbox1.Hint = GetString (Resource.String.HintPhoneText);
					editbox1.SetHintTextColor (Android.Graphics.Color.White);
				}
				else editbox1.Text = text1;

				_spinner.Visibility = ViewStates.Visible;
				_spinner.Enabled = true;

				subscribeData();

			
			}
			else if (email)
			{
				textbox.Text = GetString (Resource.String.EnterEmailText);
				if ((text1 == "") || (text1 == null)) {
					editbox1.Hint = GetString (Resource.String.HintEmailText);
					editbox1.SetHintTextColor (Android.Graphics.Color.White);
					;
				} else
					editbox1.Text = text1;
			}
			else if (nameandarea)
			{
				textbox.Text = GetString (Resource.String.InsertNameAndAreaText);
			    
				if ((text1 == "") || (text1 == null)) {
					editbox1.Hint = GetString (Resource.String.HintNameText);
					editbox1.SetHintTextColor (Android.Graphics.Color.White);

				} else
					editbox1.Text = text1;

				area.Visibility = ViewStates.Visible;
				if (!string.IsNullOrEmpty (Container.userItem.Bio)) {
					area.Text = Container.userItem.Bio;
				} else
					area.Text = GetString (Resource.String.AreaNotSetText);
				
								
			}
		    else  
			{
				textbox.Text = GetString(Resource.String.EditRadiusText);
			}

			var radiusText = (CustomTextView)popupView.FindViewById (Resource.Id.notset);
			var slider = (SeekBar)popupView.FindViewById (Resource.Id.slider);

			slider.ProgressChanged += (object sender, SeekBar.ProgressChangedEventArgs e) =>
			{
				_changes = true;
				if (e.FromUser)
				{

					if (e.Progress == 0)
					{
						radiusText.Text = Constants.SLIDERMIN.ToString();;
					}
					else 
					{
						 float interval = Constants.SLIDERMAX;

						 double _sliderValue = (float)(interval*e.Progress)/100;

						_sliderValue = (_sliderValue <= Constants.SLIDERMIN) ? Constants.SLIDERMIN : _sliderValue;


						radiusText.Text =_sliderValue.ToString() + " " + GetString(Resource.String.MetersText);
					}
				}
			};




			if (radius == -1) 
			{
				radiusText.Visibility = ViewStates.Gone;
				slider.Visibility = ViewStates.Gone;
				editbox1.Visibility = ViewStates.Visible;
			} 
			else
			{
				if (radius != 0) {

					slider.Progress = (int)(radius*100/Constants.SLIDERMAX);
					radiusText.Text = radius.ToString() + " " + GetString(Resource.String.MetersText);
				}
					
				radiusText.Visibility = ViewStates.Visible;
				slider.Visibility = ViewStates.Visible;
				editbox1.Visibility = ViewStates.Gone;
			}

			var okBtn = (Button)popupView.FindViewById(Resource.Id.okBtn);
			okBtn.Click += (sender, e) => 
			{
				_changes = true;  // ..................more to come
			
				if (email)
				{
					if (IsEmail(editbox1.Text))
					{
						_popupWindow.Dismiss();
						_popupWindow = null;
						Container.userItem.Email = editbox1.Text;
						if (_changes)RaisePopUpDismissed(editbox1.Text);

					}
					else 
					{
						Android.Widget.Toast.MakeText(this, GetString(Resource.String.WrongEmailText), ToastLength.Long).Show();
						editbox1.RequestFocus();
						editbox1.SetTextColor(Android.Graphics.Color.Red);
					}
				}
				else if (phone)
				{
					

					if ((Container.userItem.Prefix =="")||(Container.userItem.Prefix ==""))
					{
						Android.Widget.Toast.MakeText(this, GetString(Resource.String.MissingCountryText), ToastLength.Long).Show();
					}
					else if (IsPhone(editbox1.Text)&&(editbox1.Text.Length <= _max)&&(editbox1.Text.Length >= _min))
					{
						_popupWindow.Dismiss();
						_popupWindow = null;
						Container.userItem.Phone = editbox1.Text;
						if (_changes)RaisePopUpDismissed(editbox1.Text);

					}
					else 
					{
						Android.Widget.Toast.MakeText(this, GetString(Resource.String.WrongPhoneText), ToastLength.Long).Show();
						editbox1.RequestFocus();
						editbox1.SetTextColor(Android.Graphics.Color.Red);
					}

				}
				else if (nameandarea)
				{
					 Container.userItem.Name = _name.Text = editbox1.Text;
			
					_popupWindow.Dismiss();
					 if (_changes)RaisePopUpDismissed("");
					_popupWindow = null;
				
				}
			
				else if (radius != -1)
				{
					_popupWindow.Dismiss();
					_popupWindow = null;
					if (_changes)RaisePopUpDismissed(radiusText.Text.Replace(" " + GetString(Resource.String.MetersText),""));

				}

				_changes = false;

			};
		
			_popupWindow.ShowAtLocation(anchor, GravityFlags.Center, 0, -50);

			_popupWindow.Focusable=true;
			_popupWindow.Update();


		}



		void RaisePopUpDismissed(string changes)
		{
			if (PopUpDismissed != null)
			{
				PopUpDismissed(this, changes);
			}
		}


		bool IsEmail(string email)
		{
			Regex pattern = new Regex(MatchEmailPattern, RegexOptions.IgnoreCase | RegexOptions.ECMAScript);

			if (pattern.IsMatch(email))
			{
				return true;
			}

			return false;
		}

		bool IsPhone(string phone)
		{
			Regex pattern = new Regex(MatchPhonePattern, RegexOptions.IgnoreCase);

			if (pattern.IsMatch(phone))
			{
				return true;
			}

			return false;
		}

		#region gallery

		void gallery_req()
		{
			var imageIntent = new Intent();
			imageIntent.SetType("image/*");
			imageIntent.SetAction(Intent.ActionGetContent);
			StartActivityForResult(Intent.CreateChooser(imageIntent, "Select photo"), RESULT_LOAD_IMAGE);
		}

		protected override void OnActivityResult(int requestCode, Android.App.Result resultCode, Intent data)
		{

			base.OnActivityResult(requestCode, resultCode, data);


			if ((resultCode == Result.Ok) && (requestCode == RESULT_LOAD_IMAGE))
			{
				Android.Net.Uri selectedURI = data.Data;

				setImage(selectedURI);

			}

		}

		private void setImage(Android.Net.Uri img)
		{

			try
			{

				Android.Graphics.Bitmap bitmap = null;

				ImageHelper helper = new ImageHelper();

				try
				{
					bitmap = helper.readBitmap(img, this, _screenWidth/4, _screenHeight/4);
				}
				catch  { }

				int rotate = 0;

				ExifInterface exif = new ExifInterface(img.Path);
				int orientation = exif.GetAttributeInt(ExifInterface.TagOrientation, -1);

				String exifOrientation = exif.GetAttribute(ExifInterface.TagOrientation);


				switch (orientation)
				{
				case 0x00000008: // 270
					rotate = 270;
					break;
				case 0x00000003: // 180
					rotate = 180;
					break;
				case 0x00000006:
					rotate = 90;
					break;
				}

				if (bitmap != null)
				{

					byte[] bytes;

					var  height = (bitmap.Height > bitmap.Width) ? bitmap.Width : bitmap.Height;
					var width = height;

					var scaledBitmap = Bitmap.CreateBitmap(bitmap, 0, 0, width, height);

					bitmap.Dispose();


					if (rotate == 0)
					{

						helper.pictureCompress(scaledBitmap, out bytes);

					}
					else
					{

						// createa matrix for the manipulation
						Matrix matrix = new Matrix();
						// rotate the Bitmap

						matrix.PostRotate(rotate);

						Bitmap rotated = Bitmap.CreateBitmap(scaledBitmap, 0, 0, bitmap.Width, bitmap.Height, matrix, true);
						helper.pictureCompress(rotated, out bytes);
						scaledBitmap.Dispose();

					}

					if (bytes.Length > 0)
					{
						Container.userItem.Picture = new ImageItem();
						Container.userItem.Picture.Uri = System.Convert.ToBase64String(bytes, 0, bytes.Length);

						RaisePopUpDismissed("");

					}
					else Container.userItem.Picture.Uri = "";


					RunOnUiThread(() =>
					{
						if (_userImage != null)
						{
							try
							{
								_userImage.SetImageDrawable(new CircleDrawable(BitmapFactory.DecodeByteArray(bytes, 0, bytes.Length)));
							}
							catch { }
						}

					});

				}
				else
				{
					Android.Widget.Toast.MakeText(this, GetString(Resource.String.PictureErrorText), ToastLength.Long).Show();
				}

			}
			catch { }

		}


		#endregion

		protected override void OnDestroy()
		{
			base.OnDestroy();

			if (_wheelMenu != null)_wheelMenu.Dispose ();
		
			if (_geo != null)
			{
				_geo.SuspendLocationUpdates();
			}
		}

    }
}

