using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Timers;

//using Gcm.Client;
using System.IO;
using NoBadDaysPCL.ViewModel;
using SQLite.Net.Platform.XamarinAndroid;
using Gcm.Client;


namespace NoBadDays.Splash
{
    [Activity(MainLauncher = true, Theme = "@style/Theme.Splash", NoHistory = true)]
    public class SplashActivity : Activity
    {
		UserViewModel userViewModel {get; set;}

		Timer timer = new Timer(1000);

		int _type = -1;
		int _badge = -1;
		int _selection = -1;

              
		public SplashActivity()
		{
          
			_type = -1;
			_badge = -1;
			_selection = -1;
          
		}

       

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Create your application here
            SetContentView(Resource.Layout.Splash);

			Bundle extras = Intent.Extras;

			try
			{
				_selection = extras.GetInt("selection");
				_type = extras.GetInt("type");
				_badge= extras.GetInt("badge");
			}
			catch (Exception exc)
			{
				
			}

            RequestedOrientation = Android.Content.PM.ScreenOrientation.Portrait;

            //Check to ensure everything's setup right

			string libraryPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
			var path = System.IO.Path.Combine(libraryPath, Constants.SQLitePath);

			userViewModel = new UserViewModel (new SQLitePlatformAndroid(), path);

			var user = userViewModel.GetUser ();
			  
			#if !SIMULATOR
            GcmClient.CheckDevice(this);
            GcmClient.CheckManifest(this);

			if ((user != null)&&(user.UserId != 0)&&(!Container.registered))
			{
			    GcmClient.Register(this, Constants.SenderID);
			}
			#endif

        }

        protected override void OnStart()
        {
            base.OnStart();

   //         GcmClient.GetRegistrationId(this);

            timer.Elapsed += timer_Elapsed;
            timer.Enabled = true;
            timer.Start();
                   
        }

        void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            timer.Enabled = false;
            timer.Stop();

			string registrationId = "";

			#if !SIMULATOR
			         

		
			registrationId = GcmClient.GetRegistrationId(this);


			#endif

            //If it's empty, we need to register
            if (string.IsNullOrEmpty(registrationId))
            {
                Container.registered = false;
            }
            else
            {
				Container.registered = true;
    
            }
 
     

            string libraryPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            var path = Path.Combine(libraryPath, Constants.SQLitePath);
            var viewmodel = new UserViewModel (new SQLitePlatformAndroid(), path);

            var user = viewmodel.GetUser ();


            if (user == null) {

         //       StartActivity (typeof(LoginPhoneNumber));
				StartActivity(typeof(ProfileMainPage));
              
            } else {


                Container.userItem = user;

        //        StartActivity(typeof(Savings));

				if (_selection == 1) {
					StartActivity (typeof(MapMainPage));
				} else if (_selection == 2) {
					StartActivity (typeof(Community));
				}else if (_selection == 3) {
					StartActivity (typeof(Info));
				}else if (_selection == 4) {
					StartActivity (typeof(Win));
				}else if (_selection == 5) {
					StartActivity (typeof(Charity));
				}else if (_selection == 6) {
					StartActivity (typeof(Savings));
				}else if (_selection == 7) {
					StartActivity (typeof(Offers));
				}
				else StartActivity(typeof(ProfileMainPage));
            }

			OverridePendingTransition(Resource.Animation.slide_in_right, Resource.Animation.slide_out_left);
			Finish();
        }
    }
}