using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;


namespace NoBadDays
{
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;




    namespace ViewPagerIndicator
    {
        public class SavingsPageViewerAdapter : Android.Support.V4.App.FragmentPagerAdapter
        {

            int _count;
            Savings _context;

            public SavingsPageViewerAdapter(Android.Support.V4.App.FragmentManager fm, Savings context)
                : base(fm)
            {
                _context = context;

                _count = 2;
            }


            public override Android.Support.V4.App.Fragment GetItem(int position)
            {
                return new SavingsSubPages(position, _context);
            }

            public override int Count
            {
                get
                {
                    return _count;
                }
            }

            public void SetCount(int Count)
            {
                if (Count > 0 && Count <= 10)
                {
                    _count = Count;
                    NotifyDataSetChanged();
                }
            }
        }
    }
}