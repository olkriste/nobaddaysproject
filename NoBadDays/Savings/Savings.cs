﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using NoBadDays.helpers;
using Android.Views.Animations;
using System.Timers;
using Android.Support.V4.View;
using NoBadDays.ViewPagerIndicator;
using DK.Ostebaronen.Droid.ViewPagerIndicator;
using Android.Support.V4.App;

using CustomTextView = NoBadDays.helpers.CustomTextView;
using NoBadDaysPCL.ViewModel;
using NoBadDaysPCL.Items;
using SQLite.Net.Platform.XamarinAndroid; 

namespace NoBadDays
{
    [Activity (Label = "", Theme = "@style/NoBadDaysTheme")]
    public class Savings : FragmentActivity, Android.Views.Animations.Animation.IAnimationListener,
    	Android.Support.V4.View.ViewPager.IOnPageChangeListener
    {

		public event EventHandler<int> DataReady0;
		public event EventHandler<int> DataReady1;

		public SavingsViewModel savingsViewModel {get; set;}

		public SavingsViewItem savingsItem { get; set;}

		public UserViewModel userViewModel { get; set; }
//		UserItem user = new UserItem ();


        const int ANIMATIONTIME = 500;

        Timer _animTimer = new Timer();

        private float _density;
        int _screenHeight;
        int _screenWidth;

        View actionBarView;


        SavingsPageViewerAdapter _adapter;
        ViewPager _pager;
        IPageIndicator _indicator;

	
        int _page = 1;
       

        ImageView _menuButton;

     

        int _marginMin;
        int _marginMax;

        RelativeLayout.LayoutParams _relParam;
     
        View _filling;

    
  //      RelativeLayout _rel;
        
        int amount = 50;   //between 0 and 100

        NoBadDays.helpers.CustomTextView _savingText;

        int limit;
        double interval;
        int cnt;

        WheelMenu _wheelMenu;

        TranslateAnimation _listUpAnimation;
        TranslateAnimation _listDownAnimation;

		bool _up = false;

	
		RelativeLayout _rel1;
		RelativeLayout _rel2;
		ImageView _arrow1 = null;
		ImageView _arrow2 = null;


		int _selectedPage;
		View _anchor;
	
    
        protected override void OnCreate (Bundle bundle)
        {
            base.OnCreate (bundle);

            // Create your application here
                 
    //        SetContentView (Resource.Layout.Savings);
			RequestWindowFeature(WindowFeatures.IndeterminateProgress);

            SetContentView(Resource.Layout.SavingsPageViewerIndicator);

			int version = Int32.Parse(Android.OS.Build.VERSION.Sdk);

			if (version != 19)   //KitKat 4.4.2
			{
				View decorView = Window.DecorView;
				// Hide the status bar.
				int uiOptions = (int)View.SystemUiFlagFullscreen;
				decorView.SystemUiVisibility = (StatusBarVisibility)uiOptions;
			}
  
            ActionBar actionBar = ActionBar;
            actionBar.SetDisplayShowHomeEnabled(false);
            //displaying custom ActionBar
            actionBarView = LayoutInflater.Inflate(Resource.Layout.actionbarLayout, null);
            actionBar.SetCustomView(actionBarView, new Android.App.ActionBar.LayoutParams(ActionBar.LayoutParams.WrapContent, ActionBar.LayoutParams.WrapContent,
                GravityFlags.Center|GravityFlags.Left));
            actionBar.SetDisplayOptions(ActionBarDisplayOptions.ShowCustom, ActionBarDisplayOptions.ShowCustom);

            _menuButton = actionBarView.FindViewById<ImageView>(Resource.Id.wheelBut);
    //        _menuButton.Click += _menuButton_Click;
                        
           
             actionBarView.FindViewById<CustomTextView> (Resource.Id.actionbarTitle).Text = GetString (Resource.String.savingsText);

            _adapter = new SavingsPageViewerAdapter(SupportFragmentManager, this);
            _pager = FindViewById<ViewPager>(Resource.Id.pager);
 
    //        _pager.SetPagingEnabled(false);
      
            _pager.AddOnPageChangeListener(this);

			_pager.SetPageTransformer(true, new PageTransformer());

            _pager.Adapter = _adapter;

            _indicator = FindViewById<CirclePageIndicator>(Resource.Id.indicator);
            _indicator.SetViewPager(_pager);

			_page = _pager.CurrentItem = 1;


            RequestedOrientation = Android.Content.PM.ScreenOrientation.Portrait;

            Display d = WindowManager.DefaultDisplay;

            Android.Util.DisplayMetrics m = new Android.Util.DisplayMetrics();
            d.GetMetrics(m);

            _density = m.Density;

            Point size = new Point();
            d.GetSize(size);
            _screenWidth = size.X;
            _screenHeight = size.Y;

			var _skinOverlay = FindViewById<View>(Resource.Id.skinOverlay);

			_wheelMenu = new WheelMenu (this, 0, _density, _menuButton, false, _skinOverlay);

			_marginMin = (-1)*(int)(_density * (220));
            _marginMax = (int)(_density * 20);

			_listUpAnimation = MakeMoveAnimation(_marginMax, _marginMin);
			_listDownAnimation = MakeMoveAnimation(_marginMin, _marginMax);

			_up = false;
	

			string libraryPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
			var path = System.IO.Path.Combine(libraryPath, Constants.SQLitePath);

			savingsViewModel = new SavingsViewModel (new SQLitePlatformAndroid(), path);

			savingsViewModel.PhoneDataErrorOccurred += savingsViewModel_PhoneDataErrorOccurred;
		    savingsViewModel.PhoneDataSuccessful += savingsViewModel_PhoneDataSuccessful;

			userViewModel = new UserViewModel (new SQLitePlatformAndroid(), path);


        }


		public void FetchData()
		{
			savingsViewModel.GetSavings();

			//			SetProgressBarIndeterminateVisibility(true);

		}

		void savingsViewModel_PhoneDataErrorOccurred (object sender, ServerOutputItem e)
		{

			Android.Widget.Toast.MakeText(this, e.ResultString, ToastLength.Long).Show();
		}

		void savingsViewModel_PhoneDataSuccessful (object sender, ServerOutputItem e)
		{

			_pager.AddOnPageChangeListener(this);

			savingsItem = savingsViewModel.Item;

			RaiseDataReady ();
		}

		void RaiseDataReady()
		{
			if (DataReady1 != null)
			{
				DataReady1(this, 1);
			}

			if (DataReady0 != null)
			{
				DataReady0(this, 0);
			}
		}

		/*
		public void SetProgress(bool state)
		{
			SetProgressBarIndeterminateVisibility(state);
		}

*/

		protected override void OnStart()
		{
			base.OnStart();
					
		}

		void startAnimation()
		{
			_anchor = null;

			if ((_selectedPage == 0) && (_rel1 != null)) {
				_anchor = _rel1;

			} else if ((_selectedPage == 1) && (_rel2 != null)) {

				_anchor = _rel2;
		
			}

		
			if (_anchor != null) {


				_relParam = new RelativeLayout.LayoutParams (
					RelativeLayout.LayoutParams.MatchParent, RelativeLayout.LayoutParams.MatchParent);
		

				if (_up) {
					_relParam.SetMargins (0, (int)(_marginMin), 0, 0);
					_relParam.AddRule(LayoutRules.Below, Resource.Id.mainrel);
					_anchor.LayoutParameters = _relParam;

					_anchor.StartAnimation (_listUpAnimation);
				
				} else 
				{
					_relParam.SetMargins (0,(int)(_marginMax), 0, 0);
					_relParam.AddRule(LayoutRules.Below, Resource.Id.mainrel);
					_anchor.LayoutParameters = _relParam;

					_anchor.StartAnimation (_listDownAnimation);

				

				}
			}
		}


		TranslateAnimation MakeMoveAnimation(int fromMargin, int toMargin)
		{
			TranslateAnimation animation = new TranslateAnimation(0, 0, 0, fromMargin - toMargin);
			animation.Duration = ANIMATIONTIME;
			animation.FillEnabled = true;
			animation.SetAnimationListener(this);

			return animation;
		}

		#region Animation interfaces

		public void OnAnimationStart(Animation animation)
		{

		}

		public void OnAnimationEnd(Animation animation)
		{
			_anchor.ClearAnimation();

			if (_up)
			{
		
			    RunOnUiThread(() =>
				{
					_relParam.SetMargins(0, (int)(_marginMax), 0, 0);
					_relParam.AddRule(LayoutRules.Below, Resource.Id.mainrel);
					_anchor.LayoutParameters = _relParam;

					if (_selectedPage == 0)
					{
				       	_arrow1.SetImageResource(Resource.Drawable.up);
					}
					else _arrow2.SetImageResource(Resource.Drawable.up);
			
				});

				_up = false;

			}
			else
			{
	
			   RunOnUiThread(() =>
				{

					_relParam.SetMargins(0, (int)(_marginMin), 0, 0);
					_relParam.AddRule(LayoutRules.Below, Resource.Id.mainrel);
					_anchor.LayoutParameters = _relParam;

					if (_selectedPage == 0)
					{
						_arrow1.SetImageResource(Resource.Drawable.down);
					}
					else _arrow2.SetImageResource(Resource.Drawable.down);

				});
						
				_up = true;

			}

		}


		public void OnAnimationRepeat(Animation animation)
		{

		}
		#endregion


		public void SetView(int pos, View view)
		{
			if (pos == 0) {
				
				_rel1 = view.FindViewById<RelativeLayout> (Resource.Id.rellist);
		
				_arrow1 = view.FindViewById<ImageView> (Resource.Id.direction);

			   
				_arrow1.Click += (object sender, EventArgs e) => {
					
						
				startAnimation ();

				};


			} else {
				
				_rel2 = view.FindViewById<RelativeLayout> (Resource.Id.rellist);
			

				_arrow2 = view.FindViewById<ImageView> (Resource.Id.direction);


				_arrow2.Click += (object sender, EventArgs e) => {
					

				startAnimation ();

				
			};

			}


		}

		#region Viewpager interfaces


        public void OnPageSelected(int pos)
        {
			_selectedPage = pos;

	
        }

        public void OnPageScrollStateChanged(int state)
        {
			
        }

        public void OnPageScrolled(int position, float positionOffset, int positionOffsetPixels)
        {
            
        }

		#endregion


        protected override void OnResume()
        {
            base.OnResume ();

		
        }

   

        public override bool OnKeyDown(Keycode keyCode, KeyEvent e)
        {
            if (keyCode == Keycode.Back)
            {
                return _wheelMenu.CloseActivity(this);
            }

            return base.OnKeyDown(keyCode, e);
        }

        public override void OnLowMemory()
        {
            Console.WriteLine("Savings:OnLowMemory");
            GC.Collect();
        }

		protected override void OnDestroy()
		{
			base.OnDestroy();

			if (_wheelMenu != null)_wheelMenu.Dispose ();

		}
    }
}

