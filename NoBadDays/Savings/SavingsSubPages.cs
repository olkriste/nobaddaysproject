﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;

using Android.Views;
using Android.Widget;

using Fragment = Android.Support.V4.App.Fragment;
using Android.Views.Animations;
using System.Timers;
using NoBadDaysPCL.Items;
using Android.Text.Method;
using Android.Text;
using Android.Text.Style;

namespace NoBadDays
{
	public class SavingsSubPages : Fragment, Android.Views.Animations.Animation.IAnimationListener//, View.IOnTouchListener
    {

		SavingsViewItem _savingsItem { get; set;}

		const int ANIMATIONTIME = 4000;

        Timer _animTimer = new Timer();
		Timer _textTimer  = new Timer();


        private float _density;
        int _screenHeight;
        int _screenWidth;

        int _page;
        View _view = null;
        Savings _activity;


        TranslateAnimation _upAnimation;

        int _marginMin;
        int _marginMax;
		int _currentMargin;

        RelativeLayout.LayoutParams _relParam;

        View _filling;

		int amount;
		int part;

		bool _fetched = false;
		bool _data0 = false;
		bool _data1 = false;


  

   
        NoBadDays.helpers.CustomTextView _savingText;
		NoBadDays.helpers.CustomTextView _loginHereText;
		ScrollView _infoScroll;

		ImageView _arrow = null;
	
		UserItem _user;

        int _limit;
        double _interval;
        int _cnt;
		int _limitText;



		public ListView _listView;
		public SavingsListAdapter _adapter;

		ProgressBar _progressSpinner;

	

		int MAXMARGIN = 180;
		int PIGGYWIDTH = 180;


		Animation _shake;
		ImageView _piggy;
	
        
        public SavingsSubPages() 
        {
           
        }


        public SavingsSubPages(int page, Savings context)
        {
            _page = page;
            _activity = context;

			_fetched = false;
			_data0 = false;
			_data1 = false;
            
 
        }
        
        public override void OnCreate (Bundle savedInstanceState)
        {
            base.OnCreate (savedInstanceState);

            // Create your fragment here

            Display d = this.Activity.WindowManager.DefaultDisplay;

            Android.Util.DisplayMetrics m = new Android.Util.DisplayMetrics();
            d.GetMetrics(m);

            _density = m.Density;

        }

        public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            
			Savings parent = _activity as Savings;

            if (_page == 0)
            {
				try {

				_view = inflater.Inflate(Resource.Layout.SavingsPageOne, null);
		
				_savingText = _view.FindViewById<NoBadDays.helpers.CustomTextView>(Resource.Id.saving);
				_loginHereText = _view.FindViewById<NoBadDays.helpers.CustomTextView>(Resource.Id.loginHere);
				_infoScroll = _view.FindViewById<ScrollView>(Resource.Id.scroll);


				}
				catch (Exception exc) {
					
				}

				if (!_data0) {
					_activity.DataReady0 += _activity_DataReady0;
				} else {
					loaddata0();
				}
            }
            else if (_page == 1)
            {
               _view = inflater.Inflate(Resource.Layout.SavingsPageTwo, null);
	
				_progressSpinner = _view.FindViewById<ProgressBar>(Resource.Id.ProgressSpinner);
				_savingText = _view.FindViewById<NoBadDays.helpers.CustomTextView>(Resource.Id.saving);
				_loginHereText = _view.FindViewById<NoBadDays.helpers.CustomTextView>(Resource.Id.loginHere);
				_infoScroll = _view.FindViewById<ScrollView>(Resource.Id.scroll);
				_savingText.Visibility = ViewStates.Gone;
				_progressSpinner.Visibility = ViewStates.Visible;

				if (!_fetched) {
					_fetched = true;
					_activity.FetchData ();
				}

				if (!_data1) {
					_activity.DataReady1 += _activity_DataReady1;
				} else {
					loaddata1();
				}

						
            }
 
            else throw new System.SystemException("Unexpected error in viewpager");

			_listView = _view.FindViewById<ListView> (Resource.Id.Savingslist);
			_arrow = _view.FindViewById<ImageView> (Resource.Id.direction);

			_user = _activity.userViewModel.GetUser ();

			if (_user != null) {

				_adapter = new SavingsListAdapter (_activity, _page, _listView);
				_listView.Visibility = ViewStates.Visible;
				_infoScroll.Visibility = ViewStates.Gone;

				parent.SetView (_page, _view); 
			

			} else {
				_listView.Visibility = ViewStates.Gone;
				_arrow.Visibility = ViewStates.Gone;
				_infoScroll.Visibility = ViewStates.Visible;

				SpannableString spannable;

				var str = _activity.GetString (Resource.String.LoginHereText);

				spannable = new SpannableString(str);
				spannable.SetSpan(new UnderlineSpan(), 0, str.Length, 0);
			
				_loginHereText.TextFormatted = spannable;

				_loginHereText.Click+= (sender, e) => 
				{
					_activity.Finish();
					_activity.StartActivity(typeof(LoginPhoneNumber));

				};

			
				if (_progressSpinner != null) {
					_progressSpinner.Visibility = ViewStates.Gone;
				}


			}

            return _view;


        }


		void _activity_DataReady1 (object sender, int e)
		{
			
			_savingsItem = _activity.savingsViewModel.Item;
			_data1 = true;

			if (e == 1)
			{
				_data1 = true;
				loaddata1 ();
			} 

		
		}

		void loaddata1()
		{

			if ((_listView != null) && (_savingsItem.ThisMonth != null) && (_savingsItem.ThisMonth.Savings.Count > 0)) {
				_listView.Adapter = _adapter;
				_listView.Invalidate ();
				_adapter.NotifyDataSetChanged ();
			}

			_marginMin = (int)(_density * (-5));


			_savingText.Visibility = ViewStates.Visible;
			_progressSpinner.Visibility = ViewStates.Gone;

			_filling = _view.FindViewById<View> (Resource.Id.filling);


			_relParam = new RelativeLayout.LayoutParams (RelativeLayout.LayoutParams.WrapContent,
					RelativeLayout.LayoutParams.WrapContent);

			_relParam.SetMargins (0, _marginMin, 0, 0);
			_relParam.AddRule (LayoutRules.CenterHorizontal);
			_relParam.AddRule (LayoutRules.Below, Resource.Id.PiggyBank);
			_relParam.Height = 0;
	    	_relParam.Width = (int)(PIGGYWIDTH * _density);
			_filling.LayoutParameters = _relParam;
			_filling.Visibility = ViewStates.Visible;

			if (_savingsItem.ThisMonth.MaxAmount > 0) {

				StartAnimation ();

			}

		}

		void _activity_DataReady0 (object sender, int e)
		{
			
			_savingsItem = _activity.savingsViewModel.Item;

			if (e == 0)
			{
				_data0 = true;
				loaddata0 ();
			} 
		

		}

		void loaddata0()
		{
			if ((_listView != null)&&(_savingsItem.PreviousMonth != null)&&(_savingsItem.PreviousMonth.Savings.Count > 0)) {
				_listView.Adapter = _adapter;
				_listView.Invalidate ();
				_adapter.NotifyDataSetChanged ();
			}

			_savingText.Text = _savingsItem.PreviousMonth.Amount.ToString () + _savingsItem.PreviousMonth.Unit;
		}


        public override void OnStart()
        {

            base.OnStart();

        }


		public override void OnResume()
		{
			base.OnResume ();

		}



        TranslateAnimation MakeAnimation(int fromMargin, int toMargin)
        {
            TranslateAnimation animation = new TranslateAnimation(0, 0, 0, fromMargin - toMargin);
            animation.Duration = ANIMATIONTIME;
            animation.SetAnimationListener(this);
            animation.FillAfter = true;

            return animation;
        }

        #region Animation interfaces

		public void OnAnimationStart(Animation animation)
		{

		}

       
		public void StartAnimation()
        {
  
			var amountpart = _savingsItem.ThisMonth.Amount;
			var maxamount = _savingsItem.ThisMonth.MaxAmount;

			amountpart = (amountpart > maxamount) ? maxamount : amountpart;

			amount = (100 * amountpart) / maxamount;


			_limit = (MAXMARGIN) * amount / 100;
	

			_marginMax =  (int)(_density *(-MAXMARGIN) * amount / 100);

			_currentMargin = _marginMin;
            _interval = ANIMATIONTIME / _limit;
            _cnt = 0;

	//		_shake = AnimationUtils.LoadAnimation(_activity, Resource.Animation.shakeone);
	//		_piggy=	_view.FindViewById<ImageView>(Resource.Id.PiggyBank);

	//		_piggy.StartAnimation(_shake);


            _animTimer.Elapsed += _animTimer_Elapsed;
            _animTimer.Enabled = true;
            _animTimer.Interval = _interval;
            _animTimer.Start();


		    _limitText = amount;
			_interval = ANIMATIONTIME / amount;
		    _textTimer.Elapsed+= _textTimer_Elapsed;
			_textTimer.Enabled = true;
			_textTimer.Interval = _interval;
			_textTimer.Start();

        }

        void _animTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            
			try{

			if (_limit > 1)
            {
                _animTimer.Start();
                part = _cnt;
                _cnt++;
                _limit = _limit - 1;
				_currentMargin = _currentMargin - 1;

            }
            else
            {
                _limit = _cnt = 0;
                _animTimer.Enabled = false;
                _animTimer.Stop();

				part = amount;//(MAXMARGIN - 25) * amount / 100;

				_piggy.ClearAnimation();




            }



            this.Activity.RunOnUiThread(() =>
            {

				    _relParam.SetMargins(0,(int)( _currentMargin*_density), 0, 0);
					_relParam.AddRule(LayoutRules.CenterHorizontal);
					_relParam.AddRule (LayoutRules.Below, Resource.Id.PiggyBank);
					_relParam.Height = (int)((-_currentMargin-5) * _density);
					_relParam.Width = (int)(PIGGYWIDTH * _density);
					_filling.LayoutParameters = _relParam;

            });

			}
			catch (Exception exc) {


			}


        }

		void _textTimer_Elapsed (object sender, ElapsedEventArgs e)
		{

			if (_limitText > 1)
			{
				_cnt++;
				_limitText = _limitText - 1;

			}
			else
			{
				
				_limitText = _cnt = 0;
				_textTimer.Enabled = false;
				_textTimer.Stop();
							
			}


			this.Activity.RunOnUiThread(() =>
			{

				var currency = _savingsItem.ThisMonth.Unit;

				_savingText.Text = (amount-_limitText).ToString() + " " + currency;

		
			});
		}

        public void OnAnimationEnd(Animation animation)
        {
     //       _rel.ClearAnimation();


        }


        public void OnAnimationRepeat(Animation animation)
        {

        }
        #endregion

	
		public override void OnDestroy()
		{
		    base.OnDestroy ();
		
			if (_animTimer != null) {
				_animTimer.Enabled = false;
				_animTimer.Stop ();
			}

			if (_textTimer != null) {
				_textTimer.Enabled = false;
				_textTimer.Stop ();
			}
		}

    }
}

