﻿using System;
using Android.Widget;
using Android.Views;
using System.Collections.Generic;

using CustomTextView = NoBadDays.helpers.CustomTextView;
using NoBadDays.helpers;
using Android.Graphics; 

using Koush;
using NoBadDaysPCL.Items;
using System.Linq;

namespace NoBadDays
{
	public class SavingsListAdapter: BaseAdapter, IListAdapter, Koush.IUrlImageViewCallback 
	{

		public static ListView BackGroundListView = null;

		Savings _activity;


		LayoutInflater _inflater;

		int _page;

	
		ListView _prevListView;
		ListView _currentView;


		public SavingsListAdapter (Savings activity, int page, ListView listView)
		{

			_inflater = LayoutInflater.From(activity);

			_page = page;
			_activity = activity;
					

		}

		public override long GetItemId(int position)
		{

			return 0;
		}

		public override Java.Lang.Object GetItem(int position)
		{
			return null; // could wrap a Contact in a Java.Lang.Object to return it here if needed
		}

		public SavingsListItem this[int position]
		{
			get
			{
				return _activity.savingsItem.ThisMonth.Savings.ElementAt(position);

			}  
		}


		public override int Count
		{
			get 
			{

				if (_page == 0 )
				{
					return _activity.savingsItem.PreviousMonth.Savings.Count;
				}
				else return _activity.savingsItem.ThisMonth.Savings.Count;

			}

		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			SavingsViewHolder holder;

			if (convertView == null)
			{
				
				convertView = _inflater.Inflate(Resource.Layout.SavingItem, parent, false);
			
				holder = new SavingsViewHolder();
				holder.savingimage = convertView.FindViewById<ImageView>(Resource.Id.savingImage);
				holder.text = convertView.FindViewById<CustomTextView>(Resource.Id.saving);
				holder.amount = convertView.FindViewById<CustomTextView>(Resource.Id.amount);


				convertView.SetTag(Resource.Layout.SavingItem, holder);
			}
			else
			{
				holder = (SavingsViewHolder)convertView.GetTag(Resource.Layout.SavingItem);
			}

			holder.position = position;


			_activity.RunOnUiThread(() =>
			{
				if (holder.savingimage != null)
				{
					if (_page == 1)
					{
				    	try
					    {

						    UrlImageViewHelper.SetUrlDrawable(holder.savingimage, _activity.savingsItem.ThisMonth.Savings.ElementAt(position).Picture.Uri,  Resource.Drawable.GhostCircle, 60000, this);

					    }
					    catch { }
		
						holder.text.Text = _activity.savingsItem.ThisMonth.Savings.ElementAt(position).Name;
					    holder.amount.Text = _activity.savingsItem.ThisMonth.Savings.ElementAt(position).Amount;
					}
					else
					{
						try
						{
							UrlImageViewHelper.SetUrlDrawable(holder.savingimage, _activity.savingsItem.PreviousMonth.Savings.ElementAt(position).Picture.Uri,  Resource.Drawable.GhostCircle, 60000, this);
						}
						catch { }

						holder.text.Text = _activity.savingsItem.PreviousMonth.Savings.ElementAt(position).Name;
						holder.amount.Text = _activity.savingsItem.PreviousMonth.Savings.ElementAt(position).Amount;
					}
				}
			});


			return convertView;
		}



		public void OnLoaded(ImageView imageView, Bitmap loadedBitmap, string url, bool loadedFromCache)
		{
			_activity.RunOnUiThread(() =>
			{
				if (loadedBitmap != null)
				{
					imageView.SetImageDrawable(new CircleDrawable(loadedBitmap));
				}
				this.NotifyDataSetChanged();

			});

		}



		private SavingsViewHolder getViewHolder(View v)
		{
			if (v.GetTag(Resource.Layout.SavingItem) == null)
			{
				return getViewHolder((View)v.Parent);
			}
			return (SavingsViewHolder)v.GetTag(Resource.Layout.SavingItem);
		}
	}



}

