using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using CustomTextView = NoBadDays.helpers.CustomTextView; 

namespace NoBadDays
{
    class SavingsViewHolder : Java.Lang.Object
    {
        public ImageView savingimage { get; set; }
		public CustomTextView text { get; set; }
		public CustomTextView amount { get; set; }
        public int position { get; set; }
    }
}