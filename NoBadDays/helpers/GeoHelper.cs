using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Locations;
using Android.Provider;
using System.ComponentModel;
using System.Timers;



namespace NoBadDays
{
    public class GeoHelper : Java.Lang.Object, ILocationListener
    {
  
        LocationManager _locMgr;

		Activity _activity;

       
        Timer timer = new Timer();

        public GeoHelper(Activity activity)
        {
            // Request location service  
            
			_activity = activity;

			_locMgr = activity.GetSystemService(Context.LocationService) as LocationManager;


            var locationCriteria = new Criteria();

            locationCriteria.Accuracy = Accuracy.NoRequirement;
      //      locationCriteria.Accuracy = Accuracy.Fine;
            locationCriteria.PowerRequirement = Power.NoRequirement;
          
      
           
            string locationProvider = _locMgr.GetBestProvider(locationCriteria, false);


            timer.Elapsed += new ElapsedEventHandler(OnTimeEvent);
            timer.Enabled = true;
            timer.Interval = 10000;
            timer.Start();

            Location loca = _locMgr.GetLastKnownLocation(locationProvider);

            if (loca != null)
            {
                NoBadDays.Container.Longitude = loca.Longitude;
                NoBadDays.Container.Latitude = loca.Latitude;
                NoBadDays.Container.location = loca;
            }
            else
            {
                locationCriteria.PowerRequirement = Power.Medium;
                var locationProvider2 = _locMgr.GetBestProvider(locationCriteria, false);
                loca = _locMgr.GetLastKnownLocation(locationProvider2);
                if (loca != null)
                {
                    NoBadDays.Container.Longitude = loca.Longitude;
                    NoBadDays.Container.Latitude = loca.Latitude;
                    NoBadDays.Container.location = loca;
                }
                else // default middle of Denmark
                {
                    NoBadDays.Container.Longitude = 10.5886294;
                    NoBadDays.Container.Latitude = 55.8146665;
                    NoBadDays.Container.location = loca;
                }


            }

            _locMgr.RequestLocationUpdates(locationProvider, 0, 0, this);

        }

        protected void OnTimeEvent(object sender, EventArgs e)
        {
            _locMgr.RequestLocationUpdates(LocationManager.GpsProvider, 10000, 50, this);
        }

        public void ResumeLocationUpdates()
        {
            _locMgr.RequestLocationUpdates(LocationManager.GpsProvider, 10000, 50, this);
        }

        public void SuspendLocationUpdates()
        {
            _locMgr.RemoveUpdates(this);
            timer.Stop();
            timer.Enabled = false;
        }

        public void OnLocationChanged(Location loca)
        {

            NoBadDays.Container.Longitude = loca.Longitude;
            NoBadDays.Container.Latitude = loca.Latitude;
            NoBadDays.Container.location = loca;

            timer.Enabled = false;
            timer.Stop();

        }

        public void OnProviderDisabled(string provider)
        {
           
        }

        public void OnProviderEnabled(string provider)
        {
            
        }

        public void OnStatusChanged(string provider, Availability status, Bundle extras)
        {
          
        }

		#region Check location settings

		public bool checkLocationSettings()
		{
			// return false if location service is not available

			bool gps_enabled = false;
			bool network_enabled = false;

			//       Maps activity = (Maps)this;

			_locMgr = this.getLocMgr();

			try
			{

				network_enabled = _locMgr.IsProviderEnabled(LocationManager.NetworkProvider);
				gps_enabled = _locMgr.IsProviderEnabled(LocationManager.GpsProvider);

			}
			catch (Exception exc)
			{

			}

			return (gps_enabled || network_enabled);
		}

		public LocationManager getLocMgr()
		{
			return _activity.GetSystemService(Context.LocationService) as LocationManager;
		}
		#endregion

         

    }
}
