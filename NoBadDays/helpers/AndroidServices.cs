using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Net;
using Android.Telephony;

namespace NoBadDays
{
    public class AndroidServices
    {

        Activity _activity;
        
        public AndroidServices(Activity activity)
        {
          
            _activity = activity;
        }
        
        public bool check_connection()
        {
            var connectivityManager = (ConnectivityManager)_activity.GetSystemService(Android.Content.Context.ConnectivityService);
            var activeConnection = connectivityManager.ActiveNetworkInfo;
            if ((activeConnection != null) && activeConnection.IsConnected)
            {
                return true;
            }

            return false;
        }

		public bool check_telefony_network()
		{
			TelephonyManager tm = (TelephonyManager) _activity.GetSystemService(Android.Content.Context.TelephonyService);
			if (tm.NetworkType != 0)
			{
				return true;
			}

			return false;
		}
    }
}