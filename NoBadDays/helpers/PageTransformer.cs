﻿using System;
using Android.Views;
using Android.Runtime;

namespace NoBadDays
{
    public class PageTransformer : Java.Lang.Object, Android.Support.V4.View.ViewPager.IPageTransformer
    {
        private static float MIN_SCALE = 0.75f;

        public void TransformPage(View view, float position)
        {
            int pageWidth = view.Width;

            if (position < -1)
            { // [-Infinity,-1)
                // This page is way off-screen to the left.
                view.Alpha = 0;

            }
            else if (position <= 0)
            { // [-1,0]
                // Use the default slide transition when moving to the left page
                view.Alpha = 1;
                view.TranslationX = 0;
                view.ScaleX = 1;
                view.ScaleY = 1;

            }
            else if (position <= 1)
            { // (0,1]
                // Fade the page out.
                view.Alpha = (1 - position);

                // Counteract the default slide transition
                view.TranslationX = (pageWidth * -position);

                // Scale the page down (between MIN_SCALE and 1)
                float scaleFactor = MIN_SCALE
                    + (1 - MIN_SCALE) * (1 - Math.Abs(position));
                view.ScaleX = scaleFactor;
                view.ScaleY = scaleFactor;

            }
            else
            { // (1,+Infinity]
                // This page is way off-screen to the right.
                view.Alpha = 0;
            }
        }

        public IntPtr Handle
        {
          //  get { return ((IJavaObject)this).Handle; }
            get { return ((Java.Lang.Object)this).Handle;  }
        }

        public void Dispose()
        {
            ((IJavaObject)this).Dispose();
        }
    }
}

