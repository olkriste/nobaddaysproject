using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V4.View;
using Android.Util;

namespace NoBadDays.Helpers
{
    public class CustomRelative: RelativeLayout
    {

         private bool Enabled;

		public CustomRelative (IntPtr handle, JniHandleOwnership transfer)
               : base (handle, transfer)
         {

         }

		public CustomRelative(Context context, IAttributeSet attrs)
               : base(context, attrs)
         {
              this.Enabled = true;
         }


         public override bool OnTouchEvent (MotionEvent e)
         {
			return true;
         }

      
      
    }
}