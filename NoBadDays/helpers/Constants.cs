﻿using System;
using System.Collections.Generic;

namespace NoBadDays
{
    public static class Constants
    {
      
        public const string SQLitePath =  "AppDB.db3";

        public const string NOBADDAYSRED = "#FF4141";

        public const int FLIPDURATION = 250; //Flip duration for code fields

     
        public const int DIALOG_YES_NO_MESSAGE = 1;
		public const int DIALOG_NO_GPS_MESSAGE = 2;

		public const float SLIDERMAX = 20000;  //meters
		public const float SLIDERMIN = 20;


		public const int ANIMTIME = 800;

		public const string SenderID = "1042473096934"; // Google API Project Number
		public const string ListenConnectionString = 
	"Endpoint=sb://nobaddaystest-ns.servicebus.windows.net/;SharedAccessKeyName=DefaultListenSharedAccessSignature;SharedAccessKey=eR5wAaWGBFkt64vYYPITzhTzx7ToZkoP5PB+XiqwGNo=";
		public const string NotificationHubName = "nobaddaystest";

		public static Dictionary<int, int> clusterMarkers = new Dictionary<int, int>()
		{
			// Stores
			{2, Resource.Drawable.two},
			{3, Resource.Drawable.three},
			{4, Resource.Drawable.four},
			{5, Resource.Drawable.five},
			{10, Resource.Drawable.ten},
			{20, Resource.Drawable.twenty},
			{50, Resource.Drawable.fifty},
			{100, Resource.Drawable.hundred}
		};

       
    }
}

