﻿using NoBadDays;
using System;
using NoBadDaysPCL.Items;
using Android.Locations;
using System.Collections.Generic;


namespace NoBadDays
{
    static class Container
    {
        public static bool registered = false;
		public static bool editing = false;

        public static UserItem userItem = new UserItem ();

		public static List<String> listDataItem = new List<String>();

		public static string latitudeStore = "";
		public static string longitudeStore = "";
		public static float zoomLevelStore;
		public static NoBadDaysPCL.MainClass.eShopType shoptypeStore = NoBadDaysPCL.MainClass.eShopType.None;

        private static double _latitude;
        private static double _longitude;
        private static Location _location;

        public static double Latitude
        {
            get { return _latitude; }
            set
            {
                {
                    _latitude = value;
                }
            }
        }

        public static double Longitude
        {
            get { return _longitude; }
            set
            {
                {
                    _longitude = value;
                }
            }
        }

        public static Location location
        {
            get { return _location; }
            set
            {
                {
                    _location = value;
                }
            }
        }
     
    }
}

