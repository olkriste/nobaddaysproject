using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.IO;
using Android.Graphics;
using Android.Content.Res;

namespace NoBadDays
{
    public class ImageHelper
    {
        public Bitmap readBitmap(Android.Net.Uri selectedImage, Activity context, int widthReq, int heightReq)
        {
            Bitmap bm = null;
            BitmapFactory.Options options = new BitmapFactory.Options { InJustDecodeBounds = true };

            AssetFileDescriptor fileDescriptor = null;

            try
            {
                fileDescriptor = context.ContentResolver.OpenAssetFileDescriptor(selectedImage, "r");
            }
            catch (FileNotFoundException exc)
            {
               
            }
            finally
            {
                try
                {
                    bm = BitmapFactory.DecodeFileDescriptor(fileDescriptor.FileDescriptor, null, options);
                    options.InSampleSize = CalculateInSampleSize(options, widthReq, heightReq);
                    options.InJustDecodeBounds = false;

                    bm = BitmapFactory.DecodeFileDescriptor(fileDescriptor.FileDescriptor, null, options);
                    fileDescriptor.Close();
                }
                catch (IOException exc)
                {
                  
                }
            }
            return bm;
        }


        public int CalculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight)
        {


            var height = (float)options.OutHeight;
            var width = (float)options.OutWidth;
            var inSampleSize = 1D;

            if (height > reqHeight || width > reqWidth)
            {
                inSampleSize = width > height
                                    ? height / reqHeight
                                    : width / reqWidth;
            }

            //         calculat nearst number that is is power of 2 and greater than inSampleSize

            int cnt = 1;

            while (cnt < inSampleSize)
                cnt *= 2;

            return (int)cnt;

        }

        public void pictureCompress(Android.Graphics.Bitmap bitmap, out  byte[] bytes)
        {
            MemoryStream ms = new MemoryStream();
            bitmap.Compress(Bitmap.CompressFormat.Jpeg, 80, ms);
            ms.Position = 0;
            bytes = ms.ToArray();
            ms.Close();
        }


     
    }

   
}