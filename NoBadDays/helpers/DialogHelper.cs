﻿using System;
using Android.App;
using Android.Graphics;
using Android.Views;
using Android.Widget;

namespace NoBadDays
{
    public class DialogHelper
    {
        public DialogHelper()
        {

        }


        public void SetDialogButtons(Dialog dialog)
        {
            var negBut = ((AlertDialog)dialog).GetButton((int)Android.Content.DialogButtonType.Negative);
            var posBut = ((AlertDialog)dialog).GetButton((int)Android.Content.DialogButtonType.Positive);
             
			posBut.SetTextColor (Android.Graphics.Color.White);
			negBut.SetTextColor (Android.Graphics.Color.White);
          
			negBut.SetBackgroundColor(Android.Graphics.Color.Black);
			posBut.SetBackgroundColor(Android.Graphics.Color.Black);
        }

        public void SetDialogButtons2(Dialog dialog)
        {
            var negBut = ((AlertDialog)dialog).GetButton((int)Android.Content.DialogButtonType.Negative);
            var posBut = ((AlertDialog)dialog).GetButton((int)Android.Content.DialogButtonType.Positive);
            var neutralBut = ((AlertDialog)dialog).GetButton((int)Android.Content.DialogButtonType.Neutral);

			posBut.SetTextColor (Android.Graphics.Color.White);
			negBut.SetTextColor (Android.Graphics.Color.White);
			neutralBut.SetTextColor (Android.Graphics.Color.White);

			negBut.SetBackgroundColor(Android.Graphics.Color.Black);
			posBut.SetBackgroundColor(Android.Graphics.Color.Black);
			neutralBut.SetBackgroundColor(Android.Graphics.Color.Black);
        }

        public void SetDialog(out View customView, out AlertDialog.Builder builder, Activity context)
        {
                              
       
            customView = context.LayoutInflater.Inflate(Resource.Layout.customdialog, null);
             
            builder = new AlertDialog.Builder(context);
            builder.SetView(customView);
        }

    }
}

