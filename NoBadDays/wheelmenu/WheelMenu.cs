﻿using System;
using Com.Szugyi.Circlemenu.View;
using Android.Widget;
using System.Timers;
using Android.App;
using Android.Views.Animations;
using Android.Views;
using Android.Graphics;
using Android.Content.Res;
using NoBadDaysPCL.ViewModel;
using SQLite.Net.Platform.XamarinAndroid;
using Android.Content;

using CustomTextView = NoBadDays.helpers.CustomTextView; 


namespace NoBadDays
{
    public class WheelMenu:Java.Lang.Object, IDisposable,Com.Szugyi.Circlemenu.View.CircleLayout.IOnItemSelectedListener,
    Com.Szugyi.Circlemenu.View.CircleLayout.IOnItemClickListener, Com.Szugyi.Circlemenu.View.CircleLayout.IOnRotationFinishedListener,
	Com.Szugyi.Circlemenu.View.CircleLayout.IOnCenterClickListener, Android.Views.Animations.Animation.IAnimationListener,View.IOnTouchListener
    {


		public event EventHandler<bool> MenuDismissed;

		UserViewModel _viewmodel;

        public Activity _activity { get; set; }

        private float _density;
      
        CircleLayout _circleMenu;
      
      
		CustomTextView _centerText;
        ImageView _selector;

        FrameLayout.LayoutParams _param;
        RelativeLayout.LayoutParams _relParam;

        RelativeLayout _rel;

        int _marginMin;
        int _marginMax;
        
        bool _visible;

        View _anchor;

        View _overlay = null;

        TranslateAnimation _wheelUpAnimation;
        TranslateAnimation _wheelDownAnimation;

        AlphaAnimation _alphaAnimationUp;
        AlphaAnimation _alphaAnimationDown;

		string _tag;

		int _screenHeight;

		const int MARGINMAX = 400;
		const int MARGINMIN = 200;

	    

        public WheelMenu (Activity activity, int alignment, float density, View anchor, bool visible, View overlay)
        {
            _activity = activity;
            _density = density;
            _anchor = anchor;
            _visible = visible;
            _overlay = overlay;

			_overlay.SetOnTouchListener(this);

	        string libraryPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            var path = System.IO.Path.Combine(libraryPath, Constants.SQLitePath);
            _viewmodel = new UserViewModel(new SQLitePlatformAndroid(), path);


            Display d = _activity.WindowManager.DefaultDisplay;

            Android.Util.DisplayMetrics m = new Android.Util.DisplayMetrics();
            d.GetMetrics(m);

            _density = m.Density;

			Point size = new Point();
			d.GetSize(size);
			_screenHeight = size.Y;
                     
			_centerText = _activity.FindViewById<CustomTextView>(Resource.Id.centerText);
		
	//		_centerText.Visibility = ViewStates.Gone;


            _selector = _activity.FindViewById<ImageView>(Resource.Id.skin);

                  
             _circleMenu = _activity.FindViewById<CircleLayout>(Resource.Id.main_circle_layout);


		        
            _circleMenu.SetOnItemSelectedListener(this);
            _circleMenu.SetOnItemClickListener(this);
            _circleMenu.SetOnRotationFinishedListener(this);
       //     _circleMenu.SetOnCenterClickListener(this);
				
           
            _anchor.Click += _anchor_Click;

		
            _marginMin = (int)(_density*-MARGINMIN);  
            _marginMax = (int)(_density*-MARGINMAX);

            _rel = _activity.FindViewById<RelativeLayout>(Resource.Id.rellayout);
          

			_relParam = new RelativeLayout.LayoutParams((int)(MARGINMAX * _density), (int)(MARGINMAX * _density));

         
            _relParam.AddRule(LayoutRules.AlignParentBottom);
			_relParam.AddRule(LayoutRules.CenterHorizontal);
            _circleMenu.LayoutParameters = _relParam;

            _wheelUpAnimation = MakeWheelAnimation(_marginMax, _marginMin);
            _wheelDownAnimation = MakeWheelAnimation(_marginMin, _marginMax);
      
            _alphaAnimationUp = new AlphaAnimation(0.0f, 1.0f);
            _alphaAnimationDown = new AlphaAnimation(1.0f, 0.0f);
			_alphaAnimationUp.Duration = Constants.ANIMTIME;
			_alphaAnimationDown.Duration = Constants.ANIMTIME;

		

            if (!_visible) {
                _circleMenu.Visibility = ViewStates.Gone;
                _centerText.Visibility = ViewStates.Gone;
                _selector.Visibility = ViewStates.Gone;
            } 

		                
        }

	              

        TranslateAnimation MakeWheelAnimation(int fromMargin, int toMargin)
        {
               TranslateAnimation animation = new TranslateAnimation(0, 0, 0, fromMargin - toMargin);
		    	animation.Duration = Constants.ANIMTIME;
               animation.SetAnimationListener(this);
    
               return animation;
        }

        #region Animation interfaces

        public void OnAnimationStart(Animation animation)
        {
			

        }

        public void OnAnimationEnd(Animation animation)
        {
            _rel.ClearAnimation();
            if (_overlay != null)_overlay.ClearAnimation();

		
            if (_visible)
            {
                _visible = false;
            
                _activity.RunOnUiThread(() =>
                {
         
                    _param.SetMargins(0, 0, 0, (int)(_marginMax));
                    _rel.LayoutParameters = _param;
            
                    changeItem(_tag);

                });
               
            }
            else
            {
                _visible = true;
        //         GC.Collect();

                _activity.RunOnUiThread(() =>
                {

					_centerText.Text = GetMenuItem ("");
					_centerText.Visibility = ViewStates.Visible;
                   
                    _param.SetMargins(0, 0, 0, (int)(_marginMin));
                    _rel.LayoutParameters = _param;

                    if (_overlay != null) _overlay.Visibility = ViewStates.Visible;

				
                });

    
            }

        }

		public void OnClick(View view)
		{
			string t = "";
	    }


        public void OnAnimationRepeat(Animation animation)
        {
			
        }
        #endregion




        void changeItem(string item)
        {
 
			if (_overlay != null) _overlay.Visibility = ViewStates.Invisible;

			if (item == "")
				return;

			if ((_activity.GetType ().FullName == "NoBadDays.Info")&&(item ==  "Info"))
				return;

			if ((_activity.GetType ().FullName == "NoBadDays.Charity")&&(item == "Charity"))
				return;

			if ((_activity.GetType ().FullName == "NoBadDays.Community")&&(item == "Community"))
				return;

			if ((_activity.GetType ().FullName == "NoBadDays.Win")&&(item == "Win"))
				return;

			if ((_activity.GetType ().FullName == "NoBadDays.MapMainPage")&&(item == "Map"))
				return;

			if ((_activity.GetType ().FullName == "NoBadDays.Savings")&&(item == "Savings"))
				return;

			if ((_activity.GetType ().FullName == "NoBadDays.Offers")&&(item == "Offers"))
				return;

			if ((_activity.GetType ().FullName == "NoBadDays.ProfileMainPage")&&(item == "Profile"))
				return;


          
            for (int i = 0; i < _circleMenu.ChildCount; i++) {
                _circleMenu.GetChildAt (i).Dispose ();
            }


            _circleMenu.DisConnectWheel();
            _circleMenu.Dispose ();
            this.Dispose();
            GC.Collect (0);


            if (item == "Info")
            {
                _activity.StartActivity(typeof(Info));
            }
            else if (item == "Charity")
            {
                _activity.StartActivity(typeof(Charity));
            }
            else if (item == "Community")
            {
                _activity.StartActivity(typeof(Community));
            }
            else if (item == "Offers")
            {
                _activity.StartActivity(typeof(Offers));
            }
            else if (item == "Savings")
            {
                _activity.StartActivity(typeof(Savings));
            }
            else if (item == "Profile")
            {
                _activity.StartActivity(typeof(ProfileMainPage));
            }
            else if (item == "Map")
            {
                _activity.StartActivity(typeof(MapMainPage));
            }
            else if (item == "Win")
            {
                _activity.StartActivity(typeof(Win));
            }

			_activity.OverridePendingTransition(Resource.Animation.slide_in_right, Resource.Animation.slide_out_left);
			_activity.Finish();
			        
      
        }


		string GetMenuItem(string tag)
		{
			if (tag == "") {

				if (_activity.GetType ().FullName == "NoBadDays.Info") {
					return _activity.GetString (Resource.String.infoText);
				} else if (_activity.GetType ().FullName == "NoBadDays.Charity") {
					return _activity.GetString (Resource.String.charityText);
				} else if (_activity.GetType ().FullName == "NoBadDays.Community") {
					return _activity.GetString (Resource.String.communityText);
				} else if (_activity.GetType ().FullName == "NoBadDays.Win") {
					return  _activity.GetString (Resource.String.winText);
				} else if (_activity.GetType ().FullName == "NoBadDays.MapMainPage") {
					return _activity.GetString (Resource.String.mapText);
				} else if (_activity.GetType ().FullName == "NoBadDays.Savings") {
					return _activity.GetString (Resource.String.savingsText);
				} else if (_activity.GetType ().FullName == "NoBadDays.Offers") {
					return _activity.GetString (Resource.String.offersText);
				} else if (_activity.GetType ().FullName == "NoBadDays.ProfileMainPage") {
					return _activity.GetString (Resource.String.mynobaddaysText);
				}
			} else if (tag == "Info") {
				return _activity.GetString (Resource.String.infoText);
			}
			else if (tag == "Charity") {
				return _activity.GetString (Resource.String.charityText);
			}
			else if (tag == "Community") {
				return _activity.GetString (Resource.String.communityText);
			}
			else if (tag == "Win") {
				return  _activity.GetString (Resource.String.winText);
			}
			else if (tag == "Map") {
				return _activity.GetString (Resource.String.mapText);
			}
			else if (tag == "Savings") {
				return _activity.GetString (Resource.String.savingsText);
			}
			else if (tag == "Offers") {
				return _activity.GetString (Resource.String.offersText);
			}
			else if (tag == "Profile") {
				return _activity.GetString (Resource.String.mynobaddaysText);
			}

			return tag;
		}


        void _anchor_Click (object sender, EventArgs e) 
        {
          

             _tag = "";

            _param = new FrameLayout.LayoutParams(
               RelativeLayout.LayoutParams.MatchParent, RelativeLayout.LayoutParams.WrapContent);
       

            if (_visible)
            {
                _param.SetMargins(0, 0, 0, (int)(_marginMin));
                _rel.LayoutParameters = _param;
                
                _rel.StartAnimation(_wheelDownAnimation);

                if (_overlay != null)
                {
                    _overlay.StartAnimation(_alphaAnimationDown);
                }
        
              
            }
            else
            {
                _param.SetMargins(0, 0, 0, _marginMax);
                _rel.LayoutParameters = _param;
                
                _rel.StartAnimation(_wheelUpAnimation);

                if (_overlay != null)
                {
                    _overlay.StartAnimation(_alphaAnimationUp);
                }

                _circleMenu.Visibility = ViewStates.Visible;
				_selector.Visibility = ViewStates.Gone;//ViewStates.Visible;

		           

             }
		}

	

        public bool CloseActivity(Activity activity)
        {


            activity.Finish();
            
            var user = _viewmodel.GetUser();

            if (user == null)
            {
                activity.StartActivity(typeof(LoginPhoneNumber));
            }

            return true;

        }

      
      
        #region Circle layout interfaces
        public virtual void OnItemSelected(View view, string notused)
        {
			_tag = view.Tag.ToString ();

			_centerText.Visibility = ViewStates.Visible;
			_centerText.Text = GetMenuItem(_tag);
					     
            
        }

        public  void OnItemClick(View view, string notused)
        {
			var name = view.Tag.ToString();
	
			prepare_closeWheel (name);

        }


        void closewheel()
        {
            

    
            _param = new FrameLayout.LayoutParams(
                  RelativeLayout.LayoutParams.MatchParent, RelativeLayout.LayoutParams.WrapContent);
            _rel = _activity.FindViewById<RelativeLayout>(Resource.Id.rellayout);

            _param.SetMargins(0, 0, 0, (int)(_marginMin));
            _rel.LayoutParameters = _param;
            
            _rel.StartAnimation(_wheelDownAnimation);

        }


        public  void OnRotationFinished(View view, string notused)
        {
    
			Animation animation = new RotateAnimation(0, 360, view.Width / 2,view.Height / 2);
			animation.Duration = 250;
			view.StartAnimation(animation);
		

			prepare_closeWheel (_tag);


        }

		void prepare_closeWheel(string name)
		{
	
			var user = _viewmodel.GetUser();

			/*        if ((user == null)&&(name == _activity.GetString (Resource.String.savingsText)))
            {
                Android.Widget.Toast.MakeText(_activity, _activity.GetString(Resource.String.NotLoggedInText), ToastLength.Long).Show();
            }
            else */
			{

		
				_tag = name;

				closewheel();
			}

		}

        public  void OnCenterClick()
        {
            Toast.MakeText(_activity, "clicked", ToastLength.Long).Show();
        }

        #endregion

		#region ontouch

		public bool OnTouch(View v, MotionEvent e)
		{
			if (_overlay.Visibility == ViewStates.Visible) 
			{
				
				if (e.RawX< _screenHeight/2)
				{
				   _param.SetMargins(0, 0, 0, (int)(_marginMin));
				   _rel.LayoutParameters = _param;

					RaiseMenuDismissed(true);

				   _rel.StartAnimation(_wheelDownAnimation);

			    	if (_overlay != null)
				    {
					    _overlay.StartAnimation(_alphaAnimationDown);
				    }
			    }
				return true;
			}
			return false;
		}

		#endregion

		void RaiseMenuDismissed(bool state)
		{
			if (MenuDismissed != null)
			{
				MenuDismissed(this, state);
			}
		}

        #region IDisposable implementation

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        #endregion

     
    }

      
}

