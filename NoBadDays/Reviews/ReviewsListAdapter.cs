﻿using System;
using Android.Widget;
using Android.Views;
using System.Collections.Generic;

using CustomTextView = NoBadDays.helpers.CustomTextView;
using NoBadDays.helpers;
using Android.Graphics;
using NoBadDaysPCL.Items;
using System.Linq;


namespace NoBadDays
{
	public class ReviewsListAdapter: BaseAdapter, IListAdapter
	{

		List<ReviewItemTest> list = new List<ReviewItemTest>();

		public static ListView BackGroundListView = null;

		Reviews _activity;


		LayoutInflater _inflater;
			
		int count = 30;

		Bitmap tmpBitmap;

		public ReviewsListAdapter (Reviews activity)
		{

			_inflater = LayoutInflater.From(activity);


			_activity = activity;

			var amount = 10;

			#if TEST2

		
			for (int i = 0; i < count; i++) {
				list.Add (new ReviewItemTest ("rating commenthajkhajajasjkaj  jhacjhjbc  kjk<j<jkxkj<<x <x<<x", "username"+i.ToString()));
			}

		
			#endif


		}

		public override long GetItemId(int position)
		{

			return 0;
		}

		public override Java.Lang.Object GetItem(int position)
		{
			return null; // could wrap a Contact in a Java.Lang.Object to return it here if needed
		}

		#if TEST2
		public ReviewItemTest this[int position]
		#else
		public RatingItem this[int position]
		#endif 
		{
			get
			{
				#if TEST2
				return list [position];
				#else
				return _activity.shopViewModel.Items.ElementAt(position);
				#endif

			}  
		}


		public override int Count
		{
			get 
			{
				#if TEST2
				return list.Count;
				#else
				return _activity.shopViewModel.Items.Count;
				#endif

			}
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			ReviewViewHolder holder;

			if (convertView == null)
			{
				
				convertView = _inflater.Inflate(Resource.Layout.ReviewItem, parent, false);
			
				holder = new ReviewViewHolder();
				holder.userName = convertView.FindViewById<CustomTextView>(Resource.Id.username);
				holder.ratingImage = convertView.FindViewById<ImageView>(Resource.Id.ratingImage);
				holder.comment = convertView.FindViewById<CustomTextView>(Resource.Id.comment);

				convertView.SetTag(Resource.Layout.ReviewItem, holder);
			}
			else
			{
				holder = (ReviewViewHolder)convertView.GetTag(Resource.Layout.ReviewItem);
			}

			holder.position = position;


			_activity.RunOnUiThread(() =>
			{

					#if TEST2
					holder.comment.Text = list[position].comment;
					holder.userName.Text = list[position].username;
		
					#else

					var comment = _activity.shopViewModel.Items.ElementAt(position).Text;

					if ((comment != null)&&(comment != ""))
					{
				     	holder.comment.Text = comment;
					}

					var name = _activity.shopViewModel.Items.ElementAt(position).Name;

					if ((name != null)&&(name != ""))
					{
					    holder.userName.Text = name;
				    }

					var rating = _activity.shopViewModel.Items.ElementAt(position).Rating;

					if (rating == 1)
					{
					    holder.ratingImage.SetImageResource(Resource.Drawable.OneStar);
					}
					else if (rating == 2)
					{
					    holder.ratingImage.SetImageResource(Resource.Drawable.TwoStars);
					}
					else if (rating == 3)
					{
					    holder.ratingImage.SetImageResource(Resource.Drawable.ThreeStars);
					}
					else if (rating == 4)
					{
					    holder.ratingImage.SetImageResource(Resource.Drawable.FourStars);
					}
					else // (rating == 5)
					{
				    	holder.ratingImage.SetImageResource(Resource.Drawable.FiveStars);
					}

			        #endif


			});


			return convertView;
		}



		private ReviewViewHolder getViewHolder(View v)
		{
			if (v.GetTag(Resource.Layout.ReviewItem) == null)
			{
				return getViewHolder((View)v.Parent);
			}
			return (ReviewViewHolder)v.GetTag(Resource.Layout.ReviewItem);
		}
	}



	public class ReviewItemTest
	{
		public string comment;
		public string username;
	

		public ReviewItemTest(string comment, string username)
		{
			this.comment = comment;
			this.username = username;
		}
	}


}

