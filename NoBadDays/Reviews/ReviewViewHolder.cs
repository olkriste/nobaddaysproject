using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using CustomTextView = NoBadDays.helpers.CustomTextView; 

namespace NoBadDays
{
    class ReviewViewHolder : Java.Lang.Object
    {
		public CustomTextView userName { get; set; }
		public ImageView ratingImage { get; set; }
		public CustomTextView comment { get; set; }
	    public int position { get; set; }
    }
}