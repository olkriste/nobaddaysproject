﻿using System.Reflection;
using System.Runtime.CompilerServices;
using Android.App;

// Information about this assembly is defined by the following attributes.
// Change them to the values specific to your project.

[assembly: AssemblyTitle ("NoBadDays")]
[assembly: AssemblyDescription ("")]
[assembly: AssemblyConfiguration ("")]
[assembly: AssemblyCompany ("")]
[assembly: AssemblyProduct ("")]
[assembly: AssemblyCopyright ("olefa_000")]
[assembly: AssemblyTrademark ("")]
[assembly: AssemblyCulture ("")]

// The assembly version has the format "{Major}.{Minor}.{Build}.{Revision}".
// The form "{Major}.{Minor}.*" will automatically update the build and revision,
// and "{Major}.{Minor}.{Build}.*" will update just the revision.

[assembly: AssemblyVersion ("1.0.0")]


#if RELEASE 

[assembly: Application(Label = "@string/app_name", Icon = "@drawable/NoBadDaysLogoSmall", Debuggable = false)]
[assembly: MetaData ("com.google.android.maps.v2.API_KEY", Value="AIzaSyAiu6aSDVj4izRBHyKNqkakQFiUZfkIFOs")]
#else

[assembly: Application(Label = "", Theme = "@style/NoBadDaysTheme",Icon = "@drawable/NoBadDaysLogoSmall", Debuggable = true)]
[assembly: MetaData ("com.google.android.maps.v2.API_KEY", Value="AIzaSyDY3B4VnfRtKPMBEYQisNYJ_gSlq_7PoJs")]
#endif 



// if desired. See the Mono documentation for more information about signing.

//[assembly: AssemblyDelaySign(false)]
//[assembly: AssemblyKeyFile("")]

