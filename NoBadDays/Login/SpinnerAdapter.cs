﻿using System;
using Android.App;
using Android.Widget;
using Android.Views;
using NoBadDays;
using Android.Graphics;
using System.Threading;
using Android.Util;
using NoBadDaysPCL.ViewModel;

namespace NoBadDays
{
    public class SpinnerAdapter: BaseAdapter, ISpinnerAdapter
    {
       
        Activity _activity;
        LoginViewModel _viewModel;

		PopupWindow _popup;

  
        public SpinnerAdapter (Activity activity, LoginViewModel viewModel)
        {
            _activity = activity;
			_popup = null;
            _viewModel = viewModel;
    
        }

	

        public override Java.Lang.Object GetItem(int position)
        {
            return null;
        }

        public override long GetItemId(int id)
        {
            return id;
        }

        public override int Count
        {
            get 
            {

                return _viewModel.Items.Count;

            }
        }

        public override View GetDropDownView(int position, View convertView, ViewGroup parent)
        {
            return GetCustomView(position, convertView, parent, true);
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            return GetCustomView(position, convertView, parent, false);
        }

        private View GetCustomView(int position, View convertView, ViewGroup parent, bool dropdown)
        {
            var item = _viewModel.Items [position];
				
			var inflater = LayoutInflater.From (_activity);

				
            var view = inflater.Inflate (Resource.Layout.CustomSpinnerItem, parent, false);
        
            var prefix = view.FindViewById<TextView>(Resource.Id.tvLanguage);
            prefix.Text = item.Native;

            var flag = view.FindViewById<ImageView> (Resource.Id.imgLanguage);
                       

            byte[] decodedString = Base64.Decode(item.Base64Image, Android.Util.Base64Flags.Default);
            var bmp = BitmapFactory.DecodeByteArray(decodedString, 0, decodedString.Length);
            var scaled = Android.Graphics.Bitmap.CreateScaledBitmap(bmp, item.ImageWidth*2, item.ImageHeight*2, true);

            if ((flag != null)&&(scaled!=null))
            {
                flag.SetImageBitmap(scaled);
            }
   

            return view;
        }

       
       
    }
}

