﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using NoBadDays.helpers;
using Android.Graphics;
using Android.Views.InputMethods;
using System.Timers;

using NoBadDaysPCL;
using NoBadDaysPCL.ViewModel;
using SQLite.Net.Platform.XamarinAndroid;
using NoBadDaysPCL.Items;

namespace NoBadDays
{
    [Activity (Label = "",  Theme = "@style/NoBadDaysTheme")]            
    public class LoginPhoneNumber : Activity
    {

        private float _density;
        int _screenHeight;
        int _screenWidth;


        LoginViewModel _viewModel { get; set; }
  

		Timer timer = new Timer(Constants.ANIMTIME);

        Spinner _spinner;
        EditText _phoneText;
        TextView _prefix;

        int _max = -1;
        int _min = -1;
        int _index = -1;

        private List<string> _flags = new List<string>();
        private List<string> _prefixes = new List<string>();

        string _phoneNumber = "";

        ProgressBar _progressSpinner;


        ImageView _menuButton;

		bool _menu = false;

        
        protected override void OnCreate (Bundle bundle)
        {
            base.OnCreate (bundle);

            // Create your application here

            RequestWindowFeature(WindowFeatures.IndeterminateProgress);


            try
            {

                _phoneNumber = Intent.GetStringExtra("phonenumber");

            }
            catch
            {

                _phoneNumber = "";
            }

            SetContentView (Resource.Layout.LoginPhoneNumber);

			_menu = false;

		
            ActionBar actionBar = ActionBar;
            actionBar.SetDisplayShowHomeEnabled(false);

            View actionBarView = LayoutInflater.Inflate(Resource.Layout.actionbarLayout, null);
            actionBar.SetCustomView(actionBarView, new Android.App.ActionBar.LayoutParams(ActionBar.LayoutParams.WrapContent, ActionBar.LayoutParams.WrapContent,
                GravityFlags.Center | GravityFlags.Left));
            actionBar.SetDisplayOptions(ActionBarDisplayOptions.ShowCustom, ActionBarDisplayOptions.ShowCustom);

            actionBarView.FindViewById<CustomTextView>(Resource.Id.actionbarTitle).Text = GetString(Resource.String.loginText);

            _menuButton = actionBarView.FindViewById<ImageView>(Resource.Id.wheelBut);
			_menuButton.Click+= (sender, e) => 
			{
				if (!_menu)
				{
			    	InputMethodManager imm = (InputMethodManager)this.GetSystemService (Context.InputMethodService);
				    imm.HideSoftInputFromWindow (_menuButton.WindowToken, 0);
				    _menu = true;
				}
				else
				{
					_menu = false;
					timer.Elapsed += timer_Elapsed;
				    timer.Enabled = true;
				    timer.Start();
				}

		    };

            RequestedOrientation = Android.Content.PM.ScreenOrientation.Portrait;

            Display d = WindowManager.DefaultDisplay;

            Android.Util.DisplayMetrics m = new Android.Util.DisplayMetrics();
            d.GetMetrics(m);

            _density = m.Density;

            Point size = new Point();
            d.GetSize(size);
            _screenWidth = size.X;
            _screenHeight = size.Y;

            string libraryPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            var path = System.IO.Path.Combine(libraryPath, Constants.SQLitePath);

            _viewModel = new LoginViewModel (new SQLitePlatformAndroid(), path);

            _viewModel.PhoneDataSuccessful += _viewModel_PhoneDataSuccessful;
            _viewModel.PhoneDataErrorOccurred += _viewModel_PhoneDataErrorOccurred;
            _viewModel.PhoneDataSuccessfulWithIndex += _viewModel_PhoneDataSuccessfulWithIndex;
            _viewModel.PhoneDataErrorOccurred += _viewModelVerify_PhoneDataErrorOccurred;
            _viewModel.RequestSuccessful+= _viewModelVerify_RequestSuccessful;
            _viewModel.RequestNotSuccessful+= _viewModelVerify_RequestNotSuccessful;


            RequestedOrientation = Android.Content.PM.ScreenOrientation.Portrait;

            _spinner = FindViewById<Spinner>(Resource.Id.countrySelection);
            _spinner.ItemSelected += _spinner_ItemSelected;

            _phoneText = FindViewById<EditText>(Resource.Id.phoneNumber);
           
        //    _phoneText.Enabled = false;

            _prefix = FindViewById<TextView>(Resource.Id.prefix);

            _phoneText.EditorAction += _phoneText_EditorAction;

            _progressSpinner = FindViewById<ProgressBar>(Resource.Id.ProgressSpinner);



        }


        protected override void OnResume()
        {
            base.OnResume();

			View decorView = Window.DecorView;
			// Hide the status bar.
			int uiOptions = (int)View.SystemUiFlagFullscreen;
			decorView.SystemUiVisibility = (StatusBarVisibility)uiOptions;

            var _skinOverlay = FindViewById<View>(Resource.Id.skinOverlay);

            WheelMenu wheel = new WheelMenu(this, 0, _density, _menuButton, false, _skinOverlay);

			wheel.MenuDismissed+= (sender, e) => 
			{
				if(e)
				{
					_menu = false;
					timer.Elapsed += timer_Elapsed;
					timer.Enabled = true;
					timer.Start();
				}
			};

        }

        void _phoneText_EditorAction(object sender, TextView.EditorActionEventArgs e)
        {
            if ((e.ActionId == ImeAction.Done)&&(_viewModel.Items.Count > 0)) {

                if ((_phoneText.Text.Length <= _max) && (_phoneText.Text.Length >=_min)) {

                    UserItem user = new UserItem ();
                    user.Prefix =  _viewModel.Items [_index].PhonePrefix;
                    user.Region= _viewModel.Items [_index].Region;
                    user.Culture = System.Globalization.CultureInfo.CurrentUICulture.Name;
                    user.Phone = _phoneText.Text;
           //         user.=  Android.Provider.Settings.Secure.GetString(ApplicationContext.ContentResolver, Android.Provider.Settings.Secure.AndroidId);

         
                    SetProgressBarIndeterminateVisibility(true);

                    _viewModel.Request(user);

                }
                else 

                    Android.Widget.Toast.MakeText (this, GetString (Resource.String.PhoneNumberErrorText), ToastLength.Long).Show ();

            }
            else 

                Android.Widget.Toast.MakeText (this, GetString (Resource.String.DataNotReadyText), ToastLength.Long).Show ();
            

        }

        void _viewModelVerify_RequestNotSuccessful (object sender, ServerOutputItem e)
        {
            RunOnUiThread(() =>
            {
                SetProgressBarIndeterminateVisibility(false);
            });

			if (e.Result == MainClass.eServerResult.UserNotFound) {

				Android.Widget.Toast.MakeText (this, _viewModel.Items [_index].PhonePrefix+_phoneText.Text + " " + GetString (Resource.String.ProfileRequestText), ToastLength.Long).Show ();
				Android.Widget.Toast.MakeText (this, _viewModel.Items [_index].PhonePrefix+_phoneText.Text + " " + GetString (Resource.String.ProfileRequestText), ToastLength.Long).Show ();
				StartActivity(typeof(ProfileMainPage));
				OverridePendingTransition(Resource.Animation.slide_in_right, Resource.Animation.slide_out_left);
				Finish();
		
			} else {
				Android.Widget.Toast.MakeText (this, GetString (Resource.String.ServerErrorText), ToastLength.Long).Show ();
			}
        }

        void _viewModelVerify_RequestSuccessful (object sender, EventArgs e)
        {
            RunOnUiThread(() =>
                {
                    SetProgressBarIndeterminateVisibility(false);
                });

            Container.userItem.Phone = _phoneText.Text;
            Container.userItem.Prefix = _viewModel.Items [_index].PhonePrefix;
            Container.userItem.Region = _viewModel.Items [_index].Region;

            var logincode = new Intent(this, typeof(LoginCode));
            logincode.PutExtra("phonenumber", Container.userItem.Phone);
			logincode.PutExtra("context", "Login");
  
            StartActivity(logincode);

            Finish();
        }

        void _viewModelVerify_PhoneDataErrorOccurred (object sender, string e)
        {
            RunOnUiThread(() =>
                {
                    SetProgressBarIndeterminateVisibility(false);
                });

            Android.Widget.Toast.MakeText(this, GetString(Resource.String.ServerErrorText), ToastLength.Long).Show();
        }



        void _spinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            _index = e.Position;
            _max = _viewModel.Items[_index].MaxLength;
            _min = _viewModel.Items[_index].MinLength;

            _prefix.Text = _viewModel.Items[_index].PhonePrefix;
        }

        protected override void OnStart()
        {
            base.OnStart();

            subscribeData();

        }

        void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            timer.Enabled = false;
            timer.Stop();

            RunOnUiThread(() =>
            {
        
				var scroll = FindViewById<ScrollView>(Resource.Id.scroll);
				scroll.FullScroll(FocusSearchDirection.Down);

                InputMethodManager imm = (InputMethodManager)GetSystemService(Context.InputMethodService);
                imm.ShowSoftInput(_phoneText,  ShowFlags.Implicit);

			
                _phoneText.Enabled = true;
            });


        }

        #region fetch supported phones

        void subscribeData()
        {
            
            fetchSupportedPhones();


            timer.Elapsed += timer_Elapsed;
            timer.Enabled = true;
            timer.Start();


            _progressSpinner.Visibility = ViewStates.Visible;

			SetProgressBarIndeterminateVisibility(true);


        }

        void _viewModel_PhoneDataSuccessfulWithIndex(object sender, int e)
        {
            _spinner.SetSelection(e);

            _spinner.Adapter = new SpinnerAdapter(this, _viewModel);

            _progressSpinner.Visibility = ViewStates.Invisible;

			SetProgressBarIndeterminateVisibility(false);


        }

        void fetchSupportedPhones()
        {
            _viewModel.GetSupportedPhoneData(System.Globalization.CultureInfo.CurrentUICulture.Name);
        }

        void _viewModel_PhoneDataErrorOccurred(object sender, string e)
        {
			Android.Widget.Toast.MakeText(this, GetString(Resource.String.OffLineText), ToastLength.Long).Show();

            _progressSpinner.Visibility = ViewStates.Invisible;

			SetProgressBarIndeterminateVisibility(false);

        }

        void _viewModel_PhoneDataSuccessful(object sender, EventArgs e)
        {

            _spinner.Adapter = new SpinnerAdapter(this, _viewModel);

            _progressSpinner.Visibility = ViewStates.Invisible;

			SetProgressBarIndeterminateVisibility(false);

           
        }

        #endregion



    }
}

