﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Timers;
using Android.Animation;
using NoBadDays.helpers;
using Android.Graphics;
using NoBadDaysPCL;
using Android.Views.InputMethods;
using SQLite.Net.Platform.XamarinAndroid;


using Android.Views.Animations;
using NoBadDaysPCL.ViewModel;
using NoBadDaysPCL.Items;
using Gcm.Client;

namespace NoBadDays
{
    [Activity (Label = "", Theme = "@style/NoBadDaysTheme")]
    public class LoginCode : Activity, View.IOnKeyListener, Android.Animation.Animator.IAnimatorListener
    {

        LoginViewModel _viewModel {get; set;}

		UserViewModel _userViewModel {get; set;}

        private float _density;
        int _screenHeight;
        int _screenWidth;

		Timer timer = new Timer(Constants.ANIMTIME);

        EditText _codeOneText;
        EditText _codeTwoText;
        EditText _codeThreeText;
        EditText _codeFourText;

        bool _codeOne = false;
        bool _codeTwo = false;
        bool _codeThree = false;
        bool _codeFour = false;

        bool _codeOneFocus = false;
        bool _codeTwoFocus = false;
        bool _codeThreeFocus = false;
        bool _codeFourFocus = false;

        ObjectAnimator animation;
        int _anim = 0;

        string _finalCode;

        ProgressBar _progressSpinner;

        string _phoneNumber;
        string _prefix;
        string _region;

		string _context;

        
        protected override void OnCreate (Bundle bundle)
        {
            base.OnCreate (bundle);

            // Create your application here

            SetContentView (Resource.Layout.LoginCode);

            ActionBar actionBar = ActionBar;
            actionBar.SetDisplayShowHomeEnabled(false);
            View actionBarView = LayoutInflater.Inflate(Resource.Layout.actionbarLoginLayout, null);
            actionBar.SetCustomView(actionBarView, new Android.App.ActionBar.LayoutParams(ActionBar.LayoutParams.WrapContent, ActionBar.LayoutParams.WrapContent,
                GravityFlags.Center));
            actionBar.SetDisplayOptions(ActionBarDisplayOptions.ShowCustom, ActionBarDisplayOptions.ShowCustom);
			actionBar.Hide ();

            RequestedOrientation = Android.Content.PM.ScreenOrientation.Portrait;

            Display d = WindowManager.DefaultDisplay;

            Android.Util.DisplayMetrics m = new Android.Util.DisplayMetrics();
            d.GetMetrics(m);

            _density = m.Density;

            Point size = new Point();
            d.GetSize(size);
            _screenWidth = size.X;
            _screenHeight = size.Y;


			Bundle extras = Intent.Extras;

		
            try
            {

				_context = extras.GetString("context");

                _prefix = Container.userItem.Prefix;
                _phoneNumber = Container.userItem.Phone;

                Title = "("+_prefix+")"+_phoneNumber;

                _progressSpinner = FindViewById<ProgressBar>(Resource.Id.ProgressSpinner);

                _codeOneText = FindViewById<EditText>(Resource.Id.codeOneText);
                _codeTwoText = FindViewById<EditText>(Resource.Id.codeTwoText);
                _codeThreeText = FindViewById<EditText>(Resource.Id.codeThreeText);
                _codeFourText = FindViewById<EditText>(Resource.Id.codeFourText);


                _codeOneText.AfterTextChanged += codeOneText_AfterTextChanged;
                _codeTwoText.AfterTextChanged += codeTwoText_AfterTextChanged;
                _codeThreeText.AfterTextChanged += codeThreeText_AfterTextChanged;
                _codeFourText.AfterTextChanged += codeFourText_AfterTextChanged;

      /*          _codeOneText.SetOnKeyListener(this);
                _codeTwoText.SetOnKeyListener(this);
                _codeThreeText.SetOnKeyListener(this);
                _codeFourText.SetOnKeyListener(this);
*/

                _codeOneText.FocusChange += codeOneText_FocusChange;
                _codeTwoText.FocusChange += codeTwoText_FocusChange;
                _codeThreeText.FocusChange += codeThreeText_FocusChange;
                _codeFourText.FocusChange += codeFourText_FocusChange;

              
                _codeOneText.RequestFocus();
                InputMethodManager imm = (InputMethodManager)GetSystemService(Context.InputMethodService);
                imm.ShowSoftInput(_codeOneText,  ShowFlags.Implicit);

              

                string libraryPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                var path = System.IO.Path.Combine(libraryPath, Constants.SQLitePath);


				if (_context == "Login")
				{
                   _viewModel = new LoginViewModel (new SQLitePlatformAndroid(), path);

                   _viewModel.ConfirmNotSuccessful += _viewModel_ConfirmNotSuccessfull;
                   _viewModel.ConfirmSuccessful += _viewModel_ConfirmSuccessful;
				}
				else
				{
					_userViewModel = new UserViewModel (new SQLitePlatformAndroid(), path);

				}

               
                _anim = 0;


            }
            catch 
            {

            }

        }




        protected override void OnStart()
        {
            base.OnStart();

            timer.Elapsed += timer_Elapsed;
            timer.Enabled = true;
            timer.Start();

        }

        void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            timer.Enabled = false;
            timer.Stop();

            RunOnUiThread(() =>
            {
                InputMethodManager imm = (InputMethodManager)GetSystemService(Context.InputMethodService);
                imm.ShowSoftInput(_codeOneText, ShowFlags.Implicit);
            });


        }




        void _viewModel_ConfirmNotSuccessfull (object sender, ServerOutputItem e)
        {
            _progressSpinner.Visibility = ViewStates.Gone;

            Android.Widget.Toast.MakeText(this, GetString(Resource.String.WrongCodeText), ToastLength.Long).Show();

            _anim = 0;
        }

        void _viewModel_ConfirmSuccessful (object sender, EventArgs e)
        {
            _progressSpinner.Visibility = ViewStates.Gone;

       
            string libraryPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            var path = System.IO.Path.Combine(libraryPath, Constants.SQLitePath);
            var _viewmodel = new UserViewModel (new SQLitePlatformAndroid(), path);

            var user = _viewmodel.GetUser ();

            if (user != null) {

                Container.userItem.Phone = user.Phone;
           //     Container.UserItem.PhoneBarCodeBase64 = user.PhoneBarCodeBase64;
           //     Container.UserItem.PhoneBarCodeHeight = user.PhoneBarCodeHeight;
           //     Container.UserItem.PhoneBarCodeWidth = user.PhoneBarCodeWidth;
                Container.userItem.Prefix = user.Prefix;
                Container.userItem.Region = user.Region;
                Container.userItem.UserId = user.UserId;
                Container.userItem.Name = user.Name;
    //            Container.userItem.Token = user.Token;
                      
               
				#if !SIMULATOR
				GcmClient.CheckDevice(this);
				GcmClient.CheckManifest(this);

				if (!Container.registered)
				{
					GcmClient.Register(this, Constants.SenderID);
				}
				#endif








     			StartActivity(typeof(ProfileMainPage));

                Finish ();
            } else
                Finish ();

         
     
        }



		void codeFourText_FocusChange(object sender, View.FocusChangeEventArgs e)
		{
			_codeOneFocus = false;
			_codeTwoFocus = false;
			_codeThreeFocus = false;
			_codeFourFocus = true;
			if (_codeFour)
			{
				_codeFourText.SetBackgroundResource(Resource.Drawable.BorderGrayRound);
			}
			else
			{
				_codeFourText.SetBackgroundResource(Resource.Drawable.BorderRoundWhite);
			}
		}

		void codeThreeText_FocusChange(object sender, View.FocusChangeEventArgs e)
		{
			_codeOneFocus = false;
			_codeTwoFocus = false;
			_codeThreeFocus = true;
			_codeFourFocus = false;
			if (_codeThree)
			{
				_codeThreeText.SetBackgroundResource(Resource.Drawable.BorderGrayRound);
			}
			else
			{
				_codeThreeText.SetBackgroundResource(Resource.Drawable.BorderRoundWhite);
			}
		}

		void codeTwoText_FocusChange(object sender, View.FocusChangeEventArgs e)
		{
			_codeOneFocus = false;
			_codeTwoFocus = true;
			_codeThreeFocus = false;
			_codeFourFocus = false;
			if (_codeTwo)
			{
				_codeTwoText.SetBackgroundResource(Resource.Drawable.BorderGrayRound);
			}
			else
			{
				_codeTwoText.SetBackgroundResource(Resource.Drawable.BorderRoundWhite);
			}
		}

		void codeOneText_FocusChange(object sender, View.FocusChangeEventArgs e)
		{
			_codeOneFocus = true;
			_codeTwoFocus = false;
			_codeThreeFocus = false;
			_codeFourFocus = false;
			if (_codeOne)
			{
				_codeOneText.SetBackgroundResource(Resource.Drawable.BorderGrayRound);
			}
			else
			{
				_codeOneText.SetBackgroundResource(Resource.Drawable.BorderRoundWhite);
			}
		}
        void codeFourText_AfterTextChanged(object sender, Android.Text.AfterTextChangedEventArgs e)
        {
            if (_codeFourText.Text != "")
            {
                _codeFour = true;
                _codeFourText.SetBackgroundResource(Resource.Drawable.BorderGrayRound);
            }
            else
            {
                _codeFour = false;
				_codeFourText.SetBackgroundResource(Resource.Drawable.BorderRoundWhite);
            }

            if (_codeFourText.Text != "")
            {
                if (!_codeTwo)
                {
                    _codeTwoText.RequestFocus();
                }
                else if (!_codeThree)
                {
                    _codeThreeText.RequestFocus();
                }
                else if (!_codeOne)
                {
                    _codeOneText.RequestFocus();
                }

            }

            checkCodeCompleted((View)sender);
        }

        void codeThreeText_AfterTextChanged(object sender, Android.Text.AfterTextChangedEventArgs e)
        {
            if (_codeThreeText.Text != "")
            {
                _codeThree = true;
                _codeThreeText.SetBackgroundResource(Resource.Drawable.BorderGrayRound);
            }
            else
            {
                _codeThree = false;
				_codeThreeText.SetBackgroundResource(Resource.Drawable.BorderRoundWhite);
            }
            if (_codeThreeText.Text != "")
            {

                if (!_codeTwo)
                {
                    _codeTwoText.RequestFocus();
                }
                else if (!_codeOne)
                {
                    _codeOneText.RequestFocus();
                }
                else if (!_codeFour)
                {
                    _codeFourText.RequestFocus();
                }

            }

            checkCodeCompleted((View)sender);
        }

        void codeTwoText_AfterTextChanged(object sender, Android.Text.AfterTextChangedEventArgs e)
        {
            if (_codeTwoText.Text != "")
            {
                _codeTwo = true;
                _codeTwoText.SetBackgroundResource(Resource.Drawable.BorderGrayRound);
            }
            else
            {
                _codeTwo = false;
				_codeFourText.SetBackgroundResource(Resource.Drawable.BorderRoundWhite);
            }

            if (_codeTwoText.Text != "")
            {
                if (!_codeOne)
                {
                    _codeOneText.RequestFocus();
                }
                else if (!_codeThree)
                {
                    _codeThreeText.RequestFocus();
                }
                else if (!_codeFour)
                {
                    _codeFourText.RequestFocus();
                }

            }

            checkCodeCompleted((View)sender);
        }

        void codeOneText_AfterTextChanged(object sender, Android.Text.AfterTextChangedEventArgs e)
        {
            if (_codeOneText.Text != "")
            {
                _codeOne = true;
                _codeOneText.SetBackgroundResource(Resource.Drawable.BorderGrayRound);
            }
            else
            {
                _codeOne = false;
				_codeOneText.SetBackgroundResource(Resource.Drawable.BorderRoundWhite);
            }

            if (_codeOneText.Text != "")
            {
                if (!_codeTwo)
                {
                    _codeTwoText.RequestFocus();
                }
                else if (!_codeThree)
                {
                    _codeThreeText.RequestFocus();
                }
                else if (!_codeFour)
                {
                    _codeFourText.RequestFocus();
                }

            }

            checkCodeCompleted((View)sender);
        }


        void checkCodeCompleted(View view)
        {
            if (_codeOne & _codeTwo & _codeThree & _codeFour)
            {
               
                verifyCode();

            }
        
        }

        void verifyCode()
        {

            if (_codeOne & _codeTwo & _codeThree & _codeFour)
            {
                _finalCode = _codeOneText.Text + _codeTwoText.Text + _codeThreeText.Text + _codeFourText.Text;


                animation = ObjectAnimator.OfFloat(_codeOneText, "rotationY", 180f, 0.0f);
                SetAnimator(animation);
                _anim = 1;
            }
            else
            {

            }

        }

        void SetAnimator(ObjectAnimator anim)
        {
            anim.SetDuration(Constants.FLIPDURATION);
            anim.RepeatCount = 0;
            anim.AddListener(this);
            anim.SetInterpolator(new AccelerateDecelerateInterpolator());
            anim.Start(); 
        }

        public void OnAnimationStart(Animator animation)
        {

        }

        public void OnAnimationRepeat(Animator animation)
        {


        }

        public void OnAnimationCancel(Animator animation)
        {
            _anim = 0;
        }

        public void OnAnimationEnd(Animator animation)
        {
            if (_anim == 1)
            {
                _codeOneText.SetTextColor(Android.Graphics.Color.Gray);
                _codeOneText.SetBackgroundResource(Resource.Drawable.BorderGrayRound);
                animation = ObjectAnimator.OfFloat(_codeTwoText, "rotationY", 180f, 0.0f);
                SetAnimator((ObjectAnimator)animation);
                _anim = 2;
            }
            else if (_anim == 2)
            {
                _codeTwoText.SetTextColor(Android.Graphics.Color.Gray);
                _codeTwoText.SetBackgroundResource(Resource.Drawable.BorderGrayRound);
                animation = ObjectAnimator.OfFloat(_codeThreeText, "rotationY", 180f, 0.0f);
                SetAnimator((ObjectAnimator)animation);
                _anim = 3;
            }
            else if (_anim == 3)
            {
                _codeThreeText.SetTextColor(Android.Graphics.Color.Gray);
                _codeThreeText.SetBackgroundResource(Resource.Drawable.BorderGrayRound);
                animation = ObjectAnimator.OfFloat(_codeThreeText, "rotationY", 180f, 0.0f);
                SetAnimator((ObjectAnimator)animation);
                _anim = 4;
            }
            else
            {
                _codeFourText.SetTextColor(Android.Graphics.Color.Gray);
                _codeFourText.SetBackgroundResource(Resource.Drawable.BorderGrayRound);


                UserItem user = new UserItem();
                user.Prefix = Container.userItem.Prefix;
                user.Region = Container.userItem.Region;
                user.Culture = System.Globalization.CultureInfo.CurrentUICulture.Name;
                user.Phone = Container.userItem.Phone;

                _progressSpinner.Visibility = ViewStates.Visible;

				if (_context == "Login") {
					_viewModel.Confirm (user, _finalCode);   
				}
				else 				
				{

				 _userViewModel.PhoneDataSuccessful += _userViewModel_PhoneDataSuccessful;
			     _userViewModel.PhoneDataErrorOccurred += _userViewModel_PhoneDataErrorOccurred;

					user.Name = Container.userItem.Name;
					user.Bio = Container.userItem.Bio;
					user.Email = Container.userItem.Email;
					user.Phone = Container.userItem.Phone;
					user.Code = _finalCode;
					user.Guid = Container.userItem.Guid;
					user.UserId = Container.userItem.UserId;
					user.Token = Container.userItem.Token;
					user.Region = Container.userItem.Region;
					user.Prefix = Container.userItem.Prefix;

					_userViewModel.UpdateUser (user);
				}
				    
                InputMethodManager imm = (InputMethodManager)GetSystemService(Context.InputMethodService);
                imm.HideSoftInputFromWindow(_codeOneText.WindowToken, HideSoftInputFlags.None);

            }

        }

        void _userViewModel_PhoneDataErrorOccurred (object sender, ServerOutputItem e)
        {
			Android.Widget.Toast.MakeText(this, e.ResultString, ToastLength.Long).Show();
			_progressSpinner.Visibility = ViewStates.Invisible;

        }

        void _userViewModel_PhoneDataSuccessful (object sender, ServerOutputItem e)
        {
			Finish ();
        }


        public override bool OnKeyDown(Keycode keyCode, KeyEvent e)
        {

            if (keyCode == Keycode.Back)
            {
      //          handleFinish();

                Finish();
            }
            else if (keyCode == Keycode.Del)
            {
				changefocus ();

            }

            return base.OnKeyDown(keyCode, e);
        }


        public bool OnKey(View view, Android.Views.Keycode keyCode, KeyEvent Event)
        {
			if (Event.KeyCode == Keycode.Back) {
				//       handleFinish();

				Finish ();
				return true;
			} 
            else return false;
        }

		private void changefocus()
		{
			if (_codeFourFocus)
			{
				_codeThreeText.RequestFocus();
			}
			else if (_codeThreeFocus)
			{
				_codeTwoText.RequestFocus();
			}
			else if (_codeTwoFocus)
			{
				_codeOneText.RequestFocus();
			}
		}


        void _loginButton_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(Savings));
        }
    }
}

