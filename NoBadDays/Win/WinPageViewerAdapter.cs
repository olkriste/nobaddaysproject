using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;


namespace NoBadDays
{
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;




    namespace ViewPagerIndicator
    {
        public class WinPageViewerAdapter : Android.Support.V4.App.FragmentPagerAdapter
        {

            int _count;
            Win _context;

			public WinPageViewerAdapter(Android.Support.V4.App.FragmentManager fm, Win context)
                : base(fm)
            {
                _context = context;

                _count = 3;
            }


            public override Android.Support.V4.App.Fragment GetItem(int position)
            {
                return new WinSubPages(position, _context);
            }

            public override int Count
            {
                get
                {
                    return _count;
                }
            }

            public void SetCount(int Count)
            {
                if (Count > 0 && Count <= 10)
                {
                    _count = Count;
                    NotifyDataSetChanged();
                }
            }
        }
    }
}