﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using NoBadDays.helpers;
using NoBadDays.ViewPagerIndicator;
using Android.Support.V4.View;
using DK.Ostebaronen.Droid.ViewPagerIndicator;

using Android.Support.V4.App;

using CustomTextView = NoBadDays.helpers.CustomTextView;
using NoBadDaysPCL.ViewModel;
using SQLite.Net.Platform.XamarinAndroid;
using NoBadDaysPCL.Items; 

namespace NoBadDays
{
    [Activity (Label = "", Theme = "@style/NoBadDaysTheme")]            
	public class Win : FragmentActivity, Android.Support.V4.View.ViewPager.IOnPageChangeListener
    {

		public event EventHandler<int> DataReady0;
		public event EventHandler<int> DataReady1;
		public event EventHandler<int> DataReady2;

		public WinViewModel winViewModel {get; set;}


		private float _density;
        int _screenHeight;
        int _screenWidth;


        ImageView _menuButton;

        WheelMenu _wheelMenu;



		WinPageViewerAdapter _adapter;
		ViewPager _pager;
		IPageIndicator _indicator;

		int _page = 1;
		int _selectedPage;


        protected override void OnCreate (Bundle bundle)
        {
            base.OnCreate (bundle);

            // Create your application here

	//		Window.RequestFeature (WindowFeatures.ContentTransitions);

			RequestWindowFeature(WindowFeatures.IndeterminateProgress);
			          
            SetContentView (Resource.Layout.WinPageViewerIndicator);


			int version = Int32.Parse(Android.OS.Build.VERSION.Sdk);

			if (version != 19)   //KitKat 4.4.2
			{
				View decorView = Window.DecorView;
				// Hide the status bar.
				int uiOptions = (int)View.SystemUiFlagFullscreen;
				decorView.SystemUiVisibility = (StatusBarVisibility)uiOptions;
			}

			ActionBar actionBar = ActionBar;
			actionBar.SetDisplayShowHomeEnabled(false);
			//displaying custom ActionBar
			View actionBarView = LayoutInflater.Inflate(Resource.Layout.actionbarLayout, null);
			actionBar.SetCustomView(actionBarView, new Android.App.ActionBar.LayoutParams(ActionBar.LayoutParams.WrapContent, ActionBar.LayoutParams.WrapContent,
				GravityFlags.Center|GravityFlags.Left));
			actionBar.SetDisplayOptions(ActionBarDisplayOptions.ShowCustom, ActionBarDisplayOptions.ShowCustom);

			_menuButton = actionBarView.FindViewById<ImageView>(Resource.Id.wheelBut);

			actionBarView.FindViewById<CustomTextView> (Resource.Id.actionbarTitle).Text = GetString (Resource.String.winText);

			_adapter = new WinPageViewerAdapter(SupportFragmentManager, this);
			_pager = FindViewById<ViewPager>(Resource.Id.pager);

			//        _pager.SetPagingEnabled(false);
	
			_page = 1;

		//	_pager.AddOnPageChangeListener(this);

			_pager.SetPageTransformer(true, new PageTransformer());

			_pager.Adapter = _adapter;

			_indicator = FindViewById<CirclePageIndicator>(Resource.Id.indicator);
			_indicator.SetViewPager(_pager);

			RequestedOrientation = Android.Content.PM.ScreenOrientation.Portrait;

			Display d = WindowManager.DefaultDisplay;

			Android.Util.DisplayMetrics m = new Android.Util.DisplayMetrics();
			d.GetMetrics(m);

			_density = m.Density;

			Point size = new Point();
			d.GetSize(size);
			_screenWidth = size.X;
			_screenHeight = size.Y;

			var _skinOverlay = FindViewById<View>(Resource.Id.skinOverlay);

			_wheelMenu = new WheelMenu (this, 0, _density, _menuButton, false, _skinOverlay);


			string libraryPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
			var path = System.IO.Path.Combine(libraryPath, Constants.SQLitePath);

			winViewModel = new WinViewModel (new SQLitePlatformAndroid(), path);


			winViewModel.PhoneDataErrorOccurred += winViewModel_PhoneDataErrorOccurred;
			winViewModel.PhoneDataSuccessful += winViewModel_PhoneDataSuccessful;


							
        }

		public void FetchData()
		{
			winViewModel.GetWin ();

			SetProgressBarIndeterminateVisibility(true);

		}



		void winViewModel_PhoneDataSuccessful (object sender, NoBadDaysPCL.Items.ServerOutputItem e)
		{

			_pager.AddOnPageChangeListener(this);

			SetProgressBarIndeterminateVisibility(false);

		
			RaiseDataReady ();


		}


		void winViewModel_PhoneDataErrorOccurred (object sender, NoBadDaysPCL.Items.ServerOutputItem e)
		{
			SetProgressBarIndeterminateVisibility(false);
			Android.Widget.Toast.MakeText(this, e.ResultString, ToastLength.Long).Show();
		}



		void RaiseDataReady()
		{
			if (DataReady0 != null)
			{
				DataReady0(this, 0);
			}
	
			if (DataReady1 != null)
			{
				DataReady1(this, 1);
			}

			if (DataReady2 != null)
			{
				DataReady2(this,2);
			}
		}

			

        protected override void OnResume()
        {
            base.OnResume ();

			_pager.CurrentItem = 1;
	           
        }

        public override bool OnKeyDown(Keycode keyCode, KeyEvent e)
        {

            if (keyCode == Keycode.Back)
            {
                if (keyCode == Keycode.Back)
                {
                    return _wheelMenu.CloseActivity(this);
                }
                return true;
            }

            return base.OnKeyDown(keyCode, e);
        }

        public override void OnLowMemory()
        {
            Console.WriteLine("Win:OnLowMemory");
            GC.Collect();
        }


		protected override void OnPause()
		{
			base.OnPause();

			GC.Collect (0);

		}


		#region Viewpager interfaces


		public void OnPageSelected(int pos)
		{
			_selectedPage = pos;

		}

		public void OnPageScrollStateChanged(int state)
		{

		}

		public void OnPageScrolled(int position, float positionOffset, int positionOffsetPixels)
		{

		}

		#endregion

		protected override void OnDestroy()
		{
			base.OnDestroy();

			if (_wheelMenu != null)_wheelMenu.Dispose ();

		}

    }
}

