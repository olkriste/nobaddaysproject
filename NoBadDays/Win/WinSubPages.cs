﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;

using Android.Views;
using Android.Widget;

using Fragment = Android.Support.V4.App.Fragment;
using Android.Views.Animations;
using System.Timers;
using NoBadDays.helpers;
using Android.Graphics;

using CustomTextView = NoBadDays.helpers.CustomTextView;
using NoBadDaysPCL.Items;

using Koush;


namespace NoBadDays
{
	public class WinSubPages : Fragment, Koush.IUrlImageViewCallback 
    {

       
		WinItem _winItem { get; set;}


        private float _density;
 
        int _page;
        View _view = null;
        Win _activity;

	

		// Previous month page
		ImageView _winImagePrev;
		CustomTextView _titlePrev;
		CustomTextView _descriptionPrev;
		ImageView _profilePrev;

		CustomTextView _namePrev;
		CustomTextView _locationPrev;


		// This month page
		ImageView _winImage;
		CustomTextView _title;
		CustomTextView _description;

		ImageView _profile1;
		ImageView _profile2;
		ImageView _profile3;

		CustomTextView _name1;
		CustomTextView _name2;
		CustomTextView _name3;
		CustomTextView _location1;
		CustomTextView _location2;
		CustomTextView _location3;
		CustomTextView _amount1;
		CustomTextView _amount2;
		CustomTextView _amount3;


		// Next month page
		ImageView _winImageNext;
		CustomTextView _titleNext;
		CustomTextView _descriptionNext;

		bool _fetched = false;
		bool _data0 = false;
		bool _data1 = false;
		bool _data2 = false;

	
        
		public WinSubPages() 
        {
           
        }


		public WinSubPages(int page, Win activity)
        {
            _page = page;
			_activity = activity;
				

        }
        
        public override void OnCreate (Bundle savedInstanceState)
        {
            base.OnCreate (savedInstanceState);

            // Create your fragment here

            Display d = this.Activity.WindowManager.DefaultDisplay;

            Android.Util.DisplayMetrics m = new Android.Util.DisplayMetrics();
            d.GetMetrics(m);

            _density = m.Density;

			_fetched = false;
			_data0 = false;
			_data1 = false;
			_data2 = false;

        }

        public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            
			Win parent = _activity as Win;

            if (_page == 0)
            {
                _view = inflater.Inflate(Resource.Layout.WinPageOne, null);

				_winImagePrev = _view.FindViewById<ImageView> (Resource.Id.WinImage);
				_titlePrev = _view.FindViewById<CustomTextView> (Resource.Id.title);
				_descriptionPrev = _view.FindViewById<CustomTextView> (Resource.Id.description);
				_profilePrev = _view.FindViewById<ImageView> (Resource.Id.ProfileImage);
				_namePrev = _view.FindViewById<CustomTextView> (Resource.Id.name);
				_locationPrev = _view.FindViewById<CustomTextView> (Resource.Id.location);

				if (!_data0) {
					_activity.DataReady0 += _activity_DataReady0;
				} else {
					loaddata0 ();
				}
					

            }
            else if (_page == 1)
            {
                _view = inflater.Inflate(Resource.Layout.WinPageTwo, null);

				_winImage = _view.FindViewById<ImageView> (Resource.Id.WinImage);
				_title = _view.FindViewById<CustomTextView> (Resource.Id.title);
				_description = _view.FindViewById<CustomTextView> (Resource.Id.description);


				_profile1 = _view.FindViewById<ImageView>(Resource.Id.profileImg1);
				_profile2 = _view.FindViewById<ImageView>(Resource.Id.profileImg2);
				_profile3 = _view.FindViewById<ImageView>(Resource.Id.profileImg3);
				_name1 = _view.FindViewById<CustomTextView>(Resource.Id.name1);
				_name2 = _view.FindViewById<CustomTextView>(Resource.Id.name2);
				_name3 = _view.FindViewById<CustomTextView>(Resource.Id.name3);
				_location1 = _view.FindViewById<CustomTextView>(Resource.Id.location1);
				_location2 = _view.FindViewById<CustomTextView>(Resource.Id.location2);
				_location3 = _view.FindViewById<CustomTextView>(Resource.Id.location3);
				_amount1 = _view.FindViewById<CustomTextView>(Resource.Id.amount1);
				_amount2 = _view.FindViewById<CustomTextView>(Resource.Id.amount2);
				_amount3 = _view.FindViewById<CustomTextView>(Resource.Id.amount3);

				if (!_data1) {
					_activity.DataReady1 += _activity_DataReady1;
				} else {
					loaddata1 ();
				}

            }
			else if (_page == 2)
			{
				_view = inflater.Inflate(Resource.Layout.WinPageThree, null);

				_winImageNext = _view.FindViewById<ImageView> (Resource.Id.WinImage);
				_titleNext = _view.FindViewById<CustomTextView> (Resource.Id.title);
				_descriptionNext = _view.FindViewById<CustomTextView> (Resource.Id.description);

				if (!_fetched) {
					_fetched = true;
					_activity.FetchData ();
				}

				if (!_data2) {
					_activity.DataReady2 += _activity_DataReady2;
				} else {
					loaddata2();
				}

			


			}
 
            else throw new System.SystemException("Unexpected error in viewpager");

		
            return _view;

        }

		void _activity_DataReady0 (object sender, int e)
		{
			_winItem =_activity.winViewModel.Item;
		
			if (e == 0)
			{
				_data0 = true;
				loaddata0 ();
			} 
	
		}

		void loaddata0()
		{
			_namePrev.Text = (_winItem.PreviousWinner.Name != null) ?_winItem.PreviousWinner.Name : "";
			_locationPrev.Text = (_winItem.PreviousWinner.Bio != null) ?_winItem.PreviousWinner.Bio : "";
			_titlePrev.Text = (_winItem.PreviousPrize.Title != null) ?_winItem.PreviousPrize.Title : "";
			_descriptionPrev.Text = (_winItem.PreviousPrize.Description != null) ?_winItem.PreviousPrize.Description : "";

			if (_profilePrev != null) {
				try {
					UrlImageViewHelper.SetUrlDrawable (_profilePrev, _winItem.PreviousWinner.Picture.Uri, Resource.Drawable.GhostCircle, 30000, this);
				} catch {
				}
			}

			if (_winImagePrev != null) {
				try {
					UrlImageViewHelper.SetUrlDrawable (_winImagePrev, _winItem.PreviousPrize.Picture.Uri, Resource.Drawable.GhostCircle, 30000, this);
				} catch {
				}
			}

		}



		void _activity_DataReady1 (object sender, int e)
		{
			_winItem =_activity.winViewModel.Item;

			if (e == 1) {

				_data1 = true;
				loaddata1 ();
			
			} 
		}

		void loaddata1()
		{
			_title.Text = (_winItem.Prize.Title != null) ?_winItem.Prize.Title : "";
			_description.Text = (_winItem.Prize.Description != null) ?_winItem.Prize.Description : "";

			if (_winImage != null) {
				try {
					UrlImageViewHelper.SetUrlDrawable (_winImage, _winItem.Prize.Picture.Uri, Resource.Drawable.GhostCircle, 30000, this);
				} catch {
				}
			}

			if (_winItem.Users.Count > 0) {

				_name1.Text = _winItem.Users.ElementAt (0).Name;
				_name2.Text = _winItem.Users.ElementAt (1).Name;
				_name3.Text = _winItem.Users.ElementAt (2).Name;
				_amount1.Text = _winItem.Users.ElementAt (0).Points;
				_amount2.Text = _winItem.Users.ElementAt (1).Points;
				_amount3.Text = _winItem.Users.ElementAt (2).Points;


				try {
					UrlImageViewHelper.SetUrlDrawable (_profile1, _winItem.Users.ElementAt (0).Picture.Uri, Resource.Drawable.GhostProfile, 30000, this);
					UrlImageViewHelper.SetUrlDrawable (_profile2, _winItem.Users.ElementAt (1).Picture.Uri, Resource.Drawable.GhostProfile, 30000, this);
					UrlImageViewHelper.SetUrlDrawable (_profile3, _winItem.Users.ElementAt (2).Picture.Uri, Resource.Drawable.GhostProfile, 30000, this);
				} catch {
				}
			}
		}

		void _activity_DataReady2 (object sender, int e)
		{
			_winItem =_activity.winViewModel.Item;

			if (e == 2) 
			{
				_data2 = true;
				loaddata2 ();


			}

		}


		void loaddata2()
		{
			_titleNext.Text = (_winItem.NextPrize.Title != null) ?_winItem.NextPrize.Title : "";
			_descriptionNext.Text = (_winItem.NextPrize.Description != null) ?_winItem.NextPrize.Description : "";


			if (_winImageNext != null) {
				try {
					UrlImageViewHelper.SetUrlDrawable (_winImageNext, _winItem.NextPrize.Picture.Uri, Resource.Drawable.GhostCircle, 30000, this);
				} catch {
				}
			}
		}

	
		public void OnLoaded(ImageView imageView, Bitmap loadedBitmap, string url, bool loadedFromCache)
		{
			_activity.RunOnUiThread(() =>
			{
			
					/*
				if (url ==  _winItem.Prize.Picture.Uri)
				{
			     	_winImage.SetImageDrawable(new CircleDrawable(loadedBitmap));
				}
				else if ((_winItem.Users.Count > 0)&&(url == _winItem.Users.ElementAt(0).Picture.Uri))
				{
					_profile1.SetImageDrawable(new CircleDrawable(loadedBitmap));
				}
				else if  ((_winItem.Users.Count > 0)&&(url == _winItem.Users.ElementAt(1).Picture.Uri))
				{
					_profile2.SetImageDrawable(new CircleDrawable(loadedBitmap));
				}
				else if  ((_winItem.Users.Count > 0)&&(url == _winItem.Users.ElementAt(2).Picture.Uri))
				{
					_profile3.SetImageDrawable(new CircleDrawable(loadedBitmap));
				}
				else if  (url ==_winItem.PreviousPrize.Picture.Uri)
				{
					_winImagePrev.SetImageDrawable(new CircleDrawable(loadedBitmap));
				}
				else if  (url ==_winItem.PreviousWinner.Picture.Uri)
				{
					_profilePrev.SetImageDrawable(new CircleDrawable(loadedBitmap));
				}
				else if  (url ==_winItem.NextPrize.Picture.Uri)
				{
					_winImageNext.SetImageDrawable(new CircleDrawable(loadedBitmap));
				}  
		*/		
				imageView.SetImageDrawable(new CircleDrawable(loadedBitmap));
			});
		}
	

        public override void OnStart()
        {

            base.OnStart();


			if (_page == 1)
			{

				var profileimage = _view.FindViewById<ImageView>(Resource.Id.ProfileImage);
		//		var winimage = _view.FindViewById<ImageView>(Resource.Id.WinImage2);


				_activity.RunOnUiThread(() =>
					{
					if (profileimage != null)
				  	{
						try
						{
							profileimage.SetImageDrawable(new CircleDrawable(BitmapFactory.DecodeResource(this.Resources, Resource.Drawable.GhostProfile)));
				//			winimage.SetImageDrawable(new CircleDrawable(BitmapFactory.DecodeResource(this.Resources, Resource.Drawable.GhostSquarePicture)));

						}
						catch { }
					}

				});

			}
			else if (_page == 2)
			{
				     


			}
		    else if (_page == 3)
			{

				var image = _view.FindViewById<ImageView>(Resource.Id.WinImage);

				_activity.RunOnUiThread(() =>
				{
					if (image != null)
					{
						try
						{
							image.SetImageDrawable(new CircleDrawable(BitmapFactory.DecodeResource(this.Resources, Resource.Drawable.GhostCircle)));

						}
						catch { }
					}

				});

			}

       

        }
				       


       
    }
}

