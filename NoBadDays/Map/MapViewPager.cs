﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using NoBadDays.helpers;
using Android.Graphics;
using Android.Support.V4.App;
using Android.Support.V4.View;
using DK.Ostebaronen.Droid.ViewPagerIndicator;

using CustomTextView = NoBadDays.helpers.CustomTextView; 
using CustomEditText = NoBadDays.helpers.CustomEditText;
using Android.Views.InputMethods;
using SQLite.Net.Platform.XamarinAndroid;
using NoBadDaysPCL.ViewModel;
using Koush;


namespace NoBadDays
{
    [Activity (Label = "", Theme = "@style/NoBadDaysTheme")]
	public class MapViewPager : FragmentActivity, Koush.IUrlImageViewCallback , Android.Support.V4.View.ViewPager.IOnPageChangeListener
    {

		public event EventHandler<bool> DataReady;

		public ShopViewModel shopViewModel {get; set;}
		public UserViewModel userViewModel {get; set;}


		ViewPager _pager;
        MapPageViewerAdapter _adapter;
        protected IPageIndicator _indicator;

       
        
		public float density {get; set;}
        public int screenHeight;
        public int screenWidth;


        ImageView _backKey;

    //    WheelMenu _wheelMenu;

		ImageView _storeImage;
		CustomTextView _storeName;
		CustomTextView _location;
		ImageView _ratingResult;
		CustomTextView _noRatings;

		public int selectedPage { get; set; }

		CustomEditText _comment;
		ScrollView _scroll;
		LinearLayout _toolbar;

		public int shopId;



		public double latitude;
		public double longitude;

		public ScrollView scrollView  { set; get; }
		public int scrollViewMax { set; get; }







        protected override void OnCreate (Bundle bundle)
        {
            base.OnCreate (bundle);

            // Create your application here

			RequestWindowFeature(WindowFeatures.IndeterminateProgress);
        

            SetContentView (Resource.Layout.MapViewPager);

			Bundle extras = Intent.Extras;

			try
			{
				shopId = extras.GetInt("ShopId");
				latitude = double.Parse(extras.GetString("Latitude"));
				longitude = double.Parse(extras.GetString("Longitude"));
			
			}
			catch (Exception exc)
			{
				
			}


			int version = Int32.Parse(Android.OS.Build.VERSION.Sdk);

			if (version != 19)   //KitKat 4.4.2
			{
				View decorView = Window.DecorView;
				// Hide the status bar.
				int uiOptions = (int)View.SystemUiFlagFullscreen;
				decorView.SystemUiVisibility = (StatusBarVisibility)uiOptions;
			}
            ActionBar actionBar = ActionBar;
			actionBar.SetDisplayShowHomeEnabled(false);

            //displaying custom ActionBar
			View actionBarView = LayoutInflater.Inflate(Resource.Layout.actionbarShopLayout, null);
            actionBar.SetCustomView(actionBarView, new Android.App.ActionBar.LayoutParams(ActionBar.LayoutParams.WrapContent, ActionBar.LayoutParams.WrapContent,
                GravityFlags.Center|GravityFlags.Left));
            actionBar.SetDisplayOptions(ActionBarDisplayOptions.ShowCustom, ActionBarDisplayOptions.ShowCustom);


			var reviewsBtn = actionBarView.FindViewById<ImageView> (Resource.Id.reviews);
			reviewsBtn.Click += ReviewsBtn_Click;

       //     _menuButton = actionBarView.FindViewById<ImageView>(Resource.Id.wheelBut);
		//  _menuButton.Visibility = ViewStates.Gone;
			_backKey = actionBarView.FindViewById<ImageView>(Resource.Id.backkey);
			_backKey.Click+= (object sender, EventArgs e) => 
			 {
				this.Finish();
			 };
			          
            actionBarView.FindViewById<CustomTextView> (Resource.Id.actionbarTitle).Text = GetString (Resource.String.ShopInfoText);
				

            RequestedOrientation = Android.Content.PM.ScreenOrientation.Portrait;

            Display d = WindowManager.DefaultDisplay;

            Android.Util.DisplayMetrics m = new Android.Util.DisplayMetrics();
            d.GetMetrics(m);

            density = m.Density;

            Point size = new Point();
            d.GetSize(size);
            screenWidth = size.X;
            screenHeight = size.Y;

            _adapter = new MapPageViewerAdapter(SupportFragmentManager, this);

            _pager = FindViewById<ViewPager>(Resource.Id.pager);

             _pager.SetPageTransformer(true, new PageTransformer());

			_pager.AddOnPageChangeListener(this);
     
             _pager.Adapter = _adapter;

            try{

             _indicator = FindViewById<TabPageIndicator>(Resource.Id.indicator);
            }
            catch (Exception exc) {

                
            }

             #region custom fonts
             Typeface face = Typeface.CreateFromAsset(Assets, "Fonts/Roboto/Roboto-Bold.ttf");
             _indicator.SetViewPager(_pager, face);
             #endregion

			_storeImage = FindViewById<ImageView>(Resource.Id.StoreImage);
			_storeName = FindViewById<CustomTextView>(Resource.Id.name);
			_location = FindViewById<CustomTextView>(Resource.Id.location);
			_ratingResult = FindViewById<ImageView>(Resource.Id.Rating);
			_noRatings = FindViewById<CustomTextView>(Resource.Id.NoRatings);

			string libraryPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
			var path = System.IO.Path.Combine(libraryPath, Constants.SQLitePath);

			shopViewModel = new ShopViewModel (new SQLitePlatformAndroid(), path);

			shopViewModel.PhoneDataErrorOccurred += _shopViewModel_PhoneDataErrorOccurred;
			shopViewModel.PhoneDataSuccessful += _shopViewModel_PhoneDataSuccessful;


			userViewModel = new UserViewModel (new SQLitePlatformAndroid(), path);

			scrollView = FindViewById<ScrollView>(Resource.Id.scroll);

        }

        void ReviewsBtn_Click (object sender, EventArgs e)
        {
			Bundle dataBundle = new Bundle();
			dataBundle.PutInt("ShopId", shopId);
		
			var page = new Intent(this, typeof(Reviews));
			page.PutExtras(dataBundle);

			StartActivity(page);
        }

        void _shopViewModel_PhoneDataSuccessful (object sender, NoBadDaysPCL.Items.ServerOutputItem e)
        {
			_storeName.Text = shopViewModel.Item.Name;
			_location.Text = shopViewModel.Item.Street + "," + " " + shopViewModel.Item.PostalCode + " " + shopViewModel.Item.City;

			var rating = shopViewModel.Item.Rating;

			if (rating < 0) {
				_noRatings.Visibility = ViewStates.Visible;
				_ratingResult.Visibility = ViewStates.Gone;
			} else {
				_noRatings.Visibility = ViewStates.Gone;
				_ratingResult.Visibility = ViewStates.Visible;
			}

			if ((rating >= 0)&&(rating < 0.5)) 
			{
				_ratingResult.SetImageBitmap(BitmapFactory.DecodeResource(this.Resources ,Resource.Drawable.ZeroStarsTotal));
			}
			else if ((rating >= 0.5) && (rating < 1.5))
			{
				_ratingResult.SetImageBitmap(BitmapFactory.DecodeResource(this.Resources ,Resource.Drawable.OneStarTotal));
			}
			else if ((rating >= 1.5) && (rating < 2.5))
			{
				_ratingResult.SetImageBitmap(BitmapFactory.DecodeResource(this.Resources ,Resource.Drawable.TwoStarsTotal));
			}
			else if ((rating >= 2.5) && (rating < 3.5))
			{
				_ratingResult.SetImageBitmap(BitmapFactory.DecodeResource(this.Resources ,Resource.Drawable.ThreeStarsTotal));
			}
			else if ((rating >= 3.5) && (rating < 4.5))
			{
				_ratingResult.SetImageBitmap(BitmapFactory.DecodeResource(this.Resources ,Resource.Drawable.FourStarsTotal));
			}
			else 
			{
				_ratingResult.SetImageBitmap(BitmapFactory.DecodeResource(this.Resources ,Resource.Drawable.FiveStarsTotal));
			}


			_storeImage.SetImageDrawable(new CircleDrawable(BitmapFactory.DecodeResource(this.Resources,Resource.Drawable.GhostCircle)));

			UrlImageViewHelper.SetUrlDrawable(_storeImage,  shopViewModel.Item.ListPicture.Uri,  Resource.Drawable.GhostCircle, 60000, this);
			RaiseDataReady();

        }

		void RaiseDataReady()
		{
			if (DataReady != null)
			{
				DataReady(this, true);
			}
		}



        void _shopViewModel_PhoneDataErrorOccurred (object sender, NoBadDaysPCL.Items.ServerOutputItem e)
        {
			Android.Widget.Toast.MakeText (this, e.ResultString, ToastLength.Long).Show ();
        }


		public void SetProgress(bool state)
		{
			SetProgressBarIndeterminateVisibility(state);
		}


        protected override void OnResume()
        {
            base.OnResume ();

			scrollView.Measure(RelativeLayout.LayoutParams.WrapContent, RelativeLayout.LayoutParams.WrapContent);

			scrollViewMax = scrollView.MeasuredHeight+100;


			shopViewModel.GetShop (shopId);

		

        }


		public void OnLoaded(ImageView imageView, Bitmap loadedBitmap, string url, bool loadedFromCache)
		{
    	    RunOnUiThread(() =>
			{
				if (loadedBitmap != null)
				{
					_storeImage.SetImageDrawable(new CircleDrawable(loadedBitmap));
				}
			});

		}

		public void SetView(View view, View scroll, View toolbar)
		{
			_comment = (CustomEditText)view;
			_scroll = (ScrollView)scroll;
			_toolbar = (LinearLayout)toolbar;
		
	    }

        public override bool OnKeyDown(Keycode keyCode, KeyEvent e)
        {

            if (keyCode == Keycode.Back)
            {
				if (_comment != null) {
					InputMethodManager imm = (InputMethodManager)GetSystemService (Context.InputMethodService);
					imm.HideSoftInputFromWindow (_comment.WindowToken, 0);

					var scrollView = FindViewById<ScrollView>(Resource.Id.scroll);

					scrollView.Visibility = ViewStates.Visible;

				}

				this.Finish();
				return true;
						
            }

            return base.OnKeyDown(keyCode, e);
        }


        public override void OnLowMemory()
        {
            Console.WriteLine("Profile:OnLowMemory");
            GC.Collect();
        }



		protected override void OnPause()
		{
			base.OnPause();

			GC.Collect (0);

		}


		#region Viewpager interfaces


		public void OnPageSelected(int pos)
		{
			selectedPage = pos;

			var user = userViewModel.GetUser ();

			if ((user == null) && (selectedPage == 1)) {
				Toast.MakeText (this, GetString (Resource.String.YouMustLoginText), ToastLength.Long).Show ();
				_toolbar.Visibility = ViewStates.Visible;
			}

			if (selectedPage == 1)
			{
				_scroll.Visibility = ViewStates.Gone;
			}

		}

		public void OnPageScrollStateChanged(int state)
		{

		}

		public void OnPageScrolled(int position, float positionOffset, int positionOffsetPixels)
		{

		}

		#endregion
    }
}

