﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using NoBadDays.helpers;
using Android.Gms.Maps;
using Com.Szugyi.Circlemenu.View;
using NoBadDaysPCL.ViewModel;
using SQLite.Net.Platform.XamarinAndroid;
using Android.Gms.Maps.Model;
using Android.Locations;
using System.Threading;
using NoBadDaysPCL.Items;
using System.Collections;

using eShopType = NoBadDaysPCL.MainClass.eShopType;

namespace NoBadDays
{
    [Activity (Label = "", Theme = "@style/NoBadDaysTheme")]            
	public class MapMainPage : Activity, GoogleMap.IOnCameraChangeListener, GoogleMap.IOnMarkerClickListener,GoogleMap.IOnMapClickListener, GoogleMap.IOnInfoWindowClickListener
    {

        
		public MapViewModel mapViewModel {get; set;}
		public UserViewModel userViewModel { get; set; }

		MapSearchItem _mapSearchItem = new MapSearchItem ();

		ListHelper _listHelper = new ListHelper ();
		AlertDialog _dlgAlert;
		ListView _popUpList;
		AlertDialog.Builder _builder = null;

		DialogHelper diag = new DialogHelper ();

		private float _density;
        int _screenHeight;
        int _screenWidth;

        GoogleMap _map;
        MapFragment _mapFragment;

        private LatLng ondoPosition;
		private LatLng myPosition;
       
        double _latitude;
        double _longitude;
        string maptag;

		public Bitmap picture = null;
		public bool second = false;
		bool elapsed = false;
		System.Threading.Timer timer;


        ImageView _menuButton;

        WheelMenu _wheelMenu;

        MarkerOptions mapOption;

        Marker marker;
        Marker ownMarker;

        LocationManager _locMgr;

        GeoHelper _geo = null;

        float _zoomLevel = 10;

		const int _zoomlevelClusterStop = 16; //18;
		int _gridx; 
		int _gridy;
		LatLng _mapPosition;

		public Hashtable markersImageTable = new Hashtable();
	    public Hashtable markersTitleTable = new Hashtable();

		int _shopId = -1;
		int _clusterCnt = -1;

	
	

        protected override void OnCreate (Bundle bundle)
        {
            base.OnCreate (bundle);

            // Create your application here
						
            SetContentView (Resource.Layout.MapMainPage);

			int version = Int32.Parse(Android.OS.Build.VERSION.Sdk);

			if (version != 19)   //KitKat 4.4.2
			{
				View decorView = Window.DecorView;
				// Hide the status bar.
				int uiOptions = (int)View.SystemUiFlagFullscreen;
				decorView.SystemUiVisibility = (StatusBarVisibility)uiOptions;
			}

			ActionBar actionBar = ActionBar;
			actionBar.SetDisplayShowHomeEnabled(false);
			//displaying custom ActionBar
			View actionBarView = LayoutInflater.Inflate(Resource.Layout.actionbarMapLayout, null);
			actionBar.SetCustomView(actionBarView, new Android.App.ActionBar.LayoutParams(ActionBar.LayoutParams.WrapContent, ActionBar.LayoutParams.WrapContent,
				GravityFlags.Center|GravityFlags.Left));
			actionBar.SetDisplayOptions(ActionBarDisplayOptions.ShowCustom, ActionBarDisplayOptions.ShowCustom);


			actionBarView.FindViewById<CustomTextView> (Resource.Id.actionbarTitle).Text = GetString (Resource.String.mapText);

			var searchBtn = actionBarView.FindViewById<ImageView> (Resource.Id.search);
			searchBtn.Click+= SearchBtn_Click;


			RequestedOrientation = Android.Content.PM.ScreenOrientation.Portrait;

			Display d = WindowManager.DefaultDisplay;

			Android.Util.DisplayMetrics m = new Android.Util.DisplayMetrics();
			d.GetMetrics(m);

			_density = m.Density;

			Point size = new Point();
			d.GetSize(size);
			_screenWidth = size.X;
			_screenHeight = size.Y;

			_gridx = (int)((size.X / 32));
			_gridy = (int)((size.Y / 36));


			Container.latitudeStore = "";
			Container.longitudeStore = "";

			_geo = new GeoHelper(this);

			var _skinOverlay = FindViewById<View> (Resource.Id.skinOverlay);
			_menuButton = actionBarView.FindViewById<ImageView>(Resource.Id.wheelBut);

			_wheelMenu = new WheelMenu (this, 0, _density, _menuButton, false, _skinOverlay);


        }

        void SearchBtn_Click (object sender, EventArgs e)
        {
			
		
			_listHelper.add_categories(this);

			_popUpList = new ListView(this);

			_popUpList.Adapter = new AlertListViewAdapter(this, Container.listDataItem);
			_popUpList.ItemClick += _popUpList_ItemClick;



			_builder = new AlertDialog.Builder(new ContextThemeWrapper(this, Resource.Style.CustomDialogTheme));

	
			_dlgAlert = _builder.Create();
			_dlgAlert.SetTitle(GetString(Resource.String.SelectCategoryText));
		    _dlgAlert.SetView(_popUpList);


			_dlgAlert.Show();
        }

		void _popUpList_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
		{
			_popUpList.ItemClick -= _popUpList_ItemClick;

			if (_dlgAlert != null) _dlgAlert.Dismiss();

	//		Android.Widget.Toast.MakeText(this, Container.listDataItem[e.Position], ToastLength.Long).Show();

		//	if ((eShopType)e.Position == eShopType.None)
		//		return;


			Container.latitudeStore = _mapPosition.Latitude.ToString();
			Container.longitudeStore = _mapPosition.Longitude.ToString();
		
			_mapSearchItem.Nelat = _map.Projection.VisibleRegion.FarRight.Latitude;
			_mapSearchItem.Nelon = _map.Projection.VisibleRegion.FarRight.Longitude;
			_mapSearchItem.Swlat = _map.Projection.VisibleRegion.NearLeft.Latitude;
			_mapSearchItem.Swlon = _map.Projection.VisibleRegion.NearLeft.Longitude;
			_mapSearchItem.Gridx = _gridx;
			_mapSearchItem.Gridy = _gridy;
			_mapSearchItem.ZoomlevelClusterStop = _zoomlevelClusterStop;
			_mapSearchItem.Zoomlevel = (int)_zoomLevel;
			_mapSearchItem.Search = "";

			Container.shoptypeStore = _mapSearchItem.ShopType = (eShopType)e.Position;

			mapViewModel.GetShops (_mapSearchItem);

		}


		protected override void OnStart()
		{
			base.OnStart();
	


			string libraryPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
			var path = System.IO.Path.Combine(libraryPath, Constants.SQLitePath);

			userViewModel = new UserViewModel (new SQLitePlatformAndroid(), path);
			mapViewModel = new MapViewModel (new SQLitePlatformAndroid(), path);

			mapViewModel.PhoneDataErrorOccurred += MapViewModel_PhoneDataErrorOccurred;
			mapViewModel.PhoneDataSuccessful += MapViewModel_PhoneDataSuccessful;

		
			InitMap();


		
		}

		void MapViewModel_PhoneDataSuccessful (object sender, ServerOutputItem e)
		{
			markersImageTable.Clear();
			markersTitleTable.Clear();

			_map.Clear();

			AddMarkersToMap ();
		}

		void MapViewModel_PhoneDataErrorOccurred (object sender, ServerOutputItem e)
		{

			Android.Widget.Toast.MakeText (this, e.ResultString, ToastLength.Long).Show ();
		}

        protected override void OnResume()
        {
            base.OnResume ();

		
            _latitude = Container.Latitude;
            _longitude = Container.Longitude;

			if ((Container.latitudeStore == "") && (Container.longitudeStore == "")) {

				_mapPosition = new LatLng (Container.Latitude, Container.Longitude);
			} else {
				_mapPosition = new LatLng (double.Parse(Container.latitudeStore), double.Parse(Container.longitudeStore));
				_zoomLevel = Container.zoomLevelStore;
			}

            ondoPosition = new LatLng(_latitude, _longitude);

            UpdateView();

			if (!_geo.checkLocationSettings()) {
				
			
				ShowDialog (Constants.DIALOG_NO_GPS_MESSAGE);
			}


        }


		protected override void OnPrepareDialog(int id, Dialog dialog)
		{
			diag.SetDialogButtons(dialog);
		}

		protected override Dialog OnCreateDialog(int id)
		{
			AlertDialog.Builder builder;
			View customView;

			diag.SetDialog(out customView, out builder, this);

			switch (id)
			{
		    	case Constants.DIALOG_NO_GPS_MESSAGE:
				{
    				customView.FindViewById<TextView>(Resource.Id.dialogText).Text = GetString(Resource.String.ActivateGPSText);

					builder.SetPositiveButton(Resource.String.OkText, OkClicked);
					builder.SetNegativeButton(Resource.String.CancelText, CancelClicked);

					return builder.Create();
				}

			}
			return null;
		}

		private void OkClicked(object sender, DialogClickEventArgs e)
		{
			Intent intent = new Intent(Android.Provider.Settings.ActionLocationSourceSettings);
			StartActivity(intent);

		}

		private void CancelClicked(object sender, DialogClickEventArgs e)
		{

		}

        public void UpdateView()
        {
            if (MapIsSetup())
            {
                _map.MyLocationEnabled = false;
                _map.SetOnCameraChangeListener(this);

                _map.SetOnMapClickListener(this);

				_map.SetInfoWindowAdapter(new CustomInfoWindowAdapter(this));    

				// Set listeners for marker events.  See the bottom of this class for their behavior.
				_map.SetOnMarkerClickListener(this);

				_map.SetOnInfoWindowClickListener(this);   

            }

			var mypos = FindViewById<ImageView>(Resource.Id.myPosImage);
			mypos.Visibility = ViewStates.Visible;
			mypos.Click += mypos_Click;

	//		AddMarkerToMap(ondoPosition);


			_map.AnimateCamera(CameraUpdateFactory.NewLatLngZoom(_mapPosition, _zoomLevel));

        }

       
        private void InitMap()
        {
            _mapFragment = FragmentManager.FindFragmentByTag("map") as MapFragment;
            if (_mapFragment == null)
            {
                GoogleMapOptions mapOptions = new GoogleMapOptions()
                    .InvokeMapType(GoogleMap.MapTypeNormal)
                    .InvokeZoomControlsEnabled(true)
                    .InvokeCompassEnabled(true);

                Android.App.FragmentTransaction fragTx = FragmentManager.BeginTransaction();
                _mapFragment = MapFragment.NewInstance(mapOptions);
                fragTx.Add(Resource.Id.mapWithOverlay, _mapFragment, "map");
                fragTx.Commit();
            }
        }

        private bool MapIsSetup()
        {
            if (_map == null)
            {
                _map = _mapFragment.Map;
            }

            return _map != null;
        }


        public void OnCameraChange(CameraPosition position)
        {
			
			_mapPosition.Latitude = position.Target.Latitude;
			_mapPosition.Longitude = position.Target.Longitude;


			Container.latitudeStore = _mapPosition.Latitude.ToString();
			Container.longitudeStore = _mapPosition.Longitude.ToString();
			Container.zoomLevelStore = position.Zoom;
	
			_mapSearchItem.Nelat = _map.Projection.VisibleRegion.FarRight.Latitude;
			_mapSearchItem.Nelon = _map.Projection.VisibleRegion.FarRight.Longitude;
			_mapSearchItem.Swlat = _map.Projection.VisibleRegion.NearLeft.Latitude;
			_mapSearchItem.Swlon = _map.Projection.VisibleRegion.NearLeft.Longitude;
			_mapSearchItem.Gridx = _gridx;
			_mapSearchItem.Gridy = _gridy;
			_mapSearchItem.ZoomlevelClusterStop = _zoomlevelClusterStop;
			_mapSearchItem.Zoomlevel = (int)_zoomLevel;
			_mapSearchItem.Search = "";
		//	_mapSearchItem.ShopType = Container.shoptypeStore;

			mapViewModel.GetShops (_mapSearchItem);

					

        }

        public void OnMapClick(LatLng pos)
        {


        }

		public bool OnMarkerClick(Marker marker)
		{
			GetMarkerInfo (marker);

			if ((_clusterCnt > 0) && (_clusterCnt< 2)) {
		
				second = false;
				picture = null;
				try{
				marker.ShowInfoWindow ();
	
				}
				catch (Exception exc) {
					string t = exc.Message;
				}
				awaitPicture (marker);
			}

			return true;
		}

		private void MapOnMarkerClick(object sender, GoogleMap.MarkerClickEventArgs markerClickEventArgs)
		{

			Marker marker = markerClickEventArgs.Marker; // TODO [TO201212142221] Need to fix the name of this with MetaData.xml

			GetMarkerInfo (marker);

			if ((_clusterCnt > 0)&& (_clusterCnt < 2)){
				second = false;
				picture = null;
				marker.ShowInfoWindow ();
				awaitPicture (marker);
			}

		}

		public void OnInfoWindowClick(Marker marker)
		{

			marker.HideInfoWindow();

			var snippet = marker.Snippet;

			GetMarkerInfo (marker);

	//		if (Int32.TryParse (snippet, out _shopId))
			if (_shopId > 0) {
				
				Bundle dataBundle = new Bundle();
				dataBundle.PutInt("ShopId", _shopId);
				dataBundle.PutString ("Latitude", marker.Position.Latitude.ToString());
				dataBundle.PutString ("Longitude", marker.Position.Longitude.ToString());

				var page = new Intent(this, typeof(MapViewPager));
				page.PutExtras(dataBundle);

				StartActivity(page);

			}
			else Android.Widget.Toast.MakeText(this, GetString(Resource.String.SomethingWrongText), ToastLength.Long).Show();

		}


		void GetMarkerInfo(Marker marker)
		{
		
			if (marker.Snippet != "")
			{

				int firstCharacter = marker.Snippet.IndexOf("#");
				int secondCharacter = marker.Snippet.IndexOf("*");
			
				string tmp = marker.Snippet;

				_shopId = Int32.Parse(tmp.Truncate(firstCharacter));

				tmp = marker.Snippet.Substring(firstCharacter + 1, (secondCharacter - firstCharacter - 1));

				if (!Int32.TryParse(tmp, out _clusterCnt))
					_clusterCnt = -1;
						
			}
			else
			{
				_shopId = -1;
				_clusterCnt = -1;

			}
		}

		/*

        private void AddMarkerToMap(LatLng Geopos)
        {
            BitmapDescriptor icon = BitmapDescriptorFactory.FromResource(Resource.Drawable.MapPin);

            try
            {

                mapOption = new MarkerOptions()
                         .SetPosition(Geopos)
                         .InvokeIcon(icon)
                         .SetSnippet("")
                         .SetTitle("");
                marker = _map.AddMarker(mapOption);

            }

            catch { }


        }

*/

        void mypos_Click(object sender, EventArgs e)
        {

            try
            {

				myPosition = new LatLng( NoBadDays.Container.Latitude, NoBadDays.Container.Longitude);

                LatLngBounds.Builder b = new LatLngBounds.Builder();
                b.Include(ondoPosition);
                b.Include(myPosition);

                LatLngBounds boundary = b.Build();

                _map.AnimateCamera(CameraUpdateFactory.NewLatLngBounds(boundary, _screenWidth / 4, _screenWidth / 4, 0));


                BitmapDescriptor icon = BitmapDescriptorFactory.FromResource(Resource.Drawable.p99999);

                mapOption = new MarkerOptions()
                          .SetPosition(new LatLng(NoBadDays.Container.Latitude, NoBadDays.Container.Longitude))
                          .InvokeIcon(icon)
                          .SetSnippet("")
                          .SetTitle("");
                marker = _map.AddMarker(mapOption);

            }

            catch (Exception exc)
            {
                
            }


        }

		private void AddMarkersToMap()
		{
			for (var i = 0; i < mapViewModel.Items.Count; i++) 
			{
				BitmapDescriptor icon = null;


				var geopos = new LatLng(mapViewModel.Items.ElementAt(i).Latitude, mapViewModel.Items.ElementAt(i).Longitude);
				var snippet = mapViewModel.Items.ElementAt(i).ShopId.ToString() +"#" + mapViewModel.Items.ElementAt(i).ClusterCount.ToString() + "*" ;
				var name = mapViewModel.Items.ElementAt(i).Name;
				var shopid = mapViewModel.Items.ElementAt (i).ShopId;
				var image = mapViewModel.Items.ElementAt (i).ListPicture;

				if (mapViewModel.Items.ElementAt(i).ClusterCount > 1) {
					try {
						if (mapViewModel.Items.ElementAt (i).ClusterCount == 2) {
							icon = BitmapDescriptorFactory.FromResource (Constants.clusterMarkers [2]);
						} else if (mapViewModel.Items.ElementAt (i).ClusterCount == 3) {
							icon = BitmapDescriptorFactory.FromResource (Constants.clusterMarkers [3]);
						} else if (mapViewModel.Items.ElementAt (i).ClusterCount == 4) {
							icon = BitmapDescriptorFactory.FromResource (Constants.clusterMarkers [4]);
						} else if ((mapViewModel.Items.ElementAt (i).ClusterCount >= 5) && (mapViewModel.Items.ElementAt (i).ClusterCount < 10)) {
							icon = BitmapDescriptorFactory.FromResource (Constants.clusterMarkers [5]);
						} else if ((mapViewModel.Items.ElementAt (i).ClusterCount >= 10) && (mapViewModel.Items.ElementAt (i).ClusterCount < 20)) {
							icon = BitmapDescriptorFactory.FromResource (Constants.clusterMarkers [10]);
						} else if ((mapViewModel.Items.ElementAt (i).ClusterCount >= 20) && (mapViewModel.Items.ElementAt (i).ClusterCount < 50)) {
							icon = BitmapDescriptorFactory.FromResource (Constants.clusterMarkers [20]);
						} else if ((mapViewModel.Items.ElementAt (i).ClusterCount >= 50) && (mapViewModel.Items.ElementAt (i).ClusterCount < 100)) {
							icon = BitmapDescriptorFactory.FromResource (Constants.clusterMarkers [50]);
						} else if (mapViewModel.Items.ElementAt (i).ClusterCount >= 100) {
							icon = BitmapDescriptorFactory.FromResource (Constants.clusterMarkers [100]);
						} else {

							icon = BitmapDescriptorFactory.FromResource (Resource.Drawable.UnknownMarker);

						}
					} catch {
						
						icon = BitmapDescriptorFactory.FromResource (Resource.Drawable.UnknownMarker);
					}

				} 
				else if (mapViewModel.Items.ElementAt(i).ShopType == eShopType.ClothingAndbeauty)
				{
					try {
						icon = BitmapDescriptorFactory.FromResource (Resource.Drawable.ClothingMarker);
					} catch {
						icon = BitmapDescriptorFactory.FromResource (Resource.Drawable.UnknownMarker);
					}
				}
				else if (mapViewModel.Items.ElementAt(i).ShopType == eShopType.EatAndDrink)
				{
					try {
						icon = BitmapDescriptorFactory.FromResource (Resource.Drawable.EatDrinkMarker);
					} catch  {
						icon = BitmapDescriptorFactory.FromResource (Resource.Drawable.UnknownMarker);
					}
				}
				else if (mapViewModel.Items.ElementAt(i).ShopType == eShopType.Electronics)
				{
					try {
						icon = BitmapDescriptorFactory.FromResource (Resource.Drawable.ElectronicsMarker);
					} catch {
						icon = BitmapDescriptorFactory.FromResource (Resource.Drawable.UnknownMarker);
					}
				}
				else if (mapViewModel.Items.ElementAt(i).ShopType == eShopType.Food)
				{
					try {
						icon = BitmapDescriptorFactory.FromResource (Resource.Drawable.FoodMarker);
					} catch {
						icon = BitmapDescriptorFactory.FromResource (Resource.Drawable.UnknownMarker);
					}
				}
				else if (mapViewModel.Items.ElementAt(i).ShopType == eShopType.HomeAndGarden)
				{
					try {
						icon = BitmapDescriptorFactory.FromResource (Resource.Drawable.HomeGardenMarker);
					} catch {
						icon = BitmapDescriptorFactory.FromResource (Resource.Drawable.UnknownMarker);
					}
				}
				else if (mapViewModel.Items.ElementAt(i).ShopType == eShopType.HotelsAndTravel)
				{
					try {
						icon = BitmapDescriptorFactory.FromResource (Resource.Drawable.HotelTravelMarker);
					} catch {
						icon = BitmapDescriptorFactory.FromResource (Resource.Drawable.UnknownMarker);
					}
				}
				else if (mapViewModel.Items.ElementAt(i).ShopType == eShopType.KidsAndPlay)
				{
					try {
						icon = BitmapDescriptorFactory.FromResource (Resource.Drawable.KidsMarker);
					} catch {
						icon = BitmapDescriptorFactory.FromResource (Resource.Drawable.UnknownMarker);
					}
				}
				else if (mapViewModel.Items.ElementAt(i).ShopType == eShopType.Services)
				{
					try {
						icon = BitmapDescriptorFactory.FromResource (Resource.Drawable.ServicesMarker);
					} catch {
						icon = BitmapDescriptorFactory.FromResource (Resource.Drawable.UnknownMarker);
					}
				}
				else if (mapViewModel.Items.ElementAt(i).ShopType == eShopType.SportFitnessAndOutdoor)
				{
					try {
						icon = BitmapDescriptorFactory.FromResource (Resource.Drawable.SportMarker);
					} catch {
						icon = BitmapDescriptorFactory.FromResource (Resource.Drawable.UnknownMarker);
					}
				}
				else if (mapViewModel.Items.ElementAt(i).ShopType == eShopType.TicketsAndexperiences)
				{
					try {
						icon = BitmapDescriptorFactory.FromResource (Resource.Drawable.TicketsExperiencesMarker);
					} catch {
						icon = BitmapDescriptorFactory.FromResource (Resource.Drawable.UnknownMarker);
					}
				}
				else icon = BitmapDescriptorFactory.FromResource (Resource.Drawable.UnknownMarker);

				SetMarker(geopos, icon, snippet, name, shopid, image);
					

			}

		}

		void SetMarker(LatLng geopos, BitmapDescriptor icon, string snippet, string name, int shopid, string image)
		{


			var mapOption = new MarkerOptions()
				.SetPosition(geopos)
				.InvokeIcon(icon)
				.SetSnippet(snippet)
				.SetTitle(name);

			var marker = _map.AddMarker(mapOption);

			markersImageTable.Add(shopid, image);
			markersTitleTable.Add(shopid, name);



		}

		public List<MapItem> getMarkers()
		{
			List<MapItem> items = new List<MapItem>();

			foreach (MapItem element in mapViewModel.Items)
			{
				items.Add(new MapItem()
				{
					ShopId = element.ShopId,
					Name = element.Name,
					ListPicture = element.ListPicture,
					Longitude = element.Longitude,
					Latitude = element.Latitude
					
				});
			}

			return items;
		}

		void awaitPicture(Marker marker)
		{

			_map.SetOnCameraChangeListener(null);
			new Thread(new ThreadStart(() =>
				{
					elapsed = false;
					timer = new System.Threading.Timer(new TimerCallback(TimerEvent));
					timer.Change(2000, 0); 
					while ((picture == null)&&(!elapsed))
					{
						;
					}
					RunOnUiThread(() =>
						{
							marker.HideInfoWindow();
							timer.Dispose();
							second = true;
							marker.ShowInfoWindow();
							_map.SetOnCameraChangeListener(this);
						});
				})).Start();
		}

		private void TimerEvent(object source)
		{
			elapsed = true;
			timer.Dispose();
		}



	
		protected override void OnPause()
		{
			base.OnPause();

			GC.Collect (0);

		}

		protected override void OnStop()
		{
			base.OnStop();

		}

		protected override void OnDestroy()
		{
			base.OnDestroy();

			if (_wheelMenu != null)_wheelMenu.Dispose ();

			if (_geo != null)
			{
				_geo.SuspendLocationUpdates();
			}
		}


        public override bool OnKeyDown(Keycode keyCode, KeyEvent e)
        {

            if (keyCode == Keycode.Back)
            {
                return _wheelMenu.CloseActivity(this);
            }

            return base.OnKeyDown(keyCode, e);
        }

		public override void OnBackPressed()
		{

			if (_dlgAlert != null) _dlgAlert.Dismiss();

			base.OnBackPressed();

		}

        public override void OnLowMemory()
        {
           Console.WriteLine("Map:OnLowMemory");
            GC.Collect();
        }
    }

	public static class Stringhelper
	{
		public static string Truncate(this string value, int maxLength)
		{
			return value.Length <= maxLength ? value : value.Substring(0, maxLength);
		}

	}
}

