﻿using Android.Content;
using NoBadDays.Profile;
using System;

namespace NoBadDays
{
    public class MapPageViewerAdapter : Android.Support.V4.App.FragmentPagerAdapter
    {
        int _count;
		MapViewPager _context;

        private readonly string[] Content;


		public MapPageViewerAdapter(Android.Support.V4.App.FragmentManager fm, MapViewPager context)
            : base(fm)
        {
            _context = context;


            Content = new string[2];

			Content[0] = _context.GetString(Resource.String.ContactText);
			Content[1] = _context.GetString(Resource.String.RateStoreText);

            _count = Content.Length;

            _count = Content.Length; ;

        }

        public override Android.Support.V4.App.Fragment GetItem(int position)
        {
            return new MapPageSubPages(position, _context);
        }

        public override int Count
        {
            get
            {
                return _count;
            }
        }

        public override Java.Lang.ICharSequence GetPageTitleFormatted(int p0)
        {
            return new Java.Lang.String(Content[p0 % Content.Length]);
        }



        public void SetCount(int Count)
        {
            if (Count > 0 && Count <= 10)
            {
                _count = Count;
                NotifyDataSetChanged();
            }
        }
    }
}

