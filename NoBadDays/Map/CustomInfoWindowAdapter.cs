using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Views.Animations;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;


using System.Threading;
using Android.Graphics;
using Koush;
using System.Threading.Tasks;
using System.Collections;
using NoBadDaysPCL.Items;

namespace NoBadDays
{
    class CustomInfoWindowAdapter : Java.Lang.Object, GoogleMap.IInfoWindowAdapter, Koush.IUrlImageViewCallback 
    {
     //   MapActivity parent;
        Activity parent;

   //     DistanceHelper disthelper = new DistanceHelper();
    
        // These a both viewgroups containing an ImageView with id "badge" and two TextViews with id
        // "title" and "snippet".
        private readonly View mWindow;

        bool loaded = false;

            
        internal CustomInfoWindowAdapter(MapMainPage activity)
        {
            parent = activity;
            mWindow = parent.LayoutInflater.Inflate(Resource.Layout.custom_info_window, null);
        }


        public View GetInfoWindow(Marker marker)
        {

            Render(marker, mWindow);
            return mWindow;
        }

        public View GetInfoContents(Marker marker)
        {
            return null;
      
        }

        private void Render(Marker marker, View view)
        {

			MapMainPage activity = parent as MapMainPage;

            bool found = false;
			                
			
            int shopId = 0;

            if (activity != null)
            {

                List<MapItem> markers = activity.getMarkers();

                int index = 0;

                decimal lat = (decimal)marker.Position.Latitude;
                lat = Math.Round(lat, 4, MidpointRounding.AwayFromZero);
                decimal longi = (decimal)marker.Position.Longitude;
                longi = Math.Round(longi, 4, MidpointRounding.AwayFromZero);
                
                foreach (MapItem tmp in markers)
                {
                    decimal tmpLat = (decimal)tmp.Latitude;
                    tmpLat = Math.Round(tmpLat, 4, MidpointRounding.AwayFromZero);
                    decimal tmpLong = (decimal)tmp.Longitude;
                    tmpLong = Math.Round(tmpLong, 4, MidpointRounding.AwayFromZero);


                    if ((lat == tmpLat) && (longi == tmpLong))
                    {
						if (marker.Snippet != "")
						{

							int firstCharacter = marker.Snippet.IndexOf("#");
							int secondCharacter = marker.Snippet.IndexOf("*");

							string str = marker.Snippet;

							shopId = Int32.Parse(str.Truncate(firstCharacter));

							if (shopId != tmp.ShopId)
							{
								found = true;
								break;
							}
						}

                        index++;
                    }
                }
            }


			((TextView)view.FindViewById (Resource.Id.title)).Text = marker.Title;//.Replace(" ", "\n");


			string url = (string)activity.markersImageTable[shopId];    

            if (activity.picture != null)
            {
                ((ImageView)view.FindViewById(Resource.Id.place_icon)).SetImageBitmap(activity.picture);
            }
            else if ((url != "") && (!activity.second))
            {
                UrlImageViewHelper.SetUrlDrawable((ImageView)view.FindViewById<ImageView>(Resource.Id.place_icon), url, Resource.Drawable.GhostMiniOndo, 30000, this);
            }
            
            else
            {
                ((ImageView)view.FindViewById(Resource.Id.place_icon)).SetImageResource(Resource.Drawable.GhostMiniOndo);
            }
           
         if (found)
            {
                ((TextView)view.FindViewById(Resource.Id.moreinfo)).Text = activity.GetString(Resource.String.MaptagOverlapText);
                ((TextView)view.FindViewById(Resource.Id.moreinfo)).Visibility = ViewStates.Visible;
            }
            else ((TextView)view.FindViewById(Resource.Id.moreinfo)).Visibility = ViewStates.Gone;

#if __ACTIVE__
            string diststring = "";

            double dist = (int)disthelper.calcdistance(OndoDot_Android.ViewModels.LoginViewModel.Latitude, OndoDot_Android.ViewModels.LoginViewModel.Longitude,
                          marker.Position.Latitude, marker.Position.Longitude);


            diststring = ""; // parent.GetString(Resource.String.DistanceText) + " ";

            switch (disthelper.GetFormattedDistance((int)dist))
            {
                case DistanceHelper.eFormattedDistance.Within100M:
                    diststring = diststring + parent.GetString(Resource.String.Within100MText);
                    break;
                case DistanceHelper.eFormattedDistance.Within500M:
                    diststring = diststring + parent.GetString(Resource.String.Within500MText);
                    break;
                case DistanceHelper.eFormattedDistance.Within1Km:
                    diststring = diststring + parent.GetString(Resource.String.Within1KmText);
                    break;
                case DistanceHelper.eFormattedDistance.Within5Km:
                    diststring = diststring + parent.GetString(Resource.String.Within5KmText);
                    break;
                case DistanceHelper.eFormattedDistance.Within10Km:
                    diststring = diststring + parent.GetString(Resource.String.Within10KmText);
                    break;
                case DistanceHelper.eFormattedDistance.Within50Km:
                    diststring = diststring + parent.GetString(Resource.String.Within50KmText);
                    break;
                case DistanceHelper.eFormattedDistance.Within100Km:
                    diststring = diststring + parent.GetString(Resource.String.Within100KmText);
                    break;
                default:
                    diststring = diststring + parent.GetString(Resource.String.MoreThan100KmText);
                    break;
            }


            ((TextView)view.FindViewById(Resource.Id.distance)).Text = diststring;
#endif
 

        }


        public void OnLoaded(ImageView imageView, Bitmap loadedBitmap, string url, bool loadedFromCache)
        {
     
			MapMainPage activity = parent as MapMainPage;

            activity.picture = loadedBitmap;
        }

       
    }

}