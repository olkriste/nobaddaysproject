using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//using Fragment = Android.Support.V4.App.Fragment;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

using CustomTextView = NoBadDays.helpers.CustomTextView; 
using CustomEditText = NoBadDays.helpers.CustomEditText;
using Android.Views.InputMethods; 

using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using System.Threading;
using System.Timers;
using NoBadDaysPCL.Items;
using NoBadDaysPCL.ViewModel;
using SQLite.Net.Platform.XamarinAndroid;
using Android.Telephony;


namespace NoBadDays
{
       
    
	public class MapPageSubPages : Android.Support.V4.App.Fragment , View.IOnTouchListener,
                    	GoogleMap.IOnCameraChangeListener , GoogleMap.IOnMapLoadedCallback
    {
		
		System.Timers.Timer timer = new System.Timers.Timer(200);


		int _pagenumber;
        View _view = null;

		MapViewPager _activity;

		CustomTextView _openingInterval;
		CustomTextView _closedInterval;
		CustomTextView _closedHeader;

		CustomEditText _comment;

		string _ratingComment = "";


		ImageView _ratingsView;

		bool _onestar = false;
		bool _twostar = false;
		bool _threestar = false;
		bool _fourstar = false;
		bool _fivestar = false;

		ScrollView _scrollView;

		GoogleMap _map;
		SupportMapFragment  _mapFragment;

		private MapView _mapView;

	    LatLng _shopPosition;

		MarkerOptions mapOption;

		Marker marker;

		float zoomLevel = 8;


		UserItem _user;
	

		RatingItem _rateItem;

		ShopViewModel _shopViewModel;

		NoBadDays.Helpers.CustomRelative _relative1;
		NoBadDays.Helpers.CustomRelative _relative2;

		int _lastMotionY;

		ProgressBar _progress;

		LinearLayout _toolbar;
		Button _done;
		Button _login;


		public MapPageSubPages(int page, MapViewPager context)
        {
        
            _activity = context;
			_pagenumber = page;

			_onestar = false;
		    _twostar = false;
	        _threestar = false;
		    _fourstar = false;
		    _fivestar = false;

			_activity.DataReady+= _activity_DataReady;

        }


		void _activity_DataReady (object sender, bool e)
		{
			if (_pagenumber == 0) {

				_openingInterval = _view.FindViewById<CustomTextView> (Resource.Id.OpeningInterval);
				_closedInterval = _view.FindViewById<CustomTextView> (Resource.Id.ClosedInterval);
				_closedHeader = _view.FindViewById<CustomTextView> (Resource.Id.NowClosed);
			
				if (_activity.shopViewModel.Item.IsOpen)
				{
					_closedHeader.Text = _activity.GetString (Resource.String.ClosingHoursText);
					_closedHeader.SetTextColor (Android.Graphics.Color.Black);
				}

				_openingInterval.Text = _activity.shopViewModel.Item.TextLeft;
				_closedInterval.Text = _activity.shopViewModel.Item.TextRigth;
			}
		}

       
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
          
            if (_pagenumber == 0)
            {

                _view = inflater.Inflate(Resource.Layout.MapPageOne, null);
					_mapView = _view.FindViewById<MapView>(Resource.Id.mapView);
				_mapView.OnCreate(savedInstanceState);

				_mapView.OnResume();

				_relative1 = _view.FindViewById<NoBadDays.Helpers.CustomRelative> (Resource.Id.relativeOne);
				_relative1.SetOnTouchListener (this);



				var route = _view.FindViewById<Button>(Resource.Id.route);
				route.Click+= (sender, e) => 
				{
					string saddr = Container.Latitude.ToString().Replace(",", ".") + "," + Container.Longitude.ToString().Replace(",", ".");
					string daddr = _activity.latitude.ToString().Replace(",", ".") + "," + _activity.longitude.ToString().Replace(",", ".");

					Intent intent = new Intent(Intent.ActionView, Android.Net.Uri.Parse("http://maps.google.com/maps?saddr=" + saddr + "&daddr=" + daddr));

					intent.AddFlags(ActivityFlags.NewTask);
					StartActivity(intent);

				};

				var phone = _view.FindViewById<Button>(Resource.Id.phone);
				phone.Click+= (sender, e) => 
				{
					try
					{
						TelephonyManager tm = (TelephonyManager)_activity.GetSystemService(Android.Content.Context.TelephonyService);
						if (tm.NetworkType != 0)
						{
						    Intent intent = new Intent(Intent.ActionCall);
						    intent.SetData(Android.Net.Uri.Parse("tel:" + _activity.shopViewModel.Item.Phone));
						    StartActivity(intent);
						}

						else Android.Widget.Toast.MakeText(_activity, GetString(Resource.String.NoNetworkText), ToastLength.Long).Show();

					}
					catch 
					{

					}

				};


				var web = _view.FindViewById<Button>(Resource.Id.web);
				web.Click+= (sender, e) => 
				{
					try
					{
						var uri = Android.Net.Uri.Parse(_activity.shopViewModel.Item.Link);
						var intent = new Intent(Intent.ActionView, uri);
						StartActivity(intent);
					
					}
					catch 
					{

					}

				};
								  

            }
            else if (_pagenumber == 1)
            {

                _view = inflater.Inflate(Resource.Layout.MapPageTwo, null);

				_progress = _view.FindViewById<ProgressBar>(Resource.Id.ProgressSpinner);

				_ratingsView = _view.FindViewById<ImageView> (Resource.Id.rating);

				_comment = _view.FindViewById<CustomEditText> (Resource.Id.comment);

				_toolbar = _view.FindViewById<LinearLayout> (Resource.Id.toolbarDone);
				_toolbar.Visibility = ViewStates.Gone;
				_done = _view.FindViewById<Button> (Resource.Id.doneBtn);
				_done.Visibility = ViewStates.Gone;
				_login = _view.FindViewById<Button> (Resource.Id.loginBtn);
				_login.Visibility = ViewStates.Gone;


				_done.Click += (sender, e) => 
				{
    				_toolbar.Visibility = ViewStates.Gone;
					_done.Visibility = ViewStates.Gone;

					_progress.Visibility = ViewStates.Visible;

					_activity.SetProgress (true);

					_rateItem.Text = _ratingComment;
					_shopViewModel.RateShop (_rateItem);
				};

				_login.Click += (sender, e) => 
				{
					_activity.Finish();
					_activity.StartActivity(typeof(LoginPhoneNumber));
				};


				_user = _activity.userViewModel.GetUser ();

				if (_user == null) _login.Visibility = ViewStates.Visible;

				timer.Elapsed += timer_Elapsed;
				timer.Enabled = false;   
            }


            return _view;

        }

		public override void OnActivityCreated(Bundle p0)
		{
			base.OnActivityCreated(p0);
			MapsInitializer.Initialize(Activity);
		}


        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here

			InputMethodManager imm = (InputMethodManager)_activity.GetSystemService (Context.InputMethodService);

		
        }

		private void InitializeMapAndHandlers()
		{
			SetUpMapIfNeeded();

			if (_map != null)
			{
				_map.MyLocationEnabled = false;
				_map.SetOnCameraChangeListener(this);
				_map.UiSettings.CompassEnabled = true;
				_map.UiSettings.ZoomControlsEnabled = true;
				_map.UiSettings.ZoomGesturesEnabled = true;
				_map.SetOnMapLoadedCallback (this);

			}

			var mypos = _activity.FindViewById<ImageView>(Resource.Id.myPosImage);
			mypos.Visibility = ViewStates.Visible;
			mypos.Click += mypos_Click;

		}

		void mypos_Click(object sender, EventArgs e)
		{

			try
			{

				var myPosition = new LatLng(Container.Latitude, Container.Longitude);


				LatLngBounds.Builder b = new LatLngBounds.Builder();
				b.Include(_shopPosition);
				b.Include(myPosition);

				LatLngBounds boundary = b.Build();

				_map.AnimateCamera(CameraUpdateFactory.NewLatLngBounds(boundary, _activity.screenWidth / 4, _activity.screenWidth / 4, 0));


				BitmapDescriptor icon = BitmapDescriptorFactory.FromResource(Resource.Drawable.p99999);

				mapOption = new MarkerOptions()
					.SetPosition(new LatLng(Container.Latitude, Container.Longitude))
					.InvokeIcon(icon)
					.SetSnippet("")
					.SetTitle("");
				marker = _map.AddMarker(mapOption);

			}

			catch 
			{
				
			}


		}

		public void OnMapReady(GoogleMap _map)
		{

	/*		_shopPosition = new LatLng(_activity.latitude, _activity.longitude);

			 AddMarkerToMap(_shopPosition);

			_map.AnimateCamera(CameraUpdateFactory.NewLatLngZoom(_shopPosition, zoomLevel));  */
		}

		public void OnMapLoaded()
		{
					
			_shopPosition = new LatLng(_activity.latitude, _activity.longitude);

			AddMarkerToMap(_shopPosition);

			_map.AnimateCamera(CameraUpdateFactory.NewLatLngZoom(_shopPosition, zoomLevel));
		}

		private void SetUpMapIfNeeded()
		{


			if(null == _map)
			{
				_map = View.FindViewById<MapView> (Resource.Id.mapView).Map;

			} 

		
		}




        public override void OnStart()
        {

            base.OnStart();

			if (_pagenumber == 0) {

		

				InitializeMapAndHandlers();


			} else {



				_ratingsView.SetOnTouchListener (this);
				_comment.SetOnTouchListener (this);
				if (_user != null) {
					_comment.EditorAction += comment_EditorAction;
					_comment.TextChanged += (sender, e) => {
						_ratingComment = _comment.Text;
						if (_rateItem != null) {
							_toolbar.Visibility = ViewStates.Visible;
							_done.Visibility = ViewStates.Visible;
						}

					};
				} else
					_comment.Enabled = false;


				_scrollView = _activity.FindViewById<ScrollView>(Resource.Id.scroll);

				_activity.SetView (_comment, _scrollView, _toolbar);
			}


        }

	

		#region map functions


		public void OnCameraChange(CameraPosition position)
		{

		}



	


		private void AddMarkerToMap(LatLng Geopos)
		{
			BitmapDescriptor icon = BitmapDescriptorFactory.FromResource(Resource.Drawable.MapPin);

			try
			{
				
				mapOption = new MarkerOptions()
					.SetPosition(Geopos)
					.InvokeIcon(icon)
					.SetSnippet("")
					.SetTitle("");
				marker = _map.AddMarker(mapOption);

			}

			catch { }


		}


		#endregion


		public bool OnTouch(View v, MotionEvent e)
		{
			v.OnTouchEvent (e); 

			var scrollViewMax = _activity.scrollViewMax; 

			var scroll = _activity.scrollView;

	
			if (v.Tag.ToString () == "comments") {
	
				if (_user == null) {
					_toolbar.Visibility = ViewStates.Visible;
					_login.Visibility = ViewStates.Visible;
				}

				timer.Start();
				timer.Enabled = true;   

			}
			else if (v.Tag.ToString () == "relative1")
			{

				RelativeLayout.LayoutParams parameters;

				float Y = e.GetY();

				if (e.RawY < scroll.Height && e.Action == MotionEventActions.Move)return true;

				switch (e.Action) {
				case MotionEventActions.Cancel:
					break;


				case MotionEventActions.Down:

					_lastMotionY = (int)Y;

					break;
				case MotionEventActions.Up:

					break;
				case MotionEventActions.Move:

					// Finger move up
					if (Y < _lastMotionY) {

						var newHeigt = scroll.Height;
						var moveMent = (_lastMotionY - Y);
						int reduction = (int)(newHeigt - moveMent);


						if ((reduction > 0) && ((int)newHeigt > 0)) {

							parameters = new RelativeLayout.LayoutParams (RelativeLayout.LayoutParams.MatchParent, reduction);
							scroll.LayoutParameters = parameters;

							scroll.SmoothScrollTo (0, scroll.Bottom); 
							scroll.FullScroll (FocusSearchDirection.Down);

						}

					}
					// Finger move down
					else if (Y >= _lastMotionY) {

						scroll.Visibility = ViewStates.Visible;

						var newHeigt = scroll.Height;
						var moveMent = (Y - _lastMotionY);
						int increase = (int)(newHeigt + moveMent);

						if (increase > scrollViewMax)
							increase = scrollViewMax;

						if (((increase) > 0) && (newHeigt < scrollViewMax)) {
							parameters = new RelativeLayout.LayoutParams (RelativeLayout.LayoutParams.MatchParent, increase);
							scroll.LayoutParameters = parameters;
							scroll.SmoothScrollTo (0, scroll.Bottom); 
							scroll.FullScroll (FocusSearchDirection.Down);
						}

					}

					break;
				}

			}

			else if (v.Tag.ToString () == "rating") {

				if (_user == null) {
					_toolbar.Visibility = ViewStates.Visible;
					_login.Visibility = ViewStates.Visible;
					return false;
				}


				var left = v.Left;
				var right = v.Right;
				var top = v.Top;
				var bottom = v.Bottom;

				var diff = right - left;
				var piece = diff / 5;



				if (e.RawX > 4*(float)piece+left) {

					_onestar = false;
					_twostar = false;
					_threestar = false;
					_fourstar = false;
				
					if (_fivestar) {
						_ratingsView.SetImageResource (Resource.Drawable.ZeroStars);
						_fivestar = false;
					} else {
						_fivestar = true;
						_ratingsView.SetImageResource (Resource.Drawable.FiveStars);
					}

				}
				else if (e.RawX > 3*(float)piece+left) {

					_onestar = false;
					_twostar = false;
					_threestar = false;
					_fivestar = false;

					if (_fourstar) {
						_ratingsView.SetImageResource (Resource.Drawable.ZeroStars);
						_fourstar = false;
					} else {
						_fourstar = true;
						_ratingsView.SetImageResource (Resource.Drawable.FourStars);
					}
				}
				else if (e.RawX > 2*(float)piece+left) {
					_onestar = false;
					_twostar = false;
					_fourstar = false;
					_fivestar = false;

					if (_threestar) {
						_ratingsView.SetImageResource (Resource.Drawable.ZeroStars);
						_threestar = false;
					} else {
						_threestar = true;
						_ratingsView.SetImageResource (Resource.Drawable.ThreeStars);
					}

				}
				else if (e.RawX > (float)piece+left) {
					_onestar = false;
					_threestar = false;
					_fourstar = false;
					_fivestar = false;

					if (_twostar) {
						_ratingsView.SetImageResource (Resource.Drawable.ZeroStars);
						_twostar = false;
					} else {
						_twostar = true;
						_ratingsView.SetImageResource (Resource.Drawable.TwoStars);
					}
				
				}
				else 
				{
					_twostar = false;
					_threestar = false;
					_fourstar = false;
					_fivestar = false;

					if (_onestar) {
						_ratingsView.SetImageResource (Resource.Drawable.ZeroStars);
						_onestar = false;
					} else {
						_onestar = true;
						_ratingsView.SetImageResource (Resource.Drawable.OneStar);
					}
				}

				if (_user != null) {

					var rating = 1;

					if (_twostar)
						rating = 2;
					if (_threestar)
						rating = 3;
					if (_fourstar)
						rating = 4;
					if (_fivestar)
						rating = 5;
	
					_rateItem = new RatingItem ();
					_rateItem.UserId = _user.UserId;
					_rateItem.ShopId = _activity.shopId;
					_rateItem.Rating = rating;
					_rateItem.Text = _ratingComment;


					string libraryPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
					var path = System.IO.Path.Combine(libraryPath, Constants.SQLitePath);

					 _shopViewModel = new ShopViewModel (new SQLitePlatformAndroid(), path);

					_shopViewModel.PhoneDataErrorOccurred += (sender, ev) => 
					{
						Android.Widget.Toast.MakeText (_activity, ev.ResultString, ToastLength.Long).Show ();
						_activity.SetProgress (false);
						_comment.Visibility = ViewStates.Visible;
						_progress.Visibility = ViewStates.Gone;
					};
				
			
					_shopViewModel.RatingSuccessful+= (sender, ev) => 
					{
						Android.Widget.Toast.MakeText (_activity, GetString(Resource.String.RatingOKText), ToastLength.Long).Show ();
						_activity.SetProgress (false);
						_comment.Visibility = ViewStates.Visible;
						_progress.Visibility = ViewStates.Gone;
						_done.Enabled = false;  //prevent more ratings?
					};
				

					if (_ratingComment != "") {
						_toolbar.Visibility = ViewStates.Visible;
					    _done.Visibility = ViewStates.Visible;
					}
					else Android.Widget.Toast.MakeText (_activity,GetString(Resource.String.MissingCommentText), ToastLength.Long).Show ();

				}
				else Android.Widget.Toast.MakeText(_activity, GetString(Resource.String.UserDataErrorText), ToastLength.Long).Show();


			}

			return false;

		}


		void timer_Elapsed(object sender, ElapsedEventArgs e)
		{
			timer.Enabled = false;
			timer.Stop();
			timer.Elapsed -= timer_Elapsed;

			_activity.RunOnUiThread(() =>
			{

				
				int pos = _comment.Top; // getBottom(); X_pos  getLeft(); getRight();
				
				var scroll = _view.FindViewById<ScrollView>(Resource.Id.scroll2);
				scroll.FullScroll(FocusSearchDirection.Down);
					scroll.ScrollTo(0,(int)(_activity.density*80)+pos*2);

				InputMethodManager imm = (InputMethodManager)_activity.GetSystemService(Context.InputMethodService);
				imm.ShowSoftInput(_comment,  ShowFlags.Implicit);

			});


		}
       
		void comment_EditorAction (object sender, TextView.EditorActionEventArgs e)
		{
	//		if (e.ActionId == ImeAction.Done)
	//		{

			if ((e.Event.KeyCode == Keycode.Enter)|| (e.ActionId == ImeAction.Done)) {

					InputMethodManager imm = (InputMethodManager)_activity.GetSystemService (Context.InputMethodService);
					imm.HideSoftInputFromWindow (_comment.WindowToken, 0);

					_ratingComment = _comment.Text;

					if ((_ratingComment != "") && (_rateItem != null)) {
					
						_rateItem.Text = _ratingComment; 
			
					    _toolbar.Visibility = ViewStates.Visible;
				    	_done.Visibility = ViewStates.Visible;
					} else
						Android.Widget.Toast.MakeText (_activity, GetString (Resource.String.MissingRatingText), ToastLength.Long).Show ();
		

	//			}
			}

		}

		public bool OnKey(View view, Android.Views.Keycode keyCode, KeyEvent Event)
		{
			if ((Event.KeyCode == Keycode.Back) || (Event.KeyCode == Keycode.Enter))
			{
				
				return true;

			}

			return false;

		} 


    }
}