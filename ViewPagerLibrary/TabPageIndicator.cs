using System;
using Android.Content;
using Android.Support.V4.View;
using Android.Util;
using Android.Views;
using Android.Widget;
using DK.Ostebaronen.Droid.ViewPagerIndicator.Interfaces;
using Java.Lang;
using Android.Graphics;
using Android.Animation;
using Android.Views.Animations;



namespace DK.Ostebaronen.Droid.ViewPagerIndicator
{
    public class TabPageIndicator 
        : HorizontalScrollView
        , IPageIndicator, Android.Animation.ValueAnimator.IAnimatorUpdateListener
    {
        public static readonly ICharSequence EmptyTitle = new Java.Lang.String("");

        private Runnable _tabSelector;
        private readonly IcsLinearLayout _tabLayout;

        private ViewPager _viewPager;
        private ViewPager.IOnPageChangeListener _listener;

        private int _maxTabWidth;
        private int _selectedTabIndex;

        public event TabReselectedEventHandler TabReselected;
        public event PageScrollStateChangedEventHandler PageScrollStateChanged;
        public event PageSelectedEventHandler PageSelected;
        public event PageScrolledEventHandler PageScrolled;

        #region color animation
        View iconView;
        View otherView;
        float[] fromWhite = new float[3];
        float[] toBlack = new float[3];
        ValueAnimator anim;
        bool _animate = false;
        #endregion

        #region customer font
        Typeface _face = null;
      
        #endregion
              

        public TabPageIndicator(Context context)
            : this(context, null)
        {}

        public TabPageIndicator(Context context, IAttributeSet attrs)
            : base(context, attrs)
        {
            HorizontalScrollBarEnabled = false;

            _tabLayout = new IcsLinearLayout(context, Resource.Attribute.vpiTabPageIndicatorStyle);
            AddView(_tabLayout, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WrapContent, ViewGroup.LayoutParams.MatchParent));

            #region animation
            _animate = false;

            anim = ValueAnimator.OfFloat(0, 1);
            anim.SetDuration(500);
            anim.AddUpdateListener(this);
            #endregion

        }

        protected override void OnMeasure(int widthMeasureSpec, int heightMeasureSpec)
        {
            var widthMode = MeasureSpec.GetMode(widthMeasureSpec);
            var lockedExpanded = widthMode == MeasureSpecMode.Exactly;
            FillViewport = lockedExpanded;

            var childCount = _tabLayout.ChildCount;
            if (childCount > 1 && (widthMode == MeasureSpecMode.Exactly || widthMode == MeasureSpecMode.AtMost))
            {
                if (childCount > 2)
                    _maxTabWidth = (int)(MeasureSpec.GetSize(widthMeasureSpec) * 0.4f);
                else
                    _maxTabWidth = MeasureSpec.GetSize(widthMeasureSpec) / 2;
            }
            else
                _maxTabWidth = -1;

            var oldWidth = MeasuredWidth;
            base.OnMeasure(widthMeasureSpec, heightMeasureSpec);
            var newWidth = MeasuredWidth;

            if (lockedExpanded && oldWidth != newWidth)
                CurrentItem = _selectedTabIndex;
        }

        private void AnimateToTab(int position)
        {
            iconView = _tabLayout.GetChildAt(position);

          

            #region Custom color for 2 tabs

            if (_animate)anim.Start();
        

            Color.ColorToHSV(Color.ParseColor("#FFFFFF"), fromWhite);   
            Color.ColorToHSV(Color.ParseColor("#000000"), toBlack);
        
            if (_viewPager.Adapter.Count == 2) {
                
                var otherpos = (position == 0) ? 1 : 0;
                otherView = (TextView)_tabLayout.GetChildAt (otherpos);

                if (iconView != null)
                {
         /*           iconView.SetBackgroundColor(Color.ParseColor("#000000"));
                    ((TextView)iconView).SetTextColor(Color.ParseColor("#FFFFFF"));  */
                    iconView.SetBackgroundColor(Color.ParseColor("#FFFFFF"));
                    ((TextView)iconView).SetTextColor(Color.ParseColor("#000000"));
                }


                if (otherView != null)
                {
        /*            otherView.SetBackgroundColor(Color.ParseColor("#FFFFFF"));
                    ((TextView)otherView).SetTextColor(Color.ParseColor("#000000"));  */
                    otherView.SetBackgroundColor(Color.ParseColor("#000000"));
                    ((TextView)otherView).SetTextColor(Color.ParseColor("#FFFFFF"));
             
                }
     
            }
            #endregion
            #region Custom color for 3 tabs
            else if (_viewPager.Adapter.Count == 3)
            {

                if (iconView != null) 
                {
                    ((TextView)iconView).SetTextColor (Android.Graphics.Color.ParseColor ("#FFFFFF")); //danske_bank_white_hex
                }
        
              
                var color = Android.Graphics.Color.ParseColor ("#929292"); // danske_bank_grey_hex

                TextView otherview;
                if (position == 0)
                {
                   
                    otherview = (TextView)_tabLayout.GetChildAt (1);
                    if (otherview != null) {
                        otherview.SetTextColor (color);}

                    otherview = (TextView)_tabLayout.GetChildAt (2);
                    if (otherview != null) {
                        otherview.SetTextColor (color);}
                }
                else if (position == 1)
                {
                    otherview = (TextView)_tabLayout.GetChildAt (0);
                    if (otherview != null) {
                        otherview.SetTextColor (color);}

                    otherview = (TextView)_tabLayout.GetChildAt (2);
                    if (otherview != null) {
                        otherview.SetTextColor (color);}
                }            
                else if (position == 2)
                {
                    otherview = (TextView)_tabLayout.GetChildAt (0);
                    if (otherview != null) {
                        otherview.SetTextColor (color);}

                    otherview = (TextView)_tabLayout.GetChildAt (1);
                    if (otherview != null) {
                        otherview.SetTextColor (color);}
                }      
           

            }
       
            #endregion

            if (_tabSelector != null)
                RemoveCallbacks(_tabSelector);

            _tabSelector = new Runnable(() =>
            {
                var scrollPos = iconView.Left - (Width - iconView.Width) / 2;
                SmoothScrollTo(scrollPos, 0);
                _tabSelector = null;
            });
            Post(_tabSelector);
        }


        #region Animation interfaces

       

        public void OnAnimationUpdate(Android.Animation.ValueAnimator animation)
        {
            var WhiteToBlack = new float[3];

            WhiteToBlack[0] = fromWhite[0] + (toBlack[0] - fromWhite[0]) * animation.AnimatedFraction;
            WhiteToBlack[1] = fromWhite[1] + (toBlack[1] - fromWhite[1]) * animation.AnimatedFraction;
            WhiteToBlack[2] = fromWhite[2] + (toBlack[2] - fromWhite[2]) * animation.AnimatedFraction;

            var BlackToWhite = new float[3];

            BlackToWhite[0] = toBlack[0] + (fromWhite[0] - toBlack[0]) * animation.AnimatedFraction;
            BlackToWhite[1] = toBlack[1] + (fromWhite[1] - toBlack[1]) * animation.AnimatedFraction;
            BlackToWhite[2] = toBlack[2] + (fromWhite[2] - toBlack[2]) * animation.AnimatedFraction;
            /*
            iconView.SetBackgroundColor(Color.HSVToColor(WhiteToBlack));
            ((TextView)iconView).SetTextColor(Color.HSVToColor(BlackToWhite));  
          */
            iconView.SetBackgroundColor(Color.HSVToColor(BlackToWhite));
            ((TextView)iconView).SetTextColor(Color.HSVToColor(WhiteToBlack));  


            if (otherView != null)
            {
        /*        otherView.SetBackgroundColor(Color.HSVToColor(BlackToWhite));
                ((TextView)otherView).SetTextColor(Color.HSVToColor(WhiteToBlack));  */
                otherView.SetBackgroundColor(Color.HSVToColor(WhiteToBlack));
                ((TextView)otherView).SetTextColor(Color.HSVToColor(BlackToWhite));
            }
          
        }


        #endregion


        protected override void OnAttachedToWindow()
        {
            base.OnAttachedToWindow();
            if (_tabSelector != null)
                Post(_tabSelector);
        }

        protected override void OnDetachedFromWindow()
        {
            base.OnDetachedFromWindow();
            if (_tabSelector != null)
                RemoveCallbacks(_tabSelector);
        }

        private void AddTab(int index, ICharSequence text, int iconResId)
        {
            var tabView = new TabView(Context, this) {Focusable = true, Index = index, TextFormatted = text};

           
            #region custom fonts

            try
            {
                tabView.Typeface = _face;
         //       tabView.SetTextSize(ComplexUnitType.Sp, 22);
				tabView.SetTextSize(ComplexUnitType.Sp, 16);
            }
            catch {}

   
            #endregion



            tabView.Click += (sender, args) =>
            {
                var tabview = (TabView)sender;
                var oldSelected = _viewPager.CurrentItem;
                var newSelected = tabview.Index;

                _viewPager.CurrentItem = newSelected;
                if(oldSelected == newSelected && TabReselected != null)
                    TabReselected(this, new TabReselectedEventArgs { Position = newSelected });
            };

            if (iconResId != 0)
                tabView.SetCompoundDrawablesWithIntrinsicBounds(iconResId, 0, 0, 0);

            _tabLayout.AddView(tabView, new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MatchParent, 1));
        }

        public void OnPageScrollStateChanged(int state)
        {
            if (_listener != null)
                _listener.OnPageScrollStateChanged(state);

            if (null != PageScrollStateChanged)
                PageScrollStateChanged(this, new PageScrollStateChangedEventArgs { State = state });

            #region animation
            _animate = true;
            #endregion

        }

        public void OnPageScrolled(int position, float positionOffset, int positionOffsetPixels)
        {
            if (_listener != null)
                _listener.OnPageScrolled(position, positionOffset, positionOffsetPixels);

            if (null != PageScrolled)
                PageScrolled(this,
                             new PageScrolledEventArgs
                             {
                                 Position = position,
                                 PositionOffset = positionOffset,
                                 PositionOffsetPixels = positionOffsetPixels
                             });
        }

        public void OnPageSelected(int position)
        {
            CurrentItem = position;
            if (_listener != null)
                _listener.OnPageSelected(position);

            if (null != PageSelected)
                PageSelected(this, new PageSelectedEventArgs { Position = position });
        }

        public void SetOnPageChangeListener(ViewPager.IOnPageChangeListener listener) { _listener = listener; }

        public void SetViewPager(ViewPager view)
        {
            if (_viewPager == view) return;

            if (null != _viewPager)
                _viewPager.SetOnPageChangeListener(null);

            if (null == view.Adapter)
                throw new InvalidOperationException("ViewPager does not have an Adapter instance.");

            _viewPager = view;
            _viewPager.SetOnPageChangeListener(this);
            NotifyDataSetChanged();
        }

        #region custom fonts
        
        public void SetViewPager(ViewPager view, Typeface face)
        {
            if (_viewPager == view) return;

            _face = face;

            if (null != _viewPager)
                _viewPager.SetOnPageChangeListener(null);

            if (null == view.Adapter)
                throw new InvalidOperationException("ViewPager does not have an Adapter instance.");

            _viewPager = view;
            _viewPager.SetOnPageChangeListener(this);
            NotifyDataSetChanged();
        }


        #endregion




        public void SetViewPager(ViewPager view, int initialPosition)
        {
            SetViewPager(view);
            CurrentItem = initialPosition;
        }

        public void NotifyDataSetChanged()
        {
            _tabLayout.RemoveAllViews();
            var adapter = _viewPager.Adapter;
            IIconPageAdapter iconAdapter = null;
            if (adapter is IIconPageAdapter)
                iconAdapter = (IIconPageAdapter)adapter;

            var count = adapter.Count;
            for(var i = 0; i < count; i++)
            {
                var title = adapter.GetPageTitleFormatted(i) ?? EmptyTitle;

                var iconResId = 0;
                if (iconAdapter != null)
                    iconResId = iconAdapter.GetIconResId(i);
                AddTab(i, title, iconResId);
            }
            if (_selectedTabIndex > count)
                _selectedTabIndex = count - 1;
            CurrentItem = _selectedTabIndex;
            RequestLayout();
        }

        public int CurrentItem
        {
            get { return _selectedTabIndex; }
            set
            {
                if (null == _viewPager)
                    throw new InvalidOperationException("ViewPager has not been bound.");

                _viewPager.CurrentItem = value;
                _selectedTabIndex = value;

                var tabCount = _tabLayout.ChildCount;
                for (var i = 0; i < tabCount; i++)
                {
                    var child = _tabLayout.GetChildAt(i);
                    var selected = (i == value);
                    child.Selected = selected;
                    if (selected)
                        AnimateToTab(value);
                }
            }
        }

        private class TabView : TextView
        {
            private readonly TabPageIndicator _indicator;

            public TabView(Context context, TabPageIndicator indicator)
                : base(context, null, Resource.Attribute.vpiTabPageIndicatorStyle) { _indicator = indicator; }

            protected override void OnMeasure(int widthMeasureSpec, int heightMeasureSpec)
            {
                base.OnMeasure(widthMeasureSpec, heightMeasureSpec);

                //Re-measure if we went beyond our maximum size.
                if (_indicator._maxTabWidth > 0 && MeasuredWidth > _indicator._maxTabWidth)
                    base.OnMeasure(MeasureSpec.MakeMeasureSpec(_indicator._maxTabWidth, MeasureSpecMode.Exactly),
                                   heightMeasureSpec);
            }

            public int Index { get; set; }
        }
    }
}